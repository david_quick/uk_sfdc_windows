trigger AccountTrigger on Account (before insert, before update) {
	TriggerFactory.createHandler(Account.sObjectType);
}