trigger SOARContactTrigger on SOAR_Contact__c (before insert, after update) {
	TriggerFactory.createHandler(SOAR_Contact__c.sObjectType);
}