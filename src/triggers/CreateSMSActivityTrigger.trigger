trigger CreateSMSActivityTrigger on smagicinteract__smsMagic__c (before insert) {
	try{
        Date todaysDate = system.today();
        List<Task> taskList = new List<Task>();
        for(smagicinteract__smsMagic__c sms : Trigger.new){
        	Task newTask = new Task();
            newTask.OwnerId = Userinfo.getUserId();
            newTask.Status = 'Completed';
            newTask.Subject = 'SMS to number ' +sms.smagicinteract__PhoneNumber__c;
            newTask.Priority = 'Medium';
            newTask.Type = 'Text Message';
            newTask.ActivityDate = todaysDate;
            system.debug('********** SMS = ' + sms);
            system.debug('********** Contact = ' + sms.smagicinteract__Contact__c);
            system.debug('********** Lead = ' + sms.smagicinteract__Lead__c);
            if (sms.smagicinteract__ObjectType__c == 'lead') {
                newTask.WhoId = sms.smagicinteract__Lead__c; 
            }
            if (sms.smagicinteract__ObjectType__c == 'contact') {
                newTask.WhoId = sms.smagicinteract__Contact__c; 
            }
            if (sms.smagicinteract__ObjectType__c == 'account') {
                newTask.WhoId = sms.smagicinteract__Account__c; 
            }
            newTask.Description = sms.smagicinteract__SMSText__c;
            taskList.add(newTask);
        }
        system.debug('********** taskList : '+ taskList);
        insert taskList;
    }
    catch(Exception e){
        system.debug('********** exception : ' + e);
    }
}