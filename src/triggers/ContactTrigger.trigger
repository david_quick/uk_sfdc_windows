trigger ContactTrigger on Contact (before insert, after insert, before update,before delete) {
    TriggerFactory.createHandler(Contact.sObjectType);
}