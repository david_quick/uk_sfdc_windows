trigger EventTrigger on Event (before insert,  before update, after update) {
    TriggerFactory.createHandler(Event.sObjectType);
}