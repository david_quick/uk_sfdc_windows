/**
* File Name   :    OpportunityTrigger.trigger
* Description :    [NIL-9] - Added the before insert trigger
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Vincent Spehner  added the before insert
*/
trigger OpportunityTrigger on Opportunity (before insert, after update, before update) {
	TriggerFactory.createHandler(Opportunity.sObjectType); 
}