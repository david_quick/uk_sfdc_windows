trigger LeadSnapshotTrigger on LeadSnapshot__c (before update) {
    TriggerFactory.createHandler(LeadSnapshot__c.sObjectType);
}