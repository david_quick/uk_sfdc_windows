trigger LeadContactTrigger on Lead_Contact__c (before insert, before update) {
	TriggerFactory.createHandler(Lead_Contact__c.sObjectType);
}