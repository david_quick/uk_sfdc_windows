trigger TaskTrigger on Task (before insert, after insert, before update, after update, before delete) {
    TriggerFactory.createHandler(Task.sObjectType);
}