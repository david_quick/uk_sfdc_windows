/**
* File Name   :    ApprovalSettingsTest.cls
* Description :    [NIL-9] - Test Utility class to access the approval process settings
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Vincent Spehner  Initial
* 0.2 01/06/2051 Vincent Spehner  Add formated comments
*/
@isTest
public class ApprovalSettingsTest {
    
    /** 
    @description            Test the three methods of the Approval Utility class 

    @returns                void
    */
    private static testMethod void testMatrix(){
        // Create custom setting
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Profile p   = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u      = TestData.createDefaultUser(p.Id);
        u.Email     ='newuser22@testorg.com';
        u.UserName  ='newuser33@testorg.com';

        User u2     = TestData.createDefaultUser(p.Id);
        u.Email     ='newuser33@testorg.com';
        u.UserName  ='newuser33@testorg.com';         


        insert u; 
        List<User> expectedApprovers1 = [select Id, name, email,username from User where Id = :u.id];
        List<User> expectedApprovers2 = [select Id, name, email,username from User where Id = :u.id or Id = :u2.id];
    
        
        Test.startTest();

            // Test Approver matrix
            List<User> approversList1 = new List<User>();
            approversList1 = ApprovalSettings.getApprovers('uk',1);

            List<User> approversList2 = new List<User>();
            approversList2 = ApprovalSettings.getApprovers('uk',2);

            // Test Min Approver number
            Integer minApprovers1 = ApprovalSettings.getApproversMin('uk',1);
            Integer minApprovers2 = ApprovalSettings.getApproversMin('uk',2);

            // Test Approval level
            Integer level1 = ApprovalSettings.getApprovalLevel('uk','Green',200000);
            Integer level0 = ApprovalSettings.getApprovalLevel('uk','Green',50000);

        Test.stopTest();    

        // Check values have been returned as expected
        System.assertEquals(expectedApprovers1, approversList1, 'The Approver list should be a list of only ONE user');
        System.assertEquals(expectedApprovers2, approversList2, 'The Approver list should contain TWO users');

        System.assertEquals(1, minApprovers1, 'The minimum number of approvers should be equal to 1 using the ApprovalMatricData.resource csv file');
        System.assertEquals(1, minApprovers2, 'The minimum number of approvers should be equal to 1 using the ApprovalMatricData.resource csv file'); 

        System.assertEquals(0, level0, 'The default approval Level when no valid params should be equal to 0'); 
        System.assertEquals(1, level1, 'The Approval Level should be equal to 1 given the params and using the ApprovalMatricData.resource csv file');

    }
}