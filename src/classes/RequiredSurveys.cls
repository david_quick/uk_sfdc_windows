@RestResource(urlMapping='/required_surveys')
global class RequiredSurveys {
    
    global class RequiredSurveysException extends Exception{}
        private static final String ORequiredSurveys_DATETIME_MISSING = 'RequiredSurveys_DATETIME_MISSING : the RequiredSurveysDateTime is missing and is required.';
        
        global class RequiredSurveysResults{
            
            global String siteId;
            global String name;
            global String address1;
            global String address2;
            global String address3;
            global String postCode;
            global String county;
            global String country;
            global String telephone;
            global String fax;
            
        }
        
        @HTTPGet
        global static List<RequiredSurveysResults> RequiredSurveys(){
            List<RequiredSurveysResults> rs = new List<RequiredSurveysResults>();
            
            List<Opportunity> opp = [Select Id, Account.Name, Account.Business_Address_Line_1__c, Account.Business_Address_Line_2__c, Account.Business_Address_Line_3__c, Account.Business_Address_Postcode__c, Account.Business_Address_Town_City__c, Account.Business_Address_County__c, Account.Phone, Account.Fax FROM Opportunity
                                        WHERE OCS_Check_Required__c=true];
            for(Opportunity o:opp)
            {
                RequiredSurveysResults r = new RequiredSurveysResults();
                r.siteId =o.Id;
                r.name = o.Account.name;
                r.address1 = o.Account.Business_Address_Line_1__c;
                r.address2 = o.Account.Business_Address_Line_2__c;
                r.address3 =o.Account.Business_Address_Line_3__c;
                r.postCode =o.Account.Business_Address_Postcode__c;
                r.county =o.Account.Business_Address_County__c;
                r.country ='U.K';
                r.telephone = o.Account.phone;
                r.fax =o.Account.fax;
                
                rs.add(r);
            }
                        
            return rs;
            
        }

}