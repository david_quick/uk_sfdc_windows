public with sharing class ContactWrapper {

    public Contact contact {get; set;}
    public Boolean selected {get; set;}

    public ContactWrapper(Contact c) {
        contact = c;
        selected = false;
    }
}