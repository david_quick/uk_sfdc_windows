@RestResource(urlMapping='/MarketingMessages/*')
global with sharing class MarketingMessagesIntegration {
    
    static final String DEFAULT_TASKUSER = 'Business Development User';
    static final String TASK_TYPE = 'Follow Up Call';
    static final String TASK_AREA = 'Sales - Dealer';
    static final String PICKLIST_OPTION_INTERACTION1 = 'Interaction 1';
    static final String PICKLIST_OPTION_INTERACTION2 = 'Interaction 2';
    static final String PICKLIST_OPTION_PRESENTEDCLOSED = 'Presented/Closed';
    
    @HttpGet
    global static Dealer_Message__c getDealerMessage() {
        
        RestRequest req = RestContext.request;
        String contactId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        
        List<Dealer_Message__c> messages = [Select Id, Text__c, Marketing_Message__r.Link_1__c, Marketing_Message__r.Link_2__c, Marketing_Message__r.Link1_Name__c, Marketing_Message__r.Link2_Name__c, Asset_Id__c  from Dealer_Message__c where Contact__r.Id = :contactId and Rank__c != null order by Rank__c, End_Date__c limit 1];
        if (messages.size() > 0) {
            
            return messages[0];
        }  
        else
        {
            return null;
        }
        
    }
    
    @HttpPost
    global static void updateDealerMessage(string messageId, boolean isPresented, boolean isClicked1, boolean isClicked2, boolean isClosed) {
        
        
        List<Dealer_Message__c> messages = [Select Id, Retries_Remaining__c, Closed_Retries_Remaining__c, Tasks_Required__c, Last_Responded_Date__c, Link_1_Clicked_Date__c, Link_2_Clicked_Date__c, Quarantine_Status__c,Contact__c from Dealer_Message__c where Id = :messageId];
        if (messages.size() > 0) {
            
            
            if (isPresented || isClosed || isClicked1 || isClicked2) {
                messages[0].Last_Responded_Date__c = DateTime.now();
                if ( String.isBlank( messages[0].Quarantine_Status__c ) ){
                    messages[0].Quarantine_Status__c = PICKLIST_OPTION_PRESENTEDCLOSED;
                }
            }
            if(isPresented) {
                --messages[0].Retries_Remaining__c;
            }
            if(isClosed) {
                --messages[0].Closed_Retries_Remaining__c;
            }
            if(isClicked1) {
                messages[0].Link_1_Clicked_Date__c = DateTime.now();
                messages[0].Quarantine_Status__c = PICKLIST_OPTION_INTERACTION1;
            }
            if(isClicked2) {
                messages[0].Link_2_Clicked_Date__c = DateTime.now();
                if (messages[0].Quarantine_Status__c != PICKLIST_OPTION_INTERACTION1){
                    messages[0].Quarantine_Status__c = PICKLIST_OPTION_INTERACTION2;
                }
            }
            
            update messages;
            
            MessageRulesEngine rulesEngine = new MessageRulesEngine();
            rulesEngine.run(messages);
            
            if ((isClicked1 || isClicked2) && messages[0].Tasks_Required__c) {
                CreateTask(messages[0], isClicked1, isClicked2);
            } 
            
            
            
        }
        
        
    }
    
    private static void CreateTask (Dealer_Message__c message, boolean isClicked1, boolean isClicked2) {
        
        Task t = new Task();
        t.WhoId = message.Contact__c;
        if (isClicked1) {
            t.ActivityDate = Date.today();
        }
        else {
            t.ActivityDate = Date.today().addDays(13);
        }
        t.Type__c = TASK_TYPE;
        t.Task_Area__c = TASK_AREA;
        t.Subject = Label.MMTaskSubject;
        t.WhatId = message.Id;
        List<User> users = [Select Id from User where IsActive = true and Name = :DEFAULT_TASKUSER limit 1];
        if (users.size () > 0) {
            t.OwnerId = users[0].Id;
        }
        
        Database.DMLOptions dmlOptions = new Database.DMLOptions(); 
        dmlOptions.EmailHeader.TriggerUserEmail = TRUE; 
        Database.insert(t,dmlOptions);
       
        
        
    }
    
}