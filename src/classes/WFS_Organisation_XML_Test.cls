@isTest()
public with sharing class WFS_Organisation_XML_Test {

    private static testMethod void startAndEndTagsTest(){
        WFS_Organisation_XML.writeOpeningTags();
        WFS_Organisation_XML.writeClosingTags();
        
        WFS_Organisation_Data__c[] orgData = [select XML__c from WFS_Organisation_Data__c order by Name asc];
        system.assertEquals('<organisationData>', orgData[0].XML__c);
        system.assertEquals('<organisationEntities>', orgData[1].XML__c);
        system.assertEquals('</organisationEntities>', orgData[2].XML__c);
        system.assertEquals('</organisationData>', orgData[3].XML__c);
    }
    
    private static testMethod void XML_GenerationTest(){
        WFS_TestData.createWFSTestData();
        Test.startTest();
        map<id, Account> companies = new map<id, Account>([select Id from Account 
                                        where RecordTypeId IN (select id from RecordType where sObjectType='Account' and DeveloperName='Companies')  
                                        and Id in (select accountid from opportunity where StageName ='Account Live')
                                        and LastModifiedDate =LAST_N_DAYS:2
                                        order by Id asc]);
        
        list<id> companyIds = new list<id>();
        companyIds.addAll(companies.keySet());
        
        WFS_Organisation_XML.processDealers(companyIds);
        
        WFS_Organisation_Data__c[] orgData = [select XML__c from WFS_Organisation_Data__c order by Name asc];
        system.assertEquals(4, orgData.size());
        
        //Test tags
        for(integer i = 0; i < 4; i++){
            XmlStreamReader reader = new XmlStreamReader(orgData[i].XML__c);
            reader.nextTag();
            
            //Dealers
            if(i < 3){
                system.assertEquals('Dealer', reader.getLocalName());
                
                //Company
                if(i < 1){
                    system.assert(!orgData[i].XML__c.contains('<BankAccount>'), 'Found a bank account in a company');
                    system.assert(!orgData[i].XML__c.contains('<Mandate>'), 'Found a mandate in a company');
                }
                //Centres
                else{
                    system.assert(orgData[i].XML__c.contains('<BankAccount>') ,'Centre has no bank account tag');
                    system.assert(orgData[i].XML__c.contains('<Mandate>'), 'Centre has no mandate tag');
                }
            }
            //Group
            else{
                system.assertEquals('OrganisationGroup', reader.getLocalName());
            }
        }
        
        //There are a HUGE number of assertions that could be made here,  but,  I'm out of time...
        
        Test.stopTest();    
    }

    
}