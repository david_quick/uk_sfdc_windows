/**
*
* Author:       Piotr Czechumski
* Date:         29/01/2015
* Description:  Service util class supporting creation of XML
*
**/
public with sharing class WFS_XML_Utils {
    
    public static final string WFS_NULL = '(null)'; // null string for WFS integartion
    
    //Opens and closes an XML tag,  inserts value between tags
    public static void writeSingleCharacterElement(XmlStreamWriter xmlwriter, String tag,  Object tagData){
        xmlwriter.writeStartElement(null, tag, null);
        
        string value;
        if(tagData == null){
            value = WFS_NULL;
        }
        else{
            value = blankToWFS_NULL(string.valueOf(tagData));
        }
        xmlwriter.writeCharacters(value);
        xmlwriter.writeEndElement();
    }

    //Opens and closes an XML tag,  accepts a map of attributes to be used.
    //Does not write anythig other than attributes
    public static void writeSingleCharacterElementWithAttributes(XmlStreamWriter xmlwriter, String tag,  Map<String, String> attributes){
        xmlwriter.writeStartElement(null, tag, null);
        
        for(string attribute : attributes.keySet()){
            xmlwriter.writeAttribute(null, null, attribute, attributes.get(attribute));
        }
        
        xmlwriter.writeEndElement();
    }
    
    public static String blankToWFS_NULL(string value){
        //If there's no data in a tag (null,  blank space,  zero-length string) output '(null)'
        if(string.isBlank(value)){
            return WFS_NULL;
        }
        else{
            return value.trim();
        }
    }


    public static String dateToString (Date d){
        String retVal = WFS_NULL;

        if (d != null){
            retVal = String.valueOf(d).replace('-','');
        }

        return retVal;
    }
}