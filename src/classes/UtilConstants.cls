/**
* File Name   :     UtilConstants.cls
* Description :    [ProjectRef-StoryName] - UtilityConstants class
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Ioan Beschea Intial
* 0.2 22/11/2016 Simon Wilby  add simple renewals and credit limit reduction record types. add opportunity_no_approval
* 0.3 30/01/2017 Simon Wilby  add Awaiting Credit Approval Opportunity stage
*/

public class UtilConstants {

    public static final String MESSAGE_CREDIT_APPROVED                      = 'The credit is already approved for this Opportunity'; 
    public static final String ACTION_WAS_TAKEN                             = 'You have already taken action for this Opportunity';
    public static final String USER_IS_NOT_AN_APPROVER                      = 'There is no pending approval for you';
    public static final String OPPORTUNITY_CANNOT_BE_SUMITTED_FOR_APPROVAL  = 'This Opportunity has already been submitted for approval';
    public static final String OPPORTUNITY_NO_APPROVAL                      = 'This Opportunity Type does not allow approval';  
    public static final String NOT_ALLOW_TO_CHANGE_OPP_STATUS               = 'You are not allow to change manually opportunity status to Credit Approved';
    public static final String LIMIT_CREDIT_OPPORTUNITY_MESSAGE_1           = 'There is a minimum credit limit of ';    
    public static final String CANT_CHANGE_CREDIT_APPROVED_FOR_OPP          = 'The stage of the Opportunity can not be changed to \"Credit Approved\" if the Approval Level is 0.';
    public static final String CANT_CHANGE_CREDIT_APPROVED_FOR_OPP_OTHER1   = 'You need ';
    public static final String CANT_CHANGE_CREDIT_APPROVED_FOR_OPP_OTHER2   = ' approvals to be able to reach \"Credit Approved\" stage';

    //Opportunities statuses
    public static final String OPPORTUNITY_STATUS_CREDIT_APPROVED           = 'Credit Approved';
    public static final String OPPORTUNITY_STATUS_CREDIT_REJECTED           = 'Rejected';
    public static final String OPPORTUNITY_STATUS_CREDIT_ESCALATED          = 'Escalated';

    //Approval Stage statuses   
    public static final String APPROVAL_STAGE_STATUS_APPROVED               = 'Approved';
    public static final String APPROVAL_STAGE_STATUS_PENDING_APPROVAL       = 'Pending Approval';
    public static final String APPROVAL_STAGE_STATUS_REJECTED               = 'Rejected';
    public static final String APPROVAL_STAGE_STATUS_ESCALATED              = 'Escalated';
    public static final String APPROVAL_STAGE_STATUS_REFERED                = 'Refered';
    public static final String APPROVAL_STAGE_STATUS_NO_ACTION              = 'No Action Taken';

    //Record Type Values
    public static final String OPPTY_RECORD_TYPE_AUCTION_HOUSE              = 'Auction_House_On_boarding';
    public static final String OPPTY_RECORD_TYPE_AWAITING_APPROVAL          = 'Awaiting_Approval';
    public static final String OPPTY_RECORD_TYPE_AWAITING_APPROVAL_AH       = 'Awaiting_Approval_Auction_House';
    public static final String OPPTY_RECORD_TYPE_SIMPLE_RENEWALS            = 'Simple_Renewals';
    public static final String OPPTY_RECORD_TYPE_AWAITING_APPROVAL_SR       = 'Awaiting_Approval_Simple_Renewals';
    public static final String OPPTY_RECORD_TYPE_CREDIT_LIMIT_REDUCTION     = 'Credit_Limit_Reduction';
    
    //Opportunity Stages
    public static final String OPPORTUNITY_STAGE_AWAITING_CREDIT_APPROVAL   = 'Awaiting Credit Approval';
    
    // Plan Sites
    public static final String PLAN_SITE_INCORRECT                          = 'Plan Site incorrect, check attached Plan and Bank Details';
    
}