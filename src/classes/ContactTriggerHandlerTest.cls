@isTest
public class ContactTriggerHandlerTest {
    @isTest static void testInsertContact() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        insert con;
        listOfAccs.add(acc);
        DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
        instance.dataQualityCheckMethod(listOfAccs,'Account');
        test.stopTest();
    }
    
    @isTest static void testInsertContactMobile() {
        test.startTest();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec('00353787878');
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        insert con;
        Contact thisContact = [SELECT MobilePhone FROM Contact WHERE Id = :con.Id];
        system.assertEquals(thisContact.MobilePhone, '353787878');
        Contact con1 = TestDataUtilityClass.getContactRec('07803925369');
        con1.Account = acc;
        con1.AccountId = acc.id;
        con1.Lead__c = led.id;
        insert con1;
        Contact nextContact = [SELECT MobilePhone FROM Contact WHERE Id = :con1.Id];
        system.assertEquals(nextContact.MobilePhone, '447803925369');
        test.stopTest();
    }
    @isTest static void testInsertContactOptOut() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        con.OptOut_SMS__c = 'Opted Out';
        insert con;
        listOfAccs.add(acc);
        DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
        instance.dataQualityCheckMethod(listOfAccs,'Account');
        Contact thisContact = [SELECT Id, smagicinteract__SMSOptOut__c FROM Contact WHERE Id = :con.Id];
        system.assertEquals(thisContact.smagicinteract__SMSOptOut__c, true);
        test.stopTest();
    }
    
    @isTest static void testUpdateContact() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        con.OptOut_SMS__c = 'Opted In';
        insert con;
        con.Email = 'dai.appy+167@gmail.com';
        update con;
        test.stopTest();
    }
    @isTest static void testUpdateContactOptOut() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        insert con;
        con.smagicinteract__SMSOptOut__c = true;
        con.OptOut_SMS__c = 'Opted Out';
        update con;
        Contact thisContact = [SELECT Id, OptOut_SMS__c, smagicinteract__SMSOptOut__c FROM Contact WHERE Id = :con.Id];
        system.assertEquals(thisContact.smagicinteract__SMSOptOut__c, true);
        test.stopTest();
    }
    @isTest static void testUpdateContactOptOutBlank() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        insert con;
        update con;
        Contact thisContact = [SELECT Id, OptOut_SMS__c,smagicinteract__SMSOptOut__c FROM Contact WHERE Id = :con.Id];
        system.assertEquals(thisContact.smagicinteract__SMSOptOut__c, false);
        system.assert(thisContact.OptOut_SMS__c != 'Opted Out');
        test.stopTest();
    }
    @isTest static void testUpdateContactMobile() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        insert con;
        con.Email = 'dai.appy+167@gmail.com';
        con.Mobile_Phone__c = '00353787878';
        update con;
        Contact thisContact = [SELECT Id, MobilePhone, Mobile_Phone__c FROM Contact WHERE Id = :con.Id];
        system.assertEquals(thisContact.MobilePhone, '353787878');
        thisContact.Mobile_Phone__c = '07803925369';
        update thisContact;
        Contact nextContact = [SELECT Id, MobilePhone, Mobile_Phone__c FROM Contact WHERE Id = :thisContact.Id];
        system.assertEquals(nextContact.MobilePhone, '447803925369');
        test.stopTest();
    }
    @isTest static void testDeleteContact() {
        test.startTest();
        List<Account> listOfAccs = new List<Account>();
        Account acc = TestDataUtilityClass.getAccountRec();
        insert acc;
        Lead led = TestDataUtilityClass.getLeadRec() ;
        insert led;
        Contact con = TestDataUtilityClass.getContactRec();
        con.Account = acc;
        con.AccountId = acc.id;
        con.Lead__c = led.id;
        insert con;
        listOfAccs.add(acc);
        DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
        instance.dataQualityCheckMethod(listOfAccs,'Account');
        delete con;
        test.stopTest();
    }
}