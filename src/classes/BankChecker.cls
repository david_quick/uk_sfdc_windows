public class BankChecker {
    private String api_key = 'api_key=a85652a2995695bbe0e0fa68da2cef4f';
    
    public bankResult Details {get; set;}
    
    public class bankResult {
        public String message {get; set;}     
        public String sort_code {get; set;}
        public String account {get; set;}
        public String iban {get; set;}
        public String country {get; set;}
        public String bank_name {get; set;}
        public String bank_bic {get; set;}
        public String chaps_bic {get; set;}
        public String bank_address {get; set;}
        public String bank_city {get; set;}
        public String bank_postalcode {get; set;}
        public String bank_phone {get; set;}
        public String direct_debits {get; set;}
        public String pfs_payments {get; set;}
        public String chaps {get; set;}
        public String bacs {get; set;}
        public String ccc_payments {get; set;}
    }
    public bankResult getBankDetails(String sortcode, String accNo) {
        bankResult result = new bankResult();
        
        if (string.IsBlank(sortCode) || string.IsBlank(accNo)) {
            result.message = 'Sortcode or account number is blank';
            Details = result;
            return result;
        }
        string params = '?' + api_key + '&sortcode=' + sortCode + '&account=' + accNo;
        try
        {
            // Get the XML document from the external server
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            // Using Remote Site Settings 
            req.setEndpoint('https://api.iban.com/clients/api/sort-api-eiscd.php' + params);
            // Using Named Credentials
            //req.setEndpoint('callout:Iban_Check' + params);
            req.setMethod('GET');
            HttpResponse res = http.send(req);
            if (res.getStatusCode() != 200)
            {
                result.message = 'Unable to obtain bank details from remote source';
                Details = result;
                return result;
            }
            XmlStreamReader readRes = res.getXmlStreamReader();
            result = readResult(readRes);
            result.message = 'Success';
            Details = result;
            return result;            
        }
        catch (Exception ex)
        {
            System.debug('********** Exception = ' + ex.getMessage());
            result.message = 'Unable to check account details - contact system administrator';
            Details = result;
            return result;
        }
    }
    
    public String checkBankDetails(String sortcode, String accNo)
    {
        String bankValid = '';
        
        if (string.IsBlank(sortCode) || string.IsBlank(accNo)) {
            bankValid = 'Sort Code and Account must be given';
            return bankValid;
        }
        string params = '?' + api_key + '&sortcode=' + sortCode + '&account=' + accNo;
        try
        {
            // Get the XML document from the external server
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            // Using Remote Site Settings 
            req.setEndpoint('https://api.iban.com/clients/api/sort-api-eiscd.php' + params);
            // Using Named Credentials
            //req.setEndpoint('callout:Iban_Check' + params);
            req.setMethod('GET');
            HttpResponse res = http.send(req);
            if (res.getStatusCode() != 200)
            {
                bankValid = 'Unable to check account details - contact system administrator';
                return bankValid;
            }
            // Log the XML content
            bankValid = 'Bank account details are valid';
            // Generate the HTTP response as an XML stream
            XmlStreamReader reader = res.getXmlStreamReader();
            XmlStreamReader readRes = res.getXmlStreamReader();
            bankResult result = readResult(readRes);
 
            // Read through the XML
            while(reader.hasNext()) {
                integer xmlEvent = reader.next();
                if (reader.getEventType() == XmlTag.START_ELEMENT) { 
                    if (reader.getLocalName() == 'Error') {
                        bankValid = 'Invalid sort code or account number';
                        return bankValid;
                    }
                } 
                reader.next();
            }
            return bankValid;
        }
        catch (Exception ex)
        {
            System.debug('********** Exception = ' + ex.getMessage());
            bankValid = 'Unable to check account details - contact system administrator';
            return bankValid;
        }
    }
    private bankResult readResult(XmlStreamReader reader) {
        bankResult res = new bankResult();
        boolean isSafeToGetNextXmlElement = true;
        while (isSafeToGetNextXmlElement) {
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
                 res = parseResult(res,reader.getLocalName(),reader);
            }
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        return res;
    }
    private bankResult parseResult(bankResult bres, String fieldname, XmlStreamReader reader)
    {
        bankResult newRes = bres;
        String currentField;
        boolean isSafeToGetNextXmlElement = true;
        while(isSafeToGetNextXmlElement) {
            if (reader.getEventType() == XmlTag.END_ELEMENT) {
                break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS) {
                newRes = setValue(bres,fieldname,reader.getText());
            }
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }    
        }
        return newRes;
    }
    
    private bankResult setValue(bankResult bres, String fieldname, String fieldvalue) {
        bankResult newRes = bres;
        if ('sort_code' == fieldname) {
            newRes.sort_code = fieldvalue;
        }
        if ('account' == fieldname) {
            newRes.account = fieldvalue;
        }
        if ('iban' == fieldname) {
            newRes.iban = fieldvalue;
        }
        if ('country' == fieldname) {
            newRes.country = fieldvalue;
        }
        if ('bank_name' == fieldname) {
            newRes.bank_name = fieldvalue;
        }
        if ('bank_bic' == fieldname) {
            newRes.bank_bic = fieldvalue;
        }
        if ('chaps_bic' == fieldname) {
            newRes.chaps_bic = fieldvalue;
        }
        if ('bank_address' == fieldname) {
            newRes.bank_address = fieldvalue;
        }
        if ('bank_postalcode' == fieldname) {
            newRes.bank_postalcode = fieldvalue;
        }
        if ('bank_phone' == fieldname) {
            newRes.bank_phone = fieldvalue;
        }
        if ('direct_debits' == fieldname) {
            newRes.direct_debits = fieldvalue;
        }
        if ('pfs_payments' == fieldname) {
            newRes.pfs_payments = fieldvalue;
        }
        if ('chaps' == fieldname) {
            newRes.chaps = fieldvalue;
        }
        if ('bacs' == fieldname) {
            newRes.bacs = fieldvalue;
        }
        if ('ccc_payments' == fieldname) {
            newRes.ccc_payments = fieldvalue;
        }
        return newRes;
    }
}