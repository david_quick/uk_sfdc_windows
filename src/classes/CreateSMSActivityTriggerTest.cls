@isTest
public class CreateSMSActivityTriggerTest {
    @isTest public static void testSMSInsert(){
        smagicinteract__smsMagic__c sms = new smagicinteract__smsMagic__c();
        sms.smagicinteract__ObjectType__c = 'contact';
        Account acc = TestData.createAccount();
        insert acc;
        Contact c = TestData.createContact(acc, 'Petula', 'Primrose');
        insert c;
        sms.CurrencyIsoCode = 'GBP';
        sms.OwnerId = UserInfo.getUserId();
        sms.smagicinteract__Contact__c = c.Id;
        sms.smagicinteract__SMSText__c = 'TestData';
        sms.smagicinteract__external_field__c = '7cf7fa4f18f61471d9b45857e63f5252f386e4cc8550de41d398118e3be5b8a4';
        insert sms;
        System.assert(sms.smagicinteract__Contact__c == c.Id);
    }
}