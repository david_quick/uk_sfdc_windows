@RestResource(urlMapping='/survey_results')
global class SurveyResults {
    
    global class SurveyResultsException extends Exception{}
        private static final String SurveyResults_INVALID_SITEID = 'SurveyResults_INVALID_SITE : the SurveyResultsSite is invalid.';

        global class Image{
            global string imagedata;
        }
        
        
        global class SurveyQuestion{
            global integer questionId;
            global String question;
            global integer answerValue;
            global String answerText;
            global List<Image> photoData;
        }
        
        
        global class Section{
            global integer id;
            global String title;
            global integer score;
            global String scoreText;
            global List<SurveyQuestion> answers;
        }
        
        global class Result{
            global Integer jobId;
            global String siteId;
            global String surveyDate;
            global String completionTimestamp;
            global String completionGPS;
            global Integer score;
            global String scoreText;
            global List<Section> sections;
        }
        
        @HttpPost
        global static List<Result> insertSurveyResults(Integer jobId,
                                                        String siteId,
                                                        String surveyDate,
                                                        String completionTimestamp,
                                                        String completionGPS,
                                                        Integer score,
                                                        String scoreText,
                                                        List<Section> sections){
            //if(res.size() == 0) return new List<Result>();
            OCS_Check__c ocs = new OCS_Check__c();
            Opportunity o;
            Account a;
            if(siteId.startswith('006'))
            {
                try{
                    o = [SELECT id,OCS_Check_Required__c FROM Opportunity WHERE id =: siteId LIMIT 1];
                }
                catch(Exception ex)
                {
                    Throw new SurveyResultsException(SurveyResults_INVALID_SITEID);
                }
            }
            else
            {
                try{
                    a = [SELECT id, WFS_Ref__c  FROM Account WHERE WFS_Ref__c =: siteId LIMIT 1];
                }
                catch(Exception ex)
                {
                    Throw new SurveyResultsException(SurveyResults_INVALID_SITEID);
                }
            }
            //for(Result r:res)
            //{ 
                ocs.jobId__c = jobId;
                String[] completionGPSLoc = completionGPS.split(' ') ;
                ocs.completionGPS__Latitude__s = Decimal.ValueOf(completionGPSLoc[1]);
                ocs.completionGPS__Longitude__s = Decimal.ValueOf(completionGPSLoc[0]);
                ocs.Opportunity__c = siteId;
                ocs.surveyDate__c = (DateTime) JSON.deserialize(
                                    '"' + completionTimestamp + '"', DateTime.class);
                ocs.Name = 'Check ' + surveyDate;
                ocs.score__c = score;
                ocs.scoreText__c = scoreText;
                
                insert ocs;
                
                
                for(Section s:sections)
                {
                    OCS_Section__c ocsSec = new OCS_Section__c(Result__c = ocs.Id);
                    
                    ocsSec.Id__c = s.id;
                    ocsSec.score__c = s.score;
                    ocsSec.scoreText__c = s.scoreText; 
                    ocsSec.Name = s.title;  
                    insert ocsSec;  
                    for(SurveyQuestion sq:s.answers)
                    {
                        OCS_Survey_Question__c ocsQ = new OCS_Survey_Question__c(Section__c = ocsSec.Id);
                        
                        ocsQ.questionId__c = sq.questionId;
                        ocsQ.Name =  sq.question;
                        ocsQ.answerValue__c = sq.answerValue;
                        ocsQ.answerText__c = sq.answerText;
                        insert ocsQ;
                        integer j=0;
                        if(sq.photoData != null)
                        {
                            for(Image i:sq.photoData)
                            {
                                j++;
                                OCS_Image__c ocsI = new OCS_Image__c(Survey_Question__c = ocsQ.Id);
                                ocsI.Name = 'Photo'  + j;
                                ocsI.imagedata__c = '<img alt="photo" src="data&colon;image/jpeg;base64,' + i.imagedata +'></img>';
                                                            insert ocsI;
                            }
                        }
                    }
                }
            //}
            o.OCS_Check_Required__c=false;
            update o;           
            return new List<Result>();
        }
        

}