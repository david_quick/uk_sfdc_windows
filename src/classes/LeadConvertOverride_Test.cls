@isTest
public with sharing class LeadConvertOverride_Test {
    @isTest private static void testShouldAllowAddingAccountTrue() {

        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        // setup custom setting 
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
        insert setting;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);

            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            boolean result = leadOverride.shouldAllowAddingNewAccount();
            system.assertEquals(result, true);
        }
    }

    @isTest private static void testShouldAllowAddingAccountFalse() {

        // setup custom setting 
        Profile p1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p1.id);
        insert setting;

        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);

            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            boolean result = leadOverride.shouldAllowAddingNewAccount();

            system.assertEquals(result, false);
        }
    }
    @isTest private static void testGetConList(){
         // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        // setup custom setting 
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
        insert setting;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);

            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            List<selectOption> cons = leadOverride.getConList();
            system.assert(cons.size() > 0);
        }
    }
       @isTest private static void testGetAccList(){
         // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        // setup custom setting 
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
        insert setting;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);

            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            List<selectOption> accs = leadOverride.getAccList();
            system.assert(accs.size() > 0);
        }
    }
       @isTest private static void testGetLcStatuses(){
         // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        // setup custom setting 
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
        insert setting;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);

            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            List<selectOption> stats = leadOverride.getlcStatuses();
            system.assert(stats.size() > 0);
        }
    }
    @isTest private static void testContacts(){
         // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        // setup custom setting 
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
        insert setting;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            PageReference ref = leadOverride.contacts();
            system.assert(ref == null);
        }
    }
    @isTest private static void testConvertLead(){
         // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;

        // setup custom setting 
        Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
        insert setting;

        Lead newLead = new Lead(Company = 'First', email = 'test@test.com', lastname = 'lastName');
        insert newLead;
        System.runAs(testUser) {
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            PageReference ref = leadOverride.convertLead();
            System.debug('********** Ref is ' + ref);
            System.assert(ref == null);
        }
    }
    @isTest private static void testLeadConv(){
         // This code runs as the system user
        
        return;
        
        
        
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;
      
        System.runAs(testUser) {
            // setup custom setting 
            Convert_Leads_Profile_Account_Create__c setting = new Convert_Leads_Profile_Account_Create__c(name = p.id);
            insert setting;
            Lead newLead = TestData.createFullApplication();
            insert newLead;
            System.debug('********** Lead inserted');
            ApexPages.StandardController sc = new ApexPages.StandardController(newLead);
            Account acc = TestData.createAccount();
            insert acc;
            Contact con = TestData.createContact(acc, 'Walter', 'Plunge');
            LeadConvertOverride leadOverride = new LeadConvertOverride(sc);
            leadOverride.acc = 'NEW_ACCOUNT';
            leadOverride.con = null;
            leadOverride.leadConvStatus = 'Qualified';
            PageReference ref = leadOverride.leadConv();
            System.assert(ref != null);
        }
        Test.stopTest();
    }
 }