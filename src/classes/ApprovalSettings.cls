/**
* File Name   :    ApprovalSettings.cls
* Description :    [NIL-9] - Utiliy class to access the approval process settings
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Vincent Spehner  Initial
* 0.2 01/06/2051 Vincent Spehner  Add formated comments
*/

public class ApprovalSettings {

    public static Map<String, ApprovalMatrix__c> approvalMatrix = ApprovalMatrix__c.getAll();

    public static Map<String, ApprovalThreshold__c> approvalThreshold = ApprovalThreshold__c.getAll();

    /** 
    @description            Get the list of Users identified as Approvers based on Approval Matrix 

    @param country          String containing the Opportunity/Account Source country
    @param level            Integer containing the Opportunity Approval Level (defined by Trigger on Insert and Update)    
    @returns                list of Users (or empty list)
    */
    public static List<User> getApprovers(String country, Integer level) {
        List<User> users = new List<User>();
        for(ApprovalMatrix__c at : approvalMatrix.values()){ 
            if (at.Country__c == country && at.Level__c == level) {
                List<String> approversList = at.Approvers__c.split(',');
                users = [select Id, name, email,username from User where IsActive = true and username in :approversList];
                return users;
            }
        }
        return users;
    }

    /** 
    @description            Get the list of Users identified as Required Approvers based on Approval Matrix 

    @param country          String containing the Opportunity/Account Source country
    @param level            Integer containing the Opportunity Approval Level (defined by Trigger on Insert and Update)    
    @returns                list of Users (or empty list)
    */
    public static List<User> getRequiredApprovers(String country, Integer level) {
        List<User> users = new List<User>();
        for(ApprovalMatrix__c at : approvalMatrix.values()){ 
            if (at.Country__c == country && at.Level__c == level && String.isnotEmpty(at.Required_Approvers__c) ) {
                List<String> approversList = at.Required_Approvers__c.split(',');
                users = [select Id, name, email,username from User where IsActive = true and username in :approversList];
                return users;
            }
        }
        return users;
    }



    /** 
    @description            Get the minimum number of Approvers as set in the Approval Matrix 

    @param country          String containing the Opportunity/Account Source country
    @param level            Integer containing the Opportunity Approval Level (defined by Trigger on Insert and Update)    
    @returns                Number of approvers (or 0)
    */
    public static Integer getApproversMin(String country, Integer level) {
        for(ApprovalMatrix__c at : approvalMatrix.values()){ 
            if (at.Country__c == country && at.Level__c == level) {
                return integer.valueOf(at.Minimum_Approver_number__c);
            }
        }
        return 1;
    }

    /** 
    @description            Get the Approval Level as set in the Approval Matrix 

    @param country          String containing the Opportunity/Account Source country
    @param overallRating    String containing the Opportunity overallRating (Green, Amber or Red)    
    @param amount           Decimal value of the Opportunity Credit Limit    
    @returns                Approval Level value (or 0)
    */
    public static Integer getApprovalLevel(String country, String overallRating, Decimal amount){

        for(ApprovalThreshold__c at : approvalThreshold.values()) { 
            if (at.Country__c == country && at.Overall_rating__c == overallRating){
                if ((at.Credit_Limit_From__c <= amount && at.Credit_Limit_To__c == null ) ||
                      (at.Credit_Limit_From__c <= amount && at.Credit_Limit_To__c >= amount)) {
                    return Integer.valueOf(at.Level__c);
                }
            }
        }
        return 0;
    }
}