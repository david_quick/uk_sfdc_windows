public with sharing class SOARContactTriggerHandler extends TriggerHandler {
    
	public SOARContactTriggerHandler(){
        preventRecursiveTrigger(false);
    }
    
    public override void beforeInsert(List<SObject> newObjects){
        System.debug('********** SOAR Contact trigger before insert');
        boolean err = false;
        boolean match = false;
     
        for (SOAR_Contact__c c : (List<SOAR_Contact__c>)newObjects) {
            System.debug('********** SOAR Contact is ' + c);
            err = false;
            match = false;
      
        	// Check that the dealer exists
        	Id accId = TestData.getDealerId(c.Dealer_Number__c);
            if (string.isEmpty(accId))
            {
                System.debug('********** Dealer ' + c.Dealer_Number__c + ' does not exist');
                CreateTask('Dealer ' + c.Dealer_Number__c + ' does not exist', 'SOAR Contact ' + c.Name + ' has an incorrect dealer number', accId);
                err = true;
            }
        	// Check that the contact exists for that dealer
            if (string.isNotEmpty(accId)) {
                List<Contact> contacts = [SELECT Id, FirstName, LastName, Name FROM Contact where AccountId = :accId];
                for (Contact ac : contacts)
                {
                    string acName = ac.FirstName + ' ' + ac.LastName;
                    string cName = c.First_Name__c + ' ' + c.Last_Name__c;
                    System.debug('********** Checking contact ' + acName + ' against ' + cName);
                    if (acName == cName){
                        // Match found
                        match = true;
                        // Update the contact
                        System.debug('********** Contact updated');
                    	ac.SOAR_Active_User__c = true;
                    	upsert ac;
                    }
                }
                if (!match) {
                    System.debug('********** ' + c.Name + ' is not a matching contact');
                    CreateTask('Dealer ' + c.Dealer_Number__c + ' has no matching contact', 'SOAR Contact ' + c.Name + ' is not a known contact',accId);
                    err = true;
                }
            }
            if (!err) {
                // Set the SOAR contact as processed
                c.Processed__c = true;
            }
            else {
                c.Processed__c = false;
            }
        }

    }
  
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap)
   	{
        System.debug('********** SOARContactTriggerHandler - before update');
   	}
    private void CreateTask(string subject, string description, Id accId) {
        try {
            Account a = [SELECT OwnerId from Account where Id = :accId];
         	Task t = new Task();
        	t.Subject = subject;
        	t.ActivityDate = System.Today() + 2;
        	//t.OwnerId = TestData.getUserIDForTitle('Risk Queue');
        	t.OwnerId = a.OwnerId;
        	t.Task_Area__c = 'Risk';
        	t.Type__c = 'SOAR not updated';    
			t.Description = description;
			t.Status = 'Not Started';
			t.Priority = 'Normal';
        	t.WhatId = accId;
        	insert t;
        }
        catch (Exception ex) {
            System.debug('********** Task insert failed ' + ex.getMessage());
        }
    }
}