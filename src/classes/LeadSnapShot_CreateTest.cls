@isTest
public class LeadSnapShot_CreateTest {
    @isTest static void testNewLeadCreate() {
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        Test.stopTest();
        Lead leadRes = [SELECT Id, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.debug('********** After Get ' + leadRes);
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
    }
    @isTest static void testNewLeadSnapshotCreate() {
        LeadSnapShot_Create creator = new LeadSnapShot_Create();
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        creator.createSnapshot(l);
        Lead leadRes = [SELECT Id, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.debug('********** After Get ' + leadRes);
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
    }
    @isTest static void testNewLeadSnapshotCreateAndUpdate() {
        // On creation the application comes in as three seperate pages
        // The snapshot needs to cope with this
        String lid;
        LeadSnapShot_Create creator = new LeadSnapShot_Create();
        Lead l = TestData.createFullApplicationPage1();
        Test.startTest();
        insert l;
        System.debug('********** Page 1 = ' + l);
        l = TestData.createFullApplicationPage2(l);
        upsert l;
        System.debug('********** Page 2 = ' + l);
        l = TestData.createFullApplicationPage3(l);
        upsert l;
        System.debug('********** Page 3 = ' + l);
        creator.createSnapshot(l);
        Lead leadRes = l;
        System.debug('********** After Lead get ' + leadRes);
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
        LeadSnapshot__c newshot = creator.getSnapshotFromMap(leadRes.LeadAndSnapshotMapField__c);
        System.debug('********** After SS get ' + newshot);
        System.debug('********** VAT |' + newshot.VAT_Number__c + '| = |' + leadRes.VAT_Number__c + '|');
        System.assertEquals(newshot.VAT_Number__c.trim(),leadRes.VAT_Number__c.trim());
        System.debug('********** Phone |' + newshot.Phone__c + '| = |' + leadRes.Phone + '|');
        System.assertEquals(newshot.Phone__c.trim(),leadRes.Phone.trim());
        System.debug('********** Mobile |' + newshot.Mobile_Phone__c + '| = |' + leadRes.Mobile_Phone__c + '|');
        System.assertEquals(newshot.Mobile_Phone__c.trim(),leadRes.Mobile_Phone__c.trim());
        System.debug('********** Business |' + newshot.Business_Phone__c + '| = |' + leadRes.Business_Phone__c + '|');
        System.assertEquals(newshot.Business_Phone__c.trim(),leadRes.Business_Phone__c.trim());
    }
}