public class OpportunityWorkflowController {
    private final Account account;
    private final Opportunity opp;
    private BankChecker checker = new BankChecker();
    public Opp_Documentation__c document {get; set;}
    public String accountName {get; set;}
    public String accountAddress {get; set;}
    public String accountPostcode {get; set;}
    public string accountVAT {get; set;}
    public string accountSortCode {get; set;}
    public string accountNumber {get; set;}
    public string vatValid {get; set;}
    public string vatName {get; set;}
    public string vatAddress {get; set;}
    public string bankValid {get; set;}
    public string bankValid2 {get; set;}
    public User lookupUser {get; set;}
    public string creditLimit {get; set;}
    public string bankBIC {get; set;}
    public string bankIBAN {get; set;}
    public string bankBIC2 {get; set;}
    public string bankIBAN2 {get; set;}
    public string signedOff {get; set;}
    public boolean dual {get; set;}
    public string dualMessage {get; set;}
    
    public OpportunityWorkflowController() {
        opp = [SELECT Id,Name,AccountId,Credit_Limit_x__c,Credit_Limit_Band_x__c FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        system.debug('***** Opp ' + opp);
        account = [SELECT Id, Name, Business_Address_Line_1__c, 
                   Business_Address_Postcode__c, Source_Country__c, VAT_Registration_Number__c,
                   Bank_Sort_Code__c, Bank_Account_Number__c,
                   Duel_Currency__c
                   FROM Account WHERE Id = :opp.AccountId];
        system.debug('***** Account ' + account);
        accountName = account.Name;
        accountAddress = account.Business_Address_Line_1__c;
        accountPostcode = account.Business_Address_Postcode__c;
        accountVAT = account.VAT_Registration_Number__c;
        accountSortCode = account.Bank_Sort_Code__c;
        accountNumber = account.Bank_Account_Number__c;
        document = getDocumentation();
        dual = false;
        dualMessage = '';
        if (account.Duel_Currency__c == 'Yes') {
            dual = true;
            dualMessage = 'Dual currency for this opportunity';
        }
        if (opp.Credit_Limit_x__c != null)
        {
            creditLimit = opp.Credit_Limit_x__c.format();
        }
        else
        {
            creditLimit = opp.Credit_Limit_Band_x__c;
        }
        if (document.Final_Signoff__c) {
            signedOff = 'Signed off by manager';
        }
        else {
            signedOff = 'Awaiting manager signoff';
        }
        checkVATService();
        if (String.IsBlank(document.txtInsLimit__c))
        {
            document.txtInsLimit__c = creditLimit;
        }
    }
    
    private Opp_Documentation__c getDocumentation() {
        System.debug('***** Get Documentation');
        Id oppId = opp.Id;
        Opp_Documentation__c newDoc = new Opp_Documentation__c();
        newDoc = InitializeDoc(newDoc);
        try {
            if( [select id from Opp_Documentation__c where Owner_Opportunity__c = :oppId].size() > 0 ) {
                system.debug('***** Existing Doc');
                newDoc = mapDocument(oppId);     
            }
        }
        catch (Exception ex) {
            
        }
        return newDoc;
    }
      
    private Account getAccount() {
        return account;
    }
    
    private Opp_Documentation__c InitializeDoc(Opp_Documentation__c newDoc) {
        newDoc.Owner_Opportunity__c = opp.Id;
        return newDoc;
    }
     
    private Opp_Documentation__c mapDocument(Id oppid) {
        // If there is a document record then map it back
        system.debug('***** Map Doc');
        Opp_Documentation__c doc = [Select Id, Owner_Opportunity__c, 
                                    	cbDDDocMandate__c,
                                        cbDDAccount__c,
                                        cbDDChecked__c,
                                        cbDDSFMandate__c,
                                        txtDDSortcode__c,
                                        cbDDSun__c,
                                        txtDDAccNumber__c,
                                        cbDDCurrency__c,
                                        cbDDAttached__c,
                                        txtDDComments__c,
                                    	txtDDBIC__c,
                                    	txtDDIBAN__c,
                                        lkDDQC__c,
										lkDDQC2__c, 
                                        cbDD2DocMandate__c,
                                    	cbDD2Account__c,
                                        cbDD2Checked__c,
                                        cbDD2SFMandate__c,
                                        txtDD2Sortcode__c,
                                        cbDD2Sun__c,
                                        txtDD2AccNumber__c,
                                        cbDD2Currency__c,
                                        cbDD2Attached__c,
                                        txtDD2Comments__c,
                                    	txtDD2BIC__c,
                                    	txtDD2IBAN__c,
                                        lkDD2QC__c,
										lkDD2QC2__c, 
                                        cbDocCompleted__c,
                                        txtDocComments__c,
                                        lkDocQC__c,
                                    	lkDocQC2__c,
                                        cbVATValid__c,
                                        cbVATAccount__c,
                                        cbVATPlanSite__c,
                                        txtVATComments__c,
                                        lkVATQC__c,
                                    	lkVATQC2__c,
                                        cbProReq1__c,
                                        mpkProRec1__c,
                                        cbProRec1__c,
                                        cbProOpp1__c,
                                        lkProContact1__c,
                                        cbProRecent1__c,
                                        cbProReq2__c,
                                        mpkProRec2__c,
                                        cbProRec2__c,
                                        cbProOpp2__c,
                                        lkProContact2__c,
                                        cbProRecent2__c,
                                        cbProReq3__c,
                                        mpkProRec3__c,
                                        cbProRec3__c,
                                        cbProOpp3__c,
                                        lkProContact3__c,
                                        cbProRecent3__c,
                                        cbProReq4__c,
                                        mpkProRec4__c,
                                        cbProRec4__c,
                                        cbProOpp4__c,
                                        lkProContact4__c,
                                        cbProRecent4__c,
                                        lkProQC__c,
                                    	lkProQC2__c,
                                        txtProComments__c,
                                        cbIDReq1__c,
                                        cbIDRec1__c,
                                        mpkIDRec1__c,
                                        cbIDOpp1__c,
                                        lkIDContact1__c,
                                        cbIDRecent1__c,
                                        cbIDReq2__c,
                                        cbIDRec2__c,
                                        mpkIDRec2__c,
                                        cbIDOpp2__c,
                                        lkIDContact2__c,
                                        cbIDRecent2__c,
                                        cbIDReq3__c,
                                        cbIDRec3__c,
                                        mpkIDRec3__c,
                                        cbIDOpp3__c,
                                        lkIDContact3__c,
                                        cbIDRecent3__c,
                                        cbIDReq4__c,
                                        cbIDRec4__c,
                                        mpkIDRec4__c,
                                        cbIDOpp4__c,
                                        lkIDContact4__c,
                                        cbIDRecent4__c,
                                        lkIDQC__c,
                                    	lkIDQC2__c,
                                        txtIDComments__c,
                                        cbAddRec__c,
                                        cbAddUmbraco__c,
                                        cbAddContact__c,
                                        lkAddQC__c,
                                    	lkAddQC2__c,
                                        cbFraObtained__c,
                                        cbFraAttached__c,
                                        lkFraQC__c,
                                    	lkFraQC2__c,
                                        cbInsRecNote__c,
                                        cbInsRecSchedule__c,
                                        pkInsName__c,
                                        cbInsFlood__c,
                                        cbInsQuestion__c,
                                        cbInsTask__c,
                                        cbInsRisk__c,
                                        txtInsExcess__c,
                                        txtInsEmp__c,
                                        txtInsLimit__c,
                                        txtInsPub__c,
                                        txtInsVehicle__c,
                                        dtInsFrom__c,
                                        dtInsTo__c,
                                        cbInsSalesforce__c,
                                        lkInsQC__c,
                                    	lkInsQC2__c,
                                        txtInsComments__c,
                                        Final_Signoff__c,
                                        Manager_Sign_off__c
                                        from Opp_Documentation__c where Owner_Opportunity__c = :oppid];
        return doc;
    }
    
    public void checkVATService()
    {
        vatValid = 'Invalid';
        String vatNumber = account.VAT_Registration_Number__c;
        if (String.IsBlank(vatNumber)) 
        {
            return;
        }
        String countryCode = account.Source_Country__c;
        if (countryCode != 'IE')
        {
            countryCode = 'GB';
        }
        try
        {
        
        	VIES.VATCheck result = VIES.checkVAT(countryCode, vatNumber);	
            System.debug('********** VIES result = ' + result);
        	if (result.IsValid)
        	{
            	vatValid = 'Valid';
        	}
  
        	vatName = result.name;
        	vatAddress = result.address;
        }
		catch (Exception ex)
        {
            vatValid = 'Unchecked - inform administrator';
            System.debug('********** Exception on VAT Check is ' + ex.getMessage());
        }
    }
    
    public void checkBankDetails()
    {
        string sortCode = document.txtDDSortcode__c;
        string accNo = document.txtDDAccNumber__c; 
        bankValid = checker.checkBankDetails(sortcode,accNo); 
        getBankDetails();
        return;
    }
    public void checkBankDetails2()
    {
        string sortCode = document.txtDD2Sortcode__c;
        string accNo = document.txtDD2AccNumber__c; 
        bankValid = checker.checkBankDetails(sortcode,accNo); 
        getBankDetails2();
        return;
    }
    
    public void getBankDetails()
    {
        string sortCode = document.txtDDSortcode__c;
        string accNo = document.txtDDAccNumber__c;
        BankChecker.bankResult bres = checker.getBankDetails(sortcode, accNo);
        document.txtDDBIC__c = 'BIC: ' + bres.bank_bic;
        document.txtDDIBAN__c = 'IBAN: ' + bres.iban;
    }
    public void getBankDetails2()
    {
        string sortCode = document.txtDD2Sortcode__c;
        string accNo = document.txtDD2AccNumber__c;
        BankChecker.bankResult bres = checker.getBankDetails(sortcode, accNo);
        document.txtDD2BIC__c = 'BIC: ' + bres.bank_bic;
        document.txtDD2IBAN__c = 'IBAN: ' + bres.iban;
    }
 
    public PageReference save() {
        upsert document;
        //  After successful Save, navigate to the calling opportunity
        //PageReference retPage = new PageReference('/'+apexPages.CurrentPage().getParameters().get('id')); 
        //return (retPage);
        // Users asked to remain on page - return null
        return null;
    }
}