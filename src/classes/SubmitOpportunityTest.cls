/**
* File Name   :    SubmitOpportunityTest.cls
* Description :    [NIL-10] - Test class for SubmitOpportunity
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 29/05/2015 Ioan Beschea Intial
*/


@isTest
private class SubmitOpportunityTest {

    static Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    static Id awaitingApprovalAHRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL_AH LIMIT 1].Id;
    static Id auctionRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AUCTION_HOUSE LIMIT 1].Id;
    static Id awaitingApprovalRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL  LIMIT 1].Id;

     static testMethod void submitOpp() {

        Account newAccount = TestData.createAccount();
        newAccount.Source_Country__c = 'UK';
        INSERT newAccount;

        ApprovalThreshold__c currentAppT = TestData.populateApprovalThreshold('UK', 10000, 80000, 2, 'Green');
        INSERT currentAppT;

        String currentUserName = UserInfo.getUserName();

        ApprovalMatrix__c currentAppM = TestData.currentApprovalMatrix(currentUserName, 'UK', 2, 1);
        INSERT currentAppM;
        
        Opportunity newOpportunity = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c = 'Green';
        newOpportunity.Judgemental_Ratingx__c = 'Green';
        newOpportunity.Credit_Limit_x__c = 20000;
        INSERT newOpportunity;

        Opportunity currentOpp = [SELECT Approval_level__c , Overall_Ratingx__c FROM Opportunity WHERE Id = :newOpportunity.Id];
        System.debug('****' + currentOpp);

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        SubmitOpportunity ob = new SubmitOpportunity(stdController);
        ob.createApprovalStage();

        List<Approval_Stage__c> currentApprovalList = [SELECT Id, Status__c FROM Approval_Stage__c WHERE Opportunity__c = :newOpportunity.Id AND Status__c = :UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL];

        System.assertEquals(1, currentApprovalList.size(), 'There was a different number of approvers than Approval Stage records with status Pending Approval');
        System.assertEquals(currentApprovalList[0].Status__c, UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL);

        Opportunity insertedOpp = [SELECT RecordTypeId FROM Opportunity WHERE Id = :newOpportunity.Id];
        System.assertEquals(awaitingApprovalRecordTypeId, insertedOpp.RecordTypeId, 'The record type is not Awaiting Approval');
    }
    
    static testMethod void submitOppAuction() {

        Account newAccount = TestData.createAccount();
        newAccount.Source_Country__c = 'UK';
        INSERT newAccount;

        ApprovalThreshold__c currentAppT = TestData.populateApprovalThreshold('UK', 10000, 80000, 2, 'Green');
        INSERT currentAppT;

        String currentUserName = UserInfo.getUserName();

        ApprovalMatrix__c currentAppM = TestData.currentApprovalMatrix(currentUserName, 'UK', 2, 1);
        INSERT currentAppM;

        Opportunity newOpportunity = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c = 'Green';
        newOpportunity.Judgemental_Ratingx__c = 'Green';
        newOpportunity.Credit_Limit_x__c = 20000;
        newOpportunity.RecordTypeId = auctionRecordTypeId;
        INSERT newOpportunity;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        SubmitOpportunity ob = new SubmitOpportunity(stdController);
        ob.createApprovalStage();

        List<Approval_Stage__c> currentApprovalList = [SELECT Id, Status__c FROM Approval_Stage__c WHERE Opportunity__c = :newOpportunity.Id AND Status__c = :UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL];

        System.assertEquals(0, currentApprovalList.size(), 'Auction record type should have 0 approvers because the opp has level = 5');

        Opportunity insertedOpp = [SELECT RecordTypeId FROM Opportunity WHERE Id = :newOpportunity.Id];
        System.assertEquals(awaitingApprovalAHRecordTypeId, insertedOpp.RecordTypeId ,'The record type is not Awaiting Approval Auction');
    }

    static testMethod void submitOppWithApprovalStages() {

        Account newAccount = TestData.createAccount();
        newAccount.Source_Country__c = 'UK';
        INSERT newAccount;

        ApprovalThreshold__c currentAppT = TestData.populateApprovalThreshold('UK', 10000, 80000, 2, 'Green');
        INSERT currentAppT;

        String currentUserName = UserInfo.getUserName();

        ApprovalMatrix__c currentAppM = TestData.currentApprovalMatrix(currentUserName, 'UK', 2, 1);
        INSERT currentAppM;

        Opportunity newOpportunity = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c = 'Green';
        newOpportunity.Judgemental_Ratingx__c = 'Green';
        newOpportunity.Credit_Limit_x__c = 20000;
        INSERT newOpportunity;

        Approval_Stage__c currentApproval = TestData.createApprovalStageWithStatus(newOpportunity.Id, 'Pending Approval');
        INSERT currentApproval;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        SubmitOpportunity ob = new SubmitOpportunity(stdController);
        ob.createApprovalStage();

        List<Approval_Stage__c> currentApprovalList = [SELECT Id, Status__c FROM Approval_Stage__c WHERE Opportunity__c = :newOpportunity.Id AND Status__c = :UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL];

        System.assertEquals(1, currentApprovalList.size(), 'The number of approval stage record was not the same as before the attempt to submit the opportunity');
    }

}