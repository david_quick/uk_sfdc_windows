@isTest
private class ApprovalControllerTest {
    
    static testMethod void testApprovalStage() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage = TestData.createApprovalStage(newOpportunity.Id);
        INSERT newApprovalStage;

        ApexPages.StandardController stdController  = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc                       = new ApprovalController(stdController);
        ApexPages.Message[] pageMessages            = ApexPages.getMessages();
        
        // Check that the error message we are expecting is in pageMessages
        Boolean messageFound = false;

        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == UtilConstants.MESSAGE_CREDIT_APPROVED
                && message.getSeverity() == ApexPages.Severity.INFO) {
                messageFound = true;        
            }
        }

        Test.startTest();
            System.assertEquals(messageFound, False, 'The Approval page should be accessible');
        Test.stopTest();
    }

    static testMethod void testHasNoApprovalStages() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage = TestData.createApprovalStage(newOpportunity.Id);
        INSERT newApprovalStage;

        ApexPages.StandardController stdController  = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc                       = new ApprovalController(stdController);
        ApexPages.Message[] pageMessages            = ApexPages.getMessages();
        
        // Check that the error message we are expecting is in pageMessages
        Boolean messageFound = false;

        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == UtilConstants.USER_IS_NOT_AN_APPROVER
                && message.getSeverity() == ApexPages.Severity.INFO) {
                messageFound = true;        
            }
        }

        Test.startTest();
            System.assert(messageFound,'The current User should not be an Approver');
        Test.stopTest();
    }   

    static testMethod void testIsAlreadyApproved() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        newOpportunity.is_Credit_Approved__c    = true;     
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage = TestData.createApprovalStage(newOpportunity.Id);
        newApprovalStage.User__c            = UserInfo.getUserId();
        newApprovalStage.isActive__c        = true;
        newApprovalStage.Status__c          = UtilConstants.APPROVAL_STAGE_STATUS_APPROVED; 
        INSERT newApprovalStage;
                
        newOpportunity.StageName                = UtilConstants.OPPORTUNITY_STATUS_CREDIT_APPROVED;
        UPDATE newOpportunity;

        ApexPages.StandardController stdController  = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc                       = new ApprovalController(stdController);
        ApexPages.Message[] pageMessages            = ApexPages.getMessages();

        // Check that the error message we are expecting is in pageMessages
        Boolean messageFound = false;

        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == UtilConstants.MESSAGE_CREDIT_APPROVED
                && message.getSeverity() == ApexPages.Severity.INFO) {
                messageFound = true;        
            }
        }

        Test.startTest();
            System.assert(messageFound, 'The credit is already approved for this Opportunity');
        Test.stopTest();
    }       

    static testMethod void testHasApprovalStages() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage  = TestData.createApprovalStage(newOpportunity.Id);
        newApprovalStage.User__c            = UserInfo.getUserId();
        newApprovalStage.Status__c          = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT newApprovalStage;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);
        ApexPages.Message[] pageMessages    = ApexPages.getMessages();
        
        // Check that the error message we are expecting is in pageMessages
        Boolean messageFound = false;

        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == UtilConstants.USER_IS_NOT_AN_APPROVER
                && message.getSeverity() == ApexPages.Severity.INFO) {
                messageFound = true;        
            }
        }

        Test.startTest();
            System.assertEquals(messageFound,false,'The current User should be able to retrieve its Approval Page');
        Test.stopTest();
    }   

    static testMethod void testApproveMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage  = TestData.createApprovalStage(newOpportunity.Id);
        newApprovalStage.User__c            = UserInfo.getUserId();
        INSERT newApprovalStage;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);

        PageReference nextPage = oc.approveOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                              WHERE Id = :newApprovalStage.Id];     
            System.assertEquals(nextPage.getUrl(),expectedPage.getUrl(),'The user should be redirected to the Opportunity page');
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_APPROVED, 'The Approval Stage status should be equal to APPROVED');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');

        Test.stopTest();
    }   

    static testMethod void testUpdateAllApprovalsOnApprove() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Amber';
        newOpportunity.Judgemental_Ratingx__c   = 'Amber';
        newOpportunity.Credit_Limit_x__c        = 410000;
        INSERT newOpportunity;

        Approval_Stage__c firstApprovalStage    = TestData.createApprovalStage(newOpportunity.Id);
        firstApprovalStage.User__c              = UserInfo.getUserId();
        firstApprovalStage.Status__c            = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT firstApprovalStage;

        Approval_Stage__c secondApprovalStage   = TestData.createApprovalStage(newOpportunity.Id);
        secondApprovalStage.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT secondApprovalStage;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);

        PageReference nextPage = oc.approveOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                              WHERE Id = :secondApprovalStage.Id];      
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION, 'The Approval Stage status should be equal to No Action Taken');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');

        Test.stopTest();
    }   

    static testMethod void testRejectMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage  = TestData.createApprovalStage(newOpportunity.Id);
        newApprovalStage.User__c            = UserInfo.getUserId();
        newApprovalStage.isActive__c        = true;
        INSERT newApprovalStage;

        ApexPages.StandardController stdController  = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc                       = new ApprovalController(stdController);

        PageReference nextPage = oc.rejectOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage   = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                                               WHERE Id = :newApprovalStage.Id];

            System.assertEquals(nextPage.getUrl(),expectedPage.getUrl(),'The user should be redirected to the Opportunity page');
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_REJECTED, 'The Approval Stage status should be equal to REJECTED');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');

        Test.stopTest();
    }   

    static testMethod void testUpdateAllApprovalsOnRejectMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Amber';
        newOpportunity.Judgemental_Ratingx__c   = 'Amber';
        newOpportunity.Credit_Limit_x__c        = 410000;
        INSERT newOpportunity;

        Approval_Stage__c firstApprovalStage    = TestData.createApprovalStage(newOpportunity.Id);
        firstApprovalStage.User__c              = UserInfo.getUserId();
        firstApprovalStage.Status__c            = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT firstApprovalStage;

        Approval_Stage__c secondApprovalStage   = TestData.createApprovalStage(newOpportunity.Id);
        secondApprovalStage.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT secondApprovalStage;

        ApexPages.StandardController stdController  = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc                       = new ApprovalController(stdController);

        PageReference nextPage = oc.rejectOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                              WHERE Id = :secondApprovalStage.Id];      
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION, 'The Approval Stage status should be equal to No Action Taken');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');

        Test.stopTest();
    }

    static testMethod void testEscalateMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Profile p = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        User u      = TestData.createDefaultUser(p.Id);
        u.Email     = 'newuser33@testorg.com';
        u.UserName  = 'newuser33@testorg.com';
        INSERT u;       

        User u2     = TestData.createDefaultUser(p.Id);
        u2.Email    = 'newuser22@testorg.com';
        u2.UserName = 'newuser22@testorg.com';
        INSERT u2;

        Approval_Stage__c newApprovalStage  = TestData.createApprovalStage(newOpportunity.Id);
        newApprovalStage.User__c            = UserInfo.getUserId();
        INSERT newApprovalStage;


        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);

        PageReference nextPage = oc.escalateOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            List<Approval_Stage__c> previousApprovals   = [SELECT Id, Name FROM Approval_Stage__c WHERE Opportunity__c = :newOpportunity.Id AND isActive__c = false];
            List<Approval_Stage__c> newApprovalStages   = [SELECT Id, Name FROM Approval_Stage__c WHERE Opportunity__c = :newOpportunity.Id AND isActive__c = true];

            Approval_Stage__c resultApprovalStage       = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                                      WHERE Id = :newApprovalStage.Id];
            Opportunity resultOpportunity               = [SELECT Id, Name, Approval_Level__c FROM Opportunity
                                                                                              WHERE Id = :newOpportunity.Id];
            System.assertEquals(nextPage.getUrl(),expectedPage.getUrl(),'The user should be redirected to the Opportunity page');
            System.assertEquals(resultOpportunity.Approval_Level__c,2, 'The Opportunity should have been Escalated to level 2');
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_ESCALATED, 'The Approval Stage status should be equal to ESCALATED');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');
            System.assertEquals(previousApprovals.size(), 1, 'Only one Approval Stage should have been deactivated');
            System.assertEquals(newApprovalStages.size(), 2, 'Only two Approval Stage should ne approved on Level2');
        Test.stopTest();
    }   

    static testMethod void testUpdateAllApprovalsOnEscalateMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Profile p = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Amber';
        newOpportunity.Judgemental_Ratingx__c   = 'Amber';
        newOpportunity.Credit_Limit_x__c        = 410000;
        INSERT newOpportunity;

        User u      = TestData.createDefaultUser(p.Id);
        u.Email     = 'newuser33@testorg.com';
        u.UserName  = 'newuser33@testorg.com';
        INSERT u;       

        User u2     = TestData.createDefaultUser(p.Id);
        u2.Email    = 'newuser22@testorg.com';
        u2.UserName = 'newuser22@testorg.com';
        INSERT u2;

        Approval_Stage__c firstApprovalStage    = TestData.createApprovalStage(newOpportunity.Id);
        firstApprovalStage.User__c              = UserInfo.getUserId();
        firstApprovalStage.Status__c            = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT firstApprovalStage;

        Approval_Stage__c secondApprovalStage   = TestData.createApprovalStage(newOpportunity.Id);
        secondApprovalStage.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT secondApprovalStage;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);

        PageReference nextPage = oc.escalateOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                              WHERE Id = :secondApprovalStage.Id];      
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION, 'The Approval Stage status should be equal to No Action Taken');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');
        Test.stopTest();
    }   

    static testMethod void testReferMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Green';
        newOpportunity.Judgemental_Ratingx__c   = 'Green';
        newOpportunity.Credit_Limit_x__c        = 110000;
        INSERT newOpportunity;

        Approval_Stage__c newApprovalStage  = TestData.createApprovalStage(newOpportunity.Id);
        newApprovalStage.User__c            = UserInfo.getUserId();
        INSERT newApprovalStage;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);

        PageReference nextPage = oc.referOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage   = [SELECT Id, Name, Status__c,isActive__c FROM Approval_Stage__c 
                                                                                              WHERE Id = :newApprovalStage.Id];
            System.assertEquals(nextPage.getUrl(),expectedPage.getUrl(),'The user should be redirected to the Opportunity page');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');
        Test.stopTest();
    }   

    static testMethod void testUpdateAllApprovalsOnReferMethod() {
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        Account newAccount              = TestData.createAccount();
        newAccount.Source_Country__c    = 'UK';
        INSERT newAccount;
        
        Opportunity newOpportunity              = TestData.createOpportunity(newAccount.Id, 5000000, 'Awaiting Credit Approval'); 
        newOpportunity.Bureau_Ratingx__c        = 'Amber';
        newOpportunity.Judgemental_Ratingx__c   = 'Amber';
        newOpportunity.Credit_Limit_x__c        = 410000;
        INSERT newOpportunity;

        Approval_Stage__c firstApprovalStage    = TestData.createApprovalStage(newOpportunity.Id);
        firstApprovalStage.User__c              = UserInfo.getUserId();
        firstApprovalStage.Status__c            = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT firstApprovalStage;

        Approval_Stage__c secondApprovalStage   = TestData.createApprovalStage(newOpportunity.Id);
        secondApprovalStage.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
        INSERT secondApprovalStage;

        ApexPages.StandardController stdController = new ApexPages.StandardController(newOpportunity);
        ApprovalController oc               = new ApprovalController(stdController);

        PageReference nextPage = oc.referOpportunity();

        String oppUrl = '/' + + newOpportunity.Id;
        PageReference expectedPage = new PageReference(oppUrl);

        Test.startTest();
            Approval_Stage__c resultApprovalStage = [SELECT Id, Name, Status__c, isActive__c FROM Approval_Stage__c 
                                                                              WHERE Id = :secondApprovalStage.Id];      
            System.assertEquals(resultApprovalStage.Status__c,UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION, 'The Approval Stage status should be equal to No Action Taken');
            System.assertEquals(resultApprovalStage.isActive__c, false, 'The Approval stage should have been deactivated');

        Test.stopTest();
    }   
}