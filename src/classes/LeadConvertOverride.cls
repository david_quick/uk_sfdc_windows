/**
 * The code in this class is used when a new application is converted using the Convert custom button
 * 24/01/2017 David Quick UKCRM-282 - The creation of the LeadSnapshot__C record has been moved to occur
 *                                    when the application is converted via the Convert button. The code has been moved out to a new class LeadSnapShot_Create
 *                                    and is called from the LeadConvertOverride class leadConv method.
 *                                    The code in this trigger is now obsolete and should not be used
 * 02/02/2017 David Quick UKCRM-320 - The credit analysts have to edit applications for data quality. Backing out the changes for UKCRM-282 until better fix possible 
 **/


public with sharing class LeadConvertOverride {
    public lead l { get; set; }
    public opportunity dummyOpp { get; set; }
    public boolean sendEmailToOwner { get; set; }
    //public string acct { get; set; }    
    public string con { get; set; }
    public string acc { get; set; }
    public string oppName { get; set; }
    public boolean CreateOpp { get; set; }
    public boolean supplier {get; set;}
    public string leadConvStatus { get; set; }
    //public task leadTask { get; set; } 

    public list<selectOption> accList; 
    public list<selectOption> conList; 
    map<id, contact> conMap = new map<id, contact>(); 
    map<id, account> accMap = new map<id, account>(); 
    public list<selectOption> lcStatuses; 
    
    
    public LeadConvertOverride(ApexPages.StandardController controller) {
                
        // get the lead
        l = [select id, name, firstname, lastname, ownerId, LeadSource, Phone, 
             Address_details_of_other_Franchise_sites__c,Agreed_to_Terms_and_Conditions__c,Are_you_VAT_registered__c,Area_Number__c,
             Auctions_Dealer_purchases_from__c, Auctions_Used_in_Last_12_months__c, Ave_Vehicle_Sales_Value__c, Average_Number_of_Vehicles_in_Stock__c,
             Average_Stock_Turn_Days__c, Bank_Account__c, Bank_Name__c, Bank_Sort_Code__c, Business_Activities__c, Business_Address_County__c, Business_Address_Line_1__c, Business_Address_Line_2__c,
             Business_Address_Line_3__c, Business_Address_Postcode__c, Business_Address_Town_City__c, Business_Name2__c, Business_Phone__c, Car_Sales_Annual_Volume__c,
             Comments__c, company, Company_Registration_Number__c, Company_Status__c, Consumer_Credit_Lincense__c, Credit_Limit__c, CurrencyIsoCode,
             Current_Stock_Funding_Method__c, Currently_Member_of_SIMI__c, Data_Source__c, Date_of_Birth__c, Dealer_auction_reference__c, Dealer_Premises__c, Do_you_hold_a_dealer_number_from_the_Dep__c,
             Do_you_operate_from_more_than_1_site__c, Dual_Currency__c,  email, Has_new_vehicle_franchise__c, Higher_Limit_Request__c, Home_Address_County__c, 
             Home_Address_Line_1__c, Home_Address_Line_2__c, Home_Address_Line_3__c, Home_Address_Postcode__c, Home_Address_Town_City__c, Internet_based__c, 
             LeadAndSnapshotMapField__c, Length_of_Time_with_Bank__c, Middle_Initials__c, MobilePhone, Mobile_Phone__c, Name_of_Insurer__c, Name2__c, New_Vehicle_Sales_annual_volume__c, 
             Number_of_Directors__c, Number_Years_Trading__c, NVF_for_a_min_of_12_months__c, Other_Businesses__c, Permission_Additional_Director_Searches__c, 
             Permission_to_Process__c, Personal_Guarantee__c, Physical_Business_Premises__c, Premises_comments__c,
             Previous_Businesses__c, Previous_Trading_Names__c, Product__c, Prospect_Marketing_Exclusion__c, Record_type_id__c, Recruit_a_Dealer__c, Recruit_a_Dealer_Reference__c,
             Replacement_or_Incremental__c, ROI_Auctions_Dealer_Purchases_from__c, sourceCountry__c, Stage__c, TAN_Registered__c, Territory__c, TPS_flagged__c, UK_auctions_dealer_has_buyer_a_c_with__c,
             VAT_Number__c, Vehicle_Types_Sold__c, website, What_Franchise_s_do_you_hold__c, Which_Manheim_branch_referred_you__c, Which_Manheim_staff_member_referred_you__c, Willing_to_provide_Directors_guarantees__c
                 from lead
                    where id = : controller.getId()];
        Id rid = TestData.getRecordTypeID('Supplier', 'Lead');
        supplier = false;
        CreateOpp = true;
        if (l.Record_type_id__c == rid)   
        {
            supplier = true;
            CreateOpp = false;
        }
        
        // dummy opp allows owner selection
        dummyOpp = new Opportunity( 
            ownerid = ((((string)(l.ownerid)).startsWith('005')) ? l.ownerid : userInfo.getUserId())
        );
        
        // set a default opp name
        oppName = l.company;
        
        
    }    
    
    public list<selectOption> getConList() {
        if (conList == null) {
            conList = new list<selectOption>();
            
            conList.add( new SelectOption('', '--None--') );
            conList.add( new SelectOption('NEW_CONTACT', 'Create New: ' + l.name) );            
        
            // find contact matches
            conMap = new map<id, contact>([select id, name, accountid, firstname, lastname 
                                                from contact 
                                                where accountid = :acc and   
                                                        ((lastname = : l.lastname and firstname = : l.firstname) or
                                                        (email = : l.email and email != null)) limit 50]);
                
            for (contact c : conMap.values()) {
                conList.add( new SelectOption(c.id, 'Attach to Existing: ' + c.name) );                            
            }
        }
        return conList;    
    } 

    public pagereference contacts(){
        con =null;
        conlist=null;
        return null;
    }    
    
    public list<selectOption> getAccList() {
        if (AccList == null) {
            AccList = new list<selectOption>();
            AccList.add( new SelectOption('', '--None--') );
            if ( shouldAllowAddingNewAccount() ){
                AccList.add( new SelectOption('NEW_ACCOUNT', 'Create New: ' + l.company) );
            }
        
            string companyToSearch = l.company;
            companyToSearch = companyToSearch+'%';
            
            
            string emailToSearch = l.email;

            // find Account matches
            accMap = new map<id, Account>([select id, name from Account  
                                           where name like :companyToSearch or
                                           Business_Email__c = :emailToSearch
                                           limit 50]);
                
            for (Account c : accMap.values()) {
                AccList.add( new SelectOption(c.id, 'Attach to Existing: ' + c.name) );                            
            }
        }
        return accList;    
    } 
    
    public boolean shouldAllowAddingNewAccount (){
        // get all the profiles which have allow account creation rights from custom settings
        list<Convert_Leads_Profile_Account_Create__c> allowedProfiles = Convert_Leads_Profile_Account_Create__c.getAll().values();
        String trimmedProfileId = UserInfo.getProfileId();
        String trimmedProfileName;
        if ( trimmedProfileId != null ){
            // remove checksum to get 15 digit id
            trimmedProfileId = trimmedProfileId.left(15);
        }
        
        for ( Convert_Leads_Profile_Account_Create__c profile : allowedProfiles ){
            trimmedProfileName = profile.name;
            if ( trimmedProfileName != null ){
                // remove checksum to get 15 digit id
                trimmedProfileName = trimmedProfileName.left(15);
            }

            if ( profile != null && trimmedProfileName == trimmedProfileId ){
                return true;
            }
        }
        
        return false;
    }
    
    
    public list<selectOption> getlcStatuses() {
        if (lcStatuses == null) {
            lcStatuses = new list<selectOption>();
            // query
            for (LeadStatus ls : [Select Id, MasterLabel from LeadStatus where IsConverted=true] ) {
                lcStatuses.add( new SelectOption(ls.masterLabel, ls.masterLabel ));
            }
            leadConvStatus = lcStatuses[0].getValue();
        }
        return lcStatuses;
    } 
       
    public pageReference convertLead() {
        PageReference pageRef;
        return pageRef;
    }            

    public pageReference leadConv() {
        System.debug('********** CreateOpp = ' + CreateOpp);
        if (supplier)
        {
            return ConvertSupplier();
        }
        else
        {
            return ConvertOtherLead();
        }
    }
 
    public pageReference ConvertOtherLead() {
        if (con == null && acc != 'NEW_ACCOUNT') {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'You must select an option in the Contact box.'
            ));
            return null;
        }
        if (acc == null) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'You must select an option in the Account box.'
            ));
            return null;
        }
        if (l.Credit_Limit__c == 'None' || l.Credit_Limit__c == null) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'There is no credit limit given on the application.'
            ));
            return null;
        }
          
        // set up the conversion
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setDoNotCreateOpportunity(true);
        lc.setLeadId(l.Id);
        if (CreateOpp){
            lc.setOpportunityName(oppName);
            lc.setDoNotCreateOpportunity(false);
        } 
        
        lc.setConvertedStatus(leadConvStatus);
        

        // is this a merge to existing?
        if (con != 'NEW_CONTACT' && con!=null) {  
            lc.setContactId(con);
            lc.setAccountId(conMap.get(con).accountId);             
        }        

        if (acc != 'NEW_ACCOUNT') {
            lc.setAccountId(acc);             
        }        

        PageReference pageRef;
        Database.LeadConvertResult lcr;
        try {
            lcr = Database.convertLead(lc);
            Account convacc = [select a.BillingStreet, a.BillingState, a.BillingPostalCode, 
                               a.BillingCountry, a.BillingCity, a.AccountSource, a.Business_Email__c,
                               a.Do_you_operate_from_more_than_1_site__c, a.Dealer_Premises__c, a.Phone,
                               a.Business_Phone__c, a.Mobile_Phone__c, a.NVF_for_a_min_of_12_months__c,
                               a.Willing_to_provide_Directors_guarantees__c
                               from Account a where id=:lcr.getAccountId() limit 1];
            convacc.AccountSource = l.LeadSource;
            convacc.Business_Email__c = l.Email;
            convacc.Do_you_operate_from_more_than_1_site__c= l.Do_you_operate_from_more_than_1_site__c;
            convacc.Dealer_Premises__c = l.Dealer_Premises__c;
            convacc.Phone = l.Phone;
            convacc.Business_Phone__c = l.Business_Phone__c;
            convacc.Mobile_Phone__c = l.Mobile_Phone__c;
            //UKCRM-409
            if (string.isBlank(l.Business_Phone__c)) {
                convacc.Business_Phone__c = l.Phone;
            }
            convacc.NVF_for_a_min_of_12_months__c=l.NVF_for_a_min_of_12_months__c;
            convacc.Willing_to_provide_Directors_guarantees__c=l.Willing_to_provide_Directors_guarantees__c;
                 
            update convacc;
       
            if( l.LeadAndSnapshotMapField__c != null ){
                LeadSnapshot__c associatedSnapshot = new LeadSnapshot__c ( LeadAndSnapshotMapField__c = l.LeadAndSnapshotMapField__c, AssocitedAccount__c = lcr.getAccountId() );
                upsert associatedSnapshot LeadAndSnapshotMapField__c;
            }
            
            // Restart existing code
            Opportunity convopp = [select id from Opportunity where id = :lcr.getOpportunityId() limit 1];
            if ( l.Credit_Limit__c != null ){            
                convopp.Credit_Limit_x__c = Decimal.valueof(l.Credit_Limit__c.replaceAll('[^0-9]', ''));
            }
            convopp.Business_Email__c = l.Email;
            update convopp;
            
            List<Contact> unqualCon = [select id, AccountId, RecordTypeId, Lead__c from Contact where Lead__c =:l.Id];
            RecordType rt = [select id from RecordType where SobjectType = 'Contact' AND DeveloperName = 'Qualified_Contacts' ];         
            
            Contact convcon = [select RecordTypeId, HomePhone, MobilePhone, OtherPhone, Phone, Business_Phone__c, Home_Phone__c,
                               Mobile_Phone__c
                               FROM Contact WHERE id = :lcr.getContactId() limit 1];
            
            convcon.RecordTypeId = rt.Id;
            // Update contact phone numbers from lead
            convcon.Phone = l.Phone;
            convcon.HomePhone = l.Phone;
            convcon.Business_Phone__c = l.Business_Phone__c;
            convcon.MobilePhone = l.MobilePhone;
            convcon.Mobile_Phone__c = l.Mobile_Phone__c;
            convcon.HomePhone = l.Phone;
            convcon.Home_Phone__c = l.Phone;
            update convcon;

            List<Equifax_Report__c> er = [select id,Accounts__c,Lead__c,Opportunity__c FROM Equifax_Report__c WHERE Lead__c = :l.id];

            for(Equifax_Report__c e:er)
            {
                e.Accounts__c = lcr.getAccountId();
                e.Opportunity__c = lcr.getOpportunityId();
            }            
            if (er.size()>0)update er;
            
            pageRef = new PageReference('/' + lcr.getOpportunityId());
            
            for(Contact c : unqualCon)
            {
                c.AccountId = lcr.getAccountId();
                c.Lead__c = null;
                c.RecordTypeId = rt.Id;
            }
            update unqualCon;
                        
        } catch (exception e) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,    e.getMessage())
            );
        }
        return pageRef;  
    }



	public pageReference ConvertSupplier() {
       if (con == null && acc != 'NEW_ACCOUNT') {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'You must select an option in the Contact box.'
            ));
            return null;
        }
        if (acc == null) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'You must select an option in the Account box.'
            ));
            return null;
        }
        if (string.isEmpty(l.Bank_Name__c) || string.IsEmpty(l.Bank_Sort_Code__c) || string.IsEmpty(l.Bank_Account__c))
        {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'Bank Account details must all be given before conversion'
            ));
            return null;
        }
        if (l.Bank_Account__c.length() <> 8) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'Bank account must be eight numbers in length.'
            ));
            return null;
        }
        if (l.Bank_Sort_Code__c.length() != 6) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,
                'Bank SortCode must be six numbers.'
            ));
            return null;
        }
             
             // set up the conversion
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setDoNotCreateOpportunity(true);
        lc.setLeadId(l.Id);
        if (CreateOpp) {
            lc.setOpportunityName(oppName);
        	lc.setDoNotCreateOpportunity(false);
        }
        lc.setConvertedStatus(leadConvStatus);
        

        // is this a merge to existing?
        if (con != 'NEW_CONTACT' && con!=null) {  
            lc.setContactId(con);
            lc.setAccountId(conMap.get(con).accountId);             
        }        

        if (acc != 'NEW_ACCOUNT') {
            lc.setAccountId(acc);             
        }        

        PageReference pageRef;
        Database.LeadConvertResult lcr;
        Id rid = TestData.getRecordTypeID('Supplier', 'Account');
        
        try {
            lcr = Database.convertLead(lc);
            Account convacc = [select a.Bank_Account_Number__c,a.Bank_Name__c, a.Bank_Sort_Code__c,a.RecordTypeId,
                               a.Company_Registration_Number__c, a.Dealer_Auction_Reference__c, a.Business_Email__c,
                               a.Website, a.Website_URL__c, a.Vehicle_Types_Sold__c, a.What_Franchise_s_do_you_hold__c,
                               a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, a.BillingCity
                               from Account a where id=:lcr.getAccountId() limit 1];
            convacc.AccountSource = l.LeadSource;
            convacc.RecordTypeId = rid;
            convacc.Phone = l.Phone;
            convacc.Bank_Account_Number__c = l.Bank_Account__c;
            convacc.Bank_Name__c = l.Bank_Name__c;
            convacc.Bank_Sort_Code__c = l.Bank_Sort_Code__c;
            convacc.Company_Registration_Number__c = l.Company_Registration_Number__c;
            convacc.Dealer_Auction_Reference__c = l.Dealer_Auction_Reference__c;
            convacc.Business_Email__c = l.Email;
            convacc.Business_Phone__c = l.Business_Phone__c;
            convacc.VAT_Registration_Number__c = l.VAT_Number__c;
            convacc.Website = l.Website;
            convacc.Website_URL__c = l.Website;
            convacc.Vehicle_Types_Sold__c = l.Vehicle_Types_Sold__c;
            convacc.What_Franchise_s_do_you_hold__c = l.What_Franchise_s_do_you_hold__c;
            if (string.isBlank(convacc.Business_Phone__c)) {
                convacc.Business_Phone__c = l.Phone;
            }
            update convacc;
            if (CreateOpp) {
            	Opportunity convopp = [select id, Bank_Account_Number__c, Bank_Name__c, Bank_Sort_Code__c,
                                       Business_Email__c, VAT_Registration_Number__c, Vehicle_Types_Sold__c, Website_URL__c
                                       from Opportunity where id = :lcr.getOpportunityId() limit 1];
            	convopp.Business_Email__c = l.Email;
                convopp.Bank_Account_Number__c = l.Bank_Account__c;
                convopp.Bank_Name__c = l.Bank_Name__c;
                convopp.Bank_Sort_Code__c = l.Bank_Sort_Code__c;
                convopp.VAT_Registration_Number__c = l.VAT_Number__c;
                convopp.Vehicle_Types_Sold__c = l.Vehicle_Types_Sold__c;
                convopp.Website_URL__c = l.Website;
            	update convopp;
            }
            
            List<Contact> unqualCon = [select id, AccountId, RecordTypeId, Lead__c from Contact where Lead__c =:l.Id];
            RecordType rt = [select id from RecordType where SobjectType = 'Contact' AND DeveloperName = 'Qualified_Contacts' ];         
            Contact convcon = [select RecordTypeId FROM Contact WHERE id = :lcr.getContactId() limit 1];
            convcon.RecordTypeId = rt.Id;
            update convcon;
          
            pageRef = new PageReference('/' + lcr.getAccountId());
            
            for(Contact c : unqualCon)
            {
                c.AccountId = lcr.getAccountId();
                c.Lead__c = null;
                c.RecordTypeId = rt.Id;
            }
            update unqualCon;
                        
        } catch (exception e) {
            apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error,    e.getMessage())
            );
        }
        return pageRef;     
    }
}