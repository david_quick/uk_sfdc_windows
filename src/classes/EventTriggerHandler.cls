public with sharing class EventTriggerHandler extends TriggerHandler {
    
    TaskEventUpdates updater = new TaskEventUpdates();
    private static string callingId = null;
    
    public EventTriggerHandler(){
        system.debug('********** EventTriggerHandler');
        preventRecursiveTrigger(false);
    }
  
    public override void beforeInsert(List<SObject> newObjects){
        // Note - a single event goes through as a list of 1
        // but a recurring event goes through once as a list of one, then again for each recurrence
        system.debug('********** Event trigger before insert - Size of list is ' + newObjects.size());
        Integer recurs = 0;
        List<Event> newEvents = (List<Event>) newObjects;
        for (Event e : (List<Event>)newObjects)
        {
            e.SMS_Mobile_Phone_c__c = TestData.getMobileNumber(e.WhoId, e.OwnerId);
            e.Destination_Name__c = TestData.getDestinationName(e.WhoId, e.OwnerId);   
            system.debug('********** MobilePhone is ' + e.SMS_Mobile_Phone_c__c);
        }
          
        Event e1 = newEvents[0];
    
        if (newObjects.size() == 1) {
            recurs = updater.getRecurrence(e1);
        }
        if (recurs > 50) {
            if (e1.RecurrenceType == 'RecursYearly' || e1.RecurrenceType == 'RecursYearlyNth') {
                recurs = (recurs / 365) + 1;
                e1.addError('Cannot create this many recurring yearly events ( ' + recurs + ' ) - please reduce to 5 or less');   
            }
            else {
                e1.addError('Cannot create this many recurring events ( ' + recurs + ' ) - please reduce to 50 or less');
            }
            return;
        }
        String pcode = updater.getAccountPostcode(e1.Whatid);
        String aName = updater.getAccountName(e1.Whatid);
        // Note that recurring events use the master event details so the append is only needed on the first pass
        if (newEvents.size() == 1) {
            for(integer i=0; i< newEvents.size(); i++)
            {
                Event e = newEvents[i];
                if (e.Location != null) {
                    e.Location = e.Location + ' - ' + pcode;
                }
                else {
                    e.Location = pcode;
                }
                if (e.Subject != null) {
                    e.Subject = e.Subject + ' - ' + aName;
                }
                else {
                    e.Subject = aName;
                }
            }
        }
        updater.addRelatedTask(newEvents);
     }
    
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap) {
        updater.eventUpdated(newObjects);
    }
    
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        updater.checkRelatedTask(newObjects);
        updater.removeCompletedEvent(newObjects);
    }
}