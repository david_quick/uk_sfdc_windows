public with sharing class ApproveRejectOpportunityController {
	
	public Opportunity o {get;set;}
	
	public String ApproveReject;
	
	public Map<id,Profile_Settings__c> psMap = new Map<Id,Profile_Settings__c>();
	public Id ProfileID {
		get{
			if (ProfileId==null)
			{
				ProfileId = Userinfo.getProfileid();				
			}
			return ProfileID;
		}
		set;
	}
	
	
	
	Public ApproveRejectOpportunityController(ApexPages.StandardController controller)
	{
		controller.addFields(new List<String>{'US_Director_Approval__c', 'US_Director_Not_Approved__c','US_Director_Rejection_Reason__c', 
												'Credit_Manager_Approval__c', 'Credit_Manager_Not_Approved__c', 'Credit_Manager_Rejection_Reason__c',
												'Sales_Director_Approval__c','Sales_Director_Not_Approved__c', 'Sales_Director_Rejection_Reason__c',
												'Managing_Director_Approval__c','Managing_Director_Not_Approved__c','Managing_Director_Rejection_Reason__c',
												'Finance_Director_Approval__c', 'Finance_Director_Not_Approved__c', 'Finance_Director_Rejection_Reason__c',
												'Previous_RecordType__c',
												'Operations_Director_Approval__c','Operations_Director_Not_Approved__c','Operations_Director_Rejection_Reason__c'});
		o=(Opportunity) controller.getRecord();

		List<Profile_Settings__c> userProfiles = Profile_Settings__c.getAll().values();  // get the profile settings

		for (Profile_Settings__c profileInfo : userProfiles) {
    		psMap.put(profileInfo.Profile_ID__c,profileInfo);
		}
		
	}
	
	Public PageReference Init(){
		o.put(psMap.get(ProfileID).Opp_Approval_Column__c,TRUE); // approval column
		o.put(psMap.get(ProfileID).Opp_Approval_Date_Column__c,Date.Today()); // approval date column
		o.put(psMap.get(ProfileID).Opp_Rejection_Column__c,FALSE); // Rejection column
		o.put(psMap.get(ProfileID).Opp_Rejection_Reason_Column__c,''); // Rejection Reason
//		o.recordtypeid = o.Previous_RecordType__c;
		update o;
		return new PageReference('/'+o.id);
	}
	
	Public PageReference Save(){
	
		if(o.get(psMap.get(ProfileID).Opp_Rejection_Reason_Column__c)!='')
		{
			o.put(psMap.get(ProfileID).Opp_Approval_Column__c,FALSE); // approval column
			o.put(psMap.get(ProfileID).Opp_Approval_Date_Column__c,null); // approval date column
			o.put(psMap.get(ProfileID).Opp_Rejection_Column__c,TRUE); // Rejection column
			o.recordtypeid = o.Previous_RecordType__c;
			update o;
			return new PageReference('/'+o.id);
		}
		else
		{
			o.addError('You must enter a Rejection reason');
			return null;
		}
		
	}
	
	Public PageReference Cancel(){
		return new PageReference('/'+o.id);
	}

}