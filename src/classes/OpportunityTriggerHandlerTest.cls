@isTest
private class OpportunityTriggerHandlerTest{

    /************************ BEFORE INSERT : POPULATE COMMERCIAL CONTACT **************************************/

    // Create a Contact Role with Commercial role with Default = Yes
    static testMethod void testSingleRecordPositiveOpportunityBeforeInsertCaseOne(){

        // Load Approval Matrix and Trheshold
        TestData.createApprovalMatrixValue();
        TestData.createApprovalThresholdValue();

        //Create an Account record
        RecordType ar = [SELECT Id,Name FROM RecordType where developerName = 'Companies'];
        Account account = TestData.createAccountWithRecordType(ar.Id);
        account.Source_Country__c = 'UK';
        insert account;        
        
        //Create a Contact record        
        Contact contact = TestData.createContact(account,'James','Joyce');
        insert contact;

        System.assertEquals(contact.AccountId, account.Id);
        
        //Create an Opportunity record                
        Opportunity oppty = TestData.createOpportunity(account.Id, 500000, 'Awaiting approval');
        oppty.Approval_Level__c = 0;
        oppty.Bureau_Ratingx__c = 'Green';
        oppty.Judgemental_Ratingx__c = 'Green';
        oppty.Credit_Limit_x__c = 200000;
        System.debug('********* Calling Insert Opportunity');
        Test.startTest();
            insert oppty;
        Test.stopTest();
        
        // Verify that has Approval Level populated or not?     
        Opportunity opptyResult = [SELECT Id, Approval_Level__c FROM Opportunity WHERE Id =: oppty.Id];
        System.assertEquals(1, opptyResult.Approval_Level__c);        
    }
    
}