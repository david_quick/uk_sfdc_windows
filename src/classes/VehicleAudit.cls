@RestResource(urlMapping='/vehicle_audit')
global class VehicleAudit {
    global class VehicleAuditException extends Exception{}
        private static final String VehicleAudit_INVALID_SITEID = 'VehicleAuditINVALID_SITE : the VehicleAuditSite is invalid.';
        private static final String VehicleAudit_INVALID_RATING = 'VehicleAuditINVALID_RATING : the VehicleAuditRating is invalid.';
    
    global class ResultData{
            global Integer jobId;
            global String siteId;
            global String auditDate;
            global String rating;
    }
            
        @HttpPost
        global static List<ResultData> insertVehicleAudits(List<ResultData> vehicleAudit){
            Set<String> accIds = new Set<String>();
            List<Vehicle_Audit__c> val = new List<Vehicle_Audit__c>();
            Map<String, Id> mapWfs = new Map<String, Id>();
            for(ResultData rd : vehicleAudit)   
            {
                if(rd.rating != 'RED' && rd.rating !='AMBER' && rd.rating != 'GREEN')
                    Throw new VehicleAuditException(VehicleAudit_INVALID_RATING);
                accIds.add(rd.siteId);
            }
            List<Account> a = [SELECT id, WFS_Ref__c  FROM Account WHERE WFS_Ref__c IN :accIds ];
            for(Account acc: a)
            {
                if(!mapWfs.containsKey(acc.WFS_Ref__c))
                {
                    mapWfs.put(acc.WFS_Ref__c, acc.Id);   
                }
            }
            for(ResultData rd : vehicleAudit)   
            {                                                           
                Vehicle_Audit__c va = new Vehicle_Audit__c();
                if(!mapWfs.containsKey(rd.siteId))
                    Throw new VehicleAuditException(VehicleAudit_INVALID_SITEID);
                
                va.jobId__c = rd.jobId;
                va.SiteId__c = mapWfs.get(rd.siteId);
                va.auditDate__c = (DateTime) JSON.deserialize(
                                    '"' + rd.auditDate + '"', DateTime.class);
                va.rating__c = rd.rating;
                
                val.add(va);
                
            }
            if(val.size() > 0)
                insert val;
            return new List<ResultData>();
        }

}