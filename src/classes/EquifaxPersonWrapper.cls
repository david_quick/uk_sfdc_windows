public with sharing class EquifaxPersonWrapper {

    public String ptcabs { get; set; }
    public String Forename { get; set; }
    public String Surname { get; set; }
    public String Housename { get; set; }
    public String Street { get; set; }
    public String Town { get; set; }
    public String Postcode { get; set; }
    public boolean selected { get; set; }

    public EquifaxPersonWrapper(String ptcabs,String Forename,String Surname,String Housename,String Street,String Town,String Postcode){
        this.selected = false;          
        this.ptcabs = ptcabs;           
        this.Forename = Forename;           
        this.Surname = Surname;         
        this.Housename = Housename;         
        this.Street = Street;           
        this.Town = Town;           
        this.Postcode = Postcode;           
    }
    public EquifaxPersonWrapper(){
        this.selected = false;          
        this.ptcabs = '';           
        this.Forename = '';         
        this.Surname = '';          
        this.Housename = '';            
        this.Street = '';           
        this.Town = '';         
        this.Postcode = '';         
    }
}