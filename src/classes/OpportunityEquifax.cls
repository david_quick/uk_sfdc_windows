public with sharing class OpportunityEquifax {

    Opportunity o = new Opportunity();
    
    public String  compref{get;set;}
    public String stage {get;set;}
    
    public OpportunityEquifax(ApexPages.StandardController stdCtrl){
        stdCtrl.addFields(new String[] {'Account.Company_Registration_Number__c','Account.Name'});
        this.o = (Opportunity)stdCtrl.getRecord();
    }

    public List<EquifaxCompWrapper> compdetails{
        get{
            if(compdetails==null)
            {
                compdetails = Equifax.parseEquifaxCompanyList(o.Account.Name);
            }
            return compdetails;
            
        }
        set;
    }

    public List<EquifaxPersonWrapper> persondetails{
        get{
            if(persondetails==null && compref!=null)
            {
                persondetails = Equifax.parseEquifaxCompDirectors(compref);
            }
            return persondetails;
            
        }
        set;
    }

    public PageReference DirectorAddresses(){
        persondetails = equifax.parseDirectorIdentifiers(persondetails);
        id retid = Equifax.parseEquifaxResponse(compref,null,o.id,null,persondetails);
        return new pagereference('/'+retid);
    }


    public PageReference EquifaxDirectors(){
        //id retid = Equifax.parseEquifaxResponse(l.Company_Registration_Number__c,l.id,null,null);
        persondetails=Equifax.parseEquifaxCompDirectors(compref);
        return null;        
    }
    
    public PageReference EquifaxNonLim(){
        id retid = Equifax.parseEquifaxNonLimResponse(compref,null,o.id,null);
        //persondetails=Equifax.parseEquifaxCompDirectors(compref);
        return new pagereference('/'+retid);
    }
    
    public PageReference EquifaxReport(){
        //id retid = Equifax.parseEquifaxResponse(l.Company_Registration_Number__c,l.id,null,null);
        id retid = Equifax.parseEquifaxResponse(compref,null,o.id,null);
        return new pagereference('/'+retid);
    }
}