public class LeadContactTriggerHandler extends TriggerHandler{
    
    public override void beforeInsert(List<SObject> newObjects){
        // Unless the user is the API user do not allow updates - the record is read only once created
        LeadSnapShot_Create createSnapshot = new LeadSnapShot_Create();
        list<Lead_Contact__c> newLeads = ( list<Lead_Contact__c> ) newObjects;
        for(integer i=0; i< newLeads.size(); i++ ) {
            Lead_Contact__c newSnap = newLeads[i];
            string profile = createSnapshot.getCurrentProfileName();
            if (profile !='System Administrator API') {
                newLeads[i].addError('You cannot change a lead contact snapshot');
            }
        }
    }
        
	public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap)
    {
        list<Lead_Contact__c> originalLeads = ( list<Lead_Contact__c> ) oldObjects;
        list<Lead_Contact__c> updatedLeads = ( list<Lead_Contact__c> ) newObjects;
        for(integer i=0; i< originalLeads.size(); i++ ) {
           	updatedLeads[i].addError('The lead contact cannot be edited - it is a record of the original application');
       	}
   	}
}