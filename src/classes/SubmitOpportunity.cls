/**
* File Name   :    SubmitOpportunity.cls
* Description :    [NIL-10] - Custom page for supporting approval process
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Ioan Beschea Intial
* 0.2 22/11/2016 Simon Wilby  warning message added to stop approval level 6
* 0.3 30/01/2017 Simon Wilby  set the Opportunity Stage to "Awaiting Credit Approval" on submission
*/

public class SubmitOpportunity { 

    Id opportunityId;
    Opportunity currentOpp;
    Boolean messageDisplay;
    List<User> approvalUserList {get;set;}

    public SubmitOpportunity(ApexPages.StandardController stdController) {
        opportunityId = (Id) stdController.getId();
        currentOpp = [SELECT Approval_Level__c, Source_Country__c, RecordTypeId FROM Opportunity WHERE Id = :opportunityId];
        
        if (currentOpp.Approval_Level__c == 6) {
        messageDisplay = true;
            ApexPages.Message oppSubmitted1 = new ApexPages.Message(ApexPages.Severity.WARNING, UtilConstants.OPPORTUNITY_NO_APPROVAL);
            ApexPages.addMessage(oppSubmitted1);
            return;
        }
        
        messageDisplay = false;
        List<Approval_Stage__c> listOfApprovals = [SELECT Id FROM Approval_Stage__c WHERE Opportunity__c = :opportunityId AND isActive__c = true AND Status__c = :UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL];
        if (listOfApprovals.size() > 0) {
            messageDisplay = true;
            ApexPages.Message oppSubmitted = new ApexPages.Message(ApexPages.Severity.WARNING, UtilConstants.OPPORTUNITY_CANNOT_BE_SUMITTED_FOR_APPROVAL);
            ApexPages.addMessage(oppSubmitted);
        }
        
        approvalUserList = ApprovalSettings.getApprovers(currentOpp.Source_Country__c, Integer.valueOf(currentOpp.Approval_Level__c)); 
    }

    public PageReference createApprovalStage(){

        if (messageDisplay != true) { 
            List<Approval_Stage__c> currentApprovalList = new List<Approval_Stage__c> ();
            for (User currentUser :approvalUserList) {
                Approval_Stage__c currentApproval   = new Approval_Stage__c();
                currentApproval.User__c             = currentUser.Id;
                currentApproval.OwnerId             = currentUser.Id;
                currentApproval.User_email__c       = currentUser.Email;
                currentApproval.Opportunity__c      = opportunityId;
                currentApproval.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
                currentApproval.Date_submitted__c   = Date.today();
                currentApproval.Level__c            = Integer.valueOf(currentOpp.Approval_Level__c);
                currentApprovalList.add(currentApproval);
            }
            try {
                if(currentApprovalList.size() > 0) {
                    INSERT currentApprovalList;
                }
                changeRecordTypeForOpp();
                changeStageForOpp();
            }
            catch(Exception e) {
                
            }
            String urlOppPage = '/' + opportunityId;
            PageReference currentPage = new PageReference (urlOppPage);
            currentPage.setRedirect(true);
            return currentPage;
        }
        return null;
    }

    void changeRecordTypeForOpp(){
        Id auctionRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AUCTION_HOUSE LIMIT 1].Id;
        Id awaitingApprovalAHRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL_AH LIMIT 1].Id;
        Id awaitingApprovalRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL  LIMIT 1].Id;
        Id awaitingApprovalSRRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL_SR LIMIT 1].Id;
        Id SimpleRenewalsRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_SIMPLE_RENEWALS  LIMIT 1].Id;
        currentOpp.Is_Credit_Approved__c = false;

        if (currentOpp.RecordTypeId == auctionRecordTypeId) {
            currentOpp.RecordTypeId = awaitingApprovalAHRecordTypeId;
        } else if 
            (currentOpp.RecordTypeId == SimpleRenewalsRecordTypeId) {
            currentOpp.RecordTypeId = awaitingApprovalSRRecordTypeId;
        } else {
            currentOpp.RecordTypeId = awaitingApprovalRecordTypeId;
        }
        UPDATE currentOpp;
    }
    
    void changeStageForOpp(){
        currentOpp.StageName = UtilConstants.OPPORTUNITY_STAGE_AWAITING_CREDIT_APPROVAL;
        
        UPDATE currentOpp;
    }    

}