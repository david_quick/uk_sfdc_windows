/**
*
* Author:       Piotr Czechumski
* Date:         30/01/2015
* Description:  Service class for creating WFS_Credit_Data__c records with XML structure required for credit file integration
*
**/
public with sharing class WFS_Credit_XML {
    
    private static final string WFS_NULL = '(null)';
    

    public static void processCreditItems (list<id> plans){
        
        id lastAccountProcessed;
        
        List<WFS_Credit_Data__c> liCreditData = new List<WFS_Credit_Data__c>();

        for (Opp_Prod_Sites__c planSite : [ select id,Company__c,Company__r.WFS_Ref__c, Opp_Product__r.End_Date__c, Opp_Product__r.Facilities__c, Opp_Product__r.Limit__c, 
                                            Opp_Product__r.Name, Opp_Product__r.Opportunity__c, Opp_Product__r.Start_Date__c, 
                                            Plan_Site__c, Plan_Site__r.Reference_ID__c,Reference__c,Limit_for_the_product_site__c,Plan_Site__r.WFS_Ref__c, Type__c, Opp_Product__r.Ref__c 
                                            from Opp_Prod_Sites__c where Opp_Live__c = true AND Status__c='Active' and Bank_Account__c != null 
                                            and Mandate__c != null
                                            and Opp_Product__r.Id IN :plans
                                            order by Company__c, Plan_Site__c]){
            
            WFS_Credit_Data__c creditData = new WFS_Credit_Data__c();
            creditData.Plan_Site__c = planSite.id;
            String groupCredit='';
            if(lastAccountProcessed==null || planSite.Company__c != lastAccountProcessed ){
                groupCredit = getGroupCreditXML(planSite);
                lastAccountProcessed= planSite.Company__c ;
             }                              
            creditData.XML__c = groupCredit + getCreditXML(planSite);
            liCreditData.add(creditData);
        }

        insert liCreditData;
    }


    public static String getGroupCreditXML (Opp_Prod_Sites__c prodSite){
        XmlStreamWriter xmlwriter = new XmlStreamWriter();
        xmlwriter.writeStartElement(null, 'CreditLimit', null);
            Map<String, String> mAttibutes;
            // <assetTypes> ...
            addAsserTypesSection(xmlwriter);
            // <calculationType>FIXED</calculationType>
            writeSingleCharacterElement(xmlwriter, 'calculationType', 'FIXED');
            addGroupCreditUtilisationSection(xmlwriter, prodSite);

            // <dealingCompany...
            mAttibutes = new Map<String, String>();
            mAttibutes.put('type', 'DealingCompany');
            mAttibutes.put('reference', 'NEXTGEAR');
            writeSingleCharacterElementWithAttributes(xmlwriter,'dealingCompany', mAttibutes);
            //<endDate>
            writeSingleCharacterElement(xmlwriter, 'endDate', WFS_XML_Utils.dateToString(prodSite.Opp_Product__r.End_Date__c));
            // <facilities>
            addFacilitiesSection(xmlwriter, prodSite);
            
            addGroupReferenceSection(xmlwriter, prodSite);
            xmlwriter.writeEndElement();
    
            return xmlwriter.getXmlString();
    }
    public static String getCreditXML (Opp_Prod_Sites__c prodSite){
        XmlStreamWriter xmlwriter = new XmlStreamWriter();
        xmlwriter.writeStartElement(null, 'CreditLimit', null);
            Map<String, String> mAttibutes;
            // <assetTypes> ...
            addAsserTypesSection(xmlwriter);
            // <calculationType>FIXED</calculationType>
            writeSingleCharacterElement(xmlwriter, 'calculationType', prodSite.Type__c);
            addCreditUtilisationSection(xmlwriter, prodSite);

            // <dealingCompany...
            mAttibutes = new Map<String, String>();
            mAttibutes.put('type', 'DealingCompany');
            mAttibutes.put('reference', 'NEXTGEAR');
            writeSingleCharacterElementWithAttributes(xmlwriter,'dealingCompany', mAttibutes);
            //<endDate>
            writeSingleCharacterElement(xmlwriter, 'endDate', WFS_XML_Utils.dateToString(prodSite.Opp_Product__r.End_Date__c));
            // <facilities>
            addFacilitiesSection(xmlwriter, prodSite);
            
            addReferenceSection(xmlwriter, prodSite);
        xmlwriter.writeEndElement();

        return xmlwriter.getXmlString();
    }

    public static void addAsserTypesSection(XmlStreamWriter xmlwriter){
        xmlwriter.writeStartElement(null, 'assetTypes', null);
            xmlwriter.writeStartElement(null, 'AssetTypeHolder', null);
                writeSingleCharacterElement(xmlwriter, 'assetType', 'CAR');
            xmlwriter.writeEndElement();
            xmlwriter.writeStartElement(null, 'AssetTypeHolder', null);
                writeSingleCharacterElement(xmlwriter, 'assetType', 'LCV');
            xmlwriter.writeEndElement();
        xmlwriter.writeEndElement();
    }
    
    public static void addCreditUtilisationSection(XmlStreamWriter xmlwriter, Opp_Prod_Sites__c prodSite){
        xmlwriter.writeStartElement(null, 'creditUtilisation', null);
            xmlwriter.writeStartElement(null, 'CreditUtilisation', null);
                writeSingleCharacterElement(xmlwriter, 'arrears', '0'); 
                writeSingleCharacterElement(xmlwriter, 'clearing', '0');

                // assume all are Center (based on orginal Java program)    
            
                Map<String, String> mRefAtributes = new Map<String, String>();
                mRefAtributes.put('type', 'CreditLimit');
                mRefAtributes.put('reference', prodSite.Reference__c);
                writeSingleCharacterElementWithAttributes(xmlwriter, 'creditLimit', mRefAtributes);
                writeSingleCharacterElement(xmlwriter, 'creditUtilisationType', 'TOTAL');
                writeSingleCharacterElement(xmlwriter, 'pipeline', '0');
                writeSingleCharacterElement(xmlwriter, 'utilisation', '0');
            xmlwriter.writeEndElement();
        xmlwriter.writeEndElement();
    }
    public static void addGroupCreditUtilisationSection(XmlStreamWriter xmlwriter, Opp_Prod_Sites__c prodSite){
        xmlwriter.writeStartElement(null, 'creditUtilisation', null);
            xmlwriter.writeStartElement(null, 'CreditUtilisation', null);
                writeSingleCharacterElement(xmlwriter, 'arrears', '0'); 
                writeSingleCharacterElement(xmlwriter, 'clearing', '0');

                // assume all are Center (based on orginal Java program)    
            
                Map<String, String> mRefAtributes = new Map<String, String>();
                mRefAtributes.put('type', 'CreditLimit');
                mRefAtributes.put('reference', prodSite.Company__r.WFS_Ref__c + '-NGC001W');
                writeSingleCharacterElementWithAttributes(xmlwriter, 'creditLimit', mRefAtributes);
                writeSingleCharacterElement(xmlwriter, 'creditUtilisationType', 'TOTAL');
                writeSingleCharacterElement(xmlwriter, 'pipeline', '0');
                writeSingleCharacterElement(xmlwriter, 'utilisation', '0');
            xmlwriter.writeEndElement();
        xmlwriter.writeEndElement();
    }

    public static void addFacilitiesSection(XmlStreamWriter xmlwriter, Opp_Prod_Sites__c prodSite){
        xmlwriter.writeStartElement(null, 'facilities', null);
        Map<String, String> mAttibutes = new Map<String, String>();
        mAttibutes.put('type','Facility');
        mAttibutes.put('reference', 'NGC001W');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'ref',mAttibutes );
        xmlwriter.writeEndElement();
    }

    public static void addReferenceSection(XmlStreamWriter xmlwriter, Opp_Prod_Sites__c prodSite){
        Map<String, String> mAttibutes = new Map<String, String>();
        //<limit>
        writeSingleCharacterElement(xmlwriter, 'limit', prodSite.Limit_for_the_product_site__c.setscale(1));
        // <limitType>
        writeSingleCharacterElement(xmlwriter, 'limitType', 'CREDIT_LINE');
        // <name>
        writeSingleCharacterElement(xmlwriter, 'name',prodSite.Reference__c + ' Line');
        // <owner>
        mAttibutes.put('type','Dealer');
        if (String.isBlank(prodSite.Reference__c)){
            mAttibutes.put('reference', WFS_XML_Utils.WFS_NULL);
        }
        else{
            mAttibutes.put('reference', prodSite.Reference__c.substringBefore('-NGC001W'));
        }
        writeSingleCharacterElementWithAttributes(xmlwriter, 'owner', mAttibutes);
        // <reference>
        writeSingleCharacterElement(xmlwriter, 'reference', prodSite.Reference__c);
        // <reviewDate>
        writeSingleCharacterElement(xmlwriter, 'reviewDate', WFS_XML_Utils.WFS_NULL);
        // <startDate>
        writeSingleCharacterElement(xmlwriter, 'startDate', WFS_XML_Utils.dateToString(prodSite.Opp_Product__r.Start_Date__c));
        // <tolerance>
        writeSingleCharacterElement(xmlwriter, 'tolerance', '0');
        // <validForAllSupplierLoans>
        writeSingleCharacterElement(xmlwriter, 'validForAllSupplierLoans', 'true');
    }
    
    public static void addGroupReferenceSection(XmlStreamWriter xmlwriter, Opp_Prod_Sites__c prodSite){
        Map<String, String> mAttibutes = new Map<String, String>();
        //<limit>
        
        writeSingleCharacterElement(xmlwriter, 'limit', prodSite.Opp_Product__r.Limit__c.setscale(1));
        // <limitType>
        writeSingleCharacterElement(xmlwriter, 'limitType', 'CREDIT_LIMIT');
        // <name>
        writeSingleCharacterElement(xmlwriter, 'name', prodSite.Company__r.WFS_Ref__c + '-NGC001W Limit');
        // <owner>
        mAttibutes.put('type','Dealer');
        if (String.isBlank(prodSite.Company__r.WFS_Ref__c)){
            mAttibutes.put('reference', WFS_XML_Utils.WFS_NULL);
        }
        else{
            mAttibutes.put('reference', prodSite.Company__r.WFS_Ref__c);
        }
        writeSingleCharacterElementWithAttributes(xmlwriter, 'owner', mAttibutes);
        // <reference>
        writeSingleCharacterElement(xmlwriter, 'reference', prodSite.Company__r.WFS_Ref__c + '-NGC001W');
        // <reviewDate>
        writeSingleCharacterElement(xmlwriter, 'reviewDate', WFS_XML_Utils.WFS_NULL);
        // <startDate>
        writeSingleCharacterElement(xmlwriter, 'startDate', WFS_XML_Utils.dateToString(prodSite.Opp_Product__r.Start_Date__c));
        // <tolerance>
        writeSingleCharacterElement(xmlwriter, 'tolerance', '0');
        // <validForAllSupplierLoans>
        writeSingleCharacterElement(xmlwriter, 'validForAllSupplierLoans', 'true');
    }

    //Opens and closes an XML tag,  inserts value between tags
    private static void writeSingleCharacterElement(XmlStreamWriter xmlwriter, String tag,  Object tagData){
        WFS_XML_Utils.writeSingleCharacterElement(xmlwriter, tag, tagData);
    }
    
    //Opens and closes an XML tag,  accepts a map of attributes to be used.
    //Does not write anythig other than attributes
    private static void writeSingleCharacterElementWithAttributes(XmlStreamWriter xmlwriter, String tag,  Map<String, String> attributes){
        WFS_XML_Utils.writeSingleCharacterElementWithAttributes(xmlwriter, tag, attributes);
    }

    

    

}