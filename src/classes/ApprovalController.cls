/**
* File Name   :    ApprovalController.cls
* Description :    [NIL-12] - Custom page for supporting approval process
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Ioan Beschea     Initial
* 0.2 01/06/2015 Vincent Spehner  Add Reject method
* 0.3 02/06/2015 Vincent Spehner  Add Escalate method
* 0.4 09/06/2015 Ioan Beschea     Add Refer method
* 0.5 04/01/2017 Simon Wilby      Amend required approver logic UKCRM-213
*/

public class ApprovalController {

    Id currentOpportunityId {get;set;}
    List<Approval_Stage__c> currentApprovals;
    public Opportunity currentOpportunity {get;set;}  
    public Boolean displayBlock {get;set;}
    public String approvalComment {get;set;}
    public Approval_Stage__c currentApproval {get;set;}

    public ApprovalController(ApexPages.StandardController stdController) {
        displayBlock = true;
        currentOpportunityId = (Id) stdController.getId();
        currentOpportunity = [SELECT StageName, Source_Country__c, Previous_RecordType__c, Approval_Level__c, Name, Owner.Name FROM Opportunity WHERE Id = :currentOpportunityId];
        // If the Opportuntiy has Stage = 'Credit Approved', display a message for it
        if (currentOpportunity.StageName == UtilConstants.OPPORTUNITY_STATUS_CREDIT_APPROVED) {
            displayBlock = false;
            ApexPages.Message noMembershipCards = new ApexPages.Message(ApexPages.Severity.INFO, UtilConstants.MESSAGE_CREDIT_APPROVED);
            ApexPages.addMessage(noMembershipCards);
        } else {
            currentApprovals = [SELECT Id, Status__c, Comment__c, Date_submitted__c, Level__c, IsActive__c
                                FROM Approval_Stage__c 
                                WHERE IsActive__c = true
                                AND Opportunity__c = :currentOpportunityId];
            currentApproval = checkAvailableApprovalStage(); 
            // Check to see if the current user is one of the people that have to approved
            if (currentApproval == null){
                displayBlock = false;
                ApexPages.Message noMembershipCards = new ApexPages.Message(ApexPages.Severity.INFO, UtilConstants.USER_IS_NOT_AN_APPROVER);
                ApexPages.addMessage(noMembershipCards);
            } else // If the user took action, display a message informing them about this
                if (currentApproval.Status__c != UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL) { 
                    displayBlock = false;
                    ApexPages.Message noMembershipCards = new ApexPages.Message(ApexPages.Severity.INFO, UtilConstants.ACTION_WAS_TAKEN);
                    ApexPages.addMessage(noMembershipCards);
            }
        }
        
    }

    /** 
    @description            Approve functionality method       
    */
    public PageReference approveOpportunity(){
        
        currentApproval.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_APPROVED;
        currentApproval.Date_decision__c    = Date.today();
        currentApproval.Comment__c          = approvalComment;
        UPDATE currentApproval;
 
        // Set expectations for the number of approvers
        Integer minApprovers                = ApprovalSettings.getApproversMin(currentOpportunity.Source_Country__c, Integer.valueOf(currentApproval.Level__c));
        Integer numberOfApprovers           = 0;
        Integer requiredApprover            = 0;
        List<User> requiredApproversUserList= ApprovalSettings.getRequiredApprovers(currentOpportunity.Source_Country__c, Integer.valueOf(currentOpportunity.Approval_Level__c)); 
        Boolean hasRequiredApprovers        = requiredApproversUserList.size()==0;

        // Re-query the current approval to include last approvals
        currentApprovals = [SELECT Id, Status__c, Comment__c, Date_submitted__c, Level__c, IsActive__c, User__c
                            FROM Approval_Stage__c 
                            WHERE IsActive__c = true
                            AND Opportunity__c = :currentOpportunityId];

    for (Approval_Stage__c ap : currentApprovals) {
            if (ap.Status__c == UtilConstants.APPROVAL_STAGE_STATUS_APPROVED) {
                numberOfApprovers = numberOfApprovers + 1;
                
                // DQ If level 4 then all required approvers have to approve it otherwise only number of required
                 if (ap.Level__c == 4) {
                    for (User u : requiredApproversUserList) {
                        if (ap.User__c == u.Id){
                            requiredApprover = requiredApprover + 1;
                        }
                    }
                }
                else {
                     // also check if this is one of the approver required (OK for other levels than 4)
                    for (User u : requiredApproversUserList) {
                        if (ap.User__c == u.Id){
                            hasRequiredApprovers = true;
                        }
                    }
                }
             }    
        }

        //SW: Added the requiredApprover OR logic
        if (((numberOfApprovers >= minApprovers) && (hasRequiredApprovers == true)) || ((numberOfApprovers >= minApprovers) && (requiredApprover >= requiredApproversUserList.size()))){             
            for (Approval_Stage__c ap : currentApprovals) {
                if (ap.Status__c == UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL) {
                    ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION;
                }
                ap.IsActive__c = false;
            }
            UPDATE currentApprovals; 

            currentOpportunity.recordTypeId = currentOpportunity.Previous_RecordType__c;
            currentOpportunity.Is_Credit_Approved__c = true;
            currentOpportunity.Approval_Received__c = true;
            UPDATE currentOpportunity;
        }
        
        String oppUrl = '/' + + currentOpportunityId;
        PageReference pageRef = new PageReference(oppUrl);
        pageRef.setRedirect(true);
        return pageRef;
    }

    /** 
    @description            Reject functionality method       
    */
    public PageReference rejectOpportunity(){
        currentOpportunity.recordTypeId = currentOpportunity.Previous_RecordType__c; 
        UPDATE currentOpportunity;

        for (Approval_Stage__c ap : currentApprovals) {
            if (ap.Id == currentApproval.Id) {
                ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_REJECTED;
                ap.Date_decision__c = Date.today();
                ap.Comment__c = approvalComment;
            } else if (ap.Status__c == UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL) {
                ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION;
            }
            ap.IsActive__c = false;
        }
        
        UPDATE currentApprovals;        

        currentOpportunity.recordTypeId = currentOpportunity.Previous_RecordType__c;
        currentOpportunity.Is_Credit_Approved__c = false;
        currentOpportunity.Approval_Received__c = false;
        UPDATE currentOpportunity;
 
        String oppUrl = '/' + + currentOpportunityId;
        PageReference pageRef = new PageReference(oppUrl);
        pageRef.setRedirect(true);
        return pageRef;
    }

    /** 
    @description            Escalate functionality method       
    */
    public PageReference escalateOpportunity(){
        //Diactivate all of the existing Approval Stages for all existing records
        for (Approval_Stage__c ap : currentApprovals) {
            if (ap.Id == currentApproval.Id) {
                ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_ESCALATED;
                ap.Date_decision__c = Date.today();
                ap.Comment__c = approvalComment;
            }else if (ap.Status__c == UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL) {
                ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION;
            }
            ap.IsActive__c = false;
        }
        
        
        UPDATE currentApprovals;

        currentOpportunity.Is_Credit_Approved__c = false;
        currentOpportunity.Approval_Received__c = false;
        UPDATE currentOpportunity;
        
        // Retrieve the Opportunity new values especially the new Approval Level
        currentOpportunity = [SELECT StageName, Source_Country__c, Approval_Level__c, Name, Owner.Name FROM Opportunity WHERE Id = :currentOpportunityId];

        // Create the new Approval Stages
        List<User> newApproverUserList = ApprovalSettings.getApprovers(currentOpportunity.Source_Country__c, Integer.valueOf(currentOpportunity.Approval_Level__c)); 
        List<Approval_Stage__c> newApprovalList = new List<Approval_Stage__c> ();
        for (User currentUser :newApproverUserList) {
            Approval_Stage__c currentApproval   = new Approval_Stage__c();
            currentApproval.User__c             = currentUser.Id;
            currentApproval.User_email__c       = currentUser.Email;
            currentApproval.Opportunity__c      = currentOpportunityId;
            currentApproval.Status__c           = UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL;
            currentApproval.Date_submitted__c   = Date.today();
            currentApproval.Level__c            = Integer.valueOf(currentOpportunity.Approval_Level__c);
            newApprovalList.add(currentApproval);
        }
        try {
            INSERT newApprovalList;
        }
        catch(Exception e) {
            
        }
        //Return to the originar Opportunity
        String oppUrl = '/' + + currentOpportunityId;
        PageReference pageRef = new PageReference(oppUrl);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    /** 
    @description            Refer functionality method       
    */
    public PageReference referOpportunity(){

        //Diactivate all of the existing Approval Stages for all existing records
        for (Approval_Stage__c ap : currentApprovals) {
            if (ap.Id == currentApproval.Id) {
                ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_REFERED;
                ap.Date_decision__c = Date.today();
                ap.Comment__c = approvalComment;
            }else if (ap.Status__c == UtilConstants.APPROVAL_STAGE_STATUS_PENDING_APPROVAL) {
                ap.Status__c = UtilConstants.APPROVAL_STAGE_STATUS_NO_ACTION;
            }
            ap.IsActive__c = false;
        }

        UPDATE currentApprovals;
        
        currentOpportunity.recordTypeId = currentOpportunity.Previous_RecordType__c; 
        currentOpportunity.Is_Credit_Approved__c = false;
        currentOpportunity.Approval_Received__c = false;
        UPDATE currentOpportunity;
        //Return to the original Opportunity
        
        String oppUrl = '/' + + currentOpportunityId;
        PageReference pageRef = new PageReference(oppUrl);
        pageRef.setRedirect(true);
        return pageRef;
    
    }

    /** 
    @description            Check if there are any pending approvals for the current Opportunity and current User    
    @returns                ApprovalStage or null
    */
    Approval_Stage__c checkAvailableApprovalStage(){
        Id currentUserId = UserInfo.getUserId();

        List<Approval_Stage__c> currentApproval = [SELECT Id, Status__c, Comment__c, Date_submitted__c, User__c, Level__c, IsActive__c
                                                    FROM Approval_Stage__c 
                                                    WHERE User__c = :currentUserId 
                                                    AND IsActive__c = true
                                                    AND Opportunity__c = :currentOpportunityId];

        if (currentApproval.size() > 0) {
            return currentApproval[0];
        } else {
            return null;
        } 
    }
   
}