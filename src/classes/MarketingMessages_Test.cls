@isTest
private class MarketingMessages_Test {
    
    @isTest static void test_method_controllers() {
        
        
        Account a = TestDataUtilityClass.getAccountRec();
        Marketing_Message__c message = TestDataUtilityClass.getMarketingMessage();

        List<Account> accounts = new List<Account>();
        accounts.add(a);
        
        MessageWrapper w = new MessageWrapper(message);
        w.selected = false;
        List<MessageWrapper> wrappers = new List<MessageWrapper>();
        wrappers.add(w);
        
              
        PageReference pageRef = Page.DisplayMessages;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardSetController setController = new ApexPages.StandardSetController(accounts);
        MarketingMessagesController controller = new MarketingMessagesController(setController);
        
        //test processing messages with no account selected
        controller.processMessages();
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals('Please select at least one account', pageMessages[0].getSummary());
        System.assertEquals(controller.selectedAccounts.size(), 0);
      
        
        //try advance to the next page with no message selected
        controller.selectedAccounts.add(a); 
        controller.messages = wrappers;
        controller.processMessages();
        pageMessages = ApexPages.getMessages();
        System.assertEquals('Please select a message', pageMessages[1].getSummary());
             
        //advance next page with selected message 
        wrappers.clear();
        w.selected = true;
        wrappers.add(w);
        controller.messages = wrappers;
        controller.processMessages();
        System.assertNotEquals(controller.selectedMessage, null);

              
        Lead led = TestDataUtilityClass.getLeadRec() ;
        led.id =null;
        insert led; 
        insert a;      
        Contact c = TestDataUtilityClass.getContactRec();
        c.AccountId = a.Id;
        c.Lead__c = led.id;
        insert c;
        
        List<ContactWrapper> cws = new List<ContactWrapper>();
        ContactWrapper cw = new ContactWrapper(c);
        cws.add(cw);
        
        //process contacts with no contact selected
        controller.contacts = cws;
        controller.processContacts();
        pageMessages = ApexPages.getMessages();
        System.assertEquals('Please select a contact', pageMessages[2].getSummary());
        
        //process contact list
        List<ContactWrapper> cons = controller.contacts;
        System.debug('MR' + cons);

        //process contacts with a contact selected
        cws.clear();
        cw.Selected = true;
        cws.add(cw);
        controller.processContacts();

        pageMessages = ApexPages.getMessages();
        System.assertEquals('1 Dealer messages added', pageMessages[3].getSummary());

        Boolean hasN = controller.hasNext;
        Boolean hasP = controller.hasPrevious;
        Integer pageNumber = controller.pageNumber;
        controller.first();
        controller.last();
                
        
      
        Dealer_Message__c d = TestDataUtilityClass.getDealerMessage(message);
        List<Dealer_Message__c> dMessages = new List<Dealer_Message__c>();
        dMessages.add(d);
        
      
        //test recall messages
        ApexPages.StandardController stdController = new ApexPages.StandardController(message);
        MessageRecallController recallController = new MessageRecallController(stdController);
        recallController.dealerMessages = dMessages;
        recallController.recallMessages();

        pageMessages = ApexPages.getMessages();
        System.assertEquals('Removed ranking successfully from Messages.', pageMessages[4].getSummary());
   
    }


    @isTest static void test_method_utilityclasses() {

        Marketing_Message__c message = TestDataUtilityClass.getMarketingMessage();
        Dealer_Message__c d = TestDataUtilityClass.getDealerMessage(message);

        //test rules engine
        List<Dealer_Message__c> dMessages = new List<Dealer_Message__c>();
        dMessages.add(d);
        
        ApexPages.StandardController stdController1 = new ApexPages.StandardController(d);
        DealerMessageController dController = new DealerMessageController(stdController1);
        dController.runRulesEngine();

        System.assertNotEquals(d.Rank__c, null);

        d.Link_1_Clicked_Date__c = DateTime.now();
        update d;

        dController.runRulesEngine();

        d = [Select Id, Rank__c from Dealer_Message__c where Id = :d.Id][0];
        System.assertEquals(d.Rank__c, null);
        


        //test batch
        d.Rank__c = 1;
        d.Link_1_Clicked_Date__c  = null;
        d.End_Date__c = DateTime.now().addDays(-1);
        update d;

        MarketingMessageExpiry expiry = new MarketingMessageExpiry();
        SchedulableContext sc = null;
        expiry.execute(sc);
        
        d = [Select Id, Rank__c from Dealer_Message__c where Id = :d.Id][0];
        System.assertEquals(d.Rank__c, null);



    }
    
    @isTest static void test_method_triggers() {
        

        //todo bulk test

         //test going on hold  
        Account a = TestDataUtilityClass.getAccountRec();
        insert a;
        
        Opp_Prod_Sites__c site = TestDataUtilityClass.createPlanSite(a, true);
            
        Marketing_Message__c message = TestDataUtilityClass.getMarketingMessage(); 
        Dealer_Message__c d = TestDataUtilityClass.getDealerMessage(message);
        Lead led = TestDataUtilityClass.getLeadRec() ;
        led.id=null;
        Insert led;
        Contact c = [Select Id, AccountId, (SELECT Id FROM Dealer_Messages__r) from Contact where Id = :d.Contact__c][0];
        c.AccountId = a.Id;
        c.Lead__c = led.id;
        update c;

        TestDataUtilityClass.createOnHoldMessage();

        
        System.assertNotEquals(c.Dealer_Messages__r, null);

        site.Credit_Line_Status__c = 'On-Stop';
        update site;

        boolean onHoldFound = false;
        boolean prioritySet = false;
        c = [Select Id, AccountId, (SELECT Rank__c, Marketing_Message__r.On_Hold_Applied__c FROM Dealer_Messages__r) from Contact where Id = :c.Id][0];
        for (Dealer_Message__c dm : c.Dealer_Messages__r) {
            if (dm.Marketing_Message__r.On_Hold_Applied__c == true) {
                onHoldFound = true;
            }
            else if (dm.Rank__c == null) {
                prioritySet = true;
            } 
        }


        System.assertEquals(onHoldFound, true);
        System.assertEquals(prioritySet, true);


        //test contact opt out      
        c.Contact_Marketing_Opt_Out__c = 'StockMaster Messages';
        update c; 

        c = [Select Id, (SELECT Id FROM Dealer_Messages__r) from Contact where Id = :c.Id][0];

        System.assertEquals(c.Dealer_Messages__r.size(), 0);
     
        //todo add account opt out

               
        
        
    } 
    /*
    
    Case:0007510 :11 Dec 2014 Pawan Tokas (Pav)
    Requirement Changed. When a message is recalled we are not going to delete dealer message related to 
    it any more. Instead we are going to remove the ranking so that the dealer message is not displayed to the contact.
    Also the dealer messages linked to a batch number will be un ranked instead of all historical.
    */
    
    @isTest static void testUnRanking()
    {
        
       Account a = TestDataUtilityClass.getAccountRec();
       insert a;

       Marketing_Message__c selectedMessage = new Marketing_Message__c();
       selectedMessage.Active__c = true;
       selectedMessage.Closed_Retry_Limit__c = 10;
       selectedMessage.Expiry_Days__c = 5;
       selectedMessage.Minimum_Account_Open_Duration__c = 10;
       selectedMessage.Text__c = 'Test text';
       selectedMessage.Retry_Limit__c = 10;
       selectedMessage.Priority__c = 5;

       insert selectedMessage;
       DealerMessageBatch__c batch = new DealerMessageBatch__c();
       insert batch;
        
        Contact c = new Contact();
        c.Account = a;
          c.AccountId = a.id;
        c.Phone = '123';
        c.LastName = 'Smith';
        insert c;
            
            Dealer_Message__c message = new Dealer_Message__c();
            message.DealerMessageBatch__c = batch.id;
            message.Contact__c = c.Id;
            message.Start_Date__c = Datetime.now();
            message.End_Date__c = Datetime.now().addDays((Integer)selectedMessage.Expiry_Days__c);
            message.Marketing_Message__c = selectedMessage.Id;
            message.Retries_Remaining__c = selectedMessage.Retry_LImit__c;
            message.Text__c = 'ABC';
            message.Asset_Id__c = selectedMessage.Asset_Id__c;
            message.Closed_Retries_Remaining__c = selectedMessage.Closed_Retry_Limit__c;
            message.Tasks_Required__c = selectedMessage.Tasks_Required__c;
            message.Rank__c = selectedMessage.Priority__c;
            insert message;
            list<Dealer_Message__c> dslist= new list<Dealer_Message__c>();
            dslist.add(message);
            MessageHandler mh= new MessageHandler();
          ApexPages.Message apm= mh.removeRankingFromDealerMessages(dslist);
            system.assertEquals(message.Rank__c, null);
            
            
            
    }

    @isTest static void test_method_createDealerMessage() {


        Account a = TestDataUtilityClass.getAccountRec();
        a.Account_Live_Date__c =  System.now().addDays(-50);
        System.debug('TQ' + a.Account_Live_Date__c +
                      System.now()  
                    );
        insert a;
        Contact testConRec = new Contact(
            Account = a,
            FirstName = 'Test First Name',
            LastName = 'Test Last Name',
            Phone = '123456'
        );
        Marketing_Message__c message = TestDataUtilityClass.getMarketingMessage();

        List<Account> accounts = new List<Account>();
        accounts.add(a);
        
        MessageWrapper w = new MessageWrapper(message);
        w.selected = true;
        List<MessageWrapper> wrappers = new List<MessageWrapper>();
        wrappers.add(w);

        ApexPages.StandardSetController setController = new ApexPages.StandardSetController(accounts);
        MarketingMessagesController controller = new MarketingMessagesController(setController);
             
        //advance next page with selected message 
        wrappers.clear();
        w.selected = true;
        wrappers.add(w);
        controller.messages = wrappers;
        controller.processMessages();
        System.assertNotEquals(controller.selectedMessage, null);

        PageReference pageRef = Page.CreateDealerMessages;
        Test.setCurrentPage(pageRef);

        controller.selectedMessage = w;
        controller.selectedAccounts = accounts;

        List<ContactWrapper> lcon = controller.contacts;
        System.debug(lcon);

    }
            
}