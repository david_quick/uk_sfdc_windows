/**
*
* Author:       Piotr Czechumski
* Date:         30/01/2015
* Description:  Unit test for:
*               - WFS_Credit_GenerateData_Batch
*               - WFS_Credit_Delete_Batch
*
**/
@isTest (seeAllData=false)
private class WFS_Credit_GenerateData_BatchTest
{
    @isTest
    static void shouldCreateDataForCreaditFile()
    {
        // Given
        WFS_TestData.createWFSTestData();
        // Note test result depends on created data - loop size = 2 - so there will be start, end records + 2 with actual data

        // When
        Test.startTest();           
            WFS_Credit_GenerateData_Batch batch2 = new WFS_Credit_GenerateData_Batch();
            Database.executeBatch (batch2);
        Test.stopTest();

        // Then
        List<WFS_Credit_Data__c> liCredit = [select id, XML__c, name from WFS_Credit_Data__c order by name];

        System.assertEquals(4, liCredit.size(), '3 records should be created');
        System.assertEquals('<creditData>', liCredit[0].XML__c, 'Start tag expected');
        System.assert(liCredit[1].XML__c.startsWith('<CreditLimit>'), 'Should get CreditLimit');
        System.assert(liCredit[2].XML__c.startsWith('<CreditLimit>'), 'Should get CreditLimit');
        System.assertEquals('</creditData>', liCredit[3].XML__c, 'End tag expected');

    }

    @isTest
    static void shouldDeleteExistingCreditData()
    {
        // Given
        String CRON_EXP = '0 0 0 10 2 ? ' + Date.today().addYears(5).year();
        String strExpectedRunDate = Date.today().addYears(5).year() + '-02-10 00:00:00';
        insert new List<WFS_Credit_Data__c>{new WFS_Credit_Data__c(XML__c='<creditData>'),new WFS_Credit_Data__c(XML__c='<creditData>')};
        
        // When
        Test.startTest();
        // Schedule the test job
            string jobId = WFS_Credit_Delete_Batch.scheduleJob(CRON_EXP);
             
            // Get the information from the CronTrigger API object
            CronTrigger ct = [select Id, CronExpression, TimesTriggered, NextFireTime
                                from CronTrigger 
                                where id = :jobId];

            // Verify the expressions are the same
            system.assertEquals(CRON_EXP, ct.CronExpression);
            
            // Verify the next time the job will run
            system.assertEquals(strExpectedRunDate, String.valueOf(ct.NextFireTime));
          
            // Verify the scheduled job hasn't run yet.
            List<WFS_Credit_Data__c> liCredit = [select id, XML__c, name from WFS_Credit_Data__c order by name];        
            System.assertEquals(2, liCredit.size(), 'All records should not yet be deleted');
            
            //In theory,  the batch should run when Test.stopTest() is called,  that does not seem to happen though,  so,  call the ad hoc start
            WFS_Credit_Delete_Batch batch = new WFS_Credit_Delete_Batch();
            Database.executeBatch (batch);
        
        Test.stopTest();

        // Then
        liCredit = [select id, XML__c, name from WFS_Credit_Data__c order by name];
        System.assertEquals(0, liCredit.size(), 'All records should be deleted');
    }

    @isTest
    static void shouldThrowExceptionOnExitsingRecords()
    {
        // Given
        insert new List<WFS_Credit_Data__c>{new WFS_Credit_Data__c(XML__c='<creditData>'),new WFS_Credit_Data__c(XML__c='<creditData>')};

        // When
        try{
            Test.startTest();               
                WFS_Credit_GenerateData_Batch batch2 = new WFS_Credit_GenerateData_Batch();
                Database.executeBatch (batch2);             
            Test.stopTest();
            System.assert(false, 'No exception was thrown');
        }
        catch (WFS_Credit_GenerateData_Batch.Existing_CreditData_Exception ex){
            System.debug('Ok - exception thrown');
        }

        // Then
        
    }


}