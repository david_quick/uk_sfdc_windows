/*
    Write a series of XML chunks and save each as a WFS_Organisation_Data__c record
    Each record can be traced back to the constituent data by AccountId__c and ParentAccountId__c
    When all are returned as a dataset ordered by Name,  they form a complete Organisation file to be consumed by WFS
*/
public class WFS_Organisation_XML {
    private static final string WFS_NULL = '(null)';
    
    private static list<WFS_Organisation_Data__c> xmlOutput;


    public static void writeOpeningTags(){
        xmlOutput = new list<WFS_Organisation_Data__c>();
        
        addToDataset('<organisationData>');
        addToDataset('<organisationEntities>');
        
        writeToDB();
    }

    public static void writeClosingTags(){
        xmlOutput = new list<WFS_Organisation_Data__c>();
        
        addToDataset('</organisationEntities>');
        addToDataset('</organisationData>');
        
        writeToDB();
    }
    
    public static void processDealers(list<id> companies){
        id lastAccountProcessed;
        
        //These are all used to populate the OrganisationGroup data
        id companyId;
        string companyName;
        string companyWFS_Ref;
        list<string> centreWFS_Ref;
        
        xmlOutput = new list<WFS_Organisation_Data__c>();
        
        /*  GOOD TO KNOW!
            
            Company__c:  parent account/dealer
            Plan_Site__c:  child account/dealer
            
            Plan Site (Opp_Prod_Sites__c):  one per child account,  holds references to both the parent account and the child account
        */
        
        for(Opp_Prod_Sites__c planSite : [select Credit_Available__c, Credit_Line_Status__c, Id,  Limit_for_the_product_site__c, Name, Opp_Live__c, Opp_Product__c, Reference__c, Status__c, Type__c,
                                            Company__c, Company__r.Name, Company__r.WFS_Ref__c, Company__r.Business_Address_Line_1__c, Company__r.Company_Reg_No__c, Company__r.Business_Address_Line_2__c, Company__r.Business_Address_Line_3__c, Company__r.Business_Address_County__c, Company__r.Business_Address_Postcode__c, Company__r.Business_Address_Town_City__c, Company__r.VAT_Registration_Number__c,
                                            Plan_Site__c, Plan_Site__r.Name, Plan_Site__r.WFS_Ref__c, Plan_Site__r.Business_Address_Line_1__c, Plan_Site__r.Company_Reg_No__c, Plan_Site__r.Business_Address_Line_2__c, Plan_Site__r.Business_Address_Line_3__c, Plan_Site__r.Business_Address_County__c, Plan_Site__r.Business_Address_Postcode__c, Plan_Site__r.Business_Address_Town_City__c, Plan_Site__r.VAT_Registration_Number__c, Plan_Site__r.Trading_As__c,
                                            Mandate__c, Mandate__r.Name, Mandate__r.CreatedDate,
                                            Bank_Account__c, Bank_Account__r.Primary_Account__c, Bank_Account__r.Account_Number__c, Bank_Account__r.Sort_Code__c, Bank_Account__r.Name
                                            from Opp_Prod_Sites__c
                                            where Opp_Live__c = true AND Status__c='Active' and Bank_Account__c != null and Mandate__c != null
                                            and Company__c in :companies
                                            order by Company__c, Plan_Site__c]){
            
            if(planSite.Company__c != lastAccountProcessed){
                //We've moved to a different company,  so,  output the Organisation Group data
                //If this is the first time through the loop,  the value is null
                if(lastAccountProcessed != null){
                    generateGroupXML(companyId, companyName, companyWFS_Ref, centreWFS_Ref);
                }
                
                lastAccountProcessed = planSite.Company__c;
                generateCompanyXML(planSite);
                
                //Store company data for use in the Organisation Group
                companyId = planSite.Company__c;
                companyName = planSite.Company__r.Name;
                companyWFS_Ref = planSite.Company__r.WFS_Ref__c;
                
                //Initialise the centre WFS storage
                centreWFS_Ref = new list<string>();
            }
            
            generateCentreXML(planSite);
            
            //Store centre data for use in the Organisation Group
            centreWFS_Ref.add(planSite.Plan_Site__r.WFS_Ref__c);
        }
        
        //Write the final Organisation Group data
        generateGroupXML(companyId, companyName, companyWFS_Ref, centreWFS_Ref);
        
        writeToDB();
    }

    private static void generateCompanyXML(Opp_Prod_Sites__c company){
        XmlStreamWriter xmlwriter = new XmlStreamWriter();
        
        xmlwriter.writeStartElement(null, 'Dealer', null);
        writeSingleCharacterElement(xmlwriter, 'arrearsChargingStatus', 'ON');
        writeSingleCharacterElement(xmlwriter, 'auditFrequency', '30');
        writeSingleCharacterElement(xmlwriter, 'auditRegion', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'baseRateJurisdiction', new map<string, string>{'type'=>'RateJurisdiction','reference'=>'NEXTGEAR_BANK'});
        writeSingleCharacterElement(xmlwriter, 'branding', 'NEXTGEAR');
        writeSingleCharacterElement(xmlwriter, 'businessCommencementDate', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'chargesCalledDay', '0');
        writeSingleCharacterElement(xmlwriter, 'checkData', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'childAllowed', 'true');
        writeSingleCharacterElement(xmlwriter, 'companyName', company.Company__r.Name);
        writeSingleCharacterElement(xmlwriter, 'companyRegistrationNumber', company.Company__r.Company_Reg_No__c);
        writeSingleCharacterElement(xmlwriter, 'companyVATNumber', company.Company__r.VAT_Registration_Number__c);
        
        xmlwriter.writeStartElement(null, 'contacts', null);
        xmlwriter.writeStartElement(null, 'Contact', null);
        
        xmlwriter.writeStartElement(null, 'address', null);
        xmlwriter.writeStartElement(null, 'Address', null);
        
        writeSingleCharacterElement(xmlwriter, 'addressLine1', WFS_XML_Utils.blankToWFS_NULL(company.Company__r.Business_Address_Line_1__c));
        
        //2nd line of the address is made up of company.Company__r.Business_Address_Line_2__c and company.Company__r.Business_Address_Line_3__c
        //They only appear in the address if they're not blank
        string addressLine2;
        if(!string.isBlank(company.Company__r.Business_Address_Line_2__c) && !string.isBlank(company.Company__r.Business_Address_Line_3__c)){
            addressLine2 = company.Company__r.Business_Address_Line_2__c + ' ' +  company.Company__r.Business_Address_Line_3__c;
        }
        else if(!string.isBlank(company.Company__r.Business_Address_Line_2__c)){
            addressLine2 = company.Company__r.Business_Address_Line_2__c;
        }
        else if(!string.isBlank(company.Company__r.Business_Address_Line_3__c)){
            addressLine2 = company.Company__r.Business_Address_Line_3__c;
        }
        else{
            addressLine2 = WFS_NULL;
        }
        writeSingleCharacterElement(xmlwriter, 'addressLine2', addressLine2);
        
        writeSingleCharacterElement(xmlwriter, 'addressLine3', WFS_XML_Utils.blankToWFS_NULL(company.Company__r.Business_Address_Line_3__c));
        writeSingleCharacterElement(xmlwriter, 'addressLine4', WFS_XML_Utils.blankToWFS_NULL(company.Company__r.Business_Address_Town_City__c));
        writeSingleCharacterElement(xmlwriter, 'addressType', 'MAIN');
        writeSingleCharacterElement(xmlwriter, 'country', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'defaultLocation', 'true');
        writeSingleCharacterElement(xmlwriter, 'entityCreationSource', 'ONLINE');
        writeSingleCharacterElement(xmlwriter, 'locationName', '');
        writeSingleCharacterElement(xmlwriter, 'postcode', WFS_XML_Utils.blankToWFS_NULL(company.Company__r.Business_Address_Postcode__c));
        writeSingleCharacterElement(xmlwriter, 'reference', company.Company__r.WFS_Ref__c + '-L1');
        
        xmlwriter.writeEndElement();//Address
        xmlwriter.writeEndElement();//address
        
        writeSingleCharacterElement(xmlwriter, 'contactType', 'MAIN');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'email', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'entityCreationSource', 'ONLINE');
        writeSingleCharacterElement(xmlwriter, 'faxNo', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'firstName', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'invoiceAddress', 'true');
        writeSingleCharacterElement(xmlwriter, 'mobileNo', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'position', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'preferedMethodOfContact', 'TELEPHONE');
        writeSingleCharacterElement(xmlwriter, 'reference', company.Company__r.WFS_Ref__c + '-L1');
        writeSingleCharacterElement(xmlwriter, 'surname', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'telephoneNo', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'title', WFS_NULL);
        
        xmlwriter.writeEndElement();//Contact
        xmlwriter.writeEndElement();//contacts
        
        writeSingleCharacterElement(xmlwriter, 'costCentreCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'creditLinesNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'creditStatus', 'OK');
        writeSingleCharacterElement(xmlwriter, 'currency', 'GBP');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'dealerType', 'NORMAL');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'dealingCompany', new map<string, string>{'type'=>'DealingCompany','reference'=>'NEXTGEAR'});
        writeSingleCharacterElement(xmlwriter, 'documentDeliveryMethod', 'SELF_SERVICE');
        writeSingleCharacterElement(xmlwriter, 'documentPassword', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'entityCreationSource', 'ONLINE');
        writeSingleCharacterElement(xmlwriter, 'graceDays', '0');
        writeSingleCharacterElement(xmlwriter, 'groupName', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'insuranceNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'invoiceLanguage', 'en_GB');
        writeSingleCharacterElement(xmlwriter, 'invoicingNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'nextInvoiceNumber', '1');
        writeSingleCharacterElement(xmlwriter, 'nominatedParentRateReductions', 'false');
        writeSingleCharacterElement(xmlwriter, 'nonAccrual', 'NO');
        writeSingleCharacterElement(xmlwriter, 'onStopReason', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'organisationType', 'DEALER');
        writeSingleCharacterElement(xmlwriter, 'organisationWatchComment', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'parentAllowed', 'true');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'parentOrganisation', new map<string, string>{'type'=>'DealingCompany','reference'=>'NEXTGEAR'});
        
        xmlwriter.writeStartElement(null, 'paymentSettings', null);
        xmlwriter.writeStartElement(null, 'PaymentSettings', null);
        
        writeSingleCharacterElement(xmlwriter, 'creditReleaseNew', 'MANUAL');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        
        xmlwriter.writeStartElement(null, 'paymentSources', null);
        xmlwriter.writeStartElement(null, 'PaymentSource', null);
        
        writeSingleCharacterElement(xmlwriter, 'accountReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'currency', 'GBP');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'dcAccountReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'debitCreditType', 'DEBIT');
        writeSingleCharacterElement(xmlwriter, 'facilityReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'ftValueType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'itemCategory', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'mandateReference', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'organisation', new map<string, string>{'type'=>'Dealer','reference'=>company.Company__r.WFS_Ref__c});
        writeSingleCharacterElement(xmlwriter, 'paymentApproval', 'NO');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'paymentMethod', new map<string, string>{'debitCreditType'=>'DEBIT','type'=>'PaymentMethod','reference'=>'BANK_TRANSFER'});
        writeSingleCharacterElement(xmlwriter, 'paymentSourceTimingType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'paymentSourceType', 'ORGANISATION_DEFAULT');
        writeSingleCharacterElement(xmlwriter, 'pppLevelType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'pppReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'reference', 'ALL | DEBIT | BANK_TRANSFER');
        writeSingleCharacterElement(xmlwriter, 'tableReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'transactionType', WFS_NULL);
        
        xmlwriter.writeEndElement();//PaymentSource
        
        xmlwriter.writeStartElement(null, 'PaymentSource', null);
        
        writeSingleCharacterElement(xmlwriter, 'accountReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'currency', 'GBP');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'dcAccountReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'debitCreditType', 'CREDIT');
        writeSingleCharacterElement(xmlwriter, 'facilityReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'ftValueType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'itemCategory', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'mandateReference', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'organisation', new map<string, string>{'type'=>'Dealer','reference'=>company.Company__r.WFS_Ref__c});
        writeSingleCharacterElement(xmlwriter, 'paymentApproval', 'NO');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'paymentMethod', new map<string, string>{'debitCreditType'=>'CREDIT','type'=>'PaymentMethod','reference'=>'BANK_TRANSFER'});
        writeSingleCharacterElement(xmlwriter, 'paymentSourceTimingType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'paymentSourceType', 'ORGANISATION_DEFAULT');
        writeSingleCharacterElement(xmlwriter, 'pppLevelType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'pppReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'reference', 'ALL | CREDIT | BANK_TRANSFER');
        writeSingleCharacterElement(xmlwriter, 'tableReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'transactionType', WFS_NULL);
        
        xmlwriter.writeEndElement();//PaymentSource
        xmlwriter.writeEndElement();//paymentSources
        
        writeSingleCharacterElement(xmlwriter, 'rollUp', 'YES');
        
        xmlwriter.writeEndElement();//PaymentSettings
        xmlwriter.writeEndElement();//paymentSettings
        
        writeSingleCharacterElement(xmlwriter, 'reference', company.Company__r.WFS_Ref__c);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'region', new map<string, string>{'province'=>'UK','type'=>'Region','country'=>'UK'});
        writeSingleCharacterElement(xmlwriter, 'relationshipIndicator', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'requiresSettlementConfirmation', 'false');
        writeSingleCharacterElement(xmlwriter, 'retailCrossCheck', 'NO');
        writeSingleCharacterElement(xmlwriter, 'riskClassificationOne', 'RISKCLASS_1');
        writeSingleCharacterElement(xmlwriter, 'riskClassificationTwo', 'RISKCLASS_1');
        writeSingleCharacterElement(xmlwriter, 'riskRating', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'salesAreaCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'salesRegionCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'salesTaxExempt', 'NO');
        writeSingleCharacterElement(xmlwriter, 'shortName', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'taxRateJurisdiction', new map<string, string>{'type'=>'RateJurisdiction','reference'=>'NEXTGEAR_TAX'});
        writeSingleCharacterElement(xmlwriter, 'tradingName', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'transferRules', 'INANDOUT');
        writeSingleCharacterElement(xmlwriter, 'uccNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'userAccessStatus', 'ENABLED');
        
        xmlwriter.writeEndElement();//Dealer
        
        addToDataset(company.Company__c, company.Company__c, xmlwriter.getXmlString());
        
    }
    
    private static void generateCentreXML(Opp_Prod_Sites__c centre){
        XmlStreamWriter xmlwriter = new XmlStreamWriter();
        
        xmlwriter.writeStartElement(null, 'Dealer', null);
        writeSingleCharacterElement(xmlwriter, 'arrearsChargingStatus', 'ON');
        writeSingleCharacterElement(xmlwriter, 'auditFrequency', '30');
        writeSingleCharacterElement(xmlwriter, 'auditRegion', WFS_NULL);
        
        xmlwriter.writeStartElement(null, 'bankAccounts', null);
        xmlwriter.writeStartElement(null, 'BankAccount', null);

        writeSingleCharacterElement(xmlwriter, 'accountName', centre.Plan_Site__r.WFS_Ref__c + '-BANK');
        writeSingleCharacterElement(xmlwriter, 'accountNoValidated', 'VALIDATION_NOT_REQUIRED');
        writeSingleCharacterElement(xmlwriter, 'accountNumber', WFS_XML_Utils.blankToWFS_NULL(centre.Bank_Account__r.Account_Number__c));
        writeSingleCharacterElement(xmlwriter, 'bankAccountType', 'NORMAL');
        writeSingleCharacterElement(xmlwriter, 'bankSortCode', WFS_XML_Utils.blankToWFS_NULL(centre.Bank_Account__r.Sort_Code__c));
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'locale', 'en_GB');
        
        xmlwriter.writeStartElement(null, 'mandates', null);
        xmlwriter.writeStartElement(null, 'Mandate', null);
        
        writeSingleCharacterElement(xmlwriter, 'amended', 'false');
        writeSingleCharacterElement(xmlwriter, 'amendmentReason', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'contractId', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'creationDate', centre.Mandate__r.CreatedDate.format('yyyyMMdd'));
        writeSingleCharacterElement(xmlwriter, 'creditorName', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'creditorReferenceParty', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'creditorReferencePartyId', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'debtorIdentificationCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'debtorReferenceParty', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'debtorReferencePartyId', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'delayDays', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'effectiveDate', centre.Mandate__r.CreatedDate.format('yyyyMMdd'));
        writeSingleCharacterElement(xmlwriter, 'lastDataSent', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'organisation', new map<string, string>{'type'=>'Dealer','reference'=>centre.Plan_Site__r.WFS_Ref__c});
        writeSingleCharacterElement(xmlwriter, 'paymentSchema', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'processingReference', '283253');
        writeSingleCharacterElement(xmlwriter, 'reference', centre.Mandate__r.Name);
        writeSingleCharacterElement(xmlwriter, 'signature', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'signingDate', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'status', 'PENDING');
        writeSingleCharacterElement(xmlwriter, 'transactionType', 'RECURRING');
        writeSingleCharacterElement(xmlwriter, 'type', 'AUDDIS');
        
        xmlwriter.writeEndElement();//Mandate
        xmlwriter.writeEndElement();//mandates
        
        writeSingleCharacterElement(xmlwriter, 'primaryAccount', centre.Bank_Account__r.Primary_Account__c);
        writeSingleCharacterElement(xmlwriter, 'reference', centre.Plan_Site__r.WFS_Ref__c + '-' + centre.Mandate__r.Name);
        writeSingleCharacterElement(xmlwriter, 'sortCodeValidated', 'VALIDATION_NOT_REQUIRED');
        
        xmlwriter.writeEndElement();//BankAccount
        xmlwriter.writeEndElement();//bankAccounts
        
        writeSingleCharacterElementWithAttributes(xmlwriter, 'baseRateJurisdiction', new map<string, string>{'type'=>'RateJurisdiction','reference'=>'NEXTGEAR_BANK'});
        writeSingleCharacterElement(xmlwriter, 'branding', 'NEXTGEAR');
        writeSingleCharacterElement(xmlwriter, 'businessCommencementDate', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'chargesCalledDay', '0');
        writeSingleCharacterElement(xmlwriter, 'checkData', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'childAllowed', 'true');
        writeSingleCharacterElement(xmlwriter, 'companyName', centre.Company__r.Name);
        writeSingleCharacterElement(xmlwriter, 'companyRegistrationNumber', centre.Plan_Site__r.Company_Reg_No__c);
        writeSingleCharacterElement(xmlwriter, 'companyVATNumber', centre.Company__r.VAT_Registration_Number__c);
        
        xmlwriter.writeStartElement(null, 'contacts', null);
        xmlwriter.writeStartElement(null, 'Contact', null);
        
        xmlwriter.writeStartElement(null, 'address', null);
        xmlwriter.writeStartElement(null, 'Address', null);
        
        writeSingleCharacterElement(xmlwriter, 'addressLine1', WFS_XML_Utils.blankToWFS_NULL(centre.Plan_Site__r.Business_Address_Line_1__c));
        
        //2nd line of the address is made up of centre.Plan_Site__r.Business_Address_Line_2__c and centre.Plan_Site__r.Business_Address_Line_3__c
        //They only appear in the address if they're not blank
        string addressLine2;
        if(!string.isBlank(centre.Plan_Site__r.Business_Address_Line_2__c) && !string.isBlank(centre.Plan_Site__r.Business_Address_Line_3__c)){
            addressLine2 = centre.Plan_Site__r.Business_Address_Line_2__c + ' ' +  centre.Plan_Site__r.Business_Address_Line_3__c;
        }
        else if(!string.isBlank(centre.Plan_Site__r.Business_Address_Line_2__c)){
            addressLine2 = centre.Plan_Site__r.Business_Address_Line_2__c;
        }
        else if(!string.isBlank(centre.Plan_Site__r.Business_Address_Line_3__c)){
            addressLine2 = centre.Plan_Site__r.Business_Address_Line_3__c;
        }
        else{
            addressLine2 = WFS_NULL;
        }
        writeSingleCharacterElement(xmlwriter, 'addressLine2', addressLine2);
        
        writeSingleCharacterElement(xmlwriter, 'addressLine3', WFS_XML_Utils.blankToWFS_NULL(centre.Plan_Site__r.Business_Address_Line_3__c));
        writeSingleCharacterElement(xmlwriter, 'addressLine4', WFS_XML_Utils.blankToWFS_NULL(centre.Plan_Site__r.Business_Address_Town_City__c));
        writeSingleCharacterElement(xmlwriter, 'addressType', 'MAIN');
        writeSingleCharacterElement(xmlwriter, 'country', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'defaultLocation', 'true');
        writeSingleCharacterElement(xmlwriter, 'entityCreationSource', 'ONLINE');
        writeSingleCharacterElement(xmlwriter, 'locationName', '');
        writeSingleCharacterElement(xmlwriter, 'postcode', WFS_XML_Utils.blankToWFS_NULL(centre.Plan_Site__r.Business_Address_Postcode__c));
        writeSingleCharacterElement(xmlwriter, 'reference', centre.Plan_Site__r.WFS_Ref__c + '-L1');
        
        xmlwriter.writeEndElement();//Address
        xmlwriter.writeEndElement();//address
        
        writeSingleCharacterElement(xmlwriter, 'contactType', 'MAIN');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'email', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'entityCreationSource', 'ONLINE');
        writeSingleCharacterElement(xmlwriter, 'faxNo', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'firstName', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'invoiceAddress', 'true');
        writeSingleCharacterElement(xmlwriter, 'mobileNo', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'position', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'preferedMethodOfContact', 'TELEPHONE');
        writeSingleCharacterElement(xmlwriter, 'reference', centre.Plan_Site__r.WFS_Ref__c + '-L1');
        writeSingleCharacterElement(xmlwriter, 'surname', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'telephoneNo', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'title', WFS_NULL);
        
        xmlwriter.writeEndElement();//Contact
        xmlwriter.writeEndElement();//contacts
        
        writeSingleCharacterElement(xmlwriter, 'costCentreCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'creditLinesNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'creditStatus', 'OK');
        writeSingleCharacterElement(xmlwriter, 'currency', 'GBP');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'dealerType', 'NORMAL');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'dealingCompany', new map<string, string>{'type'=>'DealingCompany','reference'=>'NEXTGEAR'});
        writeSingleCharacterElement(xmlwriter, 'documentDeliveryMethod', 'SELF_SERVICE');
        writeSingleCharacterElement(xmlwriter, 'documentPassword', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'entityCreationSource', 'ONLINE');
        writeSingleCharacterElement(xmlwriter, 'graceDays', '0');
        writeSingleCharacterElement(xmlwriter, 'groupName', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'insuranceNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'invoiceLanguage', 'en_GB');
        writeSingleCharacterElement(xmlwriter, 'invoicingNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'nextInvoiceNumber', '1');
        writeSingleCharacterElement(xmlwriter, 'nominatedParentRateReductions', 'false');
        writeSingleCharacterElement(xmlwriter, 'nonAccrual', 'NO');
        writeSingleCharacterElement(xmlwriter, 'onStopReason', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'organisationType', 'DEALER');
        writeSingleCharacterElement(xmlwriter, 'organisationWatchComment', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'parentAllowed', 'true');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'parentOrganisation', new map<string, string>{'type'=>'Dealer','reference'=>centre.Company__r.WFS_Ref__c});
        
        xmlwriter.writeStartElement(null, 'paymentSettings', null);
        xmlwriter.writeStartElement(null, 'PaymentSettings', null);
        
        writeSingleCharacterElement(xmlwriter, 'creditReleaseNew', 'MANUAL');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        
        xmlwriter.writeStartElement(null, 'paymentSources', null);
        xmlwriter.writeStartElement(null, 'PaymentSource', null);
        
        writeSingleCharacterElement(xmlwriter, 'accountReference', centre.Plan_Site__r.WFS_Ref__c + '-' + centre.Mandate__r.Name);
        writeSingleCharacterElement(xmlwriter, 'currency', 'GBP');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'dcAccountReference', 'NGC_WHOLESALE');
        writeSingleCharacterElement(xmlwriter, 'debitCreditType', 'DEBIT');
        writeSingleCharacterElement(xmlwriter, 'facilityReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'ftValueType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'itemCategory', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'mandateReference', centre.Mandate__r.Name);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'organisation', new map<string, string>{'type'=>'Dealer','reference'=>centre.Plan_Site__r.WFS_Ref__c});
        writeSingleCharacterElement(xmlwriter, 'paymentApproval', 'NO');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'paymentMethod', new map<string, string>{'debitCreditType'=>'DEBIT','type'=>'PaymentMethod','reference'=>'DIRECT_DEBIT'});
        writeSingleCharacterElement(xmlwriter, 'paymentSourceTimingType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'paymentSourceType', 'ORGANISATION_DEFAULT');
        writeSingleCharacterElement(xmlwriter, 'pppLevelType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'pppReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'reference', 'ALL | DEBIT | DIRECT_DEBIT | ' + centre.Plan_Site__r.WFS_Ref__c + '-' + centre.Mandate__r.Name);
        writeSingleCharacterElement(xmlwriter, 'tableReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'transactionType', WFS_NULL);
        
        xmlwriter.writeEndElement();//PaymentSource
        
        xmlwriter.writeStartElement(null, 'PaymentSource', null);
        
        writeSingleCharacterElement(xmlwriter, 'accountReference', centre.Plan_Site__r.WFS_Ref__c + '-' + centre.Mandate__r.Name);
        writeSingleCharacterElement(xmlwriter, 'currency', 'GBP');
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'dcAccountReference', 'NGC_WHOLESALE');
        writeSingleCharacterElement(xmlwriter, 'debitCreditType', 'CREDIT');
        writeSingleCharacterElement(xmlwriter, 'facilityReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'ftValueType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'itemCategory', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'mandateReference', centre.Mandate__r.Name);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'organisation', new map<string, string>{'type'=>'Dealer','reference'=>centre.Plan_Site__r.WFS_Ref__c});
        writeSingleCharacterElement(xmlwriter, 'paymentApproval', 'NO');
        writeSingleCharacterElementWithAttributes(xmlwriter, 'paymentMethod', new map<string, string>{'debitCreditType'=>'CREDIT','type'=>'PaymentMethod','reference'=>'DIRECT_CREDIT'});
        writeSingleCharacterElement(xmlwriter, 'paymentSourceTimingType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'paymentSourceType', 'ORGANISATION_DEFAULT');
        writeSingleCharacterElement(xmlwriter, 'pppLevelType', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'pppReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'reference', 'ALL | CREDIT | DIRECT_CREDIT | ' + centre.Plan_Site__r.WFS_Ref__c + '-' + centre.Mandate__r.Name);
        writeSingleCharacterElement(xmlwriter, 'tableReference', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'transactionType', WFS_NULL);
        
        xmlwriter.writeEndElement();//PaymentSource
        xmlwriter.writeEndElement();//paymentSources
        
        writeSingleCharacterElement(xmlwriter, 'rollUp', 'YES');
        
        xmlwriter.writeEndElement();//PaymentSettings
        xmlwriter.writeEndElement();//paymentSettings
        
        writeSingleCharacterElement(xmlwriter, 'profitCentreCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'reference', centre.Plan_Site__r.WFS_Ref__c);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'region', new map<string, string>{'province'=>'UK','type'=>'Region','country'=>'UK'});
        writeSingleCharacterElement(xmlwriter, 'relationshipIndicator', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'requiresSettlementConfirmation', 'false');
        writeSingleCharacterElement(xmlwriter, 'retailCrossCheck', 'NO');
        writeSingleCharacterElement(xmlwriter, 'riskClassificationOne', 'RISKCLASS_1');
        writeSingleCharacterElement(xmlwriter, 'riskClassificationTwo', 'RISKCLASS_1');
        writeSingleCharacterElement(xmlwriter, 'riskRating', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'salesAreaCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'salesRegionCode', WFS_NULL);
        writeSingleCharacterElement(xmlwriter, 'salesTaxExempt', 'NO');
        writeSingleCharacterElement(xmlwriter, 'shortName', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'taxRateJurisdiction', new map<string, string>{'type'=>'RateJurisdiction','reference'=>'NEXTGEAR_TAX'});
        writeSingleCharacterElement(xmlwriter, 'tradingName', centre.Plan_Site__r.Trading_As__c);
        writeSingleCharacterElement(xmlwriter, 'transferRules', 'INANDOUT');
        writeSingleCharacterElement(xmlwriter, 'uccNominated', 'false');
        writeSingleCharacterElement(xmlwriter, 'userAccessStatus', 'ENABLED');
        
        xmlwriter.writeEndElement();//Dealer
        
        addToDataset(centre.Plan_Site__c, centre.Company__c, xmlwriter.getXmlString());
        
    }
    
    private static void generateGroupXML(string companyId, string companyName, string companyWFS_Ref, list<string> centreWFS_Ref){
        XmlStreamWriter xmlwriter = new XmlStreamWriter();
        
        xmlwriter.writeStartElement(null, 'OrganisationGroup', null);
        writeSingleCharacterElement(xmlwriter, 'currentEntityStatus', 'ACTIVE');
        writeSingleCharacterElement(xmlwriter, 'customGroupType', WFS_NULL);
        writeSingleCharacterElementWithAttributes(xmlwriter, 'dealingCompany', new map<string, string>{'type'=>'DealingCompany','reference'=>'NEXTGEAR'});
        writeSingleCharacterElement(xmlwriter, 'descriptiveName', companyName);
        
        xmlwriter.writeStartElement(null, 'members', null);
        
        //write the centre data first...
        for(string ref : centreWFS_Ref){
            writeSingleCharacterElementWithAttributes(xmlwriter, 'ref', new map<string, string>{'type'=>'Dealer','reference'=>ref});
        }
        //...followed by the company data
        writeSingleCharacterElementWithAttributes(xmlwriter, 'ref', new map<string, string>{'type'=>'Dealer','reference'=>companyWFS_Ref});
        
        xmlwriter.writeEndElement();//members
        
        writeSingleCharacterElementWithAttributes(xmlwriter, 'owner', new map<string, string>{'type'=>'DealingCompany','reference'=>'NEXTGEAR'});
        writeSingleCharacterElement(xmlwriter, 'reference', companyName);
        writeSingleCharacterElement(xmlwriter, 'type', WFS_NULL);
        
        xmlwriter.writeEndElement();//OrganisationGroup
        
        addToDataset(companyId, xmlwriter.getXmlString());
    }
    
    //Opens and closes an XML tag,  inserts value between tags
    private static void writeSingleCharacterElement(XmlStreamWriter xmlwriter, string tag,  object tagData){
        WFS_XML_Utils.writeSingleCharacterElement(xmlwriter, tag, tagData);
    }
    
    //Opens and closes an XML tag,  accepts a map of attributes to be used.
    //Does not write anythig other than attributes
    private static void writeSingleCharacterElementWithAttributes(XmlStreamWriter xmlwriter, string tag,  map<string, string> attributes){
        WFS_XML_Utils.writeSingleCharacterElementWithAttributes(xmlwriter, tag, attributes);
    }
    
    
    
    private static void addToDataset(string output){
        system.debug('***addToDataset: ' + output);
        
        xmlOutput.add(new WFS_Organisation_Data__c(XML__c = output));
    }
    
    
    private static void addToDataset(id parentId, string output){
        system.debug('***addToDataset: ' + parentId +'|' + output);
        
        xmlOutput.add(new WFS_Organisation_Data__c(ParentAccountId__c = parentId, XML__c = output));
    }
    
    
    private static void addToDataset(id accountId, id parentId, string output){
        system.debug('***addToDataset: ' + accountId + '|' + parentId +'|' + output);
        
        xmlOutput.add(new WFS_Organisation_Data__c(AccountId__c = accountId, ParentAccountId__c = parentId, XML__c = output));
    }
    
    private static void writeToDB(){
        insert xmlOutput;
    }

}