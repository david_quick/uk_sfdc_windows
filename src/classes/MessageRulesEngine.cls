public with sharing class MessageRulesEngine {
    public MessageRulesEngine() {
        
    }
    
    public void run(List<Dealer_Message__c> messages) {
        

        //get list of on hold contacts
        MessageHandler handler = new MessageHandler();
        List<Id> accountIdsOnHold = handler.getAccountsOnHold();
        
        //filter message list removing on hold contacts
        messages = [Select Rank__c, Start_Date__c, End_Date__c, Link_1_Clicked_Date__c, Link_2_Clicked_Date__c, Retries_Remaining__c, Closed_Retries_Remaining__c, Marketing_Message__r.Priority__c from Dealer_Message__c where Id in :messages and Contact__r.AccountId not in :accountIdsOnHold];
        
        
        SetRank(messages);    
    }
    
    
    private void setRank(List<Dealer_Message__c> messages) {
        
        //set rank on message if is not exluded based on expiry date and usage
        for (Dealer_Message__c m : messages) {
            m.Rank__c = null;
            if ((DateTime.now() > m.Start_Date__c && DateTime.now() < m.End_Date__c) && m.Link_1_Clicked_Date__c == null && m.Link_2_Clicked_Date__c == null && m.Retries_Remaining__c > 0 && m.Closed_Retries_Remaining__c > 0) {
                m.Rank__c = m.Marketing_Message__r.Priority__c;
                
            }
        }
        
        update messages;
        
    }
    
}