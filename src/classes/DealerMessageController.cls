public with sharing class DealerMessageController {


    public Dealer_Message__c message {get; set;}

    public DealerMessageController(ApexPages.StandardController stdController) {
       
        this.message = (Dealer_Message__c)stdController.getRecord();
        
    }

     public void runRulesEngine() {


        List<Dealer_Message__c> messages = new List<Dealer_Message__c>();
        messages.add(message);
        MessageRulesEngine engine = new MessageRulesEngine();
        engine.run(messages);
      
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Rules Engine Run Successfully'));


    }
}