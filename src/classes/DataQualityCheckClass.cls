public with sharing class DataQualityCheckClass {
    /**
    20 Oct 2014: Pawan Tokas (Account Name field needs to skip the validation if qualifying status='Account Live')
    */
    //Singleton pattern
    Private static DataQualityCheckClass instance;
    
    //Factory method to create and send the instance to the calling class.
    public  Static DataQualityCheckClass getDataQualityCheckClassInstance(){
        if(instance == null){
            instance = new  DataQualityCheckClass();
        }
        return instance;
    }
    
    public Set<String> getFieldNamesFromCS(String ObjectName){
        Set<String> fieldsToProcess = new Set<String>();
        Map<String,Data_Quality__c> mp;
        if(!Test.isRunningTest()){
         mp  = Data_Quality__c.getAll();
        }
        else{
            mp = TestDataUtilityClass.getListOfDataQualityCS();       
        }
        if(mp.get(ObjectName).Field_1__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_1__c);
        if(mp.get(ObjectName).Field_2__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_2__c);
        if(mp.get(ObjectName).Field_3__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_3__c);
        if(mp.get(ObjectName).Field_4__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_4__c);
        if(mp.get(ObjectName).Field_5__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_5__c);
        if(mp.get(ObjectName).Field_6__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_6__c);
        if(mp.get(ObjectName).Field_7__c != null)
            fieldsToProcess.add(mp.get(ObjectName).Field_7__c);
            
            return fieldsToProcess;
                   
    }
      
    public void dataQualityCheckMethod(List<SObject> newObjects,String ObjectName){
        Set<String> fieldsToProcess = this.getFieldNamesFromCS(ObjectName);
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(ObjectName.toLowerCase()).getDescribe().Fields.getMap();
        Map<String, String> mapFieldLabelFieldValue = new Map<String, String>();
        Map<String,Map<String,String>> testMap = new Map<String,Map<String,String>>();
        
        for(String fieldAPIName : fMap.keySet()){
            SObjectField sfield = fMap.get(fieldAPIName);
                Schema.DescribeFieldresult dfield = sfield.getDescribe();
                String fieldLabel = dfield.getLabel();
           for(SObject eachRecord:newObjects){
                if(fieldsToProcess.contains(fieldLabel)){
                    mapFieldLabelFieldValue.put(fieldLabel,String.valueOf(eachRecord.get(fieldAPIName)));
                    testMap.put(eachRecord.id,mapFieldLabelFieldValue);
                }
            }
        }
        boolean dovalidate = true;
        boolean validatefail = false;
        string valError = 'Please check the Data Quality for : ';
        for(SObject a:newObjects){
            for(String s : fieldsToProcess){
                validatefail = false;
                valError = 'Please check the Data Quality for : ';
                dovalidate=true;
                if(s=='Account Name' & ObjectName=='Account' ){
                    Account obj=(Account)a;
                    if(obj.Qualified_status__c=='Account live')dovalidate=false;
                }
                if(dovalidate)
                {
                    String revisedSentence = '';
                    String originalSentence = '';
                    String[] splitWordsToProcess = new String[]{};
                    originalSentence = testMap.get(a.id).get(s) == null ? testMap.get(a.id).get(s) : testMap.get(a.id).get(s).remove('(').remove(')');
                    if(originalSentence!=null){
                      originalSentence =originalSentence.replaceAll('\\s+', ' ');
                      splitWordsToProcess = originalSentence.split(' ');
                      if(s!='Business Address Postcode'){ // This is ALLCAPS Field o Account,
                          for(String str : splitWordsToProcess){
                            revisedSentence += str.substring(0,1).toUpperCase()+(str.substring(1,str.length()).toLowerCase())+' ';
                          }
                      }
                      else {
                        for(String str : splitWordsToProcess){
                            revisedSentence += str.substring(0,str.length()).toUpperCase()+' ';
                        }
                    }
                    revisedSentence = revisedSentence.trim();
                           
                    if(!originalSentence.equals(revisedSentence)){
                        validatefail = true;
                        valError = valError + s + ',';
                    } 
                 }
                }
                if (validatefail) {
                    a.addError(valError);
                }
            }
        }
    }
}