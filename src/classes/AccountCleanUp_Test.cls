@isTest 
public class AccountCleanUp_Test {
    @isTest static void testBatchJob() {
        boolean expectedExceptionThrown = false;
        Test.startTest();
        try
        {
            System.debug('********** Call the batch job');
            AccountCleanUp.runBatch();
        }
        catch(Exception ex)
        {
            expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);  
    }
}