public with sharing class MessageHandler {

    public static final String MARKETING_OPTOUT = 'StockMaster Messages';
    public static final String CONTACT_PREFIX = 'CONTACT';
    public static final String ACCOUNT_PREFIX = 'ACCOUNT';
    public static final String DELETE_SUCCESSMESSAGE = 'Messages deleted successfully';
    public static final String UNRANK_SUCCESSMESSAGE = 'Removed ranking successfully from Messages.';
    public static final String DELETE_FAILUREMESSAGE = 'Error deleting messages';

    public MessageHandler() {}

    public List<Id> getAccountsOnHold () {

        List<Dealer_Message__c> dealerMessagesOnHold = [Select Contact__r.AccountId from Dealer_Message__c where Marketing_Message__r.On_Hold_Applied__c = true];

        List<Id> accountsOnHold = new List<Id>();
        if (dealerMessagesOnHold.size() > 0) {
            for(Dealer_Message__c m : dealerMessagesOnHold) {
                accountsOnHold.add(m.Contact__r.AccountId);
            }
        }

        return accountsOnHold;

    }

    public List<Contact> GetDealerContacts(List<Account> accounts, Marketing_Message__c message) {

        List<Id> accountsOnHold = getAccountsOnHold();

        //get current date less min account open days
        DateTime  dateCompare = DateTime.now().addDays((Integer)(-1 * message.Minimum_Account_Open_Duration__c));

        //get message list filtered on min account open, contacts on hold and marketing opt out
        List<Contact> contactsReturn = new List<Contact>();
        List<Contact> contacts =  [Select  Account.Id, Account.Current_Utilisation__c, Account.X30_Day_avergae_utilsation__c, Account.Credit_Limit_Review_Date__c, Name, Account.Name, FirstName, LastName,  Account.OwnerId, Account.Owner.Phone,Account.Owner.MobilePhone, Account.Owner.Name, Email, (SELECT Role, AccountId, Contact.AccountId FROM AccountContactRoles)  from Contact  where  Account.Id in :accounts and contact_marketing_opt_out__c excludes(:MARKETING_OPTOUT) and Account.Id not in :accountsOnHold and Account.Account_Live_Date__c < :dateCompare  order by Account.Name, Name ];
        //filter on contact roles
        if (message.Roles__c != null) {

            String[] roles = message.Roles__c.split(';');

            for (Contact c : contacts) {
                Boolean bIsAdded = false;
                for (AccountContactRole cr : c.AccountContactRoles) {
                    if (cr.AccountId == cr.Contact.AccountId) {
                        for (String s : roles) {
                            if (s == cr.Role) {
                                contactsReturn.add(c);
                                bIsAdded = true;
                                break;
                            }
                        }
                    }
                    if (bIsAdded) break;
                }
            }
        }
        else {
            contactsReturn = contacts;
        }

        return contactsReturn;

    }


    public List<Dealer_Message__c> CreateDealerMessages(List<Contact> contacts, MessageWrapper selectedMessage) {


        DealerMessageBatch__c batch = new DealerMessageBatch__c();
        insert batch;

        List<Dealer_Message__c> dealerMessages = new List<Dealer_Message__c>();
        for (Contact c : contacts) {

            Dealer_Message__c message = new Dealer_Message__c();
            message.DealerMessageBatch__c = batch.Id;
            message.Contact__c = c.Id;
            message.Start_Date__c = Datetime.now();
            message.End_Date__c = (selectedMessage.message.Expiry_Days__c == null) ? Datetime.now() : Datetime.now().addDays((Integer)selectedMessage.message.Expiry_Days__c);
            message.Marketing_Message__c = selectedMessage.message.Id;
            message.Retries_Remaining__c = selectedMessage.message.Retry_LImit__c;
            message.Text__c = mergeText(selectedMessage.message.Text__c, c);
            message.Asset_Id__c = selectedMessage.message.Asset_Id__c;
            message.Closed_Retries_Remaining__c = selectedMessage.message.Closed_Retry_Limit__c;
            message.Tasks_Required__c = selectedMessage.message.Tasks_Required__c;
            message.PresentedClosed__c = (selectedMessage.message.PresentedClosed__c == null) ? System.today() : System.today().addDays((Integer)selectedMessage.message.PresentedClosed__c);
            message.Interaction_2__c = (selectedMessage.message.Interaction_2__c == null) ? System.today() : System.today().addDays((Integer)selectedMessage.message.Interaction_2__c);
            message.Interaction_1__c = (selectedMessage.message.Interaction_1__c == null) ? System.today() : System.today().addDays((Integer)selectedMessage.message.Interaction_1__c);

            dealerMessages.add(message);

        }

        insert dealerMessages;

        MessageRulesEngine engine = new MessageRulesEngine();
        engine.Run(dealerMessages);
        return dealerMessages;


    }

    private Schema.DisplayType getFieldType(String apiname,String typeObject){

        Map<String, Schema.SObjectField> alf;
        if (TypeObject=='Account')
        {
            alf= Schema.SObjectType.Account.fields.getMap();
        }else
        {
            alf= Schema.SObjectType.Contact.fields.getMap();
        }
        Schema.SObjectField field = alf.get(apiname);
        return field.getDescribe().getType();

    }

    private string mergeText(String inputText, Contact c) {

        sObject sC = c;
        sObject sA = c.Account;
        //replace carriage returns with space
        inputText = inputText.replace('\r\n', ' ');
        inputText = inputText.replace('\n', ' ');
        inputText = inputText.replace('\r', ' ');

        //parse out values inside %% and put in a list
        List<String> codes = new List<String>();
        String regex = '.*%%(.*)%%.*';
        Pattern p = Pattern.compile(regex);
        getCodes(inputText, p, codes);

        //replace %% with ++
        for (String s : codes) {
            inputText = inputText.Replace('%%' + s + '%%', '++' + s + '++');
        }


        //replace account and contact fields inside ++ delimited strings with actual values
        List<MessagePersonalisation__c> mcs = MessagePersonalisation__c.getAll().values();

        for (MessagePersonalisation__c m : mcs) {
            for (String s : codes) {

                if (m.FriendlyName__c == s) {
                    if (m.FriendlyName__c.startsWith(ACCOUNT_PREFIX)) {
                        String mergedstring='';
                        String apiname=m.APIName__c;
                        if(apiname.contains('.'))
                        {
                            mergedstring=String.valueOf(sA.getSObject(apiname.substringBefore('.')).get(apiname.substringAfter('.')));
                            }
                            else
                        {
                            mergedstring=String.valueOf(sA.get(apiname));
                        }

                        if(apiname=='X30_Day_avergae_utilsation__c' || apiname=='Current_Utilisation__c')
                        {

                            if(mergedstring==null)
                            {
                                mergedstring='0%';
                            }else
                            {
                                mergedstring=mergedstring+'%';
                            }
                        }
                        if(mergedstring==null)mergedstring='';
                        inputText = inputText.replace('++' + s + '++', mergedstring);

                    }
                    else if (m.FriendlyName__c.startsWith(CONTACT_PREFIX)) {

                        String mergedstring='';
                        String apiname=m.APIName__c;
                        if(apiname.contains('.'))
                        {
                            mergedstring=String.valueOf(sC.getSObject(apiname.substringBefore('.')).get(apiname.substringAfter('.')));
                            }
                            else
                        {
                            mergedstring=String.valueOf(sC.get(apiname));
                        }
                    if(mergedstring==null)mergedstring='';
                        inputText = inputText.replace('++' + s + '++', mergedstring);
                    }


                }
            }

        }


        return inputText;

    }

    public void getCodes(string inputText, Pattern p, List<String> codes) {

        Matcher m = p.matcher(inputText);

        if(m.matches()) {
            codes.add(m.group(1));
            inputText = inputText.Replace('%%' + m.group(1) + '%%', '++' + m.group(1) + '++');
            getCodes(inputText, p, codes);
        }
        else {
            return;
        }

        return;


    }


public ApexPages.Message removeRankingFromDealerMessages(List<Dealer_Message__c> dealerMessages) {
        ApexPages.Message pageMessage;
            for (Dealer_Message__c m : dealerMessages) {
                m.Rank__c = null;
            }
        try
        {
        update dealerMessages;
            pageMessage = new ApexPages.Message(ApexPages.Severity.INFO, UNRANK_SUCCESSMESSAGE );
        }catch(Exception e) {
            pageMessage = new ApexPages.Message(ApexPages.Severity.Error, 'Had issue with removing ranking from dealer messages '+ e);
        }

        return pageMessage;
    }


    public void processAccountsOnHold(List<Account> accounts) {
        //create on hold dealer message for these contacts and call rules engine with on hold flag
        Marketing_Message__c holdMarketingMessage = new Marketing_Message__c();
        try
        {
            holdMarketingMessage = [Select Id, Text__c from Marketing_Message__c where On_Hold_Applied__c = true][0];
        }
        catch (Exception ex)
        {
        }
        if (holdMarketingMessage != null) {
            List<Dealer_Message__c> messages = [Select Contact__c from Dealer_Message__c where Contact__r.AccountId in :accounts];
            List<Contact> contacts = [Select Id from Contact where Account.Id in :accounts];
            List<Dealer_Message__c> holdMessages = new List<Dealer_Message__c>();

            for (Contact c : contacts) {
                Dealer_Message__c holdMessage = new Dealer_Message__c();
                holdMessage.Marketing_Message__c = holdMarketingMessage.Id;
                holdMessage.Contact__c  = c.Id;
                holdMessage.Closed_Retries_Remaining__c = 10000;
                holdMessage.Retries_Remaining__c = 10000;
                holdMessage.Rank__c = 1;
                holdMessage.Start_Date__c = DateTime.now();
                holdMessage.End_Date__c = DateTime.now().addyears(100);
                holdMessage.Text__c = holdMarketingMessage.Text__c;
                holdMessages.add(holdMessage);
            }
            insert holdMessages;

            for (Dealer_Message__c m : messages) {
                m.Rank__c = null;
            }
            update messages;

            MessageRulesEngine engine = new MessageRulesEngine();
            engine.Run(messages);
        }

    }

    public void processAccountsOffHold(List<Account> accounts) {

        //remove any on hold dealer messages and call rules engine
        List<Dealer_Message__c> holdMessages = [Select Id from Dealer_Message__c where Contact__r.AccountId in :accounts and Marketing_Message__r.On_Hold_Applied__c = true];
        if (holdMessages.size() > 0) {
            delete holdMessages;
        }

        List<Dealer_Message__c> messages = [Select Id from Dealer_Message__c where Contact__r.AccountId in :accounts];
        MessageRulesEngine engine = new MessageRulesEngine();
        engine.Run(messages);


    }

}