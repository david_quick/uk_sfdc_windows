@isTest

public class Test_AccountEmailHandler{

    /*Creates the test data to be available for the test methods and classes*/
    @testSetup static void createTestData() {
        Profile sysadminprof = [Select Id from Profile where Name = 'System Administrator'];
        User TestUserforTask = TestData.createDefaultUser(sysadminprof.id);
        TestUserforTask.UserName = 'TestUserforTask@ngc.com.tskuser' ;
        insert TestUserforTask ; 

        }
        
     /*This method tests both positive and negative scenarios when a mail is sent to the Email Service*/   
    public static testMethod void EmailhandlerAccountmethod1(){
        
         
        //Create an Account record
        RecordType ar = [SELECT Id,Name FROM RecordType where developerName = 'Companies'];
        Account acc = TestData.createAccountWithRecordType(ar.Id);
        acc.Source_Country__c = 'UK';
        acc.Business_Email__c = 'TestMailforEmailhandler@rightmail.com';
        insert acc;     
        
               
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();       
        email.subject = 'Testing mail Subject';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Thank You';
        email.fromAddress = 'TestMailforEmailhandler@rightmail.com';
        envelope.fromAddress = 'TestMailforEmailhandler@rightmail.com';
        
        //Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[2];
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.pdf';
        binaryAttachment.body = blob.valueOf('Test attachment for class purpose');
        //binaryattachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { binaryattachment };
      
      
        Messaging.InboundEmail.textAttachment textAttachment1 = new Messaging.InboundEmail.textAttachment();
        textAttachment1.Filename = 'test.vcf';
        textAttachment1.body = 'Test attachment for class purpose';
        textAttachment1.charset = 'est attachment for class purpose' ;
        email.textAttachments = new Messaging.inboundEmail.textAttachment [] { textAttachment1 };
        
        test.starttest();
        /*Checks when the task is succesfully created*/
        AccountEmailHandler handlerinstance = new AccountEmailHandler(); 
        Messaging.InboundEmailResult  result=  handlerinstance.handleInboundEmail(email,envelope);
        system.assertequals(result.success,true);
        // checks that the tasks are succesfully created for the account
        list<Task> oneTask = [Select Id from task where WhatId =:acc.id];
         system.assert(oneTask.size() > 0);
         
        /*Checks if there is any error in the class and reaches exception*/
        acc.Business_Email__c = 'changedforEmailhandler@rightmail.com';
        update acc;
        Messaging.InboundEmailResult  result2=  handlerinstance.handleInboundEmail(email,envelope);
        system.assertequals(result2.success,false);
        test.stoptest();
        }
}