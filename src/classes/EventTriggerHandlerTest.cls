@isTest
public class EventTriggerHandlerTest {
    @isTest static void testNewUnlinkedRecurringEvent() {
        Account acc = TestData.createAccount();
        Event evt = TestData.createUnlinkedRecurringEvent(acc);
        Test.startTest();
        System.debug('********** Before Trigger ' + evt);
        insert evt;
        Test.stopTest();
        System.debug('********** After Trigger ' + evt);
        Event eventResult = [SELECT Id, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        System.debug('********** After Get ' + eventResult);
        System.assert(eventResult.Related_Task_ID__c == null);
    }
    @isTest static void testNewUnlinkedEvent() {
        // Create a new unlinked event and check there is no related task
        Account acc = TestData.createAccount();
        Event evt = TestData.createUnlinkedEvent(acc);
        Test.startTest();
        System.debug('********** Before Trigger ' + evt);
        insert evt;
        Test.stopTest();
        System.debug('********** After Trigger ' + evt);
        Event eventResult = [SELECT Id, Related_Task_ID__c, Description FROM Event WHERE Id =: evt.Id];
        System.assert(eventResult.Related_Task_ID__c == null);
    }
    @isTest static void testNewRecurringEventTooMany() {
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        // Create weekly event for number of days (Over 50 should fail)
        Event evt = TestData.createRecurringEvent(acc,1000);
        Test.startTest();
        try {
            insert evt;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
    @isTest static void testNewRecurringEvent() {
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        // Create weekly event for number of days (Over 50 should fail)
        Event evt = TestData.createRecurringEvent(acc,182);
        Test.startTest();
        try {
            insert evt;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void testCompletedEvent() {
        // First create a test event, then complete it
        Account acc = TestData.createAccount();
        Event evt = TestData.createEvent(acc);
        Test.startTest();
        insert evt;
        Test.stopTest();
        Event eventResult = [SELECT Id, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        eventResult.Event_Status__c = 'Completed';
        system.debug('********** Event marked as completed');
        update eventResult;
        List<Event> events = [SELECT Id,Event_Status__c FROM Event WHERE Id = :evt.Id];
        System.assert(events.size() == 0);
    }
    @isTest static void testUpdatedEvent() {
        // First create a test event, then amend it
        Account acc = TestData.createAccount();
        Event evt = TestData.createEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, ActivityDateTime, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        eventResult.ActivityDateTime = eventResult.ActivityDateTime.addDays(1);
        system.debug('********** Event date updated');
        Test.startTest();
        update eventResult;
        Test.stopTest();
        system.debug('********** Get the now updated event using original id');
        Event updatedEvent = [SELECT Id, ActivityDateTime, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        system.debug('********** Get the now updated task using related task id');
        List<Task> tasks = [SELECT Id, ActivityDate FROM TASK WHERE Id = :updatedEvent.Related_Task_ID__c];
        System.assert(tasks.size() == 1);
    }
    @isTest static void testUpdatedUnlinkedEvent() {
        // First create an unlinked test event, then amend it with a valid task area
        Account acc = TestData.createAccount();
        Event evt = TestData.createUnlinkedEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, Event_Task_Area__c, Event_Type__c, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        eventResult.Event_Task_Area__c = 'AE - Call';
        eventResult.Event_Type__c = 'Cold Call';
        system.debug('********** Event task area updated');
        Test.startTest();
        update eventResult;
        Test.stopTest();
        system.debug('********** Get the now updated event using original id');
        Event updatedEvent = [SELECT Id, ActivityDateTime, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        system.debug('********** Get the now updated task using related task id');
        List<Task> tasks = [SELECT Id, ActivityDate FROM TASK WHERE Id = :updatedEvent.Related_Task_ID__c];
        System.assert(tasks.size() == 1);
    }
    @isTest static void testEventAccountPostcode() {
        system.debug('********** Test account postcode');
        TaskEventUpdates updater = new TaskEventUpdates();
        Account acc = TestData.createAccount();
        Test.startTest();
        insert acc;
        Event evt = TestData.createUnlinkedEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, WhatId FROM Event WHERE Id =: evt.Id];
        system.debug('********** Event WhatID = ' + eventResult.WhatId);
        String postcode = updater.getAccountPostcode(eventResult.WhatId);
        system.debug('********** Event Postcode = ' + postcode);
        Test.stopTest();
        System.assert(postcode != null);
    }
    @isTest static void testEventAccountName() {
        system.debug('********** Test account name');
        TaskEventUpdates updater = new TaskEventUpdates();
        Account acc = TestData.createAccount();
        Test.startTest();
        insert acc;
        Event evt = TestData.createUnlinkedEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, WhatId FROM Event WHERE Id =: evt.Id];
        system.debug('********** Event WhatID = ' + eventResult.WhatId);
        String accName = updater.getAccountName(eventResult.WhatId);
        system.debug('********** Event Name = ' + accName);
        Test.stopTest();
        System.assert(accName != null);
    }
}