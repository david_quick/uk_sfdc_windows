public class ProspectActivityMoveController {
    private final Lead currentlead;
    public Lead thisLead {get; set;}
    public String leadName {get; set;}
    public Contact contactLookup {get; set;}
    public Lead prospectLookup {get; set;}
    public Account accountLookup {get; set;}
    public Id accountId {get; set;}
    public boolean isAccountTransferButton {get; set;}
    public boolean isProspectTransferButton {get; set;}
    public boolean isTransferButton {get; set;}
            
    public ProspectActivityMoveController() {
        currentlead = [SELECT Id, Salutation, Name, FirstName, LastName, RecordTypeId,
                   Cox_Automotive_Referral__c, Cox_Automotive_Referee__c, Manheim_referral_site__c,
                   Data_Source__c, BDE_Input__c, Email, Phone, Mobile_Phone__c, 
                   Business_Address_Line_1__c, Business_Address_Line_2__c, Business_Address_Line_3__c,
                   Business_Address_Postcode__c
                   FROM Lead WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        leadName = '';
        leadName = currentLead.Name;
        thislead = currentLead;
        contactLookup = new Contact();
        prospectLookup = TestData.createProspect();
        // Disable the transfer buttons until an object is selected
        isAccountTransferButton = false;
        isProspectTransferButton = false;
        isTransferButton = false;
    }
    
    public PageReference refreshTask() {
        getAccount();
        return null;
    }
    
    public void getAccount() {
        if (contactLookup.AccountId == null) {
            ApexPages.Message myMsg = new ApexPages.message(ApexPages.Severity.WARNING,'No account specified');
            ApexPages.addMessage(myMsg);
            return;
        }
        accountLookup = [Select Id, Business_Address_Line_1__c, Business_Address_Line_2__c, 
                        Business_Address_Line_3__c,Business_Address_Postcode__c, OwnerId,
                         Cox_Automotive_Referral__c, Cox_Automotive_Referee__c, Manheim_referral_site__c,
                         Data_Source__c, BDE_Input__c
                        From Account where Account.Id = :contactLookup.AccountId];
        isAccountTransferButton = true;
        IsProspectTransferButton = false;
        isTransferButton = true;
    }
    public void getProspect() {
        if (prospectLookup.Prospect__c == null) {
            ApexPages.Message myMsg = new ApexPages.message(ApexPages.Severity.WARNING,'No prospect specified');
            ApexPages.addMessage(myMsg);
            return;
        }
        Id prospectId = prospectLookup.Prospect__c;
        prospectLookup = [Select Id, Prospect__c, Email, Name, Phone, Mobile_Phone__c, Data_Source__c,
                         Business_Address_Line_1__c, Business_Address_Line_2__c, Business_Address_Line_3__c, Business_Address_Postcode__c
                          From Lead where Lead.Id = :prospectLookup.Prospect__c];
        isProspectTransferButton = true;
        isAccountTransferButton = false;
        isTransferButton = true;
        prospectLookup.Prospect__c = prospectId;
    }
    private void transferActivities(Id leadId, Id accountId) {
        integer counter = 0;
        List<Task> tasks = [Select Id, Description, WhoId, WhatId, OwnerId, AccountId from Task where WhoId = :leadId];
        for(Task t : tasks) {
            tasks[counter].WhoId = null;
            tasks[counter].WhatId = accountLookup.Id;
            counter++;
        }
        update tasks;
        List<Event> events = [Select Id, Description, WhoId, WhatId, OwnerId, AccountId from Event where WhoId = :leadId];
        counter = 0;
        for(Event e : events) {
            events[counter].WhoId = null;
            events[counter].WhatId = accountLookup.Id;
            counter++;
        }
        update events;
    }
    private void transferProspectActivities(Id leadId, Id prospectId) {
        integer counter = 0;
        List<Task> tasks = [Select Id, Description, WhoId, WhatId, OwnerId, AccountId from Task where WhoId = :leadId];
        for(Task t : tasks) {
            tasks[counter].WhoId = prospectId;
            counter++;
        }
        update tasks;
        List<Event> events = [Select Id, Description, WhoId, WhatId, OwnerId, AccountId from Event where WhoId = :leadId];
        counter = 0;
        for(Event e : events) {
            events[counter].WhoId = prospectId;
            counter++;
        }
        update events;
    }
 
    public PageReference save() {
        if (isAccountTransferButton) {
        	//  After successful Save, navigate to the default view page
        	if (contactLookup.AccountId == null) {
            	ApexPages.Message myMsg = new ApexPages.message(ApexPages.Severity.WARNING,'No account specified');
            	ApexPages.addMessage(myMsg);
            	return null;
        	}
        	transferActivities(currentLead.Id, contactLookup.AccountId);
        }
        if (isProspectTransferButton) {
            //  After successful Save, navigate to the default view page
        	if (prospectLookup.Prospect__c == null) {
            	ApexPages.Message myMsg = new ApexPages.message(ApexPages.Severity.WARNING,'No prospect specified');
            	ApexPages.addMessage(myMsg);
            	return null;
        	}
        	transferProspectActivities(currentLead.Id, prospectLookup.Prospect__c);
        }
        PageReference redirectSuccess = new ApexPages.StandardController(currentLead).view();
        return (redirectSuccess);
    }
    public PageReference cancel() {
        //  Return to the calling page 
        PageReference redirectSuccess = new ApexPages.StandardController(currentLead).view();
        return (redirectSuccess);
    }
}