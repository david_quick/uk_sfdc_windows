public class AccountTriggerHandler extends TriggerHandler {
    
    /** Added for Data Quality Check **/
    public static String userCSName = 'Free_Users';
    public SET<String> freeUsers = new SET<String>();
    public static final String MARKETING_OPTOUT = 'StockMaster Messages';

    public AccountTriggerHandler(){
        preventRecursiveTrigger(false);
    }

    
    public override void beforeInsert(List<SObject> newObjects){
        Integer AutoNum;
        Integer NumLength;
        String  Prefix;
        
        List<WFS_Settings__c> WFSSettings = WFS_Settings__c.getall().values();
        if(!Test.isRunningTest()){
            AutoNum = WFSSettings[0].Sequence_Number__c.intvalue();
            NumLength = WFSSettings[0].Length__c.intvalue();
            Prefix = (WFSSettings[0].Prefix__c).trim();
        }
        else{
            AutoNum = 12273; 
            NumLength = 6;
            Prefix ='D';
        }
        List<Recordtype> Accrt = [Select r.SobjectType, r.Name, r.Id, r.DeveloperName From RecordType r
                            where SobjectType='Account'];

        RecordType Comprt;
        RecordType Centrert;
                            
        for(RecordType rt : Accrt)
        {
            if(rt.DeveloperName == 'Companies')Comprt=rt;
            if(rt.DeveloperName == 'Centres')Centrert=rt;
        }
        Set<id> ParentIds = new Set<id>();
        for (Account a : (List<Account>)newObjects) {
            if(a.RecordTypeId == Centrert.id){
                if(a.ParentId == null)a.ParentId.AddError('Parent Company must be selected');
                ParentIds.Add(a.ParentId);
            }
        }
        
        Map<id,Account> ParentAccs = new Map<id,Account>([Select WFS_REF__c, ChildSeq__c FROM Account WHERE Id IN :ParentIds]);
         
        
        for (Account a : (List<Account>)newObjects) {
            if(a.RecordTypeId == Comprt.id)
            {
                String  formatedNum = String.valueof(AutoNum);
                for(integer i=formatedNum.length(); i< NumLength; i++)
                {
                    formatedNum = '0' + formatedNum;
                }
                a.WFS_Ref__c = Prefix + formatedNum;
                a.ChildSeq__c = 1;
                AutoNum = AutoNum + 1;
            }
            else if(a.RecordTypeId == Centrert.id)
            {
                if(a.ParentId != null)
                {
                    a.WFS_REF__c = ParentAccs.get(a.ParentId).WFS_Ref__c + '-' + ParentAccs.get(a.ParentId).ChildSeq__c;    
                    Account a1 = ParentAccs.get(a.ParentId);
                    a1.ChildSeq__c++;
                    ParentAccs.put(a.ParentId,a1);
                }
            }
        }
        if(!Test.isRunningTest()){
        WFSSettings[0].Sequence_Number__c = AutoNum;
        update WFSSettings;
        }
        if(ParentAccs!=null)update ParentAccs.values();
        
        accountHelperMethod(newObjects);
            
    }
    
   public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap)
   {
        boolean accError = false;
        String accMessage = '';
        boolean skipCheck = false;
        List<Account>OldAccounts = (List<Account>)oldObjects;
        List<Account>NewAccounts = (List<Account>)newObjects;
        User u = [select WFS_Reference_Editable__c from User where id = :UserInfo.getUserId()];
        List<Id> accountsOptedOut = new List<Id>();
        List<Id> accountsOptedIn = new List<Id>();
        Id profileId = UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        for(integer i=0; i< oldAccounts.size(); i++ )
        {
            // Validation checks here can be skipped if the user is a sysAdmin
            if (profileName !='System Administrator' && profileName != 'Sys Admin - Credit Manager') {
                Account newacc = newAccounts[i];
                Account oldacc = oldAccounts[i];
                // Check 1 - If the Qualified Status has changed from Account Live 
                if (newacc.Qualified_status__c != 'Account live' && oldacc.Qualified_status__c == 'Account live') {
                    // This is an error - you can only change from Account Live as SysAdmin
                    // However if it has changed from Account Live to Legal Action - it is OK
                    // But the action of changing from Account Live changes the Account Live Date, so we must skip check2
                    if (newacc.Qualified_status__c == 'Legal Action' && oldacc.Qualified_status__c == 'Account live') {
                        accError = false;
                        accMessage = null; 
                        skipCheck = true;
                    }
                    else {
                        accError = true;
                        accMessage = 'You do not have permission to change the Qualified Status as this is live';
                    }
                }
                // Also if the status has been set to Account Live - skip the Account Live Date check as not relevant
                if (oldAcc.Qualified_status__c != 'Account Live' && newacc.Qualified_status__c == 'Account Live') {
                    skipCheck = true;
                }
                if (!skipCheck) {
                    // Check 2 - Do not change the Account Live date unless the Account Closed Date is set
                    if (newacc.Account_Closed_Date__c == null && newacc.Account_Live_Date__c != oldacc.Account_Live_Date__c) {
                        // This is an error - you cant change the account live date unless the account is closed
                        accError = true;
                        accMessage = 'You do not have permission to change the account live date until the account is closed';
                    }
                }
                // Check 3 - If the old status was Legal Action it cannot be changed to anything else
                if (newacc.Qualified_status__c != 'Legal Action' && oldacc.Qualified_status__c == 'Legal Action') {
                    accError = true;
                    accMessage = 'You cannot change the status from Legal Action';
                }
                if (accError) {
                    Account errAcc = (Account)Trigger.newMap.get(newAcc.Id);
                    errAcc.addError(accMessage);
                    return;
                }
            }
            // Send email if account has had SOAR Audit set
            if(!OldAccounts[i].Soar_Audit__c && NewAccounts[i].Soar_Audit__c) {
                System.debug('********** Soar Audit flag set');
                SendContactEmail(NewAccounts[i]);
            }
            // Update SOAR status change date on status change
            if(OldAccounts[i].Soar_Status__c != NewAccounts[i].Soar_Status__c) {
                NewAccounts[i].Soar_Status_change_date__c = System.today();
            }
            // Update any owned contacts if account owner has changed
            if(OldAccounts[i].OwnerId != NewAccounts[i].OwnerId) {
                updateContactOwners(NewAccounts[i]);
            }
            if(OldAccounts[i].WFS_Ref__c != NewAccounts[i].WFS_Ref__c && !u.WFS_Reference_Editable__c)
            {
                NewAccounts[i].WFS_Ref__c = OldAccounts[i].WFS_Ref__c;
            }
            
            if(OldAccounts[i].account_marketing_exclusion__c != NewAccounts[i].account_marketing_exclusion__c) 
            {
                if (NewAccounts[i].account_marketing_exclusion__c) {
                    accountsOptedOut.add(OldAccounts[i].Id);
                }  
                else 
                {
                    accountsOptedIn.add(OldAccounts[i].Id);
                }
            }
        }
        for(integer a=0; a < newAccounts.size(); a++ )
        {
           updateContactSOAR(newAccounts[a]);
        }
       
        accountHelperMethod(newObjects);
          
        if (accountsOptedOut.size() > 0) {
            List<Contact> contactsUpdate = [Select Id, contact_marketing_opt_out__c from Contact where AccountId in :accountsOptedOut];
            
            if (contactsUpdate.size() > 0) {
                
                List<Dealer_Message__c> dealerMessages = [Select Id from Dealer_Message__c where Contact__c in :contactsUpdate];
                if (dealerMessages.size() > 0) {
                    delete dealerMessages;
                }
                
                
                for(Contact c : contactsUpdate) {
                    if (c.contact_marketing_opt_out__c != null) {
                        if (!c.contact_marketing_opt_out__c.contains(MARKETING_OPTOUT)) {
                            String[] values = c.contact_marketing_opt_out__c.split(';');
                            values.add(MARKETING_OPTOUT);
                            String valueUpdate = '';
                            for (String s : values) {
                                valueUpdate +=  s + ';';
                            }
                            c.contact_marketing_opt_out__c = valueUpdate.substring(0, valueUpdate.lastIndexOf(';'));
                        }
                    }
                    else {
                        c.contact_marketing_opt_out__c = MARKETING_OPTOUT;
                        
                    }
                }
                
                update contactsUpdate;
            }
            
        }
        
        if (accountsOptedIn.size() > 0) {
            List<Contact> contactsUpdate = [Select Id, contact_marketing_opt_out__c from Contact where AccountId in :accountsOptedIn];
            if (contactsUpdate.size() > 0) {
                
                for(Contact c : contactsUpdate) {
                    if (c.contact_marketing_opt_out__c != null) {
                        if (c.contact_marketing_opt_out__c == MARKETING_OPTOUT) {
                            c.contact_marketing_opt_out__c = null;
                        } 
                        else if (c.contact_marketing_opt_out__c.contains(MARKETING_OPTOUT)) {
                            String[] values = c.contact_marketing_opt_out__c.split(';');
                            String valueUpdate = '';
                            for (String s : values) {
                                if (s != MARKETING_OPTOUT) {
                                    valueUpdate +=  s + ';';
                                }
                            }
                            
                            c.contact_marketing_opt_out__c = valueUpdate.substring(0, valueUpdate.lastIndexOf(';'));
                            
                        }
                    }
                    
                }
                
                update contactsUpdate;
            }
        }
        
    
    }
    
    private void accountHelperMethod(List<SObject> newObjects){
        /** Added for Data Quality Check **/
        DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
        Data_Quality__c userCSInstance = Data_Quality__c.getValues(userCSName);
        if(!Test.isRunningTest()){
            freeUsers = instance.getFieldNamesFromCS(userCSName);
        }
        else{
            freeUsers.add('Jenny Gibson');
            freeUsers.add('Lucy Brown');
        }
        if(!freeUsers.contains(Userinfo.getName())){
         instance.dataQualityCheckMethod(newObjects,'Account');  
        }   
       
    }
    private void updateContactOwners(Account a) {
        List<Contact> contacts = new List<Contact>();
        List<Contact> newContacts = new List<Contact>();
        contacts = [Select Id, OwnerId from Contact where AccountId = :a.Id];
        for(Contact c : contacts) {
            if (c.OwnerId != a.OwnerId) {
                c.OwnerId = a.OwnerId;
                newContacts.Add(c);
            }
        }
        upsert newContacts;
    }
    private void updateContactSOAR(Account a) {
        List<Contact> contacts = [Select Id, Soar_Audit__c, Soar_First_Active_Date__c, Soar_Status__c from Contact where AccountId = :a.Id];
        for(Contact c : contacts) {
            c.SOAR_Status__c = a.SOAR_Status__c;
            update c;
        }
    }
    private void SendContactEmail(Account a) {
        string[] toAddresses = new String[] {'david.quick@nextgearcapital.co.uk'};
		string[] ccAddresses = new String[]{'davidquick@hotmail.com'};
        string subject = 'Account ' + a.Name + ' has gone live with NextGear Capital';
		string body = 'Please contact the following to register for SOAR';
        string line;
        List<Contact> contacts = [SELECT Id, Name, Email, HomePhone, Mobile_Phone__c FROM Contact WHERE AccountId = :a.Id ];
		for(Contact c : contacts) {
            System.debug('********** Sending Contact email for ' + c.Name);
            line = 'Name : ' + c.Name + ' Email : ' + c.Email + ' Home Phone : ' + c.HomePhone + ' Mobile Phone : ' + c.Mobile_Phone__c;
            body = body + '\n' + line;
        }
        TestData.sendSimpleEmail(toAddresses,ccAddresses,subject,body);
    }
 }