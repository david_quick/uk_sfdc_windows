global class WFS_Organisation_GenerateData_Batch implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        
        WFS_Organisation_XML.writeOpeningTags();
        
        return Database.getQueryLocator([select Id from Account 
                                        where RecordTypeId IN (select id from RecordType where sObjectType='Account' and DeveloperName='Companies')  
                                        and Id in (select accountid from opportunity where StageName ='Account Live')
                                        and  LastModifiedDate =LAST_N_DAYS:5
                                        order by Id asc ]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        //Create a list of Ids to sent to the xml class
        list<Id> companyIds = new list<Id>();
        for(sObject obj: scope){
            companyIds.add(string.valueOf(obj.get('Id')));
        }
        
        WFS_Organisation_XML.processDealers(companyIds);
    }
    
    global void finish(Database.BatchableContext BC){
        
        WFS_Organisation_XML.writeClosingTags();
        
    }
    
    //Call to run the batch job on an ad hoc basis
    global static void runBatch(){
      WFS_Organisation_GenerateData_Batch generateDataBatchJob = new WFS_Organisation_GenerateData_Batch(); 
      database.executebatch(generateDataBatchJob);
    }
}