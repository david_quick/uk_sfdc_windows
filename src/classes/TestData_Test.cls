@isTest
public class TestData_Test {
  
    @isTest static void testCreateAccount() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        List<Account> b = [SELECT Id, Name from Account WHERE Id = :a.Id];
        System.assert(b.size() == 1);
        Account c = b[0];
        System.assert(c.Name == 'Test');
        Test.stopTest();
    }
	@isTest static void testCreateAccountWithRecordType() {
        Test.startTest();
        RecordType ar = [SELECT Id,Name FROM RecordType where developerName = 'Companies'];
        Account a = TestData.createAccountWithRecordType(ar.Id);
        insert a;
        List<Account> b = [SELECT Id, Name, RecordTypeId from Account WHERE Id = :a.Id];
        System.assert(b.size() == 1);
        Account c = b[0];
        System.assert(c.Name == 'Test');
        System.assert(c.RecordTypeId == ar.Id);
        Test.stopTest();
    }
    @isTest static void testCreateAccountList() {
        Test.startTest();
        List<Account> a = TestData.createAccountList(10);
        insert a;
        List<Account> b = [SELECT Id, Business_Address_Postcode__c, RecordTypeId, Phone from Account WHERE Phone = '123456'];
        System.assert(b.size() == 10);
        Account c = b[0];
        System.assert(c.Business_Address_Postcode__c == 'TEST');
        Test.stopTest();
    }	
    @isTest static void testCreateAccountListWithRecordType() {
        Test.startTest();
        RecordType ar = [SELECT Id,Name FROM RecordType where developerName = 'Companies'];
        List<Account> a = TestData.createAccountListWithRecordType(10, ar.Id);
        insert a;
        List<Account> b = [SELECT Id, Business_Address_Postcode__c, RecordTypeId, Phone from Account WHERE Phone = '123456'];
        System.assert(b.size() == 10);
        Account c = b[0];
        System.assert(c.Business_Address_Postcode__c == 'TEST');
        System.assert(c.RecordTypeId == ar.Id);
        Test.stopTest();
    }	
    @isTest static void testGetListOfAccounts() {
        Test.startTest();
        List<Account> accs = TestData.getListOfAccounts(10);
        System.assert(accs.size() > 1);
        insert accs;
        Id newId = accs[0].Id;
        Account acc = [SELECT Id,Source_Country__c FROM Account WHERE Id = :newId];
        System.assert(acc.Source_Country__c == 'UK');
        Test.stopTest();
    }
    @isTest static void testCreateOpportunity() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Opportunity oppty = TestData.createOpportunity(a.Id, 500000, 'Awaiting approval');
		oppty.Approval_Level__c = 1;
        oppty.Bureau_Ratingx__c = 'Green';
        oppty.Judgemental_Ratingx__c = 'Green';
        oppty.Credit_Limit_x__c = 200000;
        insert oppty;
        Opportunity opptyResult = [SELECT Id, Approval_Level__c FROM Opportunity WHERE Id =: oppty.Id];
        System.debug('********** Result is ' + opptyResult);
        System.assert(opptyResult.Approval_Level__c == 0);
        Test.stopTest();
    }
    @isTest static void testCreateContact() {
 		Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Contact c = TestData.createContact(a, 'Peter','Prudent');
        insert c;
        List<Contact> lc = [SELECT Id, FirstName, LastName, Phone from Contact WHERE Id = :c.Id];
        System.assert(lc.size() == 1);
        Contact c1 = lc[0];
        System.assert(c1.LastName == 'Prudent');
        System.assert(c1.Phone == '123456');
        Test.stopTest();
    }
    @isTest static void testCreateRelatedContact() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Contact c = TestData.createContact(a,'Peter','Prudent');
        insert c;
        Related_Contact__c relC = TestData.createRelatedContact(c);
        insert relC;
        Related_Contact__c newC = [Select Id,Related_Contact_1__c,Related_Contact_2__c, Related_Contact_1_Account__c, Related_Contact_2_Account__c From Related_Contact__c WHERE Id = :relC.Id]; 
        Test.stopTest();
        System.assert(newC.Related_Contact_1__c != null);
        System.assert(newC.Related_Contact_1_Account__c != null);
    }
    @isTest static void testCreateRelatedContacts() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Contact c = TestData.createContact(a,'Peter','Prudent');
        insert c;
        Contact c1 = TestData.createContact(a,'Petra','Perfect');
        insert c1;
        Related_Contact__c relC = TestData.createRelatedContacts(c, c1);
        insert relC;
        Related_Contact__c newC = [Select Id,Related_Contact_1__c,Related_Contact_2__c, Related_Contact_1_Account__c, Related_Contact_2_Account__c From Related_Contact__c WHERE Id = :relC.Id]; 
        Test.stopTest();
        System.assert(newC.Related_Contact_1__c != null);
        System.assert(newC.Related_Contact_1_Account__c != null);
        System.assert(newC.Related_Contact_2__c != null);
        System.assert(newC.Related_Contact_2_Account__c != null);
    }
    @isTest static void testCreateContact2() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Contact c = TestData.createContact2(a, 'Peter','Prudent');
        insert c;
        List<Contact> lc = [SELECT Id, FirstName, LastName, Phone, OwnerId from Contact WHERE Id = :c.Id];
        System.assert(lc.size() == 1);
        Contact c1 = lc[0];
        System.assert(c1.LastName == 'Prudent');
        System.assert(c1.Phone == '123456');
        System.assert(c1.OwnerId == TestData.getUserId('Kris Dawson'));
        Test.stopTest();
    }
    @isTest static void testCreateCustomRole() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Contact c = TestData.createContact2(a, 'Peter','Prudent');
        insert c;
        Opportunity opp = TestData.createOpportunity(a.Id,10000,'Awaiting approval');
        insert opp;
        Role__c r = TestData.createCustomRole(c,opp);
        insert r;
        System.debug('********** Role__c = ' + r);
        Test.stopTest();
        Role__c newRole = [select Contact__c,
								Opportunity__c,
								Name
								from Role__c where contact__c = :c.Id];
        System.assert(newRole.Opportunity__c != null);
        System.assert(newRole.Contact__c != null);
    }
    @isTest static void testCreateApprovalStage() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Opportunity oppty = TestData.createOpportunity(a.Id, 500000, 'Awaiting approval');
        Approval_Stage__c app = TestData.createApprovalStage(oppty.Id);
        insert app;
        List<Approval_Stage__c> apps = [SELECT Id, Opportunity__c FROM Approval_Stage__c WHERE Id =: app.Id];
        System.assert(apps.size() == 1);
        Approval_Stage__c app1 = apps[0];
        System.assert(app1.Opportunity__c == oppty.Id);
        Test.stopTest();
    }
    @isTest static void testCreateApprovalStageWithStatus() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Opportunity oppty = TestData.createOpportunity(a.Id, 500000, 'Awaiting approval');
        Approval_Stage__c app = TestData.createApprovalStageWithStatus(oppty.Id,'Pending Approval');
        insert app;
        List<Approval_Stage__c> apps = [SELECT Id, Status__c FROM Approval_Stage__c WHERE Id =: app.Id];
        System.assert(apps.size() == 1);
        Approval_Stage__c app1 = apps[0];
        System.assert(app1.Status__c == 'Pending Approval');
        Test.stopTest();
    }
    @isTest static void testPopulateApprovalThreshold() {
        Test.startTest();
        ApprovalThreshold__c currentAppT = TestData.populateApprovalThreshold('UK', 10000, 80000, 2, 'Green');
		insert currentAppT;
        List<ApprovalThreshold__c> apps = [SELECT Id, Name, Country__c, Credit_Limit_To__c FROM ApprovalThreshold__c WHERE Id = :currentAppT.Id];
        System.assert(apps.size() == 1);
        ApprovalThreshold__c app = apps[0];
        System.assert(app.Country__c == 'UK');
        Test.stopTest();
    }
    @isTest static void testCurrentApprovalMatrix() {
        Test.startTest();
        String currentUserName = UserInfo.getUserName();
		ApprovalMatrix__c currentAppM = TestData.currentApprovalMatrix(currentUserName, 'UK', 2, 1);
		insert currentAppM;
        List<ApprovalMatrix__c> apps = [SELECT Id, Name, Country__c FROM ApprovalMatrix__c WHERE Id = :currentAppM.Id];
        System.assert(apps.size() == 1);
        ApprovalMatrix__c app = apps[0];
        System.assert(app.Country__c == 'UK');
        Test.stopTest();
    }
    @isTest static void testCreateDefaultUser() {
    	Test.startTest();
		Profile p = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];        
        User u = TestData.createDefaultUser(p.Id);
        insert u;
        List<User> users = [SELECT Id, Alias, ProfileId, UserName FROM User where Id = :u.Id];
        System.assert(users.size() == 1);
        User u1 = users[0];
        System.assert(u1.Alias == 'newUser');
        System.assert(u1.ProfileId == p.Id);
        Test.stopTest();
    }
    @isTest static void testCreateUser() {
        Test.startTest();
		Profile p = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];        
        User u = TestData.createUser(p.Id,'Peter', 'Perfecto');
        insert u;
        List<User> users = [SELECT Id, Alias, ProfileId, UserName FROM User where Id = :u.Id];
        System.assert(users.size() == 1);
        User u1 = users[0];
        System.assert(u1.Alias == 'newUser');
        System.assert(u1.ProfileId == p.Id);
        Test.stopTest();
    }
    @isTest static void testCreateTask() {
		Test.startTest();
		Lead led = TestData.createFullApplication();
		Task t = TestData.createTask(led);
		System.assert(t.WhoId == led.Id);
		System.assert(t.Status == 'Not Started');
		Test.stopTest();        
    }
    @isTest static void testCreateEvent() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Event e = TestData.createEvent(a);
        insert e;
        List<Event> newEvents = [SELECT Id, Related_Task_ID__c, Location FROM Event where Id = :e.Id];
        System.assert(newEvents.size() == 1);
        Event newEvent = newEvents[0];
        System.assert(newEvent.Location == 'Test - TEST');
        System.assert(newEvent.Related_Task_ID__c != null);
        test.stopTest();
    }
    @isTest static void testCreateUnlinkedEvent() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Event e = TestData.createUnlinkedEvent(a);
        insert e;
        List<Event> newEvents = [SELECT Id, Related_Task_ID__c, Location FROM Event where Id = :e.Id];
        System.assert(newEvents.size() == 1);
        Event newEvent = newEvents[0];
        System.assert(newEvent.Location == 'Test - TEST');
        System.assert(newEvent.Related_Task_ID__c == null);
        Test.stopTest();
    }
    @isTest static void testCreateUnlinkedRecurringEvent() {
        Test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Event evt = TestData.createUnlinkedRecurringEvent(a);
        insert evt;
        Event eventResult = [SELECT Id, Related_Task_ID__c FROM Event WHERE Id = :evt.Id];
        System.assert(eventResult.Related_Task_ID__c == null);
        Test.stopTest();
    }
    @isTest static void testCreateRecurringEvent() {
        Test.startTest();
        boolean expectedExceptionThrown = false; 
    	Account acc = TestData.createAccount();
        Event evt = TestData.createRecurringEvent(acc,20);
        try {
       		insert evt;
        }
        catch (Exception ex) {
            expectedExceptionThrown = true;
        }
        System.AssertEquals(expectedExceptionThrown,false);
       Test.stopTest();
    }
    @isTest static void testCreateFullApplication() {
        Test.startTest();
        Lead led = TestData.createFullApplication();
        insert led;
        Lead newLead = [SELECT Id, Business_Phone__c, Home_Address_County__c, Dealer_Premises__c FROM Lead WHERE Id = :led.Id];
        System.assertEquals(newLead.Business_Phone__c,'01244 737373');
        System.assertEquals(newLead.Home_Address_County__c,'Cheshire');
        System.assert(newLead.Dealer_Premises__c != null);
        Test.stopTest();
    }
    @isTest static void testCreateFullApplicationPage1() {
        Test.startTest();
        Lead led = TestData.createFullApplicationPage1();
        insert led;
        Lead newLead = [SELECT Id, Home_Address_County__c, Dealer_Premises__c FROM Lead WHERE Id = :led.Id];
        System.assertEquals(newLead.Home_Address_County__c,'Cheshire');
        System.assert(newLead.Dealer_Premises__c == null);
        Test.stopTest();
     }
    @isTest static void testCreateFullApplicationPage2() {
        Test.startTest();
        Lead led = TestData.createFullApplicationPage1();
        insert led;
        led = TestData.createFullApplicationPage2(led);
        update led;
        Lead newLead = [SELECT Id, Business_Phone__c, Home_Address_County__c, Dealer_Premises__c, Which_Manheim_branch_referred_you__c, Replacement_or_Incremental__c FROM Lead WHERE Id = :led.Id];
        System.assertEquals(newLead.Business_Phone__c,'01244 737373');
        System.assertEquals(newLead.Home_Address_County__c,'Cheshire');
        System.assert(newLead.Dealer_Premises__c != null);
        System.assertEquals(newLead.Which_Manheim_branch_referred_you__c,'Manchester');
        System.assertEquals(newLead.Replacement_or_Incremental__c,null);
        Test.stopTest();
     }
     @isTest static void testCreateFullApplicationPage3() {
        Test.startTest();
        Lead led = TestData.createFullApplicationPage1();
        insert led;
        led = TestData.createFullApplicationPage2(led);
        update led;
        led = TestData.createFullApplicationPage3(led);
        update led;
        Lead newLead = [SELECT Id, Business_Phone__c, Home_Address_County__c, Dealer_Premises__c, Which_Manheim_branch_referred_you__c, Replacement_or_Incremental__c FROM Lead WHERE Id = :led.Id];
        System.assertEquals(newLead.Business_Phone__c,'01244 737373');
        System.assertEquals(newLead.Home_Address_County__c,'Cheshire');
        System.assert(newLead.Dealer_Premises__c != null);
        System.assertEquals(newLead.Which_Manheim_branch_referred_you__c,'Manchester');
        System.assertEquals(newLead.Replacement_or_Incremental__c,'Incremental');
        Test.stopTest();
     }
    @isTest static void testCreateApprovalMatrixValue() {
        Test.startTest();
        List<ApprovalMatrix__c> matrix = TestData.createApprovalMatrixValue();
        System.assert(matrix.size() > 1);
        Test.stopTest();
    }
    @isTest static void testCreateApprovalThresholdValue() {
        Test.startTest();
        List<ApprovalThreshold__c> matrix = TestData.createApprovalThresholdValue();
        System.assert(matrix.size() > 1);
        Test.stopTest();
    }
    @isTest static void testGetUserID() {
        Test.startTest();
        List<User> users = [SELECT Id, Alias, ProfileId, Name FROM User];
        User u1 = users[0];
        System.debug('********** User is ' + u1);
        Id newId = TestData.getUserId(u1.Name);
        System.assert(newId != null);
        System.assert(newId == u1.Id);
        Test.stopTest();
    }
    @isTest static void testGetUserName() {
        Test.startTest();
        List<User> users = [SELECT Id, Alias, ProfileId, Name FROM User];
        User u1 = users[0];
        System.debug('********** User is ' + u1);
        String newName = TestData.getUserName(u1.Id);
        System.assert(newName != null);
        System.assert(newName == u1.Name);
        Test.stopTest();
    }
    @isTest static void testGetListOfDataQualityCS() {
        Test.startTest();
        Map<String,Data_Quality__c> mp;
        mp = TestData.getListOfDataQualityCS();
        System.assert(mp.size() > 1);
        Test.stopTest();
    }
    @isTest static void testGetListOfContacts() {
        Test.startTest();
        Account acc = TestData.createAccount();
        List<Contact> contacts = TestData.getListOfContacts(acc);
        System.assert(contacts.size() > 1);
        Contact ct = contacts[0];
        System.assert(ct.Phone == '123456');
        Test.stopTest();
    }
    @isTest static void testGetMarketingMessage(){
        Test.startTest();
        Marketing_Message__c mess = TestData.getMarketingMessage();
        System.assert(mess.Retry_Limit__c == 10);
        Test.stopTest();
    }
    @isTest static void testGetDealerMessage() {
        Test.startTest();
        Marketing_Message__c mess = TestData.getMarketingMessage();
        insert mess;
        Dealer_Message__c dmess = TestData.getDealerMessage(mess);
        System.assert(dmess.Rank__c == 1);
        Test.stopTest();
    }
    @isTest static void testCreateOnHoldMessage() {
        Test.startTest();
        Marketing_Message__c mess = TestData.createOnHoldMessage();
        insert mess;
        System.assert(mess.On_Hold_Applied__c == true);
        Test.stopTest();
    }
    @isTest static void testSendSimpleEmail() {
    	boolean expectedExceptionThrown = false; 
        test.startTest();    
        string[] toAddresses;
        string[] ccAddresses;
        string subject = 'Simple Test Mail';
        string body = 'Contact Name: Contact 1';
        try
        {
            toAddresses = new String[] {'david.quick@nextgearcapital.co.uk'};
            ccAddresses = new String[]{'davidquick@hotmail.com'};
        	TestData.sendSimpleEmail(toAddresses,ccAddresses,subject,body);
        }
        catch (Exception ex)
        {
            expectedExceptionThrown = true;
        }
        test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void testSendTemplatedEmail() {
        // The problem with this test is that if the email delivery option is set off it will fail, 
        // but if email delivery is on it will pass
        boolean expectedExceptionThrown = false; 
        test.startTest();
        Account a = TestData.createAccount();
        insert a;
        Profile p = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];        
        User u = TestData.createDefaultUser(p.Id);
        insert u;
        String[] toAddresses = new String[] {'david.quick@nextgearcapital.co.uk'};
        String[] ccAddresses = new String[]{};
        Attachment [] attachments = new Attachment[]{};
        String tName = 'Account_On_Stop';
        try
        {
        	TestData.sendTemplatedEmail(toAddresses,ccAddresses,tName,u.Id,a.Id,null,false,attachments);
        }
        catch (Exception ex)
        {
            expectedExceptionThrown = true;
        }
        test.stopTest();
        
        System.AssertEquals(expectedExceptionThrown,false);
    }
 }