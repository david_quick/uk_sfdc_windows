public class TaskEventUpdates {
    
    private static boolean hasClosed = false;
    private static string callingId = null;
    private static boolean bisUpdate = false;
    
    public TaskEventUpdates() {
        
    }
    
    public static boolean hasItClosed() {
        return hasClosed;
    }
    
    public static void setClosed() {
        hasClosed = true;
    }
    
    public static boolean IsUpdate() {
        return bisUpdate;
    }
    
    public static void SetUpdate(boolean bup)
    {
        bisUpdate = bup;
    }
    
    public Date convertToDate(DateTime indate) {
        Date outdate = Date.newInstance(indate.year(), indate.month(), indate.day());
        return outdate;
    }
    
    public DateTime convertToDateTime(Date indate) {
        DateTime outdate = DateTime.newInstance(indate.year(), indate.month(), indate.day());
        return outdate;
    }
    
    public String getAccountPostcode(string whatid) {
        if (whatid == null) {
            return '';
        }
        try
        {
            Account acc = [SELECT Id, Business_Address_Postcode__c FROM ACCOUNT WHERE ID = :whatid];
            return (String)acc.Business_Address_Postcode__c;
        }
        catch (Exception ex)
        {
            //Not an account?
            return '';
        }
    }
    
    public String getAccountName(string whatid) {
        if (whatid == null) {
            return '';
        }
        try
        {
            Account acc = [SELECT Id, Name FROM ACCOUNT WHERE ID = :whatid];
            return (String)acc.Name;
        }
        catch (Exception ex)
        {
            //Not an account?
            return '';
        }
    }
    
    public Integer daysBetween(DateTime startDate, Date endDate)
    {
        Date sDate = convertToDate(startDate);
        return sDate.daysBetween(endDate);
    }
    
    public Integer daysOff(Datetime sdate, Datetime edate)
    {
         Integer i = 0;
                
         while (sdate < edate) {
            if (sdate.format('E') == 'Sat' | sdate.format('E') == 'Sun'){
                i = i + 1;
            }
         sdate = sdate.addDays(1);
         }
         return i;
    }
    
    public Integer getRecurrence(Event evt) {
        Integer recurs = 0;
        if (!evt.IsRecurrence) {
            return 0;
        }
        DateTime startDate = evt.RecurrenceStartDateTime;
        Date endDate = evt.RecurrenceEndDateOnly;
        Integer days = daysBetween(startDate, endDate);
        Integer interval = 1;
        
        if (evt.RecurrenceInterval != null) {
            interval = evt.RecurrenceInterval;
        }
        // It is a recurring event - so calculate how many recurrences might occur
        if (evt.RecurrenceType == 'RecursDaily') {
            days = days / interval;
            if (days > 50) {
                recurs = days;
            }
        }
        if (evt.RecurrenceType == 'RecursEveryWeekday')  {
            Integer noDays = days - daysOff(startDate,convertToDateTime(endDate));
            noDays = noDays / interval;
            if (noDays > 50) {
                recurs = noDays;
            }
        }
        if (evt.RecurrenceType == 'RecursMonthly')  {
            days = days / interval;
            if (days / 30 > 50) {
                recurs = days / 30;
            }
        }
        if (evt.RecurrenceType == 'RecursMonthlyNth')  {
            days = days / interval;
            if (days / 30 > 50) {
                recurs = days / 30;
            }
        }
        if (evt.RecurrenceType == 'RecursWeekly')  {
            days = days / interval;
            Integer weeks = days / 7;
            if (weeks > 50) {
                recurs = weeks;
            }
    
        }
        if (evt.RecurrenceType == 'RecursYearly')  {
            if (days > (365 * 5)) {
                recurs = days; 
             }
        }
        if (evt.RecurrenceType == 'RecursYearlyNth')  {
            if (days > (365 * 5)) {
                recurs = days;
            }
        }
        return recurs;
    }
         
    public boolean hasRelatedEvents(Task thistask) {
        List<Event> events = [SELECT Id, Event_Status__c FROM Event WHERE Related_Task_ID__c = :thistask.id];
        if (events.size() > 0 ) {
            for(integer i=0; i< events.size(); i++)
            {
                Event ev = (Event) events[i];
                if (ev.Event_Status__c != 'Completed' && bisUpdate == false)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public void checkEventClosed(List<SObject> newObjects) {
        for(integer i=0; i< newObjects.size(); i++)
        {
            Task t = (Task) newObjects[i];
            List<Event> events = [SELECT Id, Event_Status__c, Description FROM Event WHERE Related_Task_ID__c = :t.Id];
            for(integer ti=0; ti< events.size(); ti++) {
                Event e = events[ti];
                if (e.Event_Status__c == 'Completed') {
                    setClosed();
                }
            }
        }
    }
    public void removeCompletedEvent(List<SObject> newObjects) {
        List<Id> removeId = new List<Id>();
        List<Event> newEvents = (List<Event>) newObjects;
        // Create a new Task if the Event Task Area is set
        for(integer i=0; i< newEvents.size(); i++)
        {
            Event e = newEvents[0];
            if (e.Event_Status__c == 'Completed' && e.Related_Task_ID__c != null) { 
                callingId = e.WhoId;
                removeId.Add(e.Id);
            }
            else {
                if (e.Related_Task_ID__c != null) {
                    Task t = [SELECT ID, Description FROM Task WHERE ID = :e.Related_Task_ID__c];
                    t.Description = e.Description;
                    SetUpdate(true);
                    update t;
                    SetUpdate(false);
                }
            }
        }
        List<Event> removals = [Select Id from Event where Id IN: removeId]; 
        if (removals.size() > 0) {
            delete removals;
        }
        
    }
    
    public void checkRelatedTask(List<SObject> newObjects) {
        if (!hasItClosed()) {
            List<Event> newEvents = (List<Event>) newObjects;
            for(integer i=0; i< newEvents.size(); i++)
            {
                Event e = newEvents[0];
                if (e.Event_Status__c == 'Completed' && e.Related_Task_ID__c != null)
                {
                    Task taskResult = [SELECT Id, Status, Related_Task_ID__c FROM Task WHERE Id =: e.Related_Task_ID__c];
                    if (taskResult.Status != 'Completed') {
                        taskResult.Status = 'Completed';
                        SetUpdate(true);
                        update taskResult;
                        SetUpdate(false);
                    }
                }
                else {
                    if (e.Related_Task_ID__c != null) {
                        Task taskResult = [SELECT Id, Status, ActivityDate, Related_Task_ID__c FROM Task WHERE Id =: e.Related_Task_ID__c];
                        if (taskResult.Status != 'Completed') {
                            taskResult.ActivityDate = e.ActivityDate;
                            SetUpdate(true);
                            update taskResult;
                            SetUpdate(false);
                        }
                    }
                }
            }
        }
    }
    public void checkRelatedEvents(List<SObject> newObjects) {
        if (!hasItClosed()) {
           for(integer i=0; i< newObjects.size(); i++)
            {
                Task t = (Task) newObjects[i];
                List<Event> events = [SELECT Id, ActivityDateTime, StartDateTime, EndDateTime, Event_Status__c, Description FROM Event WHERE Related_Task_ID__c = :t.Id];
                for(integer ti=0; ti< events.size(); ti++) {
                    Event e = events[ti];
                    if (e.Event_Status__c != 'Completed') {
                        if (t.Status == 'Completed') {
                            setClosed();
                            e.Event_Status__c = 'Completed';
                            update e;
                        } 
                    }
                }
            }
        }
    }
    public void eventUpdated(List<SObject> newObjects) {
        // If the event task area is now set AND the related task is null, its a change to create a related task
        List<Event> newEvents = (List<Event>) newObjects;
        for(integer i=0; i< newEvents.size(); i++)
        {
            Event e = newEvents[0];
            if (e.Event_Task_Area__c == 'AE - Call' || e.Event_Task_Area__c == 'AE - Visit' || e.Event_Task_Area__c == 'Sales - BDE') {
                if (e.Related_Task_Id__c == null) {
                    Task t = new Task();
                    t.WhoId = e.WhoId;
                    t.WhatId = e.WhatId;
                    t.Subject = e.Subject;
                    t.ActivityDate = convertToDate(e.StartDateTime);
                    t.Task_Area__c = e.Event_Task_Area__c;
                    t.Type__c = e.Event_Type__c;
                    t.Description = ' Related task from an Event - ' + e.Description;
                    t.Status = 'Not Started';
                    t.Priority = 'Normal';  
                    t.OwnerId = e.OwnerId;
                    insert t;
                    e.Related_Task_ID__c = t.Id;
                 }
            }
        }
    }
    
    public void addRelatedTask(List<SObject> newObjects){
        List<Event> newEvents = (List<Event>) newObjects;
        // Create a new Task if the Event Task Area is set
        for(integer i=0; i< newEvents.size(); i++)
        {
            Event e = newEvents[i];
            if (e.Event_Task_Area__c == 'AE - Call' || e.Event_Task_Area__c == 'AE - Visit' || e.Event_Task_Area__c == 'Sales - BDE') {
                Task t = new Task();
                t.WhoId = e.WhoId;
                t.WhatId = e.WhatId;
                t.Subject = e.Subject;
                t.ActivityDate = convertToDate(e.ActivityDateTime);
                t.Task_Area__c = e.Event_Task_Area__c;
                t.Type__c = e.Event_Type__c;
                t.Description = ' Related task from an Event - ' + e.Description;
                t.Status = 'Not Started';
                t.Priority = 'Normal'; 
                t.OwnerId = e.OwnerId;
                insert t;
                e.Related_Task_ID__c = t.Id;
            }
            else
            {
                e.Related_Task_ID__c = null;
            }
       }
    }
 }