public with sharing class MarketingMessagesController {
     
    public List<MessageWrapper> messages {get; set;}
    public List<Account> selectedAccounts {get; set;}
    public MessageWrapper selectedMessage {get; set;}
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    private List<ContactWrapper> wrappers = new List<ContactWrapper>();
    
    public static final Integer PAGE_SIZE = 200;
    public static final String SELECT_ACCOUNTWARNING =  'Please select at least one account';
    public static final String SELECT_MESSAGEWARNING =  'Please select a message';
    public static final String SELECT_1MESSAGEWARNING = 'Please select only one message';
    public static final String SELECT_CONTACTWARNING =  'Please select a contact';
    public static final String MESSAGES_ADDEDINFO =     ' Dealer messages added';
    public static final String PICKLIST_OPTION_INTERACTION1 = 'Interaction 1';
    public static final String PICKLIST_OPTION_INTERACTION2 = 'Interaction 2';
    public static final String PICKLIST_OPTION_PRESENTEDCLOSED = 'Presented/Closed';
    
    public MarketingMessagesController(ApexPages.StandardSetController stdController) {
        
        selectedAccounts = stdController.getSelected();
        
        //get all active messages
        messages = new List<MessageWrapper>();
        for (Marketing_Message__c m : [Select Name, Priority__c, Tasks_Required__c, On_Hold_Applied__c, Minimum_Account_Open_Duration__c, Link_1__c, Link_2__c, Roles__c,  Expiry_Days__c, Retry_Limit__c, Text__c, Asset_Id__c, Closed_Retry_Limit__c, PresentedClosed__c, Interaction_1__c,Interaction_2__c from Marketing_Message__c where Active__c = true]) {
            MessageWrapper wrapper = new MessageWrapper(m);
            messages.add(wrapper);
            
        }
        
        if (selectedAccounts.size() == 0) {
            ApexPages.Message pageMessage = new ApexPages.Message(ApexPages.Severity.WARNING, SELECT_ACCOUNTWARNING);
            ApexPages.addMessage(pageMessage);
        }
        
    }
    
    public List<ContactWrapper> contacts {
        get {
            wrappers.clear();
            buildContactsWrapper();              
            return wrappers;
        }
        set
        {
            wrappers = value;
        }
        
        
    }
    
    
    
    public ApexPages.StandardSetController con {
        get {
            if(con == null) {
                
                size = PAGE_SIZE;
                MessageHandler handler = new MessageHandler();
                
                con = new ApexPages.StandardSetController(handler.GetDealerContacts(selectedAccounts, selectedMessage.message));
                //noOfRecords = con.getResultSize();
                // sets the number of records in each page set
                con.setPageSize(size);
            }
            return con;
        }
        set;
    }
    
    
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    
    
    
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    
    // returns the first page of records
    public void first() {
        con.first();
    }
    
    // returns the last page of records
    public void last() {
        con.last();
    }
    
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
    
    // returns the next page of records
    public void next() {
        con.next();
    }
    
    
    
    public PageReference processMessages() {
        
        integer countMessages = 0;
        for(MessageWrapper m : messages) {
            if (m.selected) {
                countMessages++;
                selectedMessage = m;
            }
        }
        
        
        if (selectedAccounts.size() == 0) {
            
            ApexPages.Message pageMessage = new ApexPages.Message(ApexPages.Severity.WARNING, SELECT_ACCOUNTWARNING);
            ApexPages.addMessage(pageMessage);
            return null;
        }
        else {
            
            if (countMessages == 1) {
                return Page.CreateDealerMessages;
            }
            else {
                
                string errorMessage;
                if (countMessages == 0) {
                    errorMessage = SELECT_MESSAGEWARNING;
                } 
                else {
                    errorMessage = SELECT_1MESSAGEWARNING;
                }
                ApexPages.Message pageMessage = new ApexPages.Message(ApexPages.Severity.WARNING, errorMessage);
                ApexPages.addMessage(pageMessage);
                return null;
                
            }
            
        }
        
        
        
    }
    
    public PageReference processContacts() {
        
        List<Contact> selectedContacts = new List<Contact>();
        for(ContactWrapper cw : wrappers) {
            if (cw.Selected) {
                selectedContacts.add(cw.Contact);
            }
        }
        
        ApexPages.Message pageMessage;
        if (selectedContacts.size() == 0) {
            pageMessage = new ApexPages.Message(ApexPages.Severity.WARNING, SELECT_CONTACTWARNING);
        } 
        else {
            
            MessageHandler handler = new MessageHandler();
            List<Dealer_Message__c> messages = handler.CreateDealerMessages(selectedContacts, selectedMessage);
            pageMessage =  new ApexPages.Message(ApexPages.Severity.INFO, messages.size() + MESSAGES_ADDEDINFO); 
            
            
        }
        
        ApexPages.addMessage(pageMessage);
        return null;
        
    }

    private void buildContactsWrapper(){
        Map<Id,Contact> mCon = new Map<Id,Contact>((List<Contact>)con.getRecords());
        Map<Id,Contact> lContactDealer = new Map<Id,Contact>( [
                                            SELECT      Id, Name, ( SELECT  PresentedClosed__c ,Interaction_1__c, Interaction_2__c, Quarantine_Status__c  
                                                                    FROM    Dealer_Messages__r 
                                                                    WHERE   Marketing_Message__c = :selectedMessage.message.Id
                                                                    ORDER BY CreatedDate desc 
                                                                    LIMIT 1)
                                            FROM        Contact
                                            WHERE       Id IN :mCon.keySet()
                                        ] );

        for(Contact c : lContactDealer.values()) {
            try{
                if (c.Dealer_Messages__r != null){
                    if(c.Dealer_Messages__r[0].Quarantine_Status__c == PICKLIST_OPTION_INTERACTION1){
                        if (c.Dealer_Messages__r[0].Interaction_1__c > System.today()){
                            mCon.remove(c.Id);
                        }
                    }else if(c.Dealer_Messages__r[0].Quarantine_Status__c == PICKLIST_OPTION_INTERACTION2){
                        if (c.Dealer_Messages__r[0].Interaction_2__c > System.today()){
                            mCon.remove(c.Id);
                        }
                    }else if(c.Dealer_Messages__r[0].Quarantine_Status__c == PICKLIST_OPTION_PRESENTEDCLOSED){
                        if(c.Dealer_Messages__r[0].PresentedClosed__c > System.today()){  
                            mCon.remove(c.Id);
                        }
                    }else{
                        // do nothing... this implies that the Quarantine_Status__c value is null.
                    }
                }
            }catch(ListException lex){
                // caught List exception which meant the iterator went to a record which never had a dealer message
                // skip over this and do nothing... as its to be expected. it should not be removed from the list
                // as this implies it has never been sent a dealer message
            }
            
        }
   
        // (List<Contact>)con.getRecords()
        noOfRecords = mCon.size();
        for(Contact c : mCon.values()) {
            ContactWrapper cw = new ContactWrapper(c);
            wrappers.add(cw);
        }
    }


    
    
}