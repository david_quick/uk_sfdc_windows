public with sharing class MessageRecallController {
     /*
    Case:0007510 :11 Dec 2014 Pawan Tokas (Pav)
    Requirement Changed. When a message is recalled we are not going to delete dealer message related to 
    it any more. Instead we are going to remove the ranking so that the dealer message is not displayed to the contact.
    Also the dealer messages linked to a batch number will be un ranked instead of all historical.
    */
    
  public Marketing_Message__c message {get; set;}
    public List<Dealer_Message__c> dealerMessages {get; set;}
    public String messageText {get; set;}
    public Boolean recallDisabled {get; set;}
    
    public String selectedBatchId {get;  set;} 
    public list<SelectOption> options {get; set;}
    
    public static final String DELETE_INFO = 'There are no dealer messages to delete';
    public static final String DELETE_WARNING = 'You are about to delete the following number of messages: ';
    public static final String UNRANK_WARNING = 'You are about to unrank the following number of dealer messages: ';

    
    public MessageRecallController(ApexPages.StandardController stdController) {
                
        this.message = (Marketing_Message__c)stdController.getRecord();
        this.selectedBatchId='';
        this.options = new List<SelectOption>();
        options.add(new SelectOption('0', '----- Select One -----'));
        List<SelectOption> optionsToPopulate = new List<SelectOption>();
        List<Id> batchnumberid =new List<Id>();
        AggregateResult[] groupedResults=[Select DealerMessageBatch__c, count(id) from Dealer_Message__c where Marketing_Message__c =:message.Id group by DealerMessageBatch__c];
        for (AggregateResult ar : groupedResults)  
        {
            batchnumberid.add(String.valueOf(ar.get('DealerMessageBatch__c')));
        }
        if(batchnumberid.size()>0)
        {
                List<DealerMessageBatch__c> batchNumberLinkedToMessage= [Select Id,Name,createddate from DealerMessageBatch__c where id in :batchnumberid order by createddate desc ];
                if(batchNumberLinkedToMessage.size()>0)
                {
                    for(DealerMessageBatch__c dmb :batchNumberLinkedToMessage ){
                            options.add(new SelectOption(dmb.id, 'Batch Number-' + dmb.Name + ' [Created At:' + dmb.createddate + ']'));
                    }
                
                }
                else 
                {
                    recallDisabled = true;
                    messageText = DELETE_INFO;
                }
        }else
        {
            recallDisabled = true;
            messageText = DELETE_INFO;
        }
                
        
    }
    
     public void updateDMCount() {
        if(selectedBatchId=='0')
        {
            recallDisabled = true;
            messageText = 'At least one batch should be selected to perform this action.';
            return;
        }
        dealerMessages = [Select Id from Dealer_Message__c where Marketing_Message__c = :message.Id and DealerMessageBatch__c=:selectedBatchId and Rank__c != null];
        if (dealerMessages.size() > 0) {
            messageText = UNRANK_WARNING  + dealerMessages.size();
            recallDisabled = false;
        } 
        else
        {
            recallDisabled = true;
            messageText = DELETE_INFO;
        }
        
     }
    
    public void recallMessages() {
        if(selectedBatchId=='0')
        {
            recallDisabled = true;
            messageText = 'At least one batch should be selected to perform this action.';
            return;
        }
        MessageHandler handler = new MessageHandler();
        ApexPages.Message pageMessage = handler.removeRankingFromDealerMessages(dealerMessages);
        ApexPages.addMessage(pageMessage);
                
    }
    
}