public with sharing class MessageWrapper {
    
    public Marketing_Message__c message {get; set;}
    public Boolean selected {get; set;}
        
    public MessageWrapper(Marketing_Message__c m) {
        message = m;
        selected = false;
    }
}