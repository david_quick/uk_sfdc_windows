public with sharing class LeadSnapshotTriggerHandler extends TriggerHandler {
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap) {
        // Unless the user is the API user do not allow updates - the record is read only once created
        LeadSnapShot_Create createSnapshot = new LeadSnapShot_Create();
        list<LeadSnapshot__c> originalLeads = ( list<LeadSnapshot__c> ) oldObjects;
        list<LeadSnapshot__c> updatedLeads = ( list<LeadSnapshot__c> ) newObjects;
        for(integer i=0; i< originalLeads.size(); i++ ) {
            LeadSnapshot__c newSnap = updatedLeads[i];
        	LeadSnapshot__c oldSnap = originalLeads[i];
            Id profileId = createSnapshot.getProfileId('System Administrator API');
            Id userID = createSnapshot.getProfileIdFromUser(newSnap.LastModifiedById);
            if (newSnap.AssocitedAccount__c == null) {
            	if (profileId != userID) {
                   	// Error this unless the change is to LeadAndSnapShotMapField__c or AssociatedLead__c
                	if (oldSnap.LeadAndSnapshotMapField__c != newSnap.LeadAndSnapshotMapField__c || oldSnap.AssociatedLead__c != newSnap.AssociatedLead__c) {
                    	// This is expected
                	}
                	else {
                    	newSnap.addError('The lead snapshot cannot be edited - it is a record of the original application');
                	}
            	}
            }
        }
    }
}