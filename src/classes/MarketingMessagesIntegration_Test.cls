@isTest
private class MarketingMessagesIntegration_Test {
    
    @isTest static void test_method_getDealerMessage() {

        Marketing_Message__c marketingMessage = TestDataUtilityClass.getMarketingMessage();
        Dealer_Message__c dealerMessage = TestDataUtilityClass.getDealerMessage(marketingMessage);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/MarketingMessages/' + dealerMessage.Contact__c;  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        Dealer_Message__c response = MarketingMessagesIntegration.getDealerMessage();
        System.assertEquals(response.Text__c, 'Test text');
                

        
    }


    @isTest static void test_method_updateDealerMessage() {

        Marketing_Message__c marketingMessage = TestDataUtilityClass.getMarketingMessage();
        Dealer_Message__c dealerMessage = TestDataUtilityClass.getDealerMessage(marketingMessage);
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/MarketingMessages/'; 
        req.httpMethod = 'POST';
        req.addParameter('messageId', dealerMessage.Id);
                
        RestContext.request = req;
        RestContext.response = res;

        MarketingMessagesIntegration.updateDealerMessage(dealerMessage.Id, true, false, true, true);
        dealerMessage = [Select Closed_Retries_Remaining__c from Dealer_Message__c where Id = :dealerMessage.Id][0];
        System.assertEquals(4, dealerMessage.Closed_Retries_Remaining__c);

        //todo check task created correctly
              

        
    }
    
    
}