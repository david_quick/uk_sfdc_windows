@isTest
public class TaskTriggerHandlerTest {
    @isTest static void testMultipleTasks() {
        Lead led = TestData.createFullApplication();
        List<Task> tasks = new List<Task>();
        for(integer i=0; i < 20; i++)
        {
            tasks.add(TestData.createTask(led));
        }
        test.startTest();
        insert tasks;
    
        for(integer ii=0; ii < 20; ii++)
        {
            Task t = tasks[ii];
            t.Subject = 'Meeting';
        }
        upsert tasks;
        test.stopTest();
    }
 
    @isTest static void testEventCompleted() {
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        Event evt = TestData.createEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, Event_Status__c, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        System.assert(eventResult.Related_Task_ID__c != null);
        // Now update the task - it should throw an exception
        system.debug('********** Now find task for ID: ' + eventResult.Related_Task_ID__c);
        Task t = [SELECT Id, Status FROM Task WHERE Id =: eventResult.Related_Task_ID__c ];
        System.debug('********** Event status before Task update ' + eventResult.Event_Status__c);
        Test.startTest();
        try
        {
            t.Status = 'Completed';
            update t;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
    @isTest static void testEventUpdate() {
        boolean expectedExceptionThrown = false;    
        Account acc = TestData.createAccount();
        Event evt = TestData.createEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, Event_Status__c, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        System.assert(eventResult.Related_Task_ID__c != null);
        // Now update the task and check that its related event does NOT get updated too
        system.debug('********** Now find task for ID: ' + eventResult.Related_Task_ID__c);
        Task t = [SELECT Id, Status FROM Task WHERE Id =: eventResult.Related_Task_ID__c ];
        System.debug('********** Event status before Task update ' + eventResult.Event_Status__c);
        Test.startTest();
        try {
                t.ActivityDate = evt.ActivityDate.addDays(1);
                update t;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
    @isTest static void testEventDeleted() {
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        Event evt = TestData.createEvent(acc);
        insert evt;
        Event eventResult = [SELECT Id, Event_Status__c, Related_Task_ID__c FROM Event WHERE Id =: evt.Id];
        System.assert(eventResult.Related_Task_ID__c != null);
        // Now delete the task - it should throw an exception
        system.debug('********** Now find task for ID: ' + eventResult.Related_Task_ID__c);
        Task t = [SELECT Id, Status FROM Task WHERE Id =: eventResult.Related_Task_ID__c ];
        System.debug('********** Event status before Task update ' + eventResult.Event_Status__c);
        Test.startTest();
        try
        {
            delete t;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
}