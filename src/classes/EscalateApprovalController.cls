public with sharing class EscalateApprovalController {
    
    public Opportunity o {get;set;}
    
    public String ApproveReject;
    
    public Map<id,Profile_Settings__c> psMap = new Map<Id,Profile_Settings__c>();
    public Id ProfileID {
        get{
            if (ProfileId==null)
            {
                ProfileId = Userinfo.getProfileid();            
            }
            return ProfileID;
        }
        set;
    }
    
    
    
    Public EscalateApprovalController(ApexPages.StandardController controller)
    {
        controller.addFields(new List<String>{'Approval_Level_1_to_2_reason__c', 'Approval_Level_2_to_3_reason__c','Approval_Level_3_to_4_reason__c', 
                                                'Approval_level_1_to_2__c', 'Approval_level_2_to_3__c', 'Approval_level_3_to_4__c',
                                                'Approval_Level_1_to_2_date_user__c', 'Approval_Level_2_to_3_date_user__c', 'Approval_Level_3_to_4_date_user__c',
                                                'Approval_Level_Text__c'});
        o=(Opportunity) controller.getRecord();

        List<Profile_Settings__c> userProfiles = Profile_Settings__c.getAll().values();  // get the profile settings

        for (Profile_Settings__c profileInfo : userProfiles) {
            psMap.put(profileInfo.Profile_ID__c,profileInfo);
        }
        
    }
    
    Public PageReference Save(){

        String myDate = Datetime.now().format('dd/MM/YYYY HH:mm');
                
        String userAndDate= UserInfo.getFirstName() + ' ' + UserInfo.getLastName() +', ' + myDate;

        if(o.Approval_Level_Text__c=='Approval Level 1')
        {
            if(o.Approval_Level_1_to_2_reason__c!='')
            {
                o.Approval_level_1_to_2__c = TRUE;
                o.Approval_Level_1_to_2_date_user__c =  userAndDate;
                update o;
                return new PageReference('/'+o.id);
            }
            else
            {
                o.addError('You must enter an Escalation reason');
                return null;
            }
        }
        else if(o.Approval_Level_Text__c=='Approval Level 2')
        {
            if(o.Approval_Level_2_to_3_reason__c!='')
            {
                o.Approval_level_2_to_3__c = TRUE;
                o.Approval_Level_2_to_3_date_user__c =  userAndDate;
                update o;
                return new PageReference('/'+o.id);
            }
            else
            {
                o.addError('You must enter an Escalation reason');
                return null;
            }
        }
        else if(o.Approval_Level_Text__c=='Approval Level 3')
        {
            if(o.Approval_Level_3_to_4_reason__c!='')
            {
                o.Approval_level_3_to_4__c = TRUE;
                o.Approval_Level_3_to_4_date_user__c =  userAndDate;
                update o;
                return new PageReference('/'+o.id);
            }
            else
            {
                o.addError('You must enter an Escalation reason');
                return null;
            }
        }
        else
        {
                o.addError('Unknown Approval level');
                return null;
        }
        
    }
    
    Public PageReference Cancel(){
        return new PageReference('/'+o.id);
    }

}