public with sharing class PlanSiteTriggerHandler extends TriggerHandler {
    
    
    public PlanSiteTriggerHandler(){
        preventRecursiveTrigger(false);
    }
    
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap)
    {
        //check if status changed from ok to on stop
        List<Opp_Prod_Sites__c>OldSites = (List<Opp_Prod_Sites__c>)oldObjects;
        List<Opp_Prod_Sites__c>NewSites = (List<Opp_Prod_Sites__c>)newObjects;
        
        List<Id> accountsOnHoldId = new List<Id>();
        List<Id> planSitesOnHoldId = new List<Id>();   
        List<Id> accountsOffHoldId = new List<Id>();
        
        
        
        for(integer i=0; i< OldSites.size(); i++ )
        {
            if(OldSites[i].Credit_Line_Status__c != NewSites[i].Credit_Line_Status__c)
            {
                if (NewSites[i].Credit_Line_Status__c == 'On-Stop' && NewSites[i].Status__c == 'Active') {
                    accountsOnHoldId.add(OldSites[i].Company__c);
                    planSitesOnHoldId.add(OldSites[i].Id);
                }
                else if (NewSites[i].Credit_Line_Status__c == 'OK' && NewSites[i].Status__c == 'Active') {
                    accountsOffHoldId.add(OldSites[i].Company__c);
                    
                }
            }
            
            
        }
        
        if (accountsOnHoldId.size() > 0) {
            
            List<Account> accountsGoOnHold = new List<Account>();
            
            List<Account> accountsPotentialOnHold =  [Select Id, OwnerId, Name, (Select Credit_Line_Status__c  from Plan_Sites1__r where Status__c = 'Active' and Id not in :planSitesOnHoldId) from Account where Id in :accountsOnHoldId ];
            for(Account a : accountsPotentialOnHold) {
                boolean onHold = true;
                for (Opp_Prod_Sites__c  site : a.Plan_Sites1__r) {
                    if (site.Credit_Line_Status__c  != 'On-Stop') {
                        onHold = false;
                    } 
                }
                if (onHold) {
                    accountsGoOnHold.add(a);
                }
                
            }
            
            if (accountsGoOnHold.size() > 0) {
                MessageHandler handler = new MessageHandler();
                handler.processAccountsOnHold(accountsGoOnHold);
                //for(Account a : accountsGoOnHold) {
                    //sendOnStopEmail(a);
                //}
            }
        }
        
        if (accountsOffHoldId.size() > 0) {
            
            List<Account> accountsOffHold = [Select Id from Account where Id in :accountsOffHoldId];
            MessageHandler handler = new MessageHandler();
            handler.processAccountsOffHold(accountsOffHold);
            
        }
        
    }
    // Dropped in favour of a workflow rule
    private void sendOnStopEmail(Account a) {
        String uName = TestData.getUserName(a.OwnerId);
        String email = TestData.getUserEmail(a.OwnerId);
        try
        {
            String[] toAddresses = new String[] {email};
            String[] ccAddresses = new String[]{};
            Attachment [] attachments = new Attachment[]{};
            String tName = 'Account_On_Stop';
            TestData.sendTemplatedEmail(toAddresses,ccAddresses,tName,a.OwnerId,a.Id,null,false,attachments);
        }
        catch (Exception ex)
        {
            // System may not be set to send messages - just don't send it
            System.debug('Exception thrown from sendOnStopEmail is' + ex.getMessage());
        }
    }
}