/**
 * When the leads record is created from webform a copy should be created of the record as it was received
 * This is captured in LeadsSnapshot__c object
 * 24/01/2017 David Quick UKCRM-282 - The creation of the LeadSnapshot__C record has been moved to occur
 *                                    when the application is converted via the Convert button. The code has been moved out to a new class LeadSnapShot_Create
 *                                    and is called from the LeadConvertOverride class leadConv method.
 *                                    The code in this trigger is now obsolete and should not be used
 * 23/02/2017 David Quick UKCNF-18  - Prospects are bulk updated (while applications are not). A bulk update causes a DML fail in processAfterUpdate, but that doesn't need 
 *                                    to be called if the update is for Prospects
 **/
 
public with sharing class LeadsTriggerHandler extends TriggerHandler {
    /**
     *  before insert Trigger 
     */
    public override void beforeInsert(List<SObject> newObjects){
        list<Lead> newLeads = ( list<Lead> ) newObjects;
        RecordType fullAppRecordType = [select Id from RecordType where Name = 'Full Application' and SobjectType = 'Lead'];
        processBeforeInsert ( newLeads, fullAppRecordType.Id );
        // Check case of Business Address Town City and Home Address Town City
        integer counter = 0;
        for(Lead l : newLeads){
            newLeads[counter].Business_Address_Town_City__c = TestData.toProperCase(l.Business_Address_Town_City__c);
            newLeads[counter].Home_Address_Town_City__c = TestData.toProperCase(l.Home_Address_Town_City__c);
            newLeads[counter].MobilePhone = setMobilePhone(newLeads[counter].Mobile_Phone__c);
            newLeads[counter].smagicinteract__SMSOptOut__c = newLeads[counter].TPS_flagged__c;
            counter++;
        }
    }   
    /**
     *  after insert Trigger 
     */
    public override void afterInsert(List<SObject> newObjects, Map<Id,SObject> newMap) {
        list<Lead> insertedLeads = (list<Lead>)newObjects;
        RecordType fullAppRecordType = [select Id from RecordType where Name = 'Full Application' and SobjectType = 'Lead'];
        processAfterInsert( insertedLeads,fullAppRecordType.Id );
    }
    /**
     *  after update Trigger 
     */
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap) {
        RecordType fullAppRecordType = [select Id from RecordType where Name = 'Full Application' and SobjectType = 'Lead'];
        list<Lead> updatedLeads = (list<Lead>)newObjects;
        Lead firstLead = updatedLeads[0];
        if (firstLead.RecordTypeId == fullAppRecordType.Id) {
            processAfterUpdate( updatedLeads );
        }
    }
    /**
    *   before update Trigger
    */
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap) {
        List<Lead>NewLeads = (List<Lead>)newObjects;   
        for(integer i=0; i< newLeads.size(); i++ )
        {
            NewLeads[i].MobilePhone = setMobilePhone(NewLeads[i].Mobile_Phone__c);
            NewLeads[i].smagicinteract__SMSOptOut__c = NewLeads[i].TPS_flagged__c;
            if (NewLeads[i].Incoming_Opt_Out__c)
            {
                NewLeads[i].smagicinteract__SMSOptOut__c = true;
            }
        }
    }
    /**
    * Helper method for processing before insert
    */
    private void processBeforeInsert(List<Lead> newLeads,Id rid){
         // before the object is inserted ... create a snapshot and store in  
        list<LeadSnapshot__c> snapshots = new List<LeadSnapshot__c>();
        LeadSnapShot_Create createSnapshot = new LeadSnapShot_Create();
        for ( Lead newLead : newLeads ){
            if ( newLead != null ){ // and any other checks ?
                // create a snapshot to associate the lead and the snapshot we should have some unique column that can be used to cross refer each other
                newLead.LeadAndSnapshotMapField__c = 'LS'+system.currentTimeMillis();
                if(newLead.RecordTypeId == rid){ 
                    snapshots.add (createSnapshot.getNewSnapshotFromLead(newLead)) ;
                }
            }
        }
        insert snapshots;
    }

    /**
    * Helper method for processing after insert
    */
    private void processAfterInsert( List<Lead> insertedLeads, Id rid ) {    
         // here we need to associate the inserted lead record with the snapshot
        list<LeadSnapshot__c> snapshotsToUpdate = new List<LeadSnapshot__c>();
        for ( Lead insertedLead : insertedLeads ){
            if(insertedLead.RecordTypeId == rid){ 
                snapshotsToUpdate.add ( new LeadSnapshot__c ( LeadAndSnapshotMapField__c = insertedLead.LeadAndSnapshotMapField__c, AssociatedLead__c = insertedLead.id ));
            }
        }
        upsert snapshotsToUpdate LeadAndSnapshotMapField__c;
    }
        
    private void processAfterUpdate(List<Lead> updatedLeads) {
        LeadSnapShot_Create createSnapshot = new LeadSnapShot_Create();
        list<LeadSnapshot__c> snapshotsToUpdate = new List<LeadSnapshot__c>();
        for (Lead updatedLead : updatedLeads) {
            Id profileId = createSnapshot.getProfileId('System Administrator API');
            Id userID = createSnapshot.getProfileIdFromUser(updatedLead.LastModifiedById);
            if (profileId == userID) {
                snapshotsToUpdate.add(createSnapshot.getOldSnapshotFromLead(updatedLead));
            }
        }
        update snapshotsToUpdate;
        
    }
    private string setMobilePhone(string displayPhone)
    {
        string outPhone;
        if (string.isBlank(displayPhone))
        {
            return null;
        }
        if (displayPhone.startsWith('00353'))
        {
            return displayPhone.substring(2);
        }
        if (displayPhone.startsWith('07'))
        {
            outPhone = '44' + displayPhone.substring(1);
            return outPhone;
        }
        return displayPhone;
    }
    
 	private boolean setSMSOptOut(string optouts) {
        if (string.isBlank(optouts))
        {
            return false;
        }
        if (optouts.contains('SMS'))
        {
            return true;
        }
        else {
            return false;
        }
    }
}