@isTest
public class PlanSiteTriggerHandlerTest {
    @isTest static void testInsertPlanSiteStop() {
        Account acc = TestDataUtilityClass.getAccountRec();
        test.startTest();
        insert acc;
        system.Debug('********** Account is ' + acc);
        Opp_Prod_Sites__c site = TestDataUtilityClass.createPlanSite(acc, false);
        site.Status__c = 'Active';
        upsert site;
        site.Credit_Line_Status__c = 'On-Stop';
        upsert site;
        test.stopTest();
    }
    @isTest static void testInsertPlanSiteOK() {
        Account acc = TestDataUtilityClass.getAccountRec();
        test.startTest();
        insert acc;
        system.Debug('********** Account is ' + acc);
        Opp_Prod_Sites__c site = TestDataUtilityClass.createPlanSite(acc, false);
        site.Status__c = 'Active';
        upsert site;
        test.stopTest();
    }
}