global class MarketingMessageExpiry implements Schedulable {
    
    
    global void execute (SchedulableContext sc) {
        
        DateTime currentDate = DateTime.now();
        List<Dealer_Message__c> messages = [Select Rank__c, Start_Date__c , End_Date__c, Link_1_Clicked_Date__c, Link_2_Clicked_Date__c, Retries_Remaining__c from Dealer_Message__c where Rank__c != null and End_Date__c < :currentDate];
        if (messages.size() > 0) {
            MessageRulesEngine rulesEngine = new MessageRulesEngine();
            rulesEngine.Run(messages);
        }
        
    }
}