/**
 * When the leads record is created from webform a copy should be created of the record as it was received
 * This is captured in LeadsSnapshot__c object
 * 24/01/2017 David Quick UKCRM-282 - The creation of the LeadSnapshot__C record has been moved to occur
 *                                    when the application is converted via the Convert button. The code has been moved out to a new class LeadSnapShot_Create
 *                                    and is called from the LeadConvertOverride class leadConv method.
 **/

public class LeadSnapShot_Create {

    public void createSnapshot(Lead newLead) {
        System.debug('********** Create Snapshot');
        LeadSnapshot__c snapshot = new LeadSnapshot__c();
        RecordType fullAppRecordType = [select Id from RecordType where Name = 'Full Application' and SobjectType = 'Lead'];
        if ( newLead != null ){ 
            // create a snapshot to associate the lead and the snapshot we should have some unique column that can be used to cross refer each other
            newLead.LeadAndSnapshotMapField__c = 'LS'+system.currentTimeMillis();
            if(newLead.RecordTypeId == fullAppRecordType.Id){ 
                snapshot = getNewSnapshotFromLead ( newLead );
                insert snapshot;
            }
        }
     }
    
    public LeadSnapshot__c getSnapshotFromMap(String mapper) {
            LeadSnapshot__c shot = [SELECT Id, 
            AssociatedLead__c,
            email__c,
            Company__c,
             Source_Country__c,
             CurrencyIsoCode,
             Address_details_of_other_Franchise_sites__c,
             Agreed_to_Terms_and_Conditions__c,
             Area_Number__c,
             Are_you_VAT_registered__c,
             Auctions_Dealer_purchases_from__c,
             Auctions_Used_in_Last_12_months__c,
             Average_Number_of_Vehicles_in_Stock__c,
             Average_Stock_Turn_Days__c,
             Ave_Vehicle_Sales_Value__c,
             Bank_Name__c,
             Business_Activities__c,
             Business_Address_County__c,
             Business_Address_Line_1__c,
             Business_Address_Line_2__c,
             Business_Address_Line_3__c,
             Business_Address_Postcode__c,
             Business_Address_Town_City__c,
             Business_Name2__c,
             Business_Phone__c,
             Car_Sales_Annual_Volume__c,
             Company_Registration_Number__c,
             Company_Status__c,
             Consumer_Credit_Lincense__c,
             Credit_Limit__c,
             Current_Stock_Funding_Method__c,
             Date_of_Birth__c,
             Dealer_Premises__c,
             Do_you_operate_from_more_than_1_site__c,
             Has_new_vehicle_franchise__c,
             Higher_Limit_Request__c,
             Home_Address_County__c,
             Home_Address_Line_1__c,
             Home_Address_Line_2__c,
             Home_Address_Line_3__c,
             Home_Address_Postcode__c,
             Home_Address_Town_City__c,
             Length_of_Time_with_Bank__c,
             Middle_Initials__c,
             Mobile_Phone__c,
             Name2__c,
             Name_of_Insurer__c,
             New_Vehicle_Sales_annual_volume__c,
             Number_of_Directors__c,
             Number_Years_Trading__c,
             NVF_for_a_min_of_12_months__c,
             Other_Businesses__c,
             Permission_Additional_Director_Searches__c,
             Permission_to_Process__c,
             Phone__c,                       
             Physical_Business_Premises__c,
             Previous_Businesses__c,
             Previous_Trading_Names__c,
             Product__c,
             Promotion_Code__c,                       
             Personal_Guarantee__c,
             Replacement_or_Incremental__c,
             Stage__c,
             Territory__c,
             UK_auctions_dealer_has_buyer_a_c_with__c,
             What_Franchise_s_do_you_hold__c,
             Which_Manheim_branch_referred_you__c,
             Which_Manheim_staff_member_referred_you__c,
             Willing_to_provide_Directors_guarantees__c,
             Do_you_hold_a_dealer_number_from_the_Dep__c,
             Currently_Member_of_SIMI__c,
             TAN_Registered__c,
             LeadAndSnapshotMapField__c,
             Comments__c,
             Data_Source__c,
             Dual_Currency__c,
             Internet_based__c,
             Premises_comments__c,
             Record_type_id__c,
             Recruit_a_Dealer__c,
             Recruit_a_Dealer_Reference__c,
             ROI_Auctions_Dealer_Purchases_from__c,
             TPS_flagged__c,
             VAT_Number__c,                       
             Vehicles_sold_each_month__c,
             Vehicle_Types_Sold__c,
             website__c                       
            FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :mapper];
        return shot;
    }
    
      public LeadSnapshot__c getOldSnapshotFromLead ( Lead newLead ) {
          System.debug('********** Get Old Snapshot');
        LeadSnapshot__c snapshot = new LeadSnapshot__c();
        if ( newLead != null ) {
            if (newLead.LeadAndSnapshotMapField__c != null) {
                snapshot = getSnapshotFromMap(newLead.LeadAndSnapshotMapField__c);
            }
            snapshot.AssociatedLead__c = newLead.Id;
            snapshot.email__c = newLead.email;
            snapshot.Company__c = newLead.company;
            snapshot.Source_Country__c = newLead.sourceCountry__c;
            snapshot.CurrencyIsoCode = newLead.CurrencyIsoCode;
            snapshot.Address_details_of_other_Franchise_sites__c = newLead.Address_details_of_other_Franchise_sites__c;
            snapshot.Agreed_to_Terms_and_Conditions__c = newLead.Agreed_to_Terms_and_Conditions__c;
            snapshot.Area_Number__c = newLead.Area_Number__c;
            snapshot.Are_you_VAT_registered__c = newLead.Are_you_VAT_registered__c;
            snapshot.Auctions_Dealer_purchases_from__c = newLead.Auctions_Dealer_purchases_from__c;
            snapshot.Auctions_Used_in_Last_12_months__c = newLead.Auctions_Used_in_Last_12_months__c ;
            snapshot.Average_Number_of_Vehicles_in_Stock__c = newLead.Average_Number_of_Vehicles_in_Stock__c;
            snapshot.Average_Stock_Turn_Days__c = newLead.Average_Stock_Turn_Days__c;
            snapshot.Ave_Vehicle_Sales_Value__c = newLead.Ave_Vehicle_Sales_Value__c;
            snapshot.Bank_Name__c = newLead.Bank_Name__c;
            snapshot.Business_Activities__c = newLead.Business_Activities__c;
            snapshot.Business_Address_County__c = newLead.Business_Address_County__c;
            snapshot.Business_Address_Line_1__c = newLead.Business_Address_Line_1__c;
            snapshot.Business_Address_Line_2__c = newLead.Business_Address_Line_2__c;
            snapshot.Business_Address_Line_3__c = newLead.Business_Address_Line_3__c;
            snapshot.Business_Address_Postcode__c = newLead.Business_Address_Postcode__c;
            snapshot.Business_Address_Town_City__c = newLead.Business_Address_Town_City__c;
            snapshot.Business_Phone__c = newLead.Business_Phone__c;
            snapshot.Business_Name2__c = newLead.Business_Name2__c;
            snapshot.Car_Sales_Annual_Volume__c = newLead.Car_Sales_Annual_Volume__c;
            snapshot.Company_Registration_Number__c = newLead.Company_Registration_Number__c;
            snapshot.Company_Status__c = newLead.Company_Status__c;
            snapshot.Consumer_Credit_Lincense__c = newLead.Consumer_Credit_Lincense__c;
            snapshot.Credit_Limit__c = newLead.Credit_Limit__c;
            snapshot.Current_Stock_Funding_Method__c = newLead.Current_Stock_Funding_Method__c;
            snapshot.Date_of_Birth__c = newLead.Date_of_Birth__c;
            snapshot.Dealer_Premises__c = newLead.Dealer_Premises__c;
            snapshot.Do_you_operate_from_more_than_1_site__c = newLead.Do_you_operate_from_more_than_1_site__c;
            snapshot.Has_new_vehicle_franchise__c = newLead.Has_new_vehicle_franchise__c;
            snapshot.Higher_Limit_Request__c = newLead.Higher_Limit_Request__c;
            snapshot.Home_Address_County__c = newLead.Home_Address_County__c;
            snapshot.Home_Address_Line_1__c = newLead.Home_Address_Line_1__c;
            snapshot.Home_Address_Line_2__c = newLead.Home_Address_Line_2__c;
            snapshot.Home_Address_Line_3__c = newLead.Home_Address_Line_3__c;
            snapshot.Home_Address_Postcode__c = newLead.Home_Address_Postcode__c;
            snapshot.Home_Address_Town_City__c = newLead.Home_Address_Town_City__c;
            snapshot.Length_of_Time_with_Bank__c = newLead.Length_of_Time_with_Bank__c;
            snapshot.Middle_Initials__c = newLead.Middle_Initials__c;
            snapshot.Mobile_Phone__c = newLead.Mobile_Phone__c;
            snapshot.Name2__c = newLead.Name2__c;
            snapshot.Name_of_Insurer__c = newLead.Name_of_Insurer__c;
            snapshot.New_Vehicle_Sales_annual_volume__c = newLead.New_Vehicle_Sales_annual_volume__c;
            snapshot.Number_of_Directors__c = newLead.Number_of_Directors__c;
            snapshot.Number_Years_Trading__c = newLead.Number_Years_Trading__c;
            snapshot.NVF_for_a_min_of_12_months__c = newLead.NVF_for_a_min_of_12_months__c;
            snapshot.Other_Businesses__c = newLead.Other_Businesses__c;
            snapshot.Permission_Additional_Director_Searches__c = newLead.Permission_Additional_Director_Searches__c;
            snapshot.Permission_to_Process__c = newLead.Permission_to_Process__c;
            snapshot.Phone__c = newLead.Phone;
            snapshot.Physical_Business_Premises__c = newLead.Physical_Business_Premises__c;
            snapshot.Previous_Businesses__c = newLead.Previous_Businesses__c;
            snapshot.Previous_Trading_Names__c = newLead.Previous_Trading_Names__c;
            snapshot.Product__c = newLead.Product__c;
            snapshot.Promotion_Code__c = newLead.Promotion_Code__c;
            snapshot.Personal_Guarantee__c = newLead.Personal_Guarantee__c;
            snapshot.Replacement_or_Incremental__c = newLead.Replacement_or_Incremental__c;
            snapshot.Stage__c = newLead.Stage__c;
            snapshot.Territory__c = newLead.Territory__c;
            snapshot.UK_auctions_dealer_has_buyer_a_c_with__c = newLead.UK_auctions_dealer_has_buyer_a_c_with__c;
            snapshot.What_Franchise_s_do_you_hold__c = newLead.What_Franchise_s_do_you_hold__c;
            snapshot.Which_Manheim_branch_referred_you__c = newLead.Which_Manheim_branch_referred_you__c;
            snapshot.Which_Manheim_staff_member_referred_you__c = newLead.Which_Manheim_staff_member_referred_you__c;
            snapshot.Willing_to_provide_Directors_guarantees__c = newLead.Willing_to_provide_Directors_guarantees__c;
            snapshot.Do_you_hold_a_dealer_number_from_the_Dep__c = newLead.Do_you_hold_a_dealer_number_from_the_Dep__c;
            snapshot.Currently_Member_of_SIMI__c = newLead.Currently_Member_of_SIMI__c;
            snapshot.TAN_Registered__c = newLead.TAN_Registered__c;
            snapshot.LeadAndSnapshotMapField__c = newLead.LeadAndSnapshotMapField__c;
            snapshot.Comments__c = newLead.Comments__c;
            snapshot.Data_Source__c = newLead.Data_Source__c;
            snapshot.Dual_Currency__c = newLead.Dual_Currency__c;
            snapshot.Internet_based__c = newLead.Internet_based__c;
            snapshot.Premises_comments__c = newLead.Premises_comments__c;
            snapshot.Record_type_id__c = newLead.Record_type_id__c;
            snapshot.Recruit_a_Dealer__c = newLead.Recruit_a_Dealer__c;
            snapshot.Recruit_a_Dealer_Reference__c = newLead.Recruit_a_Dealer_Reference__c;
            snapshot.ROI_Auctions_Dealer_Purchases_from__c = newLead.ROI_Auctions_Dealer_Purchases_from__c;
            snapshot.TPS_flagged__c = newLead.TPS_flagged__c;
            snapshot.VAT_Number__c = newLead.VAT_Number__c;
            snapshot.Vehicles_sold_each_month__c = newLead.Vehicles_sold_each_month__c;
            snapshot.Vehicle_Types_Sold__c = newLead.Vehicle_Types_Sold__c;
            snapshot.website__c = newLead.website;
        }
        return snapshot;
    }
        
     public LeadSnapshot__c getNewSnapshotFromLead ( Lead newLead ) {
        System.debug('********** Get New Snapshot');
        LeadSnapshot__c snapshot = new LeadSnapshot__c();
        if ( newLead != null ) {
            snapshot.AssociatedLead__c = newLead.Id;
            snapshot.email__c = newLead.email;
            snapshot.Company__c = newLead.company;
            snapshot.Source_Country__c = newLead.sourceCountry__c;
            snapshot.CurrencyIsoCode = newLead.CurrencyIsoCode;
            snapshot.Address_details_of_other_Franchise_sites__c = newLead.Address_details_of_other_Franchise_sites__c;
            snapshot.Agreed_to_Terms_and_Conditions__c = newLead.Agreed_to_Terms_and_Conditions__c;
            snapshot.Area_Number__c = newLead.Area_Number__c;
            snapshot.Are_you_VAT_registered__c = newLead.Are_you_VAT_registered__c;
            snapshot.Auctions_Dealer_purchases_from__c = newLead.Auctions_Dealer_purchases_from__c;
            snapshot.Auctions_Used_in_Last_12_months__c = newLead.Auctions_Used_in_Last_12_months__c ;
            snapshot.Average_Number_of_Vehicles_in_Stock__c = newLead.Average_Number_of_Vehicles_in_Stock__c;
            snapshot.Average_Stock_Turn_Days__c = newLead.Average_Stock_Turn_Days__c;
            snapshot.Ave_Vehicle_Sales_Value__c = newLead.Ave_Vehicle_Sales_Value__c;
            snapshot.Bank_Name__c = newLead.Bank_Name__c;
            snapshot.Business_Activities__c = newLead.Business_Activities__c;
            snapshot.Business_Address_County__c = newLead.Business_Address_County__c;
            snapshot.Business_Address_Line_1__c = newLead.Business_Address_Line_1__c;
            snapshot.Business_Address_Line_2__c = newLead.Business_Address_Line_2__c;
            snapshot.Business_Address_Line_3__c = newLead.Business_Address_Line_3__c;
            snapshot.Business_Address_Postcode__c = newLead.Business_Address_Postcode__c;
            snapshot.Business_Address_Town_City__c = newLead.Business_Address_Town_City__c;
            snapshot.Business_Name2__c = newLead.Business_Name2__c;
            snapshot.Business_Phone__c = newLead.Business_Phone__c;
            snapshot.Car_Sales_Annual_Volume__c = newLead.Car_Sales_Annual_Volume__c;
            snapshot.Company_Registration_Number__c = newLead.Company_Registration_Number__c;
            snapshot.Company_Status__c = newLead.Company_Status__c;
            snapshot.Consumer_Credit_Lincense__c = newLead.Consumer_Credit_Lincense__c;
            snapshot.Credit_Limit__c = newLead.Credit_Limit__c;
            snapshot.Current_Stock_Funding_Method__c = newLead.Current_Stock_Funding_Method__c;
            snapshot.Date_of_Birth__c = newLead.Date_of_Birth__c;
            snapshot.Dealer_Premises__c = newLead.Dealer_Premises__c;
            snapshot.Do_you_operate_from_more_than_1_site__c = newLead.Do_you_operate_from_more_than_1_site__c;
            snapshot.Has_new_vehicle_franchise__c = newLead.Has_new_vehicle_franchise__c;
            snapshot.Higher_Limit_Request__c = newLead.Higher_Limit_Request__c;
            snapshot.Home_Address_County__c = newLead.Home_Address_County__c;
            snapshot.Home_Address_Line_1__c = newLead.Home_Address_Line_1__c;
            snapshot.Home_Address_Line_2__c = newLead.Home_Address_Line_2__c;
            snapshot.Home_Address_Line_3__c = newLead.Home_Address_Line_3__c;
            snapshot.Home_Address_Postcode__c = newLead.Home_Address_Postcode__c;
            snapshot.Home_Address_Town_City__c = newLead.Home_Address_Town_City__c;
            snapshot.Phone__c = newLead.Phone;
            snapshot.Length_of_Time_with_Bank__c = newLead.Length_of_Time_with_Bank__c;
            snapshot.Middle_Initials__c = newLead.Middle_Initials__c;
            snapshot.Mobile_Phone__c = newLead.Mobile_Phone__c;
            snapshot.Name2__c = newLead.Name2__c;
            snapshot.Name_of_Insurer__c = newLead.Name_of_Insurer__c;
            snapshot.New_Vehicle_Sales_annual_volume__c = newLead.New_Vehicle_Sales_annual_volume__c;
            snapshot.Number_of_Directors__c = newLead.Number_of_Directors__c;
            snapshot.Number_Years_Trading__c = newLead.Number_Years_Trading__c;
            snapshot.NVF_for_a_min_of_12_months__c = newLead.NVF_for_a_min_of_12_months__c;
            snapshot.Other_Businesses__c = newLead.Other_Businesses__c;
            snapshot.Permission_Additional_Director_Searches__c = newLead.Permission_Additional_Director_Searches__c;
            snapshot.Permission_to_Process__c = newLead.Permission_to_Process__c;
            snapshot.Physical_Business_Premises__c = newLead.Physical_Business_Premises__c;
            snapshot.Previous_Businesses__c = newLead.Previous_Businesses__c;
            snapshot.Previous_Trading_Names__c = newLead.Previous_Trading_Names__c;
            snapshot.Product__c = newLead.Product__c;
            snapshot.Promotion_code__c = newLead.Promotion_Code__c;
            snapshot.Personal_Guarantee__c = newLead.Personal_Guarantee__c;
            snapshot.Replacement_or_Incremental__c = newLead.Replacement_or_Incremental__c;
            snapshot.Stage__c = newLead.Stage__c;
            snapshot.Territory__c = newLead.Territory__c;
            snapshot.UK_auctions_dealer_has_buyer_a_c_with__c = newLead.UK_auctions_dealer_has_buyer_a_c_with__c;
            snapshot.What_Franchise_s_do_you_hold__c = newLead.What_Franchise_s_do_you_hold__c;
            snapshot.Which_Manheim_branch_referred_you__c = newLead.Which_Manheim_branch_referred_you__c;
            snapshot.Which_Manheim_staff_member_referred_you__c = newLead.Which_Manheim_staff_member_referred_you__c;
            snapshot.Willing_to_provide_Directors_guarantees__c = newLead.Willing_to_provide_Directors_guarantees__c;
            snapshot.Do_you_hold_a_dealer_number_from_the_Dep__c = newLead.Do_you_hold_a_dealer_number_from_the_Dep__c;
            snapshot.Currently_Member_of_SIMI__c = newLead.Currently_Member_of_SIMI__c;
            snapshot.TAN_Registered__c = newLead.TAN_Registered__c;
            snapshot.LeadAndSnapshotMapField__c = newLead.LeadAndSnapshotMapField__c;
            snapshot.Comments__c = newLead.Comments__c;
            snapshot.Data_Source__c = newLead.Data_Source__c;
            snapshot.Dual_Currency__c = newLead.Dual_Currency__c;
            snapshot.Internet_based__c = newLead.Internet_based__c;
            snapshot.Premises_comments__c = newLead.Premises_comments__c;
            snapshot.Record_type_id__c = newLead.Record_type_id__c;
            snapshot.Recruit_a_Dealer__c = newLead.Recruit_a_Dealer__c;
            snapshot.Recruit_a_Dealer_Reference__c = newLead.Recruit_a_Dealer_Reference__c;
            snapshot.ROI_Auctions_Dealer_Purchases_from__c = newLead.ROI_Auctions_Dealer_Purchases_from__c;
            snapshot.TPS_flagged__c = newLead.TPS_flagged__c;
            snapshot.VAT_Number__c = newLead.VAT_Number__c;
            snapshot.Vehicles_sold_each_month__c = newLead.Vehicles_sold_each_month__c;
            snapshot.Vehicle_Types_Sold__c = newLead.Vehicle_Types_Sold__c;
            snapshot.website__c = newLead.website;
        }
        return snapshot;
    }
    // Create a lead contact record from a Contact for snapshot details
    public void CreateLeadContact(Contact lCont) {
        Id lid = lCont.Lead__c;
        string lName;
        Lead l = [SELECT Id, LeadAndSnapshotMapField__c FROM Lead WHERE Id = :lid ];
        LeadSnapshot__c snap = [SELECT Id, LeadAndSnapshotMapField__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :l.LeadAndSnapshotMapField__c];
        // So now we have the contact, lead, and leadsnapshot we can assemble the leadcontact
        Lead_Contact__c lContact = new Lead_Contact__c();
        lContact.Date_of_Birth__c = lCont.Date_of_Birth__c;
        lContact.Email__c = lCont.Email;
        lContact.First_Name__c = lCont.FirstName;
        lContact.Home_Address_County__c = lCont.Home_Address_County__c;
        lContact.Home_Address_Line_1__c = lCont.Home_Address_Line_1__c;
        lContact.Home_Address_Line_2__c = lCont.Home_Address_Line_2__c;
        lContact.Home_Address_Line_3__c = lCont.Home_Address_Line_3__c;
        lContact.Home_Address_Postcode__c = lCont.Home_Address_Postcode__c;
        lContact.Home_Address_Town_City__c = lCont.Home_Address_Town_City__c;
        lContact.Last_Name__c = lCont.LastName;
        lContact.LeadSnapshot_Parent__c = snap.Id;
        lContact.Middle_Initials__c = lCont.Middle_Initials__c;
        lContact.Salutation__c = lCont.Salutation;
        lName = lCont.Salutation + ' ' + lCont.FirstName + ' ' + lCont.LastName;
        lContact.Name = lName;
        lContact.Home_Phone__c = lCont.Home_Phone__c;
        lContact.Mobile_Phone__c = lCont.Mobile_Phone__c;
        lContact.Business_Phone__c = lCont.Business_Phone__c;
        insert lContact;
    }
    
    public string getCurrentProfileName() {
        Id id1 = userinfo.getProfileId();
        Profile p = [Select Name from Profile where Id = :id1][0];
        return p.Name;
    }
    
    // Get the profile ID from a User ID
    public Id getProfileIdFromUser(Id userId) {
        try
        {
            Profile p = [select Id, Name from Profile where Id in (select ProfileId from User where Id = :userId)];
            return p.Id;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    
    // Get the profile ID from a profile name
    public Id getProfileId(String profName) {
        try
        {
            Profile p = [select Id, Name from Profile where Name = :profName];
            return p.Id;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}