/**
 * @description         Trigger handler factory class.
 * @author              Adam Marchbanks (adam.marchbanks@sandyxsystems.co.uk)
 * @copyright           2011 Sandyx Systems Ltd. All Rights Reserved.
 *
 * IMPORTANT: This code is the property of Sandyx Systems Ltd and does not
 * grant you to share this code with any third party or grant permission
 * to modify its contents without written consent of Sandyx Systems Ltd.
 * This class header must remain in the file at all times, removal will
 * break the terms and conditions set out for use of this code. 
 */
public with sharing class TriggerFactory {

    /**
     * @description     Class which will be instanstiated if no handler can be located
     */
    private class EmptyTriggerHandler extends TriggerHandler {}
    
    
    /**
     * @description     Locates and instantiates the registered handler for an sObject.
     * @param soType    The sObject type to locate the handler for
     */
    public static void createHandler(Schema.sObjectType soType){
        getHandler(soType).execute();
    }

    /**
     * @description     Instantiates the correct handler for the sObject type being processed.
     * @param soType    The sObject type to instantiate the handler for
     * @return          Instance of a new trigger handler for the requested sObject type
     */
    private static TriggerHandler getHandler(Schema.sObjectType soType){
 	    if(soType == Account.sObjectType) {
            return new AccountTriggerHandler();
        }
          
        if(soType == Opportunity.sObjectType) {
            return new OpportunityTriggerHandler();
        }

        if(soType == Contact.sObjectType) {
            return new ContactTriggerHandler();
        }

        if(soType == Opp_Prod_Sites__c.sObjectType) {
            return new PlanSiteTriggerHandler();
        }
            
        if(soType == Opp_Prod_Sites__c.sObjectType) {
            return new PlanSiteTriggerHandler();
        }

        if(soType == Lead.sObjectType) {
            return new LeadsTriggerHandler(); 
        }
        if(soType == LeadSnapshot__c.sObjectType) {
            return new LeadSnapshotTriggerHandler(); 
        }
        if(soType == Event.sObjectType) {
            return new EventTriggerHandler(); 
        }
        if(soType == Task.sObjectType) {
            return new TaskTriggerHandler(); 
        }
        if(soType == Lead_Contact__c.sObjectType) {
            return new LeadContactTriggerHandler(); 
        }
        if(soType == SOAR_Contact__c.sObjectType) {
            return new SOARContactTriggerHandler(); 
        }

        return new EmptyTriggerHandler();
        
    }
    
}