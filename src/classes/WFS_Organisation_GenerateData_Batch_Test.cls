@isTest
private class WFS_Organisation_GenerateData_Batch_Test {

    static testMethod void myUnitTest() {
        Test.startTest();
        
        WFS_TestData.createWFSTestData();
        
        WFS_Organisation_GenerateData_Batch.runBatch();
        
        Test.stopTest();
        
        list<WFS_Organisation_Data__c> orgData = [select id from WFS_Organisation_Data__c];
        system.assertEquals(8, orgData.size());
    }
}