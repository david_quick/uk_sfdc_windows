/**
* File Name   :    CreateNewOpportunity.cls
*
*
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 29/11/2016 Simon Wilby        add Awaiting Approval - Simple Renewals to SOQL
* 0.2 10/01/2017 Simon Wilby        modified url redirect to auto populate the close date with today
*/

public with sharing class CreateNewOpportunity {
    private final Opportunity o;

    public CreateNewOpportunity(ApexPages.StandardController stdController) {
        this.o = (Opportunity)stdController.getRecord();
    }

    public List<Recordtype> rts {
        get{
            if(rts==null)
            {
                rts=[SELECT Id, Name,description FROM Recordtype WHERE Sobjecttype='Opportunity' and name not in ('Awaiting Approval','Awaiting Approval - Auction House','Awaiting Approval - Simple Renewals')];
            }
            return rts;     
        }
        private set;
    }
        

    public List<selectOption> getOppRT() {
        List<selectOption> options = new List<selectOption>(); 
        for (RecordType r : rts)
        {
            options.add(new selectOption(r.Id, r.Name)); 
        }
        return options; 
    }

    public pagereference cont(){
         PageReference prc = new PageReference('/006/e?retURL='+ApexPages.currentPage().getParameters().get('retURL')+'&RecordType='+this.o.recordtypeid+'&opp9='+((System.Now()).dayGmt())+'/'+((System.Now()).monthGmt())+'/'+((System.Now()).yearGmt())+'&ent=Opportunity&nooverride=1');
         return prc;

    }

}