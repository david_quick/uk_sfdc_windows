public with sharing class WFS_TestData {
    public static void createWFSTestData(){
        integer loopMax = 2;
        
        map<string, id> recordtypes = new map<string, id>();
        for(RecordType rt : [select Name, id from RecordType where SobjectType = 'Account' and IsActive = true]){
            recordtypes.put(rt.Name, rt.id);
        }
        
        
        //COMPANY
        Account company = new Account();
        company.RecordTypeId = recordtypes.get('Companies');
        company.Name = 'Joe\'s Hot Garage';
        company.Phone = '01234 567 890';
        company.WFS_Ref__c = 'X123';
        company.Business_Address_Line_1__c = 'Line 1';
        company.Business_Address_Line_2__c = 'Line 2';
        company.Business_Address_Line_3__c = 'Line 3';
        company.Business_Address_County__c = 'Northfordshire';
        company.Business_Address_Postcode__c = 'W1 GAN';
        company.Business_Address_Town_City__c = 'Wigan';
        company.VAT_Registration_Number__c = 'VAT-123';
        company.Company_Reg_No__c = 123;
        company.Trading_As__c = 'Joe\'s Hot Garage';
        company.Reference_ID__c = 'Brzeczyszczykiewicz';
        insert company;
        
        
        //BANK ACCOUNTS
        list<Bank_Account__c> bankAccounts = new list<Bank_Account__c>();
        for(integer i = 0; i < loopMax; i++){
            Bank_Account__c bankAccount = new Bank_Account__c();
            
            bankAccount.Name = 'D0000' + string.valueOf(i) + '-BANK';
            bankAccount.Bank_Account_Type__c = 'Normal';
            bankAccount.Sort_Code__c = '00000' + string.valueOf(i);
            bankAccount.Account_Number__c = '0000000' + string.valueOf(i);
            
            if(i == 0){
                bankAccount.Primary_Account__c = true;
            }
            else{
                bankAccount.Primary_Account__c = false;
            }
            
            bankAccount.Account__c = company.Id;
            
            bankAccounts.add(bankAccount);
        }
        insert bankAccounts;
        
        
        //MANDATES
        list<Mandate__c> mandates = new list<Mandate__c>();
        for(integer i = 0; i < loopMax; i++){
            Mandate__c mandate = new Mandate__c();
            
            mandate.Bank_Account__c = bankAccounts[i].Id;
            mandates.add(mandate);
        }
        insert mandates;
        
        //CENTRES
        list<Account> centres = new list<account>();
        for(integer i = 0; i < loopMax; i++){
            Account centre = new Account();
            
            centre.Name = 'Joe-' + string.valueOf(i);
            centre.RecordTypeId = recordtypes.get('Centres');
            centre.Phone = '01234 567 88'  + string.valueOf(i);
            centre.WFS_Ref__c = 'Y32' + string.valueOf(i);
            centre.Business_Address_Line_1__c = 'Line 1' + string.valueOf(i);
            
            //Leave one of these null for testing
            if(i == 0){
                centre.Business_Address_Line_2__c = 'Line 2' + string.valueOf(i);
            }
            else{
                centre.Business_Address_Line_3__c = 'Line 3' + string.valueOf(i);
            }
            
            centre.Business_Address_County__c = 'Somewhere' + string.valueOf(i);
            centre.Business_Address_Postcode__c = 'W2 GAN' + string.valueOf(i);
            centre.Business_Address_Town_City__c = 'Wigan' + string.valueOf(i);
            centre.VAT_Registration_Number__c = 'VAT-32' + string.valueOf(i);
            centre.Company_Reg_No__c = 320 + i;
            centre.ParentId = company.Id;
            centre.Reference_ID__c = 'Acc_refID';
            centre.Trading_As__c = 'Joe\'s Hot Garage No.' + string.valueOf(i);
            centres.add(centre);
        }
        insert centres;
        
        
        //OPPORTUNITY - needed to create Plan
        Opportunity opp = new Opportunity();
        opp.Name = company.Name;
        opp.AccountId = company.Id;
        opp.CloseDate = date.today();
        opp.Credit_Limit_x__c = 30000;
        opp.StageName ='Application In Progress';
        insert opp;
        
        
        //PLAN (Opp_Product__c) - needed to create Plan Site
        Opp_Product__c oppProd = new Opp_Product__c();
        oppProd.Name = 'Joe\'s - Cars & LCVs';
        oppProd.Opportunity__c = opp.Id;
        oppProd.Credit_Status__c = 'Active';
        oppProd.Facilities__c = 'NGC001W';
        oppProd.Limit__c = 150000.0;
        oppProd.Start_Date__c = Date.newInstance(2015, 02, 14);
        insert oppProd;
        opp.StageName='Final Document Approval';         
        opp.Managerial_Sign_Off__c= true;       
        opp.Managerial_Sign_Off_by__c= Userinfo.getUserId();        
        opp.Managerial_Sign_off_Date__c = Date.today().addDays(10);         
        update opp;         
        opp.StageName ='Account Live';      
        update opp; 
        
        //PLAN SITES (Opp_Prod_Sites__c)
        list<Opp_Prod_Sites__c> planSites = new list<Opp_Prod_Sites__c>();
        for(integer i = 0; i < loopMax; i++){
            Opp_Prod_Sites__c planSite = new Opp_Prod_Sites__c();
            planSite.Company__c = company.Id;
            planSite.Plan_Site__c = centres[i].Id;
            planSite.Mandate__c = mandates[i].Id;
            planSite.Bank_Account__c = bankAccounts[i].Id;
            planSite.Opp_Live__c = true;
            planSite.Status__c = 'Active';
            planSite.Opp_Product__c = oppProd.Id;
            planSite.Credit_Line_Status__c = 'OK';
            planSite.Limit_for_the_product_site__c=150000.0;
            planSites.add(planSite);
        }
        insert planSites;

    }

    public void genereateTestData(){
        integer loopMax = 1;
        
        map<string, id> recordtypes = new map<string, id>();
        for(RecordType rt : [select Name, id from RecordType where SobjectType = 'Account' and IsActive = true]){
            recordtypes.put(rt.Name, rt.id);
        }
        
        
        //COMPANY
        Account company = new Account();
        company.RecordTypeId = recordtypes.get('Companies');
        company.Name = 'Joe\'s Hot Garage';
        company.Phone = '01234 567 890';
        company.WFS_Ref__c = 'X123';
        company.Business_Address_Line_1__c = 'Line 1';
        company.Business_Address_Line_2__c = 'Line 2';
        company.Business_Address_Line_3__c = 'Line 3';
        company.Business_Address_County__c = 'Northfordshire';
        company.Business_Address_Postcode__c = 'W1 GAN';
        company.Business_Address_Town_City__c = 'Wigan';
        company.VAT_Registration_Number__c = 'VAT-123';
        company.Company_Reg_No__c = 123;
        company.Trading_As__c = 'Joe\'s Hot Garage';
        company.Reference_ID__c = 'Brzeczyszczykiewicz';
        insert company;
        
        
        //BANK ACCOUNTS
        list<Bank_Account__c> bankAccounts = new list<Bank_Account__c>();
        for(integer i = 0; i < loopMax; i++){
            Bank_Account__c bankAccount = new Bank_Account__c();
            
            bankAccount.Name = 'D0000' + string.valueOf(i) + '-BANK';
            bankAccount.Bank_Account_Type__c = 'Normal';
            bankAccount.Sort_Code__c = '00000' + string.valueOf(i);
            bankAccount.Account_Number__c = '0000000' + string.valueOf(i);
            
            if(i == 0){
                bankAccount.Primary_Account__c = true;
            }
            else{
                bankAccount.Primary_Account__c = false;
            }
            
            bankAccount.Account__c = company.Id;
            
            bankAccounts.add(bankAccount);
        }
        insert bankAccounts;
        
        
        //MANDATES
        list<Mandate__c> mandates = new list<Mandate__c>();
        for(integer i = 0; i < loopMax; i++){
            Mandate__c mandate = new Mandate__c();
            
            mandate.Bank_Account__c = bankAccounts[i].Id;
            mandates.add(mandate);
        }
        insert mandates;
        
        //CENTRES
        list<Account> centres = new list<account>();
        for(integer i = 0; i < loopMax; i++){
            Account centre = new Account();
            
            centre.Name = 'Joe-' + string.valueOf(i);
            centre.RecordTypeId = recordtypes.get('Centres');
            centre.Phone = '01234 567 88'  + string.valueOf(i);
            centre.WFS_Ref__c = 'Y32' + string.valueOf(i);
            centre.Business_Address_Line_1__c = 'Line 1' + string.valueOf(i);
            
            //Leave one of these null for testing
            if(i == 0){
                centre.Business_Address_Line_2__c = 'Line 2' + string.valueOf(i);
            }
            else{
                centre.Business_Address_Line_3__c = 'Line 3' + string.valueOf(i);
            }
            
            centre.Business_Address_County__c = 'Somewhere' + string.valueOf(i);
            centre.Business_Address_Postcode__c = 'W2 GAN' + string.valueOf(i);
            centre.Business_Address_Town_City__c = 'Wigan' + string.valueOf(i);
            centre.VAT_Registration_Number__c = 'VAT-32' + string.valueOf(i);
            centre.Company_Reg_No__c = 320 + i;
            centre.ParentId = company.Id;
            centre.Trading_As__c = 'Joe\'s Hot Garage No.' + string.valueOf(i);
            centres.add(centre);
        }
        insert centres;
        
        
        //OPPORTUNITY - needed to create Plan
        Opportunity opp = new Opportunity();
        opp.Name = company.Name;
        opp.Credit_Limit_x__c = 300000;
        opp.AccountId = company.Id;
        opp.CloseDate = date.today();
        opp.StageName = 'Test';
        insert opp;
        
        
        //PLAN (Opp_Product__c) - needed to create Plan Site
        Opp_Product__c oppProd = new Opp_Product__c();
        oppProd.Name = 'Joe\'s - Cars & LCVs';
        oppProd.Opportunity__c = opp.Id;
        oppProd.Credit_Status__c = 'Active';
        oppProd.Facilities__c = 'NGC001W';
        oppProd.Limit__c = 150000.0;
        oppProd.Start_Date__c = Date.newInstance(2015, 02, 14);
        insert oppProd;
    
        
        //PLAN SITES (Opp_Prod_Sites__c)
        list<Opp_Prod_Sites__c> planSites = new list<Opp_Prod_Sites__c>();
        for(integer i = 0; i < loopMax; i++){
            Opp_Prod_Sites__c planSite = new Opp_Prod_Sites__c();
            planSite.Company__c = company.Id;
            planSite.Plan_Site__c = centres[i].Id;
            planSite.Mandate__c = mandates[i].Id;
            planSite.Bank_Account__c = bankAccounts[i].Id;
            planSite.Opp_Live__c = true;
            planSite.Status__c = 'Active';
            planSite.Opp_Product__c = oppProd.Id;
            planSite.Credit_Line_Status__c = 'OK';
            planSite.Limit_for_the_product_site__c=150000.0;
            planSites.add(planSite);
        }
        insert planSites;

    }
}