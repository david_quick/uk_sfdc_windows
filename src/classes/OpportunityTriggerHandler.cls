/**
* File Name   :    OpportunityTriggerHandler.cls
* Description :    [NIL-9] - Added the before insert trigger and the setOpportunityApprovalLevel 
*                  
*
* Modification Log
* ============================================================================
* Ver Date       Author           Modification
* --- ---------- ---------------- --------------------------
* 0.1 27/05/2015 Vincent Spehner  before insert
* 0.2 29/05/2015 Vincent Spehner  Refactor the trigger to use methods
* 0.3 01/06/2051 Vincent Spehner  Add formated comments
* 0.4 22/11/2016 Simon Wilby      add Simple Renewals and Credit Limit Reduction approval levels
*/
public class OpportunityTriggerHandler extends TriggerHandler {

    static Id auctionRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AUCTION_HOUSE LIMIT 1].id;
    static Id auctionAwaitingRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL_AH LIMIT 1].Id;
    static Id simpleRenewalAwaitingRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_AWAITING_APPROVAL_SR LIMIT 1].Id;
    static Id simpleRenewalRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_SIMPLE_RENEWALS LIMIT 1].Id;
    static Id creditlimitreduction = [SELECT Id FROM RecordType WHERE DeveloperName = :UtilConstants.OPPTY_RECORD_TYPE_CREDIT_LIMIT_REDUCTION LIMIT 1].Id;

    static Map<String, Minimum_Limit__c> Minimum_Limit = Minimum_Limit__c.getAll();

    public OpportunityTriggerHandler() {
        preventRecursiveTrigger(false);
    }

    public void beforeInsert(List<SObject> newObjects, Map<Id, SObject> newMap) {
         // Original Set Overall Rating method
        setOverallRating(newObjects);

        // Set the Opportunity Approval level based on Country, Overall Rating and Opportunity amount
        setOpportunityApprovalLevel(newObjects);
        // Check for mimimum credit limit 
        checkMinimumLimit(newObjects);
    }


    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        // Original beforeUpdate Trigger refactored in a dedicated private method 
        // Set the Date and User for OfferLetter, Agreement, DDI/Mandate, Acknowledgment, Personal_Guarantee, Joint & Several Guarantee, 
        // Omnibus Guarantee, Landlords Waiver, Legal Charges, Legal Priorit, Statement of Net Worth, Bank Details
        setOverallRating(newObjects);
        setDateAndUsers(oldObjects, newObjects, oldMap, newMap);

        // Set the Opportunity Approval level based on Country, Overall Rating and Opportunity amount
        setOpportunityApprovalLevel(newObjects);

        // Check for mimimum credit limit 
        checkMinimumLimit(newObjects);

        //Check to see the corect number of approvers are met in order to change the Stage to Credit Approved
        checkChangeStageCreditApproved(newObjects);
    }

    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        Set<Id> OppId = new Set<Id>();
        Set<Id> Accids = new Set<Id>();
        List<Opp_Prod_Sites__c> planstoupdate = new List<Opp_Prod_Sites__c>();

        for (Opportunity o:(List<Opportunity>) oldObjects) {
            if (o.StageName != 'Account Live' && ((Opportunity) newmap.get(o.id)).StageName == 'Account Live') {
                OppId.add(o.id);
                Accids.add(o.accountid);
            }
        }

        Map<Id, Account> accswithplans = new Map<Id, Account>([
                select id, (select Opp_Live__c, Reference__c, Status__c from Plan_Sites1__r)
                from Account
                where id in :accids
        ]);

        List<Opp_Prod_Sites__c> planSite = [
                Select Company__c,
                        reference__c,
                        Opp_Product__r.Opportunity__c,
                        Opp_Product__r.Opportunity__r.AccountId
                FROM Opp_Prod_Sites__c
                WHERE Opp_Product__r.Opportunity__c IN :OppId
        ];


        for (Opportunity o:(List<Opportunity>) oldObjects) {
            for (Opp_Prod_Sites__c op : planSite) {
                if (op.reference__c.endsWith('_PENDING') && (op.Opp_Product__r.Opportunity__c == o.id)) {
                    system.debug('Found pending ' + op.id);
                    for (Opp_Prod_Sites__c ap: (accswithplans.get(o.AccountID)).Plan_Sites1__r) {
                        system.debug(ap.reference__c);
                        system.debug(op.reference__c);
                        if (ap.reference__c == op.reference__c.replaceAll('_PENDING', '')) {
                            ap.reference__c = ap.reference__c + '_SUPERSEDED' + Date.today();
                            ap.status__c = 'Superseded';
                            planstoupdate.add(ap);
                        }
                    }
                }
            }
        }
  
        if (planstoupdate.size() > 0) update planstoupdate;

        for (Opp_Prod_Sites__c ops : planSite) {
            ops.Opp_Live__c = TRUE;
            ops.Reference__c = ops.Reference__c.replaceAll('_PENDING', '');
            ops.Status__c = 'Active';
        }
        try {
            update planSite;
        }
        catch (Exception ex) {
			apexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.Error, UtilConstants.PLAN_SITE_INCORRECT)
            );
        }
    }

    /** 
    @description            Set the Approval Level of an inserted or updated Opportunity 

    @param newObjects       Define the Approval level based on Record Type, Credit_Manager_Decline, Country, Overall Rating and Amount
    @returns                void
    */
    private void setOpportunityApprovalLevel(List<SObject> newObjects) {
        List<Id> opptyList = new List<Id>();

        for (Opportunity oppty : (List<Opportunity>) newObjects) {
            opptyList.add(oppty.accountId);
        }

        Map<Id, Account> relatedAccounts = new Map<ID, Account>([select Id, Name, Source_Country__c from Account where Id in :opptyList]);
        for (Opportunity oppty : (List<Opportunity>) newObjects) {
     
            if (oppty.recordTypeId == auctionRecordTypeId || oppty.recordTypeId == auctionAwaitingRecordTypeId || oppty.Previous_RecordType__c == auctionRecordTypeId) {
                oppty.Approval_Level__c = 5;
                return;
            }
            if (oppty.Credit_Manager_Decline__c == true) {
                oppty.Approval_Level__c = 0;
                return;
            }
            if ((oppty.recordTypeId == simpleRenewalRecordTypeId || oppty.recordTypeId == simpleRenewalAwaitingRecordTypeId) && oppty.Credit_Limit_x__c <= 350000) {
                oppty.Approval_Level__c = 7;
                return;
            }
            if (oppty.recordTypeId == creditlimitreduction) {
                oppty.Approval_Level__c = 6;
                return;
            }    
            oppty.Approval_Level__c = ApprovalSettings.getApprovalLevel(relatedAccounts.get(oppty.accountId).Source_Country__c, oppty.Overall_Rating_Result_x__c, oppty.Credit_Limit_x__c);

            AggregateResult[] groupedResults = [
                    SELECT COUNT(Id)cnt
                    FROM Approval_Stage__c
                    WHERE Opportunity__c = :oppty.Id
                    AND Status__c = 'Escalated'
            ];
            Object countEscalatedApprovals = groupedResults[0].get('cnt');

            if (oppty.Approval_Level__c + Integer.valueOf(countEscalatedApprovals) > 5) {
                oppty.Approval_Level__c = 5;
            } else {
                oppty.Approval_Level__c = oppty.Approval_Level__c + Integer.valueOf(countEscalatedApprovals);
            }

        }

    }

    private void setOverallRating(List<SObject> newObjects) {
        for (Opportunity o:(List<Opportunity>) newObjects) {
            String overallrating = 'Amber';
            if (o.Bureau_Ratingx__c == 'Green' && o.Judgemental_Ratingx__c == 'Green') overallrating = 'Green';
            if (o.Bureau_Ratingx__c == 'Green' && o.Judgemental_Ratingx__c == 'Red') overallrating = 'Red';
            if (o.Bureau_Ratingx__c == 'Red' && o.Judgemental_Ratingx__c == 'Green') overallrating = 'Red';
            if (o.Bureau_Ratingx__c == 'Red' && o.Judgemental_Ratingx__c == 'Amber') overallrating = 'Red';
            if (o.Bureau_Ratingx__c == 'Red' && o.Judgemental_Ratingx__c == 'Red') overallrating = 'Red';
            if (o.Bureau_Ratingx__c == 'Amber' && o.Judgemental_Ratingx__c == 'Red') overallrating = 'Red';

            o.Overall_Rating_Result_x__c = overallrating;

        }
    }

    private void checkMinimumLimit(List<SObject> newObjects) {
        List<Id> opptyList = new List<Id>();

        for (Opportunity oppty : (List<Opportunity>) newObjects) {
            opptyList.add(oppty.accountId);
        }

        Map<Id, Account> relatedAccounts = new Map<ID, Account>([select Id, Name, Source_Country__c from Account where Id in :opptyList]);
        for (Opportunity oppty : (List<Opportunity>) newObjects) {
            if (oppty.Credit_Limit_x__c < getMinCredit(relatedAccounts.get(oppty.AccountId).Source_Country__c)) {
                oppty.addError(UtilConstants.LIMIT_CREDIT_OPPORTUNITY_MESSAGE_1 + getMinCredit(relatedAccounts.get(oppty.AccountId).Source_Country__c) + ' for ' + relatedAccounts.get(oppty.AccountId).Source_Country__c);
            }
        }
    }

    private static Integer getMinCredit(String country) {
        for (Minimum_Limit__c ml :Minimum_Limit.values()) {
            if (ml.Country__c == country) {
                return integer.valueOf(ml.Minimum__c);
            }
        }
        return 0;
    }

    private void checkChangeStageCreditApproved(List<SObject> newObjects) {
        Map<Id, Opportunity> changingOpps = new Map<Id, Opportunity>();
        Map<Id, Integer> oppApprovedMap = new Map<Id, Integer>();
        Set<Id> opptySetId = new Set<Id>();
        Set<Id> accountSetIds = new Set<Id>();
        for (Opportunity currentOpp :(List<Opportunity>) newObjects) {
            if (currentOpp.StageName == UtilConstants.OPPORTUNITY_STATUS_CREDIT_APPROVED) {
                changingOpps.put(currentOpp.Id, currentOpp);
                opptySetId.add(currentOpp.Id);
                accountSetIds.add(currentOpp.AccountId);
                oppApprovedMap.put(currentOpp.Id, 0);
            }
        }

        Map<Id, Account> relatedAccounts = new Map<ID, Account>([SELECT Id, Name, Source_Country__c FROM Account WHERE Id IN :accountSetIds]);

        for (Opportunity currentOpp :changingOpps.values()) {
            if (currentOpp.Approval_Level__c == 0) {
                currentOpp.addError(UtilConstants.CANT_CHANGE_CREDIT_APPROVED_FOR_OPP);
            } else {
                if (currentOpp.Is_Credit_Approved__c == false) {
                    currentOpp.addError(UtilConstants.CANT_CHANGE_CREDIT_APPROVED_FOR_OPP_OTHER1 + ApprovalSettings.getApproversMin(relatedAccounts.get(currentOpp.AccountId).Source_Country__c, (Integer) currentOpp.Approval_Level__c) + UtilConstants.CANT_CHANGE_CREDIT_APPROVED_FOR_OPP_OTHER2);
                }
            }
        }
    }

    private void setDateAndUsers(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        String myDate = Datetime.now().format('dd/MM/YYYY HH:mm');
        String userAndDate = UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ', ' + myDate;
        for (Opportunity o:(List<Opportunity>) newObjects) {
            if (o.recordtypeid != ((Opportunity) oldmap.get(o.id)).recordtypeid)o.Previous_RecordType__c = ((Opportunity) oldmap.get(o.id)).recordtypeid;

            if (o.Approval_Received__c != ((Opportunity) oldmap.get(o.id)).Approval_Received__c) {
                if (o.Approval_Received__c == true)
                    o.recordtypeid = ((Opportunity) oldmap.get(o.id)).Previous_RecordType__c;
            }


            //setting OfferLetter Dates/User
            if (o.Offer_Letter_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Sent__c == false) {
                o.Offer_Letter_Sent_Date_User__c = userAndDate;
            }

            if (o.Offer_Letter_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Sent__c == true) {
                o.Offer_Letter_Sent_Date_User__c = '';
            }
            if (o.Offer_Letter_Received__c == true && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Received__c == false) {
                o.Offer_Letter_Received_Date_User__c = userAndDate;
            }

            if (o.Offer_Letter_Received__c == false && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Received__c == true) {
                o.Offer_Letter_Received_Date_User__c = '';
            }

            if (o.Offer_Letter_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Reviewed__c == false) {
                o.Offer_Letter_Reviewed_Date_User__c = userAndDate;
            }

            if (o.Offer_Letter_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Reviewed__c == true) {
                o.Offer_Letter_Reviewed_Date_User__c = '';
            }

            if (o.Offer_Letter_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Signed_Off__c == false) {
                o.Offer_Letter_Signed_Off_Date_User__c = userAndDate;
            }

            if (o.Offer_Letter_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Offer_Letter_Signed_Off__c == true) {
                o.Offer_Letter_Signed_Off_Date_User__c = '';
            }
            //setting Agreement Dates/User
            if (o.Agreement_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Agreement_Sent__c == false) {
                o.Agreement_Sent_Date_User__c = userAndDate;
            }

            if (o.Agreement_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Agreement_Sent__c == true) {
                o.Agreement_Sent_Date_User__c = '';
            }
            if (o.Agreement_Received__c == true && ((Opportunity) oldmap.get(o.id)).Agreement_Received__c == false) {
                o.Agreement_Received_Date_User__c = userAndDate;
            }

            if (o.Agreement_Received__c == false && ((Opportunity) oldmap.get(o.id)).Agreement_Received__c == true) {
                o.Agreement_Received_Date_User__c = '';
            }
            if (o.Agreement_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Agreement_Reviewed__c == false) {
                o.Agreement_Reviewed_Date_User__c = userAndDate;
            }

            if (o.Agreement_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Agreement_Reviewed__c == true) {
                o.Agreement_Reviewed_Date_User__c = '';
            }
            if (o.Agreement_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Agreement_Signed_Off__c == false) {
                o.Agreement_Signed_Off_Date_User__c = userAndDate;
            }

            if (o.Agreement_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Agreement_Signed_Off__c == true) {
                o.Agreement_Signed_Off_Date_User__c = '';
            }

            //setting DDI/MAndate Dates/User
            if (o.DDI_Mandate_Sent__c == true && ((Opportunity) oldmap.get(o.id)).DDI_Mandate_Sent__c == false) {
                o.DDI_Mandate_Sent_Date_User__c = userAndDate;
            }

            if (o.DDI_Mandate_Sent__c == false && ((Opportunity) oldmap.get(o.id)).DDI_Mandate_Sent__c == true) {
                o.DDI_Mandate_Sent_Date_User__c = '';
            }
            if (o.Mandate_Received__c == true && ((Opportunity) oldmap.get(o.id)).Mandate_Received__c == false) {
                o.Mandate_Received_Date_User__c = userAndDate;
            }
            if (o.Mandate_Received__c == false && ((Opportunity) oldmap.get(o.id)).Mandate_Received__c == true) {
                o.Mandate_Received_Date_User__c = '';
            }
            if (o.DDI_Mandate_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).DDI_Mandate_Reviewed__c == false) {
                o.DDI_Mandate_Reviewed_Date_User__c = userAndDate;
            }
            if (o.DDI_Mandate_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).DDI_Mandate_Reviewed__c == true) {
                o.DDI_Mandate_Reviewed_Date_User__c = '';
            }
            if (o.DDI_Mandate_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).DDI_Mandate_Signed_Off__c == false) {
                o.DDI_Mandate_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.DDI_Mandate_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).DDI_Mandate_Signed_Off__c == true) {
                o.DDI_Mandate_Signed_Off_Date_User__c = '';
            }

            //setting Acknowledgement Dates/User
            if (o.Acknowledgement_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Sent__c == false) {
                o.Acknowledgement_Sent_Date_User__c = userAndDate;
            }
            if (o.Acknowledgement_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Sent__c == true) {
                o.Acknowledgement_Sent_Date_User__c = '';
            }
            if (o.Acknowledgement_Received_del__c == true && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Received_del__c == false) {
                o.Acknowledgement_Received_Date_User__c = userAndDate;
            }
            if (o.Acknowledgement_Received_del__c == false && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Received_del__c == true) {
                o.Acknowledgement_Received_Date_User__c = '';
            }
            if (o.Acknowledgement_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Reviewed__c == false) {
                o.Acknowledgement_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Acknowledgement_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Reviewed__c == true) {
                o.Acknowledgement_Reviewed_Date_User__c = '';
            }
            if (o.Acknowledgement_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Signed_Off__c == false) {
                o.Acknowledgement_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Acknowledgement_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Acknowledgement_Signed_Off__c == true) {
                o.Acknowledgement_Signed_Off_Date_User__c = '';
            }

            //setting Personal_Guarantee Dates/User
            if (o.Personal_Guarantee_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Sent__c == false) {
                o.Personal_Guarantee_Sent_Date_User__c = userAndDate;
            }
            if (o.Personal_Guarantee_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Sent__c == true) {
                o.Personal_Guarantee_Sent_Date_User__c = '';
            }
            if (o.Personal_Guarantee_Received__c == true && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Received__c == false) {
                o.Personal_Guarantee_Received_Date_User__c = userAndDate;
            }
            if (o.Personal_Guarantee_Received__c == false && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Received__c == true) {
                o.Personal_Guarantee_Received_Date_User__c = '';
            }
            if (o.Personal_Guarantee_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Reviewed__c == false) {
                o.Personal_Guarantee_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Personal_Guarantee_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Reviewed__c == true) {
                o.Personal_Guarantee_Reviewed_Date_User__c = '';
            }
            if (o.Personal_Guarantee_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Signed_Off__c == false) {
                o.Personal_Guarantee_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Personal_Guarantee_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Personal_Guarantee_Signed_Off__c == true) {
                o.Personal_Guarantee_Signed_Off_Date_User__c = '';
            }

            //setting Joint & Several Guarantee Dates/User
            if (o.Joint_Several_Guarantee_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Sent__c == false) {
                o.Joint_Several_G_tee_Sent_Date_User__c = userAndDate;
            }
            if (o.Joint_Several_Guarantee_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Sent__c == true) {
                o.Joint_Several_G_tee_Sent_Date_User__c = '';
            }
            if (o.Joint_Several_Guarantee_Received__c == true && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Received__c == false) {
                o.Joint_Several_G_tee_Received_Date_User__c = userAndDate;
            }
            if (o.Joint_Several_Guarantee_Received__c == false && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Received__c == true) {
                o.Joint_Several_G_tee_Received_Date_User__c = '';
            }
            if (o.Joint_Several_Guarantee_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Reviewed__c == false) {
                o.Joint_Several_G_tee_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Joint_Several_Guarantee_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Reviewed__c == true) {
                o.Joint_Several_G_tee_Reviewed_Date_User__c = '';
            }
            if (o.Joint_Several_Guarantee_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Signed_Off__c == false) {
                o.Joint_Several_G_tee_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Joint_Several_Guarantee_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Joint_Several_Guarantee_Signed_Off__c == true) {
                o.Joint_Several_G_tee_Signed_Off_Date_User__c = '';
            }

            //setting Omnibus Guarantee Dates/User
            if (o.Omnibus_Guarantee_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Sent__c == false) {
                o.Omnibus_Guarantee_Sent_Date_User__c = userAndDate;
            }
            if (o.Omnibus_Guarantee_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Sent__c == true) {
                o.Omnibus_Guarantee_Sent_Date_User__c = '';
            }
            if (o.Omnibus_Guarantee_Received__c == true && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Received__c == false) {
                o.Omnibus_Guarantee_Received_Date_User__c = userAndDate;
            }
            if (o.Omnibus_Guarantee_Received__c == false && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Received__c == true) {
                o.Omnibus_Guarantee_Received_Date_User__c = '';
            }
            if (o.Omnibus_Guarantee_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Reviewed__c == false) {
                o.Omnibus_Guaranteee_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Omnibus_Guarantee_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Reviewed__c == true) {
                o.Omnibus_Guaranteee_Reviewed_Date_User__c = '';
            }
            if (o.Omnibus_Guarantee_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Signed_Off__c == false) {
                o.Omnibus_Guarantee_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Omnibus_Guarantee_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Omnibus_Guarantee_Signed_Off__c == true) {
                o.Omnibus_Guarantee_Signed_Off_Date_User__c = '';
            }

            //setting Corporate  Guarantee Dates/User
            if (o.Corporate_Guarantee_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Sent__c == false) {
                o.Corporate_Guarantee_Sent_Date_User__c = userAndDate;
            }
            if (o.Corporate_Guarantee_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Sent__c == true) {
                o.Corporate_Guarantee_Sent_Date_User__c = '';
            }
            if (o.Corporate_Guarantee_Received__c == true && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Received__c == false) {
                o.Corporate_Guarantee_Received_Date_User__c = userAndDate;
            }
            if (o.Corporate_Guarantee_Received__c == false && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Received__c == true) {
                o.Corporate_Guarantee_Received_Date_User__c = '';
            }
            if (o.Corporate_Guarantee_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Reviewed__c == false) {
                o.Corporate_Guarantee_Reviewed_User_Date__c = userAndDate;
            }
            if (o.Corporate_Guarantee_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Reviewed__c == true) {
                o.Corporate_Guarantee_Reviewed_User_Date__c = '';
            }
            if (o.Corporate_Guarantee_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Signed_Off__c == false) {
                o.Corporate_Guarantee_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Corporate_Guarantee_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Corporate_Guarantee_Signed_Off__c == true) {
                o.Corporate_Guarantee_Signed_Off_Date_User__c = '';
            }

            //setting Landlords Waiver Dates/User
            if (o.Landlords_Waiver_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver_Sent__c == false) {
                o.Landlords_Waiver_Sent_Date_User__c = userAndDate;
            }
            if (o.Landlords_Waiver_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver_Sent__c == true) {
                o.Landlords_Waiver_Sent_Date_User__c = '';
            }
            if (o.Landlords_Waiver__c == true && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver__c == false) {
                o.Landlords_Waiver_Received_Date_User__c = userAndDate;
            }
            if (o.Landlords_Waiver__c == false && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver__c == true) {
                o.Landlords_Waiver_Received_Date_User__c = '';
            }
            if (o.Landlords_Waiver_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver_Reviewed__c == false) {
                o.Landlords_Waiver_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Landlords_Waiver_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver_Reviewed__c == true) {
                o.Landlords_Waiver_Reviewed_Date_User__c = '';
            }
            if (o.Landlords_Waiver_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver_Signed_Off__c == false) {
                o.Landlords_Waiver_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Landlords_Waiver_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Landlords_Waiver_Signed_Off__c == true) {
                o.Landlords_Waiver_Signed_Off_Date_User__c = '';
            }

            //setting Legal Charges Dates/User
            if (o.Legal_Charges_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Sent__c == false) {
                o.Legal_Charges_Sent_Date_User__c = userAndDate;
            }
            if (o.Legal_Charges_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Sent__c == true) {
                o.Legal_Charges_Sent_Date_User__c = '';
            }
            if (o.Legal_Charges_Received__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Received__c == false) {
                o.Legal_Charges_Received_Date_User__c = userAndDate;
            }
            if (o.Legal_Charges_Received__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Received__c == true) {
                o.Legal_Charges_Received_Date_User__c = '';
            }
            if (o.Legal_Charges_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Reviewed__c == false) {
                o.Legal_Charges_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Legal_Charges_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Reviewed__c == true) {
                o.Legal_Charges_Reviewed_Date_User__c = '';
            }
            if (o.Legal_Charges_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Signed_Off__c == false) {
                o.Legal_Charges_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Legal_Charges_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Charges_Signed_Off__c == true) {
                o.Legal_Charges_Signed_Off_Date_User__c = '';
            }

            //setting Legal Priority Dates/User
            if (o.Legal_Priority_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Sent__c == false) {
                o.Legal_Priority_Sent_Date_User__c = userAndDate;
            }
            if (o.Legal_Priority_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Sent__c == true) {
                o.Legal_Priority_Sent_Date_User__c = '';
            }
            if (o.Legal_Priority_Received__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Received__c == false) {
                o.Legal_Priority_Received_Date_User__c = userAndDate;
            }
            if (o.Legal_Priority_Received__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Received__c == true) {
                o.Legal_Priority_Received_Date_User__c = '';
            }
            if (o.Legal_Priority_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Reviewed__c == false) {
                o.Legal_Priority_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Legal_Priority_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Reviewed__c == true) {
                o.Legal_Priority_Reviewed_Date_User__c = '';
            }
            if (o.Legal_Priority_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Signed_Off__c == false) {
                o.Legal_Priority_Signed_Off_Date_User__c = userAndDate;
            }
            if (o.Legal_Priority_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Legal_Priority_Signed_Off__c == true) {
                o.Legal_Priority_Signed_Off_Date_User__c = '';
            }

            //setting Statement of Net Worth Dates/User
            if (o.Statement_of_Net_Worth_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Sent__c == false) {
                o.Statement_of_Net_Worth_Sent_Date_User__c = userAndDate;
            }
            if (o.Statement_of_Net_Worth_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Sent__c == true) {
                o.Statement_of_Net_Worth_Sent_Date_User__c = '';
            }
            if (o.Statement_of_Net_Worth_Received__c == true && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Received__c == false) {
                o.Statement_of_Net_Worth_R_cd_Date_User__c = userAndDate;
            }
            if (o.Statement_of_Net_Worth_Received__c == false && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Received__c == true) {
                o.Statement_of_Net_Worth_R_cd_Date_User__c = '';
            }
            if (o.Statement_of_Net_Worth_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Reviewed__c == false) {
                o.Statement_of_Net_Worth_Rev_d_Date_User__c = userAndDate;
            }
            if (o.Statement_of_Net_Worth_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Reviewed__c == true) {
                o.Statement_of_Net_Worth_Rev_d_Date_User__c = '';
            }
            if (o.Statement_of_Net_Worth_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Signed_Off__c == false) {
                o.Statement_of_Net_Worth_S_nd_Off_Date_Use__c = userAndDate;
            }
            if (o.Statement_of_Net_Worth_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Statement_of_Net_Worth_Signed_Off__c == true) {
                o.Statement_of_Net_Worth_S_nd_Off_Date_Use__c = '';
            }

            //setting Bank Details Dates/User
            if (o.Bank_Details_Request_Sent__c == true && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Sent__c == false) {
                o.Bank_Details_Sent_Date_User__c = userAndDate;
            }
            if (o.Bank_Details_Request_Sent__c == false && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Sent__c == true) {
                o.Bank_Details_Sent_Date_User__c = '';
            }
            if (o.Bank_Details_Request_Received__c == true && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Received__c == false) {
                o.Bank_Details_Request_Received_Date_User__c = userAndDate;
            }
            if (o.Bank_Details_Request_Received__c == false && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Received__c == true) {
                o.Bank_Details_Request_Received_Date_User__c = '';
            }
            if (o.Bank_Details_Request_Reviewed__c == true && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Reviewed__c == false) {
                o.Bank_Details_Request_Reviewed_Date_User__c = userAndDate;
            }
            if (o.Bank_Details_Request_Reviewed__c == false && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Reviewed__c == true) {
                o.Bank_Details_Request_Reviewed_Date_User__c = '';
            }
            if (o.Bank_Details_Request_Signed_Off__c == true && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Signed_Off__c == false) {
                o.Bank_Details_Request_Signed_Off_Date_Use__c = userAndDate;
            }
            if (o.Bank_Details_Request_Signed_Off__c == false && ((Opportunity) oldmap.get(o.id)).Bank_Details_Request_Signed_Off__c == true) {
                o.Bank_Details_Request_Signed_Off_Date_Use__c = '';
            }
        }

    }

}