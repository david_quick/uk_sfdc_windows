global class WFS_Organisation_Delete_Batch implements Database.Batchable<sObject>{

/*
    Delete all WFS_Organisation_Data__c records and then repopulate them by calling the WFS_Organisation_GenerateData_Batch.runBatch()
*/

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id from WFS_Organisation_Data__c]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC){
        //Test classes are not allowed to execute more than one batch job per test.  So,  if this batch is
        //tested,  prevent this final piece running
        if(!test.isRunningTest()){
            WFS_Organisation_GenerateData_Batch.runBatch();
        }
    }
    
    //Call to run the batch job on an ad hoc basis
    global static void runBatch(){
        WFS_Organisation_Delete_Batch deleteBatchJob = new WFS_Organisation_Delete_Batch(); 
        database.executebatch(deleteBatchJob);
    }

}