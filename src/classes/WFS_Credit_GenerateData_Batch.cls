/**
*
* Author:       Piotr Czechumski
* Date:         30/01/2015
* Description:  Batch job to generate Credit file data for all Opp_Prod_Sites__c.
*               Records are ordered by name (autonumber), first records has start tag, last one has ending tag
*               all between contains XML nodes "CreditLimit"
*
**/
global class WFS_Credit_GenerateData_Batch implements Database.Batchable<sObject> {
    
    global WFS_Credit_GenerateData_Batch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if ([select count() from WFS_Credit_Data__c] != 0){
            // Table have first be set empty
            throw new Existing_CreditData_Exception('There were existing records found in Credit data table.'); 
        }
        else{
            // create starting tag element
            WFS_Credit_Data__c creditData = new WFS_Credit_Data__c();
            creditData.XML__c = '<creditData>';
            insert creditData;

        }
        return Database.getQueryLocator([select Id from Opp_Product__c 
                                            where Opportunity__c in (select id from opportunity where StageName ='Account Live') order by Id asc]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        list<Id> planIds = new list<Id>();
        for(sObject obj: scope){
            planIds.add(string.valueOf(obj.get('Id')));
        }
        
        WFS_Credit_XML.processCreditItems(planIds);
    }
    
    global void finish(Database.BatchableContext BC) {
        // create end tag element
        WFS_Credit_Data__c creditData = new WFS_Credit_Data__c();
        creditData.XML__c = '</creditData>';
        insert creditData;
    }
    
    public class Existing_CreditData_Exception extends Exception{

    }
}