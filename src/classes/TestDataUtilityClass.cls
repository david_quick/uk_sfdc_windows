public class TestDataUtilityClass {
        
        Static List<Account> testAccList = new List<Account>();
        Static Account testAccRec;
        
        Static Data_Quality__c testDataQualityCSRec;
        Static List<Data_Quality__c> testDataQualityCSList = new List<Data_Quality__c>();
        
        Static Contact testConRec;
        Static List<Contact> testConList = new List<Contact>();
        
        Static Set<String> testFieldNameSet;
        
        public Static Map<String,Data_Quality__c> testMap = new Map<String,Data_Quality__c>();
        
        Static Lead testLeadRec;
        
        
        
        
        /** List of Account Method **/
         public Static List<Account> getListOfAccounts() {
            Recordtype Comprt = [Select r.SobjectType, r.Name, r.Id, r.DeveloperName From RecordType r
                      where SobjectType='Account'
                      and DeveloperName = 'Companies'];
            for(Integer i=0;i<10;i++){
                testAccRec = new Account();
                testAccRec.Name = 'Test'+i;
                testAccRec.Source_Country__c = 'UK';
                testAccRec.Trading_As__c  = 'testing'+i;
                testAccRec.recordtypeid = Comprt.id;
                testAccRec.Phone = '123456';
                testAccRec.Business_Address_Line_1__c = 'Test Address Line 1';
                testAccRec.Business_Address_Postcode__c = 'BA14RT';
                testAccList.add(testAccRec);
            } 
            
            System.debug('********** Account List Returned ****' + testAccList);
            return testAccList;
        }
        
        /**  Account Method **/
        public Static Account getAccountRec(){
            Recordtype Comprt = [Select r.SobjectType, r.Name, r.Id, r.DeveloperName From RecordType r
                      where SobjectType='Account'
                      and DeveloperName = 'Companies'];
                testAccRec = new Account();
                testAccRec.Source_Country__c = 'UK';
                testAccRec.Name = 'Test';
                testAccRec.Trading_As__c  = 'Testing';
                testAccRec.recordtypeid = Comprt.id;
                testAccRec.Phone = '123456';
                testAccRec.Business_Address_Line_1__c = 'Test Address Line 1';
                testAccRec.Business_Address_Postcode__c = 'BA14RT';
                 
            System.debug('********** Account Returned ****' + testAccRec);
            return testAccRec;
        }
        
        /** Data Quality Custom Setting Method Method **/
        public Static Map<String,Data_Quality__c> getListOfDataQualityCS(){
            testDataQualityCSRec = new Data_Quality__c(Name= 'Account');
            testDataQualityCSRec.Field_1__c ='Account Name';
            testDataQualityCSRec.Field_2__c ='Business Address Line 1';
            testDataQualityCSRec.Field_3__c ='Business Address Line 2';
            testDataQualityCSRec.Field_4__c ='Business Address Line 3';
            testDataQualityCSRec.Field_5__c ='Business Address Town/City';
            testDataQualityCSRec.Field_6__c ='Business Address County';
            testDataQualityCSRec.Field_7__c ='Business Address Postcode';
            testMap.put('Account',testDataQualityCSRec);
            testDataQualityCSList.add(testDataQualityCSRec);
            testDataQualityCSRec = new Data_Quality__c(Name= 'Contact');
            testDataQualityCSRec.Field_1__c ='First Name';
            testDataQualityCSRec.Field_2__c ='Last Name';
            testMap.put('Contact',testDataQualityCSRec);
            testDataQualityCSList.add(testDataQualityCSRec);
            testDataQualityCSRec = new Data_Quality__c(Name= 'Free_Users');
            testDataQualityCSRec.Field_1__c ='Jenny Gibson';
            testDataQualityCSRec.Field_2__c ='Lucy Brown';
            testDataQualityCSRec.Field_2__c ='Tquila Support';
            testMap.put('Free_Users',testDataQualityCSRec);
            testDataQualityCSList.add(testDataQualityCSRec);
            return testMap;
        }
        
        /**List of Contact Method **/
        public Static List<Contact> getListOfContacts(){
             for(Integer i=0;i<10;i++){
                testConRec = new Contact();
                testConRec.Accountid = getAccountRec().id;
                testConRec.FirstName ='Test First Name'+i;
                testConRec.LastName ='Test Last Name'+i;
                testConRec.Phone ='123456';
                testConList.add(testConRec);
             }
             return testConList;
        }
        
         /** Contact Method **/
         public Static Contact getContactRec(){
              testConRec = new Contact();
              testConRec.Accountid = getAccountRec().id;
               
              testConRec.FirstName ='Test First Name';
              testConRec.LastName ='Test Last Name';
              testConRec.Phone ='123456';
              return testConRec;
         } 
     	public Static Contact getContactRec(string mobilePhone){
              testConRec = new Contact();
              testConRec.Accountid = getAccountRec().id;
               
              testConRec.FirstName ='Test First Name';
              testConRec.LastName ='Test Last Name';
              testConRec.Phone ='123456';
              testConRec.Mobile_Phone__c = mobilePhone;
              return testConRec;
         } 
         
              /** Lead Method **/
         public Static Lead getLeadRec()
         {
            testLeadRec = new Lead();
            testLeadRec.FirstName ='Test First Name';
            testLeadRec.LastName ='Test Last Name';
            testLeadRec.Phone ='123456';
            testLeadRec.Company ='ABC Corp Xion';
                
            return testLeadRec;
         } 
        
         public Static Map<String,Data_Quality__c> getDataQualityCS_Account(){
            
            testDataQualityCSRec = new Data_Quality__c(Name= 'Account');
            testDataQualityCSRec.Field_1__c ='Account Name';
            testDataQualityCSRec.Field_2__c ='Business Address Line 1';
            testDataQualityCSRec.Field_3__c ='Business Address Line 2';
            testDataQualityCSRec.Field_4__c ='Business Address Line 3';
            testDataQualityCSRec.Field_5__c ='Business Address Town/City';
            testDataQualityCSRec.Field_6__c ='Business Address County';
            testDataQualityCSRec.Field_7__c ='Business Address Postcode';
            
            //insert testDataQualityCSRec;
            testMap.put('Account',testDataQualityCSRec);
            return testMap;
         }
            
         public Static Set<String>  getFieldNamesFromCSTest(){
            testFieldNameSet = new Set<String>();
            testFieldNameSet.add('Account Name');
            testFieldNameSet.add('Business Address Line 1');
            testFieldNameSet.add('Business Address Line 2');
            testFieldNameSet.add('Business Address Line 3');
            testFieldNameSet.add('Business Address Town/City');
            testFieldNameSet.add('Business Address County');
            testFieldNameSet.add('Business Address Postcode');
            return testFieldNameSet;
         }   

         public Static Marketing_Message__c getMarketingMessage() {

           Marketing_Message__c message = new Marketing_Message__c();
           message.Active__c = true;
           message.Closed_Retry_Limit__c = 10;
           message.Expiry_Days__c = 5;
           message.Minimum_Account_Open_Duration__c = 10;
           message.Text__c = 'Test text';
           message.Retry_Limit__c = 10;
           message.Priority__c = 5;
           message.Link_1__c= 'www.link1.com';
           message.Link_2__c = 'www.link2.com';
           message.Link1_Name__c= 'link 1 name';
           message.Link2_name__c = 'link 2 name';
           message.PresentedClosed__c = 10;
           message.Interaction_1__c = 20;
           message.Interaction_2__c = 30;
           insert message;

           return message;

         }
         
         public Static Dealer_Message__c  getDealerMessage(Marketing_Message__c mm) {

           Account acc = getAccountRec() ; 
           Lead led = getLeadRec();
           led.id=null;
           insert acc;
           insert led;
           Contact c = getContactRec();
           c.Accountid = acc.id;

           c.Lead__c = led.id;
           insert c;
           
           Dealer_Message__c message = new Dealer_Message__c();
           message.Start_Date__c = DateTime.now();
           message.End_Date__c = DateTime.now().addMonths(1);
           message.Contact__c = c.Id;
           message.Marketing_Message__c = getMarketingMessage().Id;
           message.Closed_Retries_Remaining__c = 5;
           message.Retries_Remaining__c = 5;
           message.Text__c = 'Test text';
           message.Rank__c = 1;
           message.Tasks_Required__c = true;
           message.PresentedClosed__c = System.today().addDays((Integer)mm.PresentedClosed__c);
           message.Interaction_1__c =  System.today().addDays((Integer)mm.Interaction_1__c);
           message.Interaction_2__c =  System.today().addDays((Integer)mm.Interaction_2__c);
           insert message;
         
           return message;

         }
         
          public Static void createOnHoldMessage() {

           Marketing_Message__c message = new Marketing_Message__c();
           message.Active__c = true;
           message.Closed_Retry_Limit__c = 10000;
           message.Expiry_Days__c = 1000;
           message.Minimum_Account_Open_Duration__c = 0;
           message.Text__c = 'On hold text';
           message.Retry_Limit__c = 10000;
           message.Priority__c = 1;
           message.On_Hold_Applied__c = true;
          
           insert message;

          

         }

         public Static  Opp_Prod_Sites__c  createPlanSite(Account a, boolean onStop) {

            Opportunity o = new Opportunity();
            o.Name = 'test opp';
            o.AccountId = a.id;
            o.Credit_Limit_x__c = 20000;
            o.StageName = 'Awaiting Credit Approval';
            o.Overall_Rating_Result_x__c = '3';
            o.CloseDate = Date.today();
            insert o;
            
            
            Opp_Product__c plan = new Opp_Product__c();
            plan.Name = 'Test Plan';
            plan.Opportunity__c = o.Id;
            insert plan;
            plan.Credit_Status__c = 'OK';
            if (onStop) {
                     plan.Credit_Status__c = 'On Stop';
            }
            update plan;

            Opp_Prod_Sites__c site = new Opp_Prod_Sites__c();
            site.Company__c = a.Id;
            site.Opp_Product__c = plan.Id;
            site.Reference__c = 'test reference xxx';
            insert site;
            
            site.Type__c = 'FIXED';
            update site;

            return site;

         }
         
        public static Account createAccount(String recordTypeName) {

          Recordtype Comprt = [Select r.SobjectType, r.Name, r.Id, r.DeveloperName From RecordType r
                                where SobjectType='Account'
                                and DeveloperName = :recordTypeName];
          testAccRec = new Account();
          testAccRec.Name = 'Test';
          testAccRec.Trading_As__c  = 'testing';
          testAccRec.recordtypeid = Comprt.id;
          testAccRec.Phone = '123456';
          testAccRec.Source_Country__c = 'UK';
          testAccRec.Business_Address_Line_1__c = 'Test';
          testAccRec.Business_Address_Postcode__c = 'TEST';

          return testAccRec;

        }

        public static Contact createContact(Account a) {

          testConRec = new Contact();
          testConRec.Accountid = a.id;
          testConRec.FirstName ='Test First Name';
          testConRec.LastName ='Test Last Name';
          testConRec.Phone ='123456';

          return testConRec;

        }

        public static Opportunity createOpportunity(Account a) {
          Opportunity testOppRec = new Opportunity();
          testOppRec.Name = 'test opp';
          testOppRec.AccountId = a.id;
          testOppRec.Credit_Limit_x__c = 10000;
          testOppRec.StageName = 'Awaiting Credit Approval';
          testOppRec.CloseDate = Date.today();
          
          return testOppRec;
          
        }
    }