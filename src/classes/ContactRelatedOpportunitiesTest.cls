@isTest
public class ContactRelatedOpportunitiesTest {
    @isTest public static void testInitialize(){
        Test.startTest();
        PageReference pageRef = Page.ContactRelatedOpportunitiesDisplay;
        Test.setCurrentPage(pageRef);
        Account acc = TestData.createAccount();
        insert acc;
        Contact c = TestData.createContact(acc, 'Petra', 'Dumbkopfski');
        insert c;
        Opportunity opp = TestData.createOpportunity(acc.Id,10000,'Awaiting approval');
        insert opp;
        Related_Contact__c relC = TestData.createRelatedContact(c);
        insert relC;
        Role__c r = TestData.createCustomRole(c,opp);
        insert r;
        ApexPages.StandardController standard = new ApexPages.StandardController(c);
        ContactRelatedOpportunities controller = new ContactRelatedOpportunities(standard);
        List<Related_Contact__c> relatedContacts = controller.RelatedCons;
        List<Role__c> relatedRoles = controller.RelatedOpportunities;
        Test.stopTest();
        System.assert(relatedContacts.size() != 0);
        System.assert(relatedRoles.size() != 0);
    }
}