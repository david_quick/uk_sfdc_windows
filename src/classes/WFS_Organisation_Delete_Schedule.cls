global class WFS_Organisation_Delete_Schedule implements Schedulable{
    
    global void execute(SchedulableContext SC) {
      WFS_Organisation_Delete_Batch deleteBatchJob = new WFS_Organisation_Delete_Batch(); 
      database.executebatch(deleteBatchJob);
    }
    
    global static string scheduleJob(string cron){
        //Cron string format = Seconds|Minutes|Hours|Day_of_month|Month|Day_of_week|optional_year
        //e.g cron = '0 0 01 * * ?' = everyday at 1am
        
        WFS_Organisation_Delete_Schedule deleteSchedule = new WFS_Organisation_Delete_Schedule();

        return system.schedule('Regenerate WFS Organisation Data', cron, deleteSchedule);
    }
}