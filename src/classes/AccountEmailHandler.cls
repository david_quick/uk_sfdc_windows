/*This class willl handle emails received for accounts and creates a task for the
 Customer Services User*/
 global class AccountEmailHandler implements Messaging.InboundEmailHandler 
 {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
      {
          Messaging.InboundEmailResult response= new Messaging.InboundEmailResult();
        try{
          Account acc = new Account();
          acc = [Select ID from Account where Account.Business_Email__c =:email.fromAddress Limit 1] ; 
          User CustService;
          If (test.isRunningTest())
              {CustService= [Select id from user where UserName ='TestUserforTask@ngc.com.tskuser' Limit 1];}
          else    
              {CustService= [Select id from user where UserName =: Label.Customer_Services_User Limit 1];}

          If (acc.id != null  &&  CustService !=null)
          {
              Task newTask = New Task(
              Subject= Email.Subject,
              Send_Mail_Email_handler__c=true,
              WhatId = acc.Id,
              Task_Area__c= 'CST - Admin Tasks',
              OwnerId = CustService.id ,
              Status='Not Started');
              /*Sets the task completion date to today or tommorow if received after 4 PM*/
              If(System.now().hour() > 15) 
                 {newTask.ActivityDate = system.today().adddays(1); }// sets the task for next day after 4 pm.}
              else 
                 {newTask.ActivityDate = system.today();}        // Otherwise sets the task for the same day
              /**Sets the Emails body to the task description*/   
                  {newTask.Description= email.plainTextBody;} 
 
              
              //Set EmailHeader.triggerUserEmail to true for sending mail to the task user
               Database.DMLOptions dmlo = new Database.DMLOptions();
               dmlo.EmailHeader.triggerUserEmail = true;
               insert newTask ; // Inserts the task 
               
               // Checks and inserts any attachments  received in the mail
               
               //Handles the Binary Attachments and add them to the all attachments list
                List<Attachment> allAttachments = new List<Attachment>();
               if(email.BinaryAttachments!=null && !email.BinaryAttachments.isempty())
               {
               for(Messaging.InboundEmail.BinaryAttachment binAttachment: email.binaryAttachments)
                   {
                     Attachment attachment = new Attachment();
                     attachment.Body = binAttachment.Body;
                     attachment.Name = binAttachment.fileName;
                     attachment.ParentId = newTask.id; 
                     attachment.ownerid = CustService.id;
                     allattachments.add(attachment );
                     
                    }
                 
               }
                
              //Handles the text Attachments and add them to the all attachments list
              if(email.textAttachments!=null && !email.textAttachments.isempty())
               {
               for(Messaging.InboundEmail.textAttachment textAttachment: email.textAttachments)
                   {
                     Attachment attachment = new Attachment();
                     attachment.Body = Blob.valueOf(textAttachment.charset);
                     attachment.Name = textAttachment.fileName;
                     attachment.ParentId = newTask.id; 
                     attachment.ownerid = CustService.id;
                     allattachments.add(attachment );
                    }
                 
               }
              
              if(!allattachments.isEmpty())
                   {insert allattachments;}
              
               response.success =  true;
               response.message = 'Thank you for your e mail, the Customer Service Team will review the contents and if we have any questions we will come back to you.';
              
          }
      }
    Catch(Exception ex)       
    {    response.success =  false;
         
    }
 return response;
      }
  }