public with sharing class LeadEquifax {

    Lead l = new Lead();
    
    public String  compref{get;set;}
    public String stage {get;set;}
    
    public LeadEquifax(ApexPages.StandardController stdCtrl){
        stdCtrl.addFields(new String[] {'Company_Registration_Number__c','Company'});
        this.l = (Lead)stdCtrl.getRecord();
      
    }

    public List<EquifaxCompWrapper> compdetails{
        get{
            if(compdetails==null)
            {
                compdetails = Equifax.parseEquifaxCompanyList(l.Company);
            }
            return compdetails;
            
        }
        set;
    }

    public List<EquifaxPersonWrapper> persondetails{
        get{
            if(persondetails==null && compref!=null)
            {
                persondetails = Equifax.parseEquifaxCompDirectors(compref);
            }
            return persondetails;
            
        }
        set;
    }

    public PageReference DirectorAddresses(){
        persondetails = equifax.parseDirectorIdentifiers(persondetails);
        id retid = Equifax.parseEquifaxResponse(compref,l.id,null,null,persondetails);
        return new pagereference('/'+retid);
    }


    public PageReference EquifaxDirectors(){
        //id retid = Equifax.parseEquifaxResponse(l.Company_Registration_Number__c,l.id,null,null);
        persondetails=Equifax.parseEquifaxCompDirectors(compref);
        return null;        
    }
    
    public PageReference EquifaxNonLim(){
        id retid = Equifax.parseEquifaxNonLimResponse(compref,l.id,null,null);
        //persondetails=Equifax.parseEquifaxCompDirectors(compref);
        return new pagereference('/'+retid);
    }
    
    public PageReference EquifaxReport(){
        //id retid = Equifax.parseEquifaxResponse(l.Company_Registration_Number__c,l.id,null,null);
        id retid = Equifax.parseEquifaxResponse(compref,l.id,null,null);
        return new pagereference('/'+retid);
    }
}