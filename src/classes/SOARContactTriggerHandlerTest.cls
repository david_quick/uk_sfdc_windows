@isTest
public class SOARContactTriggerHandlerTest {
    @isTest static void insertNewContact() {
        SOAR_Contact__c contact = TestData.createSOARContact();
        test.StartTest();
        System.debug('********** Inserting SOAR contact');
        insert contact;
        SOAR_Contact__c cont1 = [SELECT Id, Name, Email__c, Mobile_Phone__c, Home_Phone__c, Processed__c FROM SOAR_Contact__c WHERE Id = :contact.Id];
        test.stopTest();
        System.debug('********** Contact is ' + cont1);
        System.assert(cont1.Name != null);
    }
}