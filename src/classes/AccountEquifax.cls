public with sharing class AccountEquifax {
    
    Account a = new Account();
    
    public String compref{get;set;}
    public String stage {get;set;}
    
    public AccountEquifax(ApexPages.StandardController stdCtrl){
        stdCtrl.addFields(new String[] {'Company_Registration_Number__c','Name'});
        this.a = (Account)stdCtrl.getRecord();
      
    }

    public List<EquifaxCompWrapper> compdetails{
        get{
            if(compdetails==null)
            {
                compdetails = Equifax.parseEquifaxCompanyList(a.Name);
            }
            return compdetails;
            
        }
        set;
    }

    public List<EquifaxPersonWrapper> persondetails{
        get{
            if(persondetails==null && compref!=null)
            {
                persondetails = Equifax.parseEquifaxCompDirectors(compref);
            }
            return persondetails;
            
        }
        set;
    }

    public PageReference DirectorAddresses(){
        persondetails = equifax.parseDirectorIdentifiers(persondetails);
        id retid = Equifax.parseEquifaxResponse(compref,null,null,a.id,persondetails);
        return new pagereference('/'+retid);
    }


    public PageReference EquifaxDirectors(){
        //id retid = Equifax.parseEquifaxResponse(l.Company_Registration_Number__c,l.id,null,null);
        persondetails=Equifax.parseEquifaxCompDirectors(compref);
        return null;        
    }
    
    public PageReference EquifaxNonLim(){
        id retid = Equifax.parseEquifaxNonLimResponse(compref,null,null,a.id);
        //persondetails=Equifax.parseEquifaxCompDirectors(compref);
        return new pagereference('/'+retid);
    }
    
    public PageReference EquifaxReport(){
        //id retid = Equifax.parseEquifaxResponse(l.Company_Registration_Number__c,l.id,null,null);
        id retid = Equifax.parseEquifaxResponse(compref,null,null,a.id);
        return new pagereference('/'+retid);
    }
}