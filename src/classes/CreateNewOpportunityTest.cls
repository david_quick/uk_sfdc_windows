@isTest
public class CreateNewOpportunityTest {
    @isTest public static void testInitialize(){
        Test.startTest();
        PageReference pageRef = Page.Create_New_Opportunity;
        Test.setCurrentPage(pageRef);
        Account acc = TestData.createAccount();
        insert acc;
        Opportunity opp = TestData.createOpportunity(acc.Id,10000,'Awaiting approval');
        insert opp;
        ApexPages.StandardController standard = new ApexPages.StandardController(opp);
        CreateNewOpportunity controller = new CreateNewOpportunity(standard);
        List<RecordType> rts = controller.rts;
        System.assert(rts.size() > 0);
        Test.stopTest();
    }
    @isTest public static void testReportTypes(){
        Test.startTest();
        PageReference pageRef = Page.Create_New_Opportunity;
        Test.setCurrentPage(pageRef);
        Account acc = TestData.createAccount();
        insert acc;
        Opportunity opp = TestData.createOpportunity(acc.Id,10000,'Awaiting approval');
        insert opp;
        ApexPages.StandardController standard = new ApexPages.StandardController(opp);
        CreateNewOpportunity controller = new CreateNewOpportunity(standard);
        List<RecordType> rts = controller.rts;
        System.assert(rts.size() > 0);
        Test.stopTest();
    }
    @isTest public static void testOppRecordtypes(){
        Test.startTest();
        PageReference pageRef = Page.Create_New_Opportunity;
        Test.setCurrentPage(pageRef);
        Account acc = TestData.createAccount();
        insert acc;
        Opportunity opp = TestData.createOpportunity(acc.Id,10000,'Awaiting approval');
        insert opp;
        ApexPages.StandardController standard = new ApexPages.StandardController(opp);
        CreateNewOpportunity controller = new CreateNewOpportunity(standard);
        List<selectOption> oppRT = controller.getOppRT();
        System.assert(oppRT.size() > 0);
        Test.stopTest();
    }
    @isTest public static void testPageReference(){
        Test.startTest();
        PageReference pageRef = Page.Create_New_Opportunity;
        Test.setCurrentPage(pageRef);
        Account acc = TestData.createAccount();
        insert acc;
        Opportunity opp = TestData.createOpportunity(acc.Id,10000,'Awaiting approval');
        insert opp;
        ApexPages.StandardController standard = new ApexPages.StandardController(opp);
        CreateNewOpportunity controller = new CreateNewOpportunity(standard);
        PageReference ref = controller.cont();
        System.assert(ref != null);
        Test.stopTest();
    }
}