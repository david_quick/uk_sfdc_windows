public with sharing class AccountRelatedOpportunities {
public Account a { get; set; }
    //public Contact c { get; set; }

    public AccountRelatedOpportunities(ApexPages.StandardController controller) {
        // get the contact
        a = [select id, name,  ownerId from Account
            where id = : controller.getId()];
        //c = [select id, name from Contact Where Account.Id = :o.Account.Id];
    }
    
    public Map<Id, Contact> contacts {
        get{
            if(contacts==null){
                contacts = new Map<Id, Contact>([select id, name from Contact Where Account.Id = :a.Id]);
            }
            return contacts;
        }
        set;
    }
    
    public list<Related_Contact__c> RelatedCons {
        get{
            if(RelatedCons==null){
                    RelatedCons = [select Related_Contact_1__c,Related_Contact_2__c from Related_Contact__c where Related_Contact_1__c IN :contacts.keySet() or Related_Contact_2__c IN :contacts.keySet()];
                }
            return RelatedCons;
        }
        set;
    }

    public list<Role__c> RelatedOpportunities {
        get{
            if(RelatedOpportunities==null){
                Set<Id> Contactids = new Set<Id>();  // set to hold contact ids
                
                for(Related_Contact__c rc: RelatedCons){
                    Contactids.add(rc.Related_Contact_1__c); // add contact 1 to set
                    Contactids.add(rc.Related_Contact_2__c); // add contact2 to set
                }
                
                RelatedOpportunities = [select Contact__c,
                                                Contact__r.name,
                                                contact__r.Accountid,
                                                contact__r.Account.Name,
                                                Opportunity__c,
                                                Opportunity__r.name,
                                                Opportunity__r.Credit_Limit_x__c,
                                                Opportunity__r.Overall_Ratingx__c,
                                                Opportunity__r.Accountid,
                                                Name 
                                        from Role__c where Opportunity__r.Accountid != :a.id and (contact__c in :Contactids 
                                        OR contact__c in :contacts.keySet())]; // get the account, contact and opp for the set of contacts
                
            }
            return RelatedOpportunities;
        }
        set;
    }
}