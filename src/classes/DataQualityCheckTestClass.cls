@isTest(seeAllData = false)
private class DataQualityCheckTestClass {

    private static testMethod void testWithMultipleAccounts() {
        test.startTest();
            List<Account> listOfAccs = TestDataUtilityClass.getListOfAccounts();
            insert listOfAccs;
            DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
            instance.dataQualityCheckMethod(listOfAccs,'Account');
        test.stopTest();
    }
    
    private static testMethod void testWithSingleAccount() {
        test.startTest();
            List<Account> listOfAccs = new List<Account>();
            Account acc = TestDataUtilityClass.getAccountRec();
            insert acc;
            Lead led = TestDataUtilityClass.getLeadRec() ;
            insert led;
            Contact con = TestDataUtilityClass.getContactRec();
            con.Account = acc;
            con.AccountId = acc.id;
            con.Lead__c = led.id;
            insert con;
            listOfAccs.add(acc);
            DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
            instance.dataQualityCheckMethod(listOfAccs,'Account');
        test.stopTest();
    }
    private static testMethod void testFailWithSingleAccount() {
        boolean expectedExceptionThrown = false; 
        test.startTest();
            Account acc = TestDataUtilityClass.getAccountRec();
            insert acc;
            Lead led = TestDataUtilityClass.getLeadRec() ;
            insert led;
            Account aNew = [SELECT Id,Name,Business_Address_Line_1__c from Account where Id =:acc.Id];
            aNew.Business_Address_Line_1__c = 'TEST ';
            try
            {
                update aNew;
            }
            catch (Exception ex)
            {
                expectedExceptionThrown = true;
            }
        test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
}