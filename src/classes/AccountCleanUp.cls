/**
*
* Author:       David Quick
* Date:         27/02/2017
* Description:  Schedulable batch class to read all accounts and linked contacts and set contact owner to account owner.
*
**/
global class AccountCleanUp implements Database.Batchable<sObject>, Schedulable {
    
    String query;
        
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([select id, name, OwnerId from Account]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Now process the input list (from the Start method)
        List<Account> accounts = (List<Account>)scope;
        System.debug('********** Processing ' + accounts.size() + ' Accounts');
        for (Account a : accounts) {
            //processContacts(a);
            processContactRoles(a);
        }
        
    }
    global void execute(SchedulableContext SC) {
        
    }
    
    global void finish(Database.BatchableContext BC) {

    }
    
    //Call to run the batch job on an ad hoc basis
    global static void runBatch(){
        AccountCleanUp accountBatchJob = new AccountCleanUp(); 
        database.executebatch(accountBatchJob);  
    }
    private static void processContacts(Account a){
        Id owner = a.OwnerId;
        List<Contact> contacts = new List<Contact>();
        contacts = [Select Id, OwnerId from Contact where AccountId = :a.Id];
        for(Contact c : contacts) {
            if (c.OwnerId != owner) {
                c.OwnerId = owner;
            }
        }
        update contacts;
    }
    private static void processContactRoles(Account a) {
        List<AccountContactRole> cr = [SELECT Contact.Name, ContactId, Role, IsPrimary FROM AccountContactRole WHERE Account.id = :a.Id];
        List<Contact> contacts = new List<Contact>();
        for(AccountContactRole ac : cr) {
            System.debug('********** ACR Contact Id ' + ac.ContactId);
            contacts = [SELECT Id, IsPrimary__c FROM Contact WHERE Id = :ac.ContactId];
            for(Contact c : contacts )
            {
                c.IsPrimary__c = ac.IsPrimary;
            	update c;
            }
         }
    }
}