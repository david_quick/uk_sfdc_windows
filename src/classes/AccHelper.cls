public with sharing class AccHelper {
    public Account acc;
    public AccHelper( ApexPages.StandardController stdController ) {
        acc = ( Account )stdController.getRecord();
		System.debug('********** AccHelper called for account ' + acc);       
    }
    public void checkContacts(){
        System.debug('********** AccHelper.checkContactsFuture called for account ' + acc); 
        AccHelper.checkContactsFuture( acc.Id );    
    }
    @future public static void checkContactsFuture( Id accId ) {
        System.debug('********** Check Contacts Roles');
        List<AccountContactRole> cr = [SELECT Contact.name, Contact.Id, Role, IsPrimary FROM AccountContactRole WHERE Account.id = :accId];
        
        for(AccountContactRole ac : cr) {
            System.debug('********** Contact ' + ac.Contact.Name + ' Primary = ' + ac.IsPrimary);
            Contact c = [SELECT Id,IsPrimary__c FROM Contact WHERE Id = :ac.ContactId];
            c.IsPrimary__c = ac.IsPrimary;
            System.debug('********** Contact IsPrimary is now ' + c.IsPrimary__c);
            update c;
        }
    }
}