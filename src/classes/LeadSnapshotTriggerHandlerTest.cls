@isTest
public class LeadSnapshotTriggerHandlerTest {
	@isTest static void testCreateFullApplication() {
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        Test.stopTest();
        Lead leadRes = [SELECT Id, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.debug('********** After Get ' + leadRes);
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
    }
    @isTest static void testAmendFullApplicationAsUser() {
        boolean expectedExceptionThrown = false; 
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        Lead newLead = [SELECT Id, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        LeadSnapshot__c leadRes = [SELECT Id, website__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c =: newLead.LeadAndSnapshotMapField__c];
        System.debug('********** LeadSnapshot website = ' + leadRes.website__c);
        try
        {
           leadRes.website__c = 'www.google.co.uk';
           update leadRes; 
        }
        catch (Exception ex)
        {
            expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.assertEquals(expectedExceptionThrown,true);
    }
    @isTest static void testAmendFullApplicationAsAPIUser() {
        boolean expectedExceptionThrown = false; 
        
        Test.startTest();
        // This code runs as the API user
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@testorgtest.com');
        insert testUser;
       
        System.runAs(testUser) {
            Lead l = TestData.createFullApplication();
            insert l;
        	Lead newLead = [SELECT Id, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        	LeadSnapshot__c leadRes = [SELECT Id, website__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c =: newLead.LeadAndSnapshotMapField__c];
        	System.debug('********** LeadSnapshot website = ' + leadRes.website__c);
        	try
        	{
           		leadRes.website__c = 'www.google.co.uk';
           		update leadRes; 
        	}
        	catch (Exception ex)
        	{
            	expectedExceptionThrown = true;
        	}
        }
        Test.stopTest();
        System.assertEquals(expectedExceptionThrown,false);
    }
}