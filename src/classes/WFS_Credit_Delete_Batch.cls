/**
*
* Author:       Piotr Czechumski
* Date:         30/01/2015
* Description:  Schedulable batch class deleting all records from WFS_Credit_Data__c table and starting new batch job to refill it.
*
**/
global class WFS_Credit_Delete_Batch implements Database.Batchable<sObject>, Schedulable {
    
    String query;
    
    global WFS_Credit_Delete_Batch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([select id, name from WFS_Credit_Data__c]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        // Start data generation if is not test
        if (!Test.isRunningTest()){
            WFS_Credit_GenerateData_Batch generateDataBatch = new WFS_Credit_GenerateData_Batch();
            database.executebatch(generateDataBatch);
        }
    }

    global void execute(SchedulableContext SC) {
        WFS_Credit_Delete_Batch deleteBatchJob = new WFS_Credit_Delete_Batch(); 
        database.executebatch(deleteBatchJob);
    }
    
    global static string scheduleJob(string cron){
        //Cron string format = Seconds|Minutes|Hours|Day_of_month|Month|Day_of_week|optional_year
        //e.g cron = '0 0 01 * * ?' = everyday at 1am
        
        WFS_Credit_Delete_Batch deleteSchedule = new WFS_Credit_Delete_Batch();

        return system.schedule('Regenerate WFS Credit Data', cron, deleteSchedule);
    }
    
}