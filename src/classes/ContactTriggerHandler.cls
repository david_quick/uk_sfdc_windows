public class ContactTriggerHandler extends TriggerHandler{
    
    
    private static String userCSName = 'Free_Users';
    SET<String> freeUsers = new SET<String>();
    
    public static final String MARKETING_OPTOUT = 'StockMaster Messages';
    
    public ContactTriggerHandler(){
        preventRecursiveTrigger(false);
    }
    
    public override void beforeInsert(List<SObject> newObjects){
        contactHelperMethod(newObjects);
        list<Contact> insertedContacts = (list<Contact>)newObjects;
        for ( Contact insertedContact : insertedContacts ){
            insertedContact.MobilePhone = setMobilePhone(insertedContact.Mobile_Phone__c);
            if (insertedContact.OptOut_SMS__c.contains('Opted Out'))
            {
            	insertedContact.smagicinteract__SMSOptOut__c = true;
            }
            else
            {
                insertedContact.smagicinteract__SMSOptOut__c = false;
            }
        }
    }
    public override void afterInsert(List<SObject> newObjects, Map<Id,SObject> newMap) {
        LeadSnapShot_Create createSnapshot = new LeadSnapShot_Create();
        list<Contact> insertedContacts = (list<Contact>)newObjects;
        for ( Contact insertedContact : insertedContacts ){
            Id profileId = createSnapshot.getProfileId('System Administrator API');
            Id userID = createSnapshot.getProfileIdFromUser(insertedContact.CreatedById);
            if (profileId == userID) {
                createSnapshot.CreateLeadContact(insertedContact);
            }
        }
    }
    
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap)
    {
        contactHelperMethod(newObjects);
        
        List<Contact>OldContacts = (List<Contact>)oldObjects;
        List<Contact>NewContacts = (List<Contact>)newObjects;
        
        
        Set<Id> contactsUpdate = new Set<Id>();
        
        for(integer i=0; i< oldContacts.size(); i++ )
        {
            if(OldContacts[i].contact_marketing_opt_out__c!= NewContacts[i].contact_marketing_opt_out__c &&  NewContacts[i].contact_marketing_opt_out__c != null) 
            {
                if (NewContacts[i].contact_marketing_opt_out__c.contains(MARKETING_OPTOUT)) {
                    
                    contactsUpdate.add(NewContacts[i].Id);
                }
            }
            NewContacts[i].MobilePhone = setMobilePhone(NewContacts[i].Mobile_Phone__c);
            if (!string.isBlank(NewContacts[i].OptOut_SMS__c))
            {
            	if (NewContacts[i].OptOut_SMS__c.contains('Opted Out'))
            	{
                	NewContacts[i].smagicinteract__SMSOptOut__c = true;
            	}
            	else
            	{
                	NewContacts[i].smagicinteract__SMSOptOut__c = false;
            	}
            }
            else
            {
                NewContacts[i].smagicinteract__SMSOptOut__c = false;
            }
         	if (NewContacts[i].Incoming_Opt_Out__c)
            {
                NewContacts[i].smagicinteract__SMSOptOut__c = true;
                NewContacts[i].OptOut_SMS__c = 'Opted Out';
            }
        }
        
        if (contactsUpdate.size() > 0) {
            List<Dealer_Message__c> dealerMessages = [Select Id from Dealer_Message__c where Contact__c in :contactsUpdate];
            if (dealerMessages.size() > 0) {
                delete dealerMessages;
            }
        }
    }
    
    public override void beforeDelete(List<SObject> oldObjects,Map<Id,SObject> newMap)
    {
        //Deletes all the dealer messages associated to a contact when a contact is deleted
        List <Dealer_Message__c> toDeleteDms= [Select Id from Dealer_Message__c where   Contact__c IN:oldObjects] ; 
    
        if(!toDeleteDms.isEmpty())
        Database.delete(toDeleteDms);  // 
    }
   
    private string setMobilePhone(string displayPhone)
    {
        string outPhone;
        if (string.isBlank(displayPhone))
        {
            return null;
        }
        if (displayPhone.startsWith('00353'))
        {
            return displayPhone.substring(2);
        }
        if (displayPhone.startsWith('07'))
        {
            outPhone = '44' + displayPhone.substring(1);
            return outPhone;
        }
        return displayPhone;
    }
  
    private void contactHelperMethod(List<SObject> newObjects){
        DataQualityCheckClass instance = DataQualityCheckClass.getDataQualityCheckClassInstance();
        Data_Quality__c userCSInstance = Data_Quality__c.getValues(userCSName);
        if(!Test.isRunningTest()){
             freeUsers = instance.getFieldNamesFromCS(userCSName);
        }
        else{
            freeUsers.add('Jenny Gibson');
            freeUsers.add('Lucy Brown');
            freeUsers.add('David Quick');
            freeUsers.add('Kris Dawson');
        }
        if(!freeUsers.contains(Userinfo.getName())){
            instance.dataQualityCheckMethod(newObjects,'Contact');  
        }
    }
}