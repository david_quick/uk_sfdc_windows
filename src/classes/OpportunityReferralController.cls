public with sharing class OpportunityReferralController {
    
    public Opportunity o {get;set;}
    
    public String Refer;
    
    public Map<id,Profile_Settings__c> psMap = new Map<Id,Profile_Settings__c>();
    public Id ProfileID {
        get{
            if (ProfileId==null)
            {
                ProfileId = Userinfo.getProfileid();                
            }
            return ProfileID;
        }
        set;
    } 
    
    Public OpportunityReferralController(ApexPages.StandardController controller)
    {
        controller.addFields(new List<String>{'US_Director_Referral_Reason__c', 'US_Director_Referral__c', 
                                                'Credit_Manager_Referral_Reason__c', 'Credit_Manager_Referral__c',
                                                'Sales_Director_Referral_Reason__c','Sales_Director_Referral__c',
                                                'Managing_Director_Referral_Reason__c','Managing_Director_Referral__c',
                                                'Finance_Director_Referral_Reason__c', 'Finance_Director_Referral__c',
                                                'Previous_RecordType__c',
                                                'Operations_Legal_Director_Refer_Reason__c','Operations_Legal_Director_Referral__c'});
        o=(Opportunity) controller.getRecord();

        List<Profile_Settings__c> userProfiles = Profile_Settings__c.getAll().values();  // get the profile settings

        for (Profile_Settings__c profileInfo : userProfiles) {
            psMap.put(profileInfo.Profile_ID__c,profileInfo);
        }
        
    }
    
    Public PageReference Init(){
        o.put(psMap.get(ProfileID).Opp_Refer_Column__c,TRUE); // referral column
        o.put(psMap.get(ProfileID).Opp_Referral_Reason__c,''); // Referral Reason
        update o;
        return new PageReference('/'+o.id);
    }
    
    Public PageReference Save(){
        if(o.get(psMap.get(ProfileID).Opp_Referral_Reason__c)!='')
        {
            o.put(psMap.get(ProfileID).Opp_Refer_Column__c,TRUE); // referral column
            o.recordtypeid = o.Previous_RecordType__c;
            update o;
            return new PageReference('/'+o.id);
        }
        else
        {
            o.addError('You must enter a Referral reason');
            return null;
        }
    }

}