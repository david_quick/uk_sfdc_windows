@isTest
private class WFS_Organisation_Delete_Schedule_Test {
    private static string CRON_EXP = '0 0 0 15 3 ? 2022';
   
   
    private static testMethod void testSchedule() {
        Test.startTest();
        
        //Create the data to be deleted
        list<WFS_Organisation_Data__c> orgData = new list<WFS_Organisation_Data__c>();
        for(integer i = 0; i < 200; i++){
            string pos = string.valueOf(i);
            orgData.add(new WFS_Organisation_Data__c(AccountId__c = pos, ParentAccountId__c = pos, XML__c = '<root>' + pos +'</root>'));
        }
        insert orgData;
        
        //Confirm the data is there
        orgData = [select id from WFS_Organisation_Data__c];
        system.assertEquals(200, orgData.size());

        // Schedule the test job
        string jobId = WFS_Organisation_Delete_Schedule.scheduleJob(CRON_EXP);
         
        // Get the information from the CronTrigger API object
        CronTrigger ct = [select Id, CronExpression, TimesTriggered, NextFireTime
                            from CronTrigger 
                            where id = :jobId];

        // Verify the expressions are the same
        system.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the next time the job will run
        system.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
      
        // Verify the scheduled job hasn't run yet.
        orgData = [select id from WFS_Organisation_Data__c];
        system.assert(!orgData.isEmpty());
        
        //In theory,  the batch should run when Test.stopTest() is called,  that does not seem to happen though,  so,  call the ad hoc start
        WFS_Organisation_Delete_Batch.runBatch();
        
        Test.stopTest();
        
        //Confirm that the data has been removed
        orgData = [select id from WFS_Organisation_Data__c];
        system.assert(orgData.isEmpty(), 'Found ' + orgData.size() + ' rows of data.  Expected 0');
    }
   
}