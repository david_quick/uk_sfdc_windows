public with sharing class TaskTriggerHandler extends TriggerHandler {
    
    public TaskTriggerHandler(){
        preventRecursiveTrigger(false);
    }
    
    TaskEventUpdates updater = new TaskEventUpdates();
    
    public override void beforeInsert(List<SObject> newObjects){
        // Get the contact if TaskArea is visit and set the SMS mobile phone if contact mobilephone is there
        for (Task t : (List<Task>)newObjects)
        {
            t.SMS_Mobile_Phone_c__c = TestData.getMobileNumber(t.WhoId, t.OwnerId);
            t.Destination_Name__c = TestData.getDestinationName(t.WhoId, t.OwnerId);   
            system.debug('********** MobilePhone is ' + t.SMS_Mobile_Phone_c__c);
        }
    }
 
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap) {
        if (newObjects.size() < 10) {
            // Check if the task has a related event but do not error if it is an owning record type change (WhoId)
            for(integer i=0; i< newObjects.size(); i++) {
                Task oldun = (Task)oldObjects[i];
                Task newun = (Task)newObjects[i];
                if (oldun.WhoId == newun.WhoId) {
                    if (updater.hasRelatedEvents(newun)) {
                        newun.addError('Do not edit this task - edit the related event instead');
                    }
                }
            }
        }
    }
    
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id,SObject> oldMap, Map<Id,SObject> newMap) {
        if (newObjects.size() < 10) {
            updater.checkEventClosed(newObjects);
            updater.checkRelatedEvents(newObjects);
        }
    }

    public override void beforeDelete(List<SObject> newObjects, Map<Id,SObject> oldMap){
        // Check if the task has a related event
        if (newObjects.size() < 10) {
            for(integer i=0; i< newObjects.size(); i++) {
                Task t = (Task) newObjects[i];
                if (updater.hasRelatedEvents(t)) {
                    t.addError('Do not delete this task - close the related event instead');
                }
            }
        }
    }
}