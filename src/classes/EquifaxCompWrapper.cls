public with sharing class EquifaxCompWrapper {

    public String Reference { get; set; }
    public String Name { get; set; }
    public String Address1 { get; set; }
    public String Address2 { get; set; }
    public String Town { get; set; }
    public String County { get; set; }
    public String Postcode { get; set; }
    public String comptype { get; set; }
    public boolean selected { get; set; }

    public EquifaxCompWrapper(String Reference,String Name,String Address1,String Address2,String Town,String County,String Postcode){
        this.selected = false;          
        this.Reference = Reference;         
        this.Name = Name;           
        this.Address1 = Address1;           
        this.Address2 = Address2;           
        this.Town = Town;           
        this.County = County;           
        this.Postcode = Postcode;           
    }
    public EquifaxCompWrapper(String Reference,String Name,String Address1,String Address2,String Town,String County,String Postcode,String comptype){
        this.selected = false;          
        this.Reference = Reference;         
        this.Name = Name;           
        this.Address1 = Address1;           
        this.Address2 = Address2;           
        this.Town = Town;           
        this.County = County;           
        this.Postcode = Postcode;           
        this.comptype = comptype;           
    }
    public EquifaxCompWrapper(){
        this.selected = false;          
        this.Reference = '';            
        this.Name = '';         
        this.Address1 = '';         
        this.Address2 = '';         
        this.Town = '';         
        this.County = '';           
        this.Postcode = '';
        this.comptype='';           
    }
}