@isTest
public class LeadsTriggerHandlerTest {
    @isTest static void testCreateFullApplication() {
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        Test.stopTest();
        Lead leadRes = [SELECT Id, Record_type_id__c,Business_Address_Town_City__c, Home_Address_Town_City__c FROM Lead WHERE Id =: l.Id];
        System.debug('********** After Get ' + leadRes);
        System.assert(leadRes.Record_type_id__c != null);
        string busCity = leadRes.Business_Address_Town_City__c;
        string homeCity = leadRes.Home_Address_Town_City__c;
        System.assert(busCity.isAllUpperCase() != true);
        System.assert(homeCity.isAllUpperCase() != true);
    }
    
    @isTest static void testCreateFullApplicationWithContacts() {
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        Contact con1 = TestData.createLeadContact(l,'Mr','Juan','Sheet');
        insert con1;
        Contact con2 = TestData.createLeadContact(l,'Mrs','Juanita','Sheet');
        insert con2;
        Lead leadRes = [SELECT Id, Record_type_id__c FROM Lead WHERE Id =: l.Id];
        System.debug('********** After Get ' + leadRes);
        System.assert(leadRes.Record_type_id__c != null);
        List<Contact> contacts = [Select Id from Contact where Lead__c = :l.Id];
        System.assert(contacts.size() == 2);
        Test.stopTest();
    }
    
    @isTest static void testUpdateLeadMobile() {
        Test.startTest();
        Lead l = TestData.createFullApplication();
        l.MobilePhone = '';
        l.Mobile_Phone__c = '';
        insert l;
        Lead leadRes = [SELECT Id, Record_type_id__c, OwnerId, MobilePhone, Mobile_Phone__c FROM Lead WHERE Id =: l.Id];
        leadRes.Mobile_Phone__c = '00353787878';
        System.debug('********** Set mobile phone to Irish number');
        update leadRes;
        Lead leadRes1 = [SELECT Id, Record_type_id__c, OwnerId, MobilePhone, Mobile_Phone__c FROM Lead WHERE Id =: leadRes.Id];
        System.debug('********** Mobile phone set to ' + leadRes1.MobilePhone);
        System.assert(leadRes1.MobilePhone == '353787878');
        leadRes1.Mobile_Phone__c = '07803925369';
        System.debug('********** Set mobile phone to UK number');
        update leadRes1;
        Lead leadRes2 = [SELECT Id, Record_type_id__c, OwnerId, MobilePhone, Mobile_Phone__c FROM Lead WHERE Id =: leadRes1.Id];
        System.debug('********** Mobile phone set to ' + leadRes2.MobilePhone);
        System.assert(leadRes2.MobilePhone == '447803925369');
        Test.stopTest();
    }
    
    @isTest static void testChangeLeadOwner() {
        Lead l = TestData.createFullApplication();
        Test.startTest();
        insert l;
        Lead leadRes = [SELECT Id, Record_type_id__c, OwnerId FROM Lead WHERE Id =: l.Id];
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        System.assert(leadRes.Record_type_id__c != null);
        User testUser = new User(Alias = 'apiTest', Email = 'apiuser@testorg.com',
                EmailEncodingKey = 'UTF-8', FirstName = 'Api', LastName = 'Test', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'apiuser@testorg.com');
        insert testUser;
        User thisUser = [SELECT Id, Name from User where Id = :testUser.Id][0];
        leadRes.OwnerID = thisUser.Id;
        Id prosp = TestData.getRecordTypeID('Prospect','Lead');
        leadRes.RecordTypeId = prosp;
        upsert leadRes;
        Lead leadRes1 = [SELECT Id, Record_type_id__c, OwnerId FROM Lead WHERE Id =: l.Id];
        System.assert(leadRes1.OwnerId == thisUser.Id);
        Test.stopTest();
    }
    @isTest static void testCreateSupplier() {
        Test.startTest();
        Id rid = TestData.getRecordTypeId('Supplier','Lead');
        Lead led = new Lead();
        led = TestData.CreateSupplier(1,rid);
        insert led;
        Lead newlead = [SELECT Id,Name,RecordTypeId from Lead WHERE Id = :led.Id];
        System.assert(newlead.RecordTypeId == rid);
        Test.stopTest();
    }
 
    @isTest static void testCreateMultipleProspects() {
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Credit Analyst'];
        User testUser = new User(Alias = 'tUser', Email = 'testuser@testorg.com',
                EmailEncodingKey = 'UTF-8', FirstName = 'Api', LastName = 'Test', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'apiuser@testorgtest.com');
        insert testUser;
        User thisUser = [SELECT Id, Name from User where Id = :testUser.Id][0];
        Id rid = TestData.getRecordTypeId('Prospect','Lead');
        // Create prospects
        List<Lead> leads = new List<Lead>();
        for(integer i=0;i<130;i++) {
            leads.Add(TestData.CreateProspect(i,rid));
        }
        insert leads;
        System.debug('********** Leads inserted');
        System.assert(leads.size() > 10);
        // Update owner and recordtype
        for(Lead l:leads) {
            l.OwnerID = thisUser.Id;
            l.RecordTypeId = rid;
        }
        upsert leads;
        System.debug('********** Leads upserted');
        Lead lRes = leads[0];
        System.assert(lRes.OwnerId == thisUser.Id);
        System.assert(lRes.RecordTypeId == rid);
        Test.stopTest();
    }
    
    @isTest static void testMultiplePagesAPIUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'apiTest', Email = 'apiuser@testorg.com',
                EmailEncodingKey = 'UTF-8', FirstName = 'Api', LastName = 'Test', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'apiuser@testorgtest.com');
        insert testUser;
        User thisUser = [SELECT Name from User where Id = :testUser.Id][0];
        System.debug('********** UserName is ' + thisUser.Name);
        Lead l = new Lead();
        System.runAs(testUser) {
            l = TestData.createFullApplicationPage1();
            Test.startTest();
            insert l;
            l = TestData.createFullApplicationPage2(l);
            update l;
            l = TestData.createFullApplicationPage3(l);
            update l;
        }
        Test.stopTest();
        Lead leadRes = [SELECT Id, Record_type_id__c, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
        List<LeadSnapshot__c> shots = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        System.assert(shots.size() == 1);
        LeadSnapshot__c snapshot = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        // From page 1
        System.assert(snapshot.Business_Phone__c != null);
        // From page 2
        System.assert(snapshot.Ave_Vehicle_Sales_Value__c != null);
        // From page 3
        System.assert(snapshot.Bank_Name__c != null);
    }
    @isTest static void testMultiplePagesWithContactsAPIUser() {
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator API'];
        User testUser = new User(Alias = 'apiTest', Email = 'apiuser@testorg.com',
                EmailEncodingKey = 'UTF-8', FirstName = 'Api', LastName = 'Test', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'apiuser@testorgtest.com');
        insert testUser;
        User thisUser = [SELECT Name from User where Id = :testUser.Id][0];
        Lead l = new Lead();
        System.runAs(testUser) {
            l = TestData.createFullApplicationPage1();
            insert l;
            l = TestData.createFullApplicationPage2(l);
            update l;
            l = TestData.createFullApplicationPage3(l);
            update l;
            Contact con1 = TestData.createLeadContact(l,'Mr','Juan','Sheet');
            insert con1;
            Contact con2 = TestData.createLeadContact(l,'Mrs','Juanita','Sheet');
            insert con2;
        }
        Test.stopTest();
        Lead leadRes = [SELECT Id, Record_type_id__c, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
        List<LeadSnapshot__c> shots = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        System.assert(shots.size() == 1);
        LeadSnapshot__c snapshot = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        // From page 1
        System.assert(snapshot.Business_Phone__c != null);
        // From page 2
        System.assert(snapshot.Ave_Vehicle_Sales_Value__c != null);
        // From page 3
        System.assert(snapshot.Bank_Name__c != null);
        List<Contact> contacts = [Select Id from Contact where Lead__c = :l.Id];
        System.assert(contacts.size() == 2);
    }
    @isTest static void testMultiplePagesWithContactsOtherUser() {
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Credit Analyst'];
        User testUser = new User(Alias = 'apiTest', Email = 'apiuser@testorg.com',
                EmailEncodingKey = 'UTF-8', FirstName = 'Api', LastName = 'Test', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'apiuser@testorgtest.com');
        insert testUser;
        User thisUser = [SELECT Name from User where Id = :testUser.Id][0];
        Lead l = new Lead();
        System.runAs(testUser) {
            l = TestData.createFullApplicationPage1();
            insert l;
            l = TestData.createFullApplicationPage2(l);
            update l;
            l = TestData.createFullApplicationPage3(l);
            update l;
            Contact con1 = TestData.createLeadContact(l,'Mr','Juan','Sheet');
            insert con1;
            Contact con2 = TestData.createLeadContact(l,'Mrs','Juanita','Sheet');
            insert con2;
        }
        Test.stopTest();
        Lead leadRes = [SELECT Id, Record_type_id__c, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
        List<LeadSnapshot__c> shots = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        System.assert(shots.size() == 1);
        LeadSnapshot__c snapshot = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        System.debug('********** Snapshot = ' + snapshot);
        // From page 1
        System.assert(snapshot.Business_Phone__c == null);
        // From page 2
        System.assert(snapshot.Bank_Name__c == null);
        // From page 3
        System.assert(snapshot.Ave_Vehicle_Sales_Value__c == null);
        
        List<Contact> contacts = [Select Id from Contact where Lead__c = :l.Id];
        System.assert(contacts.size() == 2);
    }
    @isTest static void testMultiplePagesOtherUser() {
        // Create lead as another user profile and then edit lead. Snapshot should only be created once and should not change when lead is edited
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User testUser = new User(Alias = 'apiTest', Email = 'apiuser@testorg.com',
                EmailEncodingKey = 'UTF-8', FirstName = 'Api', LastName = 'Test', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'apiuser@testorgtest.com');
        insert testUser;
        User thisUser = [SELECT Name from User where Id = :testUser.Id][0];
        System.debug('********** UserName is ' + thisUser.Name);
        Lead l = new Lead();
        System.runAs(testUser) {
            l = TestData.createFullApplication();
            Test.startTest();
            insert l;
            l.Name_of_Insurer__c = 'Hastings';
            update l;
        }
        Test.stopTest();
        Lead leadRes = [SELECT Id, Record_type_id__c, LeadAndSnapshotMapField__c FROM Lead WHERE Id =: l.Id];
        System.assert(leadRes.LeadAndSnapshotMapField__c != null);
        List<LeadSnapshot__c> shots = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        System.assert(shots.size() == 1);
        LeadSnapshot__c snapshot = [SELECT Id, LeadAndSnapshotMapField__c, Business_Phone__c, Ave_Vehicle_Sales_Value__c, Bank_Name__c, Name_of_Insurer__c FROM LeadSnapshot__c WHERE LeadAndSnapshotMapField__c = :leadRes.LeadAndSnapshotMapField__c ];
        // From page 1
        System.assert(snapshot.Business_Phone__c != null);
        // From page 2
        System.assert(snapshot.Ave_Vehicle_Sales_Value__c != null);
        // From page 3
        System.assert(snapshot.Bank_Name__c != null);
        // Test update has not changed snapshot
        System.assert(snapshot.Name_of_Insurer__c == 'Aviva');
    }
}