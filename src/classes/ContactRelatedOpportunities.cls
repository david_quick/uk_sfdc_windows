public with sharing class ContactRelatedOpportunities {

    public contact c { get; set; }

    public ContactRelatedOpportunities(ApexPages.StandardController controller) {
        // get the contact
        c = [select id, name, firstname, lastname, Account.Name, email, ownerId from contact
            where id = : controller.getId()];
    }
    
    public list<Related_Contact__c> RelatedCons {
        get{
            if(RelatedCons==null){
                RelatedCons = [select Related_Contact_1__c,Related_Contact_2__c from Related_Contact__c where Related_Contact_1__c=:c.id or Related_Contact_2__c=:c.id];
            }
            return RelatedCons;
        }
        set;
    }

    public list<Role__c> RelatedOpportunities {
        get{
            if(RelatedOpportunities==null){
                Set<Id> Contactids = new Set<Id>();  // set to hold contact ids
                
                for(Related_Contact__c rc: RelatedCons){
                    Contactids.add(rc.Related_Contact_1__c); // add contact 1 to set
                    Contactids.add(rc.Related_Contact_2__c); // add contact2 to set
                }
                        
                RelatedOpportunities = [select Contact__c,
                                                Contact__r.name,
                                                contact__r.Accountid,
                                                contact__r.Account.Name,
                                                Opportunity__c,
                                                Opportunity__r.name,
                                                Opportunity__r.Credit_Limit_x__c,
                                                Opportunity__r.Overall_Ratingx__c,
                                                Name
                                        from Role__c where contact__c in :Contactids]; // get the account, contact and opp for the set of contacts
                
            }
            return RelatedOpportunities;
        }
        set;
    }
    
}