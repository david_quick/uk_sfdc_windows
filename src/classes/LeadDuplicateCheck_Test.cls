@isTest
private class LeadDuplicateCheck_Test {

    // test positive 
    private static testMethod void positiveTest() {
        // create a lead
        Test.startTest();
        Lead firstLead = new Lead( Company='First', email = 'test@test.com', lastname='lastName'  );
        insert firstLead;
        
        list<lead> leads = [select id, company, email from Lead];
        system.assertEquals(leads.size(), 1);
        
        //insert another record
        Lead secondLead = new Lead( Company='First', email = 'test@test.com', lastname='lastName'  );
        insert secondLead;

        leads = [select id, company, email from Lead];
        system.assertEquals(leads.size(), 2); // should have 2 records 
        test.stopTest();
    }

  

}