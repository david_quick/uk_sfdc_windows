@isTest
public class AccountTriggerHandlerTest {
    @isTest static void insertNewAccount() {
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        Account acc1 = [SELECT Id, Name FROM ACCOUNT WHERE Id = :acc.Id];
        test.stopTest();
        System.assert(acc1.Name != null);
    }
    @isTest static void insertMultipleAccounts() {
        boolean expectedExceptionThrown = false; 
        Test.startTest();
        List<Account> accs = TestData.getListOfAccounts(20);
        System.Debug('********** Created ' + accs.size() + ' accounts ');
        try
        {
        	insert accs;
        	for (Account a : accs)
        	{
            	a.Credit_Limit_Review_Date__c = System.today().addDays(120);
        	}
        	update accs;
        }
        catch (Exception ex)
        {
            expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void insertNewAccountWithContacts() {
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        Contact c1 = TestData.createContact(acc,'James','Joyce');
        insert c1;
        Contact c2 = TestData.createContact(acc,'Flann','Flynn');
        insert c2;
        Account acc1 = [SELECT Id, Name FROM ACCOUNT WHERE Id = :acc.Id];
        test.stopTest();
        System.assert(acc1.Name != null);
        List<Contact> contactsUpdate = [Select Id, OwnerId from Contact where AccountId = :acc.Id];
        system.Debug('********** Number of contacts = ' + contactsUpdate.size());
        system.Assert(contactsUpdate.size() == 2); 
    }
    
    @isTest static void updateSingleAccountToFail() {
        // This name change should fail data quality check
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        try
        {
            Account acc1 = [SELECT Id, Name FROM ACCOUNT WHERE Id = :acc.Id];
            System.assert(acc1.Name != null);
        	acc1.Name = 'Name changed to protect the innocent';
        	update acc1;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
    @isTest static void updateSingleAccountToPass() {
        // This name change should fail data quality check
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        try
        {
            Account acc1 = [SELECT Id, Name FROM ACCOUNT WHERE Id = :acc.Id];
            System.assert(acc1.Name != null);
        	acc1.Name = 'Mikes Motors';
        	update acc1;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void accountUpdateAsStandardUserQualifiedStatus() {
        System.debug('********** Starting accountValidationChecks');
        boolean expectedExceptionThrown = false; 
        boolean check1 = true;
        Test.startTest();
        Account acc = TestData.createAccount();
       	System.debug('********** Insert account');
        insert acc;
       	Account acc1 = [SELECT Id, Qualified_Status__c, Account_Live_Date__c, Account_Closed_Date__c FROM ACCOUNT WHERE Id = :acc.Id];
        acc1.Qualified_status__c = 'Account Live';
        acc1.Account_Live_Date__c = System.today();
        System.debug('********** Update account');
        update acc1;
        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];        
        User u1 = TestData.createUser(p1.Id, 'Jan', 'Standard');
        System.debug('********** Update as ' + u1.Name);
        System.runAs(u1) {
        try
            {
                System.debug('********** Running as ' + u1.Name);
        		acc1.Qualified_status__c = 'Suspect';
                update acc1;
        		System.debug('********** Updated account');
            }
            catch (exception ex)
            {
                expectedExceptionThrown = true;
            }
        	
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
     @isTest static void accountUpdateAsStandardUserLegalStatus() {
        System.debug('********** Starting accountValidationChecks');
        boolean expectedExceptionThrown = false; 
        Test.startTest();
        
        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'Credit Analyst'][0];        
        User u1 = TestData.createUser(p1.Id, 'Jan', 'CreditAnalyst');
        insert u1;
        System.runAs(u1) {
            Account acc = TestData.createAccount();
       		System.debug('********** Insert account');
        	insert acc;
            System.debug('********** Account = ' + acc);
          	acc.Qualified_status__c = 'Account Live';
        	System.debug('********** Update account');
        	update acc;
          	System.debug('********** Update as ' + u1.Name);
            Account acc2 = [SELECT Id, Qualified_Status__c, Account_Live_Date__c, Account_Closed_Date__c FROM ACCOUNT WHERE Id = :acc.Id];
        	acc2.Qualified_status__c = 'Legal Action';
            try
            {
                update acc2;
        		System.debug('********** Updated account');
            }
            catch (exception ex)
            {
                expectedExceptionThrown = true;
            }
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
     @isTest static void accountUpdateAsStandardUserLiveDate() {
        boolean expectedExceptionThrown = false; 
        boolean check1 = true;
        Test.startTest();
        Account acc = TestData.createAccount();
       	System.debug('********** Insert account');
        insert acc;
       	Account acc1 = [SELECT Id, Qualified_Status__c, Account_Live_Date__c, Account_Closed_Date__c FROM ACCOUNT WHERE Id = :acc.Id];
        acc1.Qualified_status__c = 'Account Live';
        acc1.Account_Live_Date__c = System.today();
        System.debug('********** Update account');
        update acc1;
        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];        
        User u1 = TestData.createUser(p1.Id, 'Jan', 'Standard');
        insert u1;
        System.runAs(u1) {
          	System.debug('********** Update as ' + u1.Name);
        	acc1.Account_Live_Date__c = System.today().addDays(-3);
            try
            {
                update acc1;
        		System.debug('********** Updated account');
            }
            catch (exception ex)
            {
                expectedExceptionThrown = true;
            }
        	
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,true);
    }
        @isTest static void accountUpdateAsAdminUserLiveDate() {
        boolean expectedExceptionThrown = false; 
        boolean check1 = true;
        Test.startTest();
        Account acc = TestData.createAccount();
       	System.debug('********** Insert account');
        insert acc;
       	Account acc1 = [SELECT Id, Qualified_Status__c, Account_Live_Date__c, Account_Closed_Date__c FROM ACCOUNT WHERE Id = :acc.Id];
        acc1.Qualified_status__c = 'Account Live';
        acc1.Account_Live_Date__c = System.today();
        System.debug('********** Update account');
        update acc1;
        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'System Administrator'][0];        
        User u1 = TestData.createUser(p1.Id, 'Jan', 'Sysadmin');
        insert u1;
        System.runAs(u1) {
          	System.debug('********** Update as ' + u1.Name);
        	acc1.Account_Live_Date__c = System.today().addDays(-3);
            try
            {
                update acc1;
        		System.debug('********** Updated account');
            }
            catch (exception ex)
            {
                expectedExceptionThrown = true;
            }
        	
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void accountUpdateAsAdminUserQualifiedStatus() {
        System.debug('********** Starting accountValidationChecks');
        boolean expectedExceptionThrown = false; 
        boolean check1 = true;
        Test.startTest();
        Account acc = TestData.createAccount();
       	System.debug('********** Insert account');
        insert acc;
       	Account acc1 = [SELECT Id, Qualified_Status__c, Account_Live_Date__c, Account_Closed_Date__c FROM ACCOUNT WHERE Id = :acc.Id];
        acc1.Qualified_status__c = 'Account Live';
        acc1.Account_Live_Date__c = System.today();
        System.debug('********** Update account');
        update acc1;

        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'System Administrator'][0];        
        User u1 = TestData.createUser(p1.Id, 'Jan', 'Sysadmin');
        insert u1;
        System.runAs(u1) {
         	System.debug('********** Update as ' + u1.Name);
        	acc1.Qualified_status__c = 'Suspect';
            try
            {
                update acc1;
        		System.debug('********** Updated account');
            }
            catch (exception ex)
            {
                expectedExceptionThrown = true;
            }
        	
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void updateSingleAccountWithContacts() {
        boolean expectedExceptionThrown = false; 
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        Contact c1 = TestData.createContact(acc,'James','Joyce');
        insert c1;
        Contact c2 = TestData.createContact(acc,'Flann','Flynn');
        insert c2;
        try
        {
            Account acc1 = [SELECT Id, Name FROM ACCOUNT WHERE Id = :acc.Id];
            System.assert(acc1.Name != null);
        	acc1.Name = 'Mikes Motors';
        	update acc1;
        }
        catch (Exception ex) {
                expectedExceptionThrown = true;
        }
        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown,false);
    }
    @isTest static void updateSingleAccountWithContactsOwnerStandardUser() {
        // Should update the account with affecting the number of contacts - Run as standard user should fail
        boolean expectedExceptionThrown = false;
        String aId;
        Profile p = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];
        User runUser = TestData.createUser(p.Id,'George','Shaw');
        insert runUser;  
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        Contact c1 = TestData.createContact(acc,'James','Joyce');
        insert c1;
        Contact c2 = TestData.createContact(acc,'Flann','Flynn');
        insert c2;
        // Now create a new user and allocate the account to them
        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];
        User newUser = TestData.createUser(p1.Id,'Bernard','Shaw');
        insert newUser;
        system.Debug('********** New user name is ' + newUser.Name);
        try
        {
        	System.runAs(runUser) { 
        		try
        		{
                    System.debug('********** Change account owner');
            		Account acc1 = [SELECT Id, Name, OwnerId FROM ACCOUNT WHERE Id = :acc.Id];
            		System.assert(acc1.Name != null);
            		system.Debug('********** Original owner is ' + TestData.getUserName(acc1.OwnerId));
          			acc1.Name = 'Mikes Motors';
            		acc1.OwnerId = newUser.Id;
            		system.Debug('********** Changed owner to ' + TestData.getUserName(acc1.OwnerId));
        			update acc1;
            		aId = acc1.Id;
        		}
        		catch (Exception ex) {
                    System.debug('********** Account change fails');
                	expectedExceptionThrown = true;
        		}
        		Account a = [SELECT Id, OwnerId from Account WHERE Id =:aId];
        		System.AssertEquals(expectedExceptionThrown,false);
        		List<Contact> contactsUpdate = [Select Id, OwnerId from Contact where AccountId = :acc.Id];
        		Test.stopTest();
        		system.Debug('********** Number of contacts = ' + contactsUpdate.size());
        		system.Assert(contactsUpdate.size() == 2); 
        		for(Contact c : contactsUpdate) {
            		system.Debug('********* Contact owner is ' + TestData.getUserName(c.OwnerId));
            		system.Assert(a.OwnerId == c.OwnerId);
        		}
        	}
        }
        catch (Exception e1) {
            expectedExceptionThrown = true;
        }
        System.AssertEquals(expectedExceptionThrown,true);    
    }
  
     @isTest static void updateSingleAccountWithContactsOwnerAdmin() {
        // Should update the account with affecting the number of contacts - Run as admin user should pass
        boolean expectedExceptionThrown = false;
        String aId;
        Profile p = [SELECT Id, Name from Profile WHERE NAME = 'System Administrator'][0];
        User runUser = TestData.createUser(p.Id,'George','Shaw');
        insert runUser;  
        Account acc = TestData.createAccount();
        test.startTest();
        insert acc;
        Contact c1 = TestData.createContact(acc,'James','Joyce');
        insert c1;
        Contact c2 = TestData.createContact(acc,'Flann','Brian');
        insert c2;
        // Now create a new user and allocate the account to them
        Profile p1 = [SELECT Id, Name from Profile WHERE NAME = 'Standard User'][0];
        User newUser = TestData.createUser(p1.Id,'Bernard','Shaw');
        insert newUser;
        system.Debug('********** New user name is ' + newUser.Name);
        try
        {
        	System.runAs(runUser) { 
        		try
        		{
                    System.debug('********** Change account owner');
            		Account acc1 = [SELECT Id, Name, OwnerId FROM ACCOUNT WHERE Id = :acc.Id];
            		System.assert(acc1.Name != null);
            		system.Debug('********** Original owner is ' + TestData.getUserName(acc1.OwnerId));
          			acc1.Name = 'Mikes Motors';
            		acc1.OwnerId = newUser.Id;
            		system.Debug('********** Changed owner to ' + TestData.getUserName(acc1.OwnerId));
        			update acc1;
            		aId = acc1.Id;
        		}
        		catch (Exception ex) {
                    System.debug('********** Account change fails');
                	expectedExceptionThrown = true;
        		}
        		Account a = [SELECT Id, OwnerId from Account WHERE Id =:aId];
        		System.AssertEquals(expectedExceptionThrown,false);
        		List<Contact> contactsUpdate = [Select Id, OwnerId from Contact where AccountId = :acc.Id];
        		Test.stopTest();
        		system.Debug('********** Number of contacts = ' + contactsUpdate.size());
        		system.Assert(contactsUpdate.size() == 2); 
        		for(Contact c : contactsUpdate) {
            		system.Debug('********* Contact owner is ' + TestData.getUserName(c.OwnerId));
            		system.Assert(a.OwnerId == c.OwnerId);
        		}
        	}
        }
        catch (Exception e1) {
            expectedExceptionThrown = true;
        }
        System.AssertEquals(expectedExceptionThrown,false);    
    }
}