<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_cust_services_to_flag_incomplete_application_7_days_old</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>Email to cust services to flag incomplete application &gt; 7 days old</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Incomplete_application</template>
    </alerts>
    <alerts>
        <fullName>Incomplete_Application_over_7_days</fullName>
        <ccEmails>customerservice@nextgearcapital.co.uk</ccEmails>
        <description>Incomplete Application over 7 days old</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Incomplete_Application_over_7_days_old</template>
    </alerts>
    <alerts>
        <fullName>Incomplete_Application_over_7_days_old</fullName>
        <ccEmails>customerservice@nextgearcapital.co.uk</ccEmails>
        <description>Incomplete Application over 7 days old</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Incomplete_Application_over_7_days_old</template>
    </alerts>
    <alerts>
        <fullName>New_Application_received</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <ccEmails>BusDevSupport@nextgearcapital.co.uk</ccEmails>
        <description>New Application received</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Application_received_in_Salesforce</template>
    </alerts>
    <alerts>
        <fullName>New_Incomplete_Application</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>New Incomplete Application</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Incomplete_application</template>
    </alerts>
    <rules>
        <fullName>Completed Application received in Salesforce</fullName>
        <actions>
            <name>New_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( TEXT(Credit_Limit__c)  &lt;&gt; &apos;&apos;, NOT( RecordType.DeveloperName = &apos;Prospect&apos; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Incomplete Application over 7 days old</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Credit_Limit__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Supplier</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Incomplete_Application_over_7_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Incomplete application received</fullName>
        <actions>
            <name>New_Incomplete_Application</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Credit_Limit__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Supplier</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>lead_notification</fullName>
        <assignedTo>david.quick@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>SMS-Notification-Lead-0b7e</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>lead notification</subject>
    </tasks>
</Workflow>
