<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Mail_to_Task_owner</fullName>
        <description>Send Mail to Task owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/Notification_to_Task_Owner</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_assigned_or_complete_time</fullName>
        <field>Assigned_or_completed_time__c</field>
        <formula>NOW()</formula>
        <name>Update assigned or complete time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BDE_Appointment_Booked</fullName>
        <actions>
            <name>BDE_Appointment</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Task_Area__c</field>
            <operation>equals</operation>
            <value>AE - Visit</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Complete assigned or completed time</fullName>
        <actions>
            <name>Update_assigned_or_complete_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(Assigned_or_completed_time__c) &amp;&amp; ISPICKVAL(Status, &apos;Completed&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to task owner</fullName>
        <actions>
            <name>Send_Mail_to_Task_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Send_Mail_Email_handler__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Task_Area__c</field>
            <operation>equals</operation>
            <value>AE - Visit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>BDE_Appointment</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Task-7359</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Task.ActivityDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>BDE Appointment</subject>
    </tasks>
</Workflow>
