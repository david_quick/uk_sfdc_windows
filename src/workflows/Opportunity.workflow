<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Live</fullName>
        <ccEmails>BusDevSupport@nextgearcapital.co.uk</ccEmails>
        <description>Account Live</description>
        <protected>false</protected>
        <recipients>
            <recipient>andrew.edwards@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jenny.gibson@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Live</template>
    </alerts>
    <alerts>
        <fullName>Account_Live_stage_Welcome_Call_arrangement</fullName>
        <description>Account Live stage Welcome Call arrangement</description>
        <protected>false</protected>
        <recipients>
            <recipient>jenny.gibson@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Live_stage_Welcome_Call_arrangement</template>
    </alerts>
    <alerts>
        <fullName>Credit_decision_reached_by_Credit_Approvers_and_Approved</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>Credit decision reached by Credit Approvers and Approved</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_decision_reached_by_Credit_Approvers_and_Approved</template>
    </alerts>
    <alerts>
        <fullName>Credit_decision_reached_by_Credit_Approvers_and_Approved_Dealer</fullName>
        <description>Credit decision reached by Credit Approvers and Approved (Dealer)</description>
        <protected>false</protected>
        <recipients>
            <field>Business_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Decision_reached_by_NextGear_Capital_and_Approved_Dealer</template>
    </alerts>
    <alerts>
        <fullName>Credit_decision_reached_by_Credit_Approvers_and_Declined_Credit</fullName>
        <description>Credit decision reached by Credit Approvers and Declined (Credit)</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_decision_reached_by_Credit_Approvers_and_Declined_Credit</template>
    </alerts>
    <alerts>
        <fullName>Credit_decision_reached_by_Credit_Approvers_and_Declined_Dealer</fullName>
        <description>Credit decision reached by Credit Approvers and Declined (Dealer)</description>
        <protected>false</protected>
        <recipients>
            <field>Business_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Decision_reached_by_NextGear_Capital_Unable_to_proceed_Dealer</template>
    </alerts>
    <alerts>
        <fullName>Credit_decision_reached_by_Credit_Approvers_and_Refer</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>Credit decision reached by Credit Approvers and Refer</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_decision_reached_by_Credit_Approvers_and_Refer</template>
    </alerts>
    <alerts>
        <fullName>Credit_decision_reached_by_Credit_Approvers_and_Resubmit</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>Credit decision reached by Credit Approvers and Resubmit</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_decision_reached_by_Credit_Approvers_and_Resubmit</template>
    </alerts>
    <alerts>
        <fullName>Documentation_Issued</fullName>
        <ccEmails>customerservice@nextgearcapital.co.uk</ccEmails>
        <description>Documentation Issued</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Documentation_Issued</template>
    </alerts>
    <alerts>
        <fullName>Documentation_Issued_signed_paperwork_not_received_within_7_days</fullName>
        <ccEmails>customerservice@nextgearcapital.co.uk</ccEmails>
        <description>Documentation Issued - signed paperwork not received within 7 days</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Documentation_Issued_signed_paperwork_not_received_within_7_days</template>
    </alerts>
    <alerts>
        <fullName>Documentation_Review_Complete</fullName>
        <description>Documentation Review Complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>gareth.griffiths@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Document_Review_Complete</template>
    </alerts>
    <alerts>
        <fullName>Documents_ready_for_Issue</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>Documents ready for Issue</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Documents_Ready_for_Issue</template>
    </alerts>
    <alerts>
        <fullName>Final_Documents_Selected</fullName>
        <description>Final Documents Selected</description>
        <protected>false</protected>
        <recipients>
            <recipient>Operations</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Final_Documents_Approval</template>
    </alerts>
    <alerts>
        <fullName>Required_Documents_Selected_for_Issue</fullName>
        <description>Required Documents Selected for Issue</description>
        <protected>false</protected>
        <recipients>
            <recipient>gareth.griffiths@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Required_Documents_Selected</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_when_the_opportunity_is_rejected</fullName>
        <description>Send an email when the opportunity is rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_when_the_opportunity_is_rejected_Auction_House_On_boarding</fullName>
        <description>Send an email when the opportunity is rejected Auction House On-boarding</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_when_the_opportunity_is_rejected_Change_Credit</fullName>
        <description>Send an email when the opportunity is rejected Change Credit</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_when_the_opportunity_is_rejected_Create_New_Facility</fullName>
        <description>Send an email when the opportunity is rejected Create New Facility</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_when_the_opportunity_is_rejected_Renewal</fullName>
        <description>Send an email when the opportunity is rejected Renewal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected_Email</template>
    </alerts>
    <alerts>
        <fullName>Site_Review_request_field_completed_by_Credit</fullName>
        <description>Site Review request field completed by Credit</description>
        <protected>false</protected>
        <recipients>
            <recipient>jenny.gibson@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Site_Review_request_field_completed_by_Credit</template>
    </alerts>
    <fieldUpdates>
        <fullName>Opp_Record_Type_Auction_House_On_board</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Auction_House_On_boarding</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp Record Type Auction House On-board</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Record_Type_Change_to_Create_New_Fac</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Create_New_Facility</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp Record Type Change to Create New Fac</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Record_Type_to_Change_Credit_Limit</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Change_Credit_Limit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp Record Type to Change Credit Limit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Record_Type_to_Renewal</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Renewal</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp Record Type to Renewal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Live_date</fullName>
        <field>Account_Live_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Account Live date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Received_Lvl1</fullName>
        <field>Approval_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update Approval Received Lvl1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Received_Lvl2</fullName>
        <field>Approval_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update Approval Received Lvl2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Received_Lvl3</fullName>
        <field>Approval_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update Approval Received Lvl3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Received_Lvl4</fullName>
        <field>Approval_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update Approval Received Lvl4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CM_approval_Flag</fullName>
        <field>Credit_Manager_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Update CM approval Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Count</fullName>
        <field>Director_Counter__c</field>
        <formula>if ( Sales_Director_Approval__c ,1,0) + if ( Managing_Director_Approval__c ,1,0) + if ( Finance_Director_Approval__c ,1,0) + if ( Operations_Director_Approval__c ,1,0) + if ( US_Director_Approval__c ,1,0)</formula>
        <name>Update Director Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Document_Issued_Date</fullName>
        <field>Document_Issued_Date__c</field>
        <formula>Today()</formula>
        <name>Update Document Issued Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Date</fullName>
        <description>Changes the Status Change Date on Opp to today</description>
        <field>Status_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_for_level_0</fullName>
        <field>Approval_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update approval for level 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_for_level_5</fullName>
        <field>Approval_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update approval for level 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>close_date_to_created_date</fullName>
        <description>Set the close date to the created date for an opportunity</description>
        <field>CloseDate</field>
        <formula>DATEVALUE(CreatedDate)</formula>
        <name>close_date_to_created_date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_MD_approval_flag</fullName>
        <field>Managing_Director_Approval__c</field>
        <literalValue>1</literalValue>
        <name>update MD approval flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Account Live</fullName>
        <actions>
            <name>Account_Live</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Live_stage_Welcome_Call_arrangement</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Account_Live_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Account Live</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.Account_Live_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account Live - Customer Services</fullName>
        <actions>
            <name>Account_Live_Customer_Service_Actions</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Account Live</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit decision reached by Credit Approvers and Approved</fullName>
        <actions>
            <name>Credit_decision_reached_by_Credit_Approvers_and_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Credit Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit decision reached by Credit Approvers and Approved %28Dealer%29</fullName>
        <actions>
            <name>Credit_decision_reached_by_Credit_Approvers_and_Approved_Dealer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Credit Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit decision reached by Credit Approvers and Declined %28Credit%29</fullName>
        <actions>
            <name>Credit_decision_reached_by_Credit_Approvers_and_Declined_Credit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit decision reached by Credit Approvers and Declined %28Dealer%29</fullName>
        <actions>
            <name>Credit_decision_reached_by_Credit_Approvers_and_Declined_Dealer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit decision reached by Credit Approvers and Refer</fullName>
        <actions>
            <name>Credit_decision_reached_by_Credit_Approvers_and_Refer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Refer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit decision reached by Credit Approvers and Resubmit</fullName>
        <actions>
            <name>Credit_decision_reached_by_Credit_Approvers_and_Resubmit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Resubmitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Director Counter</fullName>
        <actions>
            <name>Update_Director_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Docs issued - signed not received in 7 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Documentation Issued</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Documentation_Issued_signed_paperwork_not_received_within_7_days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Document_Issued_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Documentation Review Complete</fullName>
        <actions>
            <name>Documentation_Review_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Documentation Reviewed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Legal Documents Completed</fullName>
        <actions>
            <name>Legal_Documents_Completed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Legal Documents Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Record Type Change to Renewal</fullName>
        <actions>
            <name>Send_an_email_when_the_opportunity_is_rejected_Renewal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Record_Type_to_Renewal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Previous_RecordType__c</field>
            <operation>equals</operation>
            <value>012w000000066NT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Credit_Manager_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Finance_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Managing_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Operations_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.US_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Record Type to Change Credit Limit after rejection</fullName>
        <actions>
            <name>Send_an_email_when_the_opportunity_is_rejected_Change_Credit</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Record_Type_to_Change_Credit_Limit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Previous_RecordType__c</field>
            <operation>equals</operation>
            <value>012w000000066NJ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Credit_Manager_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Finance_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Managing_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Operations_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.US_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Record Type to Create New Facility after rejection</fullName>
        <actions>
            <name>Send_an_email_when_the_opportunity_is_rejected_Create_New_Facility</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Record_Type_Change_to_Create_New_Fac</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Previous_RecordType__c</field>
            <operation>equals</operation>
            <value>012w000000066NE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Finance_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Managing_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Operations_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.US_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Credit_Manager_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>if anyone rejects authorization on the opportunity, the record type goes back to the original record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Record Type toAuction House On-boarding after rejection</fullName>
        <actions>
            <name>Send_an_email_when_the_opportunity_is_rejected_Auction_House_On_boarding</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Record_Type_Auction_House_On_board</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Previous_RecordType__c</field>
            <operation>equals</operation>
            <value>012w000000066NY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Credit_Manager_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Finance_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Managing_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Operations_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.US_Director_Not_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Stage Change</fullName>
        <actions>
            <name>Update_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Fires when a stage changes on Opportunity</description>
        <formula>ISCHANGED(StageName)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage Change to Final Document Approval</fullName>
        <actions>
            <name>Final_Documents_Selected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Final Document Approval</value>
        </criteriaItems>
        <description>Once Stage changes to &apos;Final Document Approval&apos; auto-email to Operations</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Document Issued Date</fullName>
        <actions>
            <name>Update_Document_Issued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Documentation Issued</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lvl 0 Approval Received Checkbox</fullName>
        <actions>
            <name>Update_approval_for_level_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approval_Level_Text__c</field>
            <operation>equals</operation>
            <value>Approval Level 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Level_0_Approval_Achieved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lvl 1 Approval Received Checkbox</fullName>
        <actions>
            <name>Update_Approval_Received_Lvl1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approval_Level_Text__c</field>
            <operation>equals</operation>
            <value>Approval Level 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Level_1_Approval_Achieved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lvl 2 Approval Received Checkbox</fullName>
        <actions>
            <name>Update_Approval_Received_Lvl2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approval_Level_Text__c</field>
            <operation>equals</operation>
            <value>Approval Level 2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Level_2_Approval_Achieved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lvl 3 Approval Received Checkbox</fullName>
        <actions>
            <name>Update_Approval_Received_Lvl3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approval_Level_Text__c</field>
            <operation>equals</operation>
            <value>Approval Level 3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Level_3_Approval_Achieved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lvl 4 Approval Received Checkbox</fullName>
        <actions>
            <name>Update_Approval_Received_Lvl4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approval_Level_Text__c</field>
            <operation>equals</operation>
            <value>Approval Level 4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Level_4_Approval_Achieved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lvl 5 Approval Received Checkbox</fullName>
        <actions>
            <name>Update_approval_for_level_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approval_Level_Text__c</field>
            <operation>equals</operation>
            <value>Approval Level 5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Level_5_Approval_Achieved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>close_date_to_created_date</fullName>
        <actions>
            <name>close_date_to_created_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the close date on the opportunity to the created date</description>
        <formula>CloseDate  &lt;&gt;  DATEVALUE( CreatedDate )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Account_Live_Customer_Service_Actions</fullName>
        <assignedTo>customerservice@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The Account has been made live please action the following tasks:

WFS Dealer Set Up
Welcome Call

Thank you</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Account Live - Customer Service Actions</subject>
    </tasks>
    <tasks>
        <fullName>Legal_Documents_Completed</fullName>
        <assignedTo>customerservice@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The legal documents have been signed and executed, please chase the manual documentation.

Thank you</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Legal Documents Completed</subject>
    </tasks>
</Workflow>
