<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Receipt_of_a_Site_Review</fullName>
        <description>Receipt of a Site Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>lucy.brown@nextgearcapital.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Receipt_of_a_Site_Review</template>
    </alerts>
    <alerts>
        <fullName>Site_Review_results_received_from_OCS</fullName>
        <ccEmails>credit@nextgearcapital.co.uk</ccEmails>
        <description>Site Review results received from OCS</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Site_Review_results_received_from_OCS</template>
    </alerts>
    <rules>
        <fullName>OCS-Notification of a Site Review</fullName>
        <actions>
            <name>Receipt_of_a_Site_Review</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OCS_Check__c.Info_received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Site Review results received from OCS</fullName>
        <actions>
            <name>Site_Review_results_received_from_OCS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
