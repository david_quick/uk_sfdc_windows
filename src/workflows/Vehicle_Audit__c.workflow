<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Original</fullName>
        <description>Update original rating</description>
        <field>rating__c</field>
        <formula>IF(ISPICKVAL(Rating2__c, &quot;Red&quot;),&quot;Red&quot;,
IF(ISPICKVAL(Rating2__c, &quot;Amber&quot;),&quot;Amber&quot;,
IF(ISPICKVAL(Rating2__c, &quot;Green&quot;),&quot;Green&quot;,
&quot;Amber&quot;
)))</formula>
        <name>Update Original</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Old Rating</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Vehicle_Audit__c.Rating2__c</field>
            <operation>equals</operation>
            <value>Red,Amber,Green</value>
        </criteriaItems>
        <description>Update the old rating field with the value selected in new Rating picklist</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Rating</fullName>
        <actions>
            <name>Update_Original</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Vehicle_Audit__c.Rating2__c</field>
            <operation>equals</operation>
            <value>Red,Amber,Green</value>
        </criteriaItems>
        <description>Update the rating from the picklist Rating2</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
