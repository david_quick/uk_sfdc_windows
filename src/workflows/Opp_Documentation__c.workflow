<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Insurance Renewal</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opp_Documentation__c.dtInsFrom__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Create a task 14 days before the insurance renewal date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Insurance_check_due</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opp_Documentation__c.dtInsTo__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Insurance_check_due</fullName>
        <assignedTo>customerservice@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Insurance check due for account. Please check the Insurance due report for details</description>
        <dueDateOffset>-14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opp_Documentation__c.dtInsTo__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Insurance check due</subject>
    </tasks>
</Workflow>
