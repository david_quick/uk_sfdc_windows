<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_account_owner</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>Send email to account owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/Account_On_Stop_HTML</template>
    </alerts>
    <fieldUpdates>
        <fullName>Calculate_OnStop_Days</fullName>
        <field>On_Stop_Days__c</field>
        <formula>IF(ISBLANK(Company__r.On_Stop_Days__c),0 + (Now() -  DATETIMEVALUE(Company__r.On_Stop_Date__c)),Company__r.On_Stop_Days__c + (Now() -  DATETIMEVALUE(Company__r.On_Stop_Date__c)))</formula>
        <name>Calculate OnStop Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Company__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>On_Stop_Count</fullName>
        <description>Increment the account on stop count</description>
        <field>On_Stop_Count__c</field>
        <formula>IF(ISBLANK(Company__r.On_Stop_Count__c),1,Company__r.On_Stop_Count__c + 1)</formula>
        <name>On Stop Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Company__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reference_Creation_for_Plan_Site</fullName>
        <field>Reference__c</field>
        <formula>Plan_Site__r.WFS_Ref__c  + &quot;-&quot; +    Opp_Product__r.Product_cust__r.Name+&apos;_PENDING&apos;</formula>
        <name>Reference Creation for Plan Site</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_OnStop_Date</fullName>
        <field>On_Stop_Date__c</field>
        <name>Reset OnStop Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>Company__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OnStop_Date</fullName>
        <field>On_Stop_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set OnStop Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Company__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Account OK</fullName>
        <actions>
            <name>Calculate_OnStop_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opp_Prod_Sites__c.Credit_Line_Status__c</field>
            <operation>equals</operation>
            <value>OK</value>
        </criteriaItems>
        <description>Reset account on stop date, update the total number of days on stop</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account On Stop</fullName>
        <actions>
            <name>Send_email_to_account_owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>On_Stop_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_OnStop_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opp_Prod_Sites__c.Credit_Line_Status__c</field>
            <operation>equals</operation>
            <value>On-Stop</value>
        </criteriaItems>
        <description>Send email to Account Owner when plan placed on hold</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Reference for Plan Site</fullName>
        <actions>
            <name>Reference_Creation_for_Plan_Site</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Account_On_Stop_SMS</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Account-f968</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Account On Stop SMS</subject>
    </tasks>
</Workflow>
