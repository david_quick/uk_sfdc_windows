<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Dealer_Account_Activation_Request_Welcome_Pack</fullName>
        <ccEmails>customerservice@nextgearcapital.co.uk</ccEmails>
        <description>Dealer Account Activation - Request Welcome Pack</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Dealer_Account_Activation_Request_Welcome_Pack</template>
    </alerts>
    <alerts>
        <fullName>First_SOAR_reminder</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>First SOAR reminder</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Account_for_AE</fullName>
        <description>Send email to AE when account goes live (for SOAR)</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_Account_Live</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Complete_Reminder_1</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Complete Reminder 1</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Complete_Reminder_2</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Complete Reminder 2</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Complete_Reminder_3</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Complete Reminder 3</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Registration_Reminder_1</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Registration Reminder 1</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Registration_Reminder_2</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Registration Reminder 2</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Registration_Reminder_3</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Registration Reminder 3</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Training_Audit</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>Send email to AE when dealer is doing Training Audit</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Training</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Training_Reminder_1</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Training Reminder 1</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Training_Reminder_2</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Training Reminder 2</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <alerts>
        <fullName>SOAR_Training_Reminder_3</fullName>
        <ccEmails>david.quick@nextgearcapital.co.uk</ccEmails>
        <description>SOAR Training Reminder 3</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Task_mails/SOAR_AE_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>Day_21_Predicted_units</fullName>
        <field>Day_21_Predicted_units_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Day 21 Predicted units</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Day_7_Predicted_units_last_updated</fullName>
        <field>Day_7_Predicted_units_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Day 7 Predicted units last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Facility_last_updated</fullName>
        <field>Facility_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Facility last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Credit_Limit_Review_Last_Upd</fullName>
        <field>Credit_Limit_Review_Last_Updated__c</field>
        <formula>NOW()</formula>
        <name>Populate Credit Limit Review Last Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prediction_last_updated</fullName>
        <field>Predication_on_track_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Prediction last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prediction_narrative_last_updated</fullName>
        <field>Predication_narrative_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Prediction narrative last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SOAR_Audit_is_true</fullName>
        <description>Checks the SOAR Audit checkbox on a new ROI dealer</description>
        <field>SOAR_Audit__c</field>
        <literalValue>1</literalValue>
        <name>SOAR Audit is true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SOAR_First_Active_Date_updated</fullName>
        <description>Updates the SOAR First Active field in line with the checkbox being true</description>
        <field>SOAR_First_Active_Date__c</field>
        <formula>NOW()</formula>
        <name>SOAR First Active Date updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SOAR_Status_Update</fullName>
        <description>Set SOAR Status to default</description>
        <field>SOAR_Status__c</field>
        <literalValue>Registration Requested</literalValue>
        <name>SOAR Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SoarFirstActiveDate</fullName>
        <field>SOAR_First_Active_Date__c</field>
        <formula>IF( SOAR_Audit__c = True, TODAY(), null )</formula>
        <name>SoarFirstActiveDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>U_last_updated</fullName>
        <field>U_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Utilisation last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Units_MTD_last_updated</fullName>
        <field>Units_MTD_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Units MTD last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Units_YTD_last_updated</fullName>
        <field>Units_YTD_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Units YTD last updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Units_last_month</fullName>
        <field>Units_last_month_last_updated__c</field>
        <formula>NOW()</formula>
        <name>Units last month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Live_Date</fullName>
        <field>Account_Live_Date__c</field>
        <formula>Now()</formula>
        <name>Update Live Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Utilisation_Last_Updated</fullName>
        <description>Updates Utilisation Last Updated on change of Current Utilization field being updated</description>
        <field>Utilisation_Last_Updated__c</field>
        <formula>now()</formula>
        <name>Update Utilisation Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allocate Area and Territory</fullName>
        <actions>
            <name>Please_allocate_territory</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>AE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Rule to fire when new Account is added by an AE.  Management will need to allocate correct Owner, Area and Territory.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Credit Limit Review Date Population</fullName>
        <actions>
            <name>Populate_Credit_Limit_Review_Last_Upd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   AND (        ISNEW(),        NOT(ISNULL(Credit_Limit_Review_Date__c))   ),   ISCHANGED(Credit_Limit_Review_Date__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Day 21 Predicted units</fullName>
        <actions>
            <name>Day_21_Predicted_units</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Day_21_Predicted_units__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Day 7 Predicted units</fullName>
        <actions>
            <name>Day_7_Predicted_units_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Day_7_Predicted_units__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Facility last updated</fullName>
        <actions>
            <name>Facility_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Facility__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Account SOAR Active</fullName>
        <actions>
            <name>SOAR_Account_for_AE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SOAR_Audit_is_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SOAR_First_Active_Date_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SOAR_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the SOAR Status to Registration Requested when a new account goes live.</description>
        <formula>AND(ISPICKVAL(Qualified_status__c , &quot;Account Live&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Predicition last updated</fullName>
        <actions>
            <name>Prediction_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Prediction_on_track__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Prediction narrative last updated</fullName>
        <actions>
            <name>Prediction_narrative_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Predication_narrative__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SOAR Registration Complete Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.SOAR_Status__c</field>
            <operation>equals</operation>
            <value>Registration Complete</value>
        </criteriaItems>
        <description>Sent to remind AE to chase dealer when registration complete but training not done</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Complete_Reminder_3</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SOAR_status_not_changed_for_Account_Account_Name</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Complete_Reminder_2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Complete_Reminder_1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SOAR Registration Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.SOAR_Status__c</field>
            <operation>equals</operation>
            <value>Registration Requested</value>
        </criteriaItems>
        <description>Reminder messages when account stuck on Registration Requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Registration_Reminder_3</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SOAR_dealer_not_registered</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Registration_Reminder_1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Registration_Reminder_2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SOAR Training Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.SOAR_Status__c</field>
            <operation>equals</operation>
            <value>Training Audit Email Sent</value>
        </criteriaItems>
        <description>Reminder sent to AE after 7, 14, 21 days to chase stalled SOAR dealer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Training_Reminder_2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Training_Reminder_3</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>SOAR_Training_Task_3</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Training_Reminder_1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>SOAR_Training_Audit</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Live Date</fullName>
        <actions>
            <name>Update_Live_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Qualified_status__c</field>
            <operation>equals</operation>
            <value>Account live</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SoarFirstActiveDate</fullName>
        <actions>
            <name>SoarFirstActiveDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SOAR_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>Logs the date the Account was first made active on SOAR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Units MTD last updated</fullName>
        <actions>
            <name>Units_MTD_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Units_MTD__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Units YTD last updated</fullName>
        <actions>
            <name>Units_YTD_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Units_YTD__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Units last month</fullName>
        <actions>
            <name>Units_last_month</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Units_last_month__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Utilisation Last Updated</fullName>
        <actions>
            <name>Update_Utilisation_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Utilisation Last Updated field is updated each time current utilization changes</description>
        <formula>ISCHANGED( Current_Utilisation__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Utilisation last updated</fullName>
        <actions>
            <name>U_last_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Utilisation__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Please_allocate_territory</fullName>
        <assignedTo>lucy.brown@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new Account has been created by the Account Executives.  Please allocate the correct Owner, Area and Territory from the current list.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Please allocate territory</subject>
    </tasks>
    <tasks>
        <fullName>SOAR_Training_Task_3</fullName>
        <assignedTo>riskteam@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>This account has not changed SOAR status in 21 days</description>
        <dueDateOffset>22</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>SOAR status not changed for Account - {!Account.Name}</subject>
    </tasks>
    <tasks>
        <fullName>SOAR_dealer_not_registered</fullName>
        <assignedTo>riskteam@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Task added to risk queue after 21 days stuck at registration</description>
        <dueDateOffset>22</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>SOAR dealer not registered</subject>
    </tasks>
    <tasks>
        <fullName>SOAR_status_not_changed_for_Account_Account_Name</fullName>
        <assignedTo>riskteam@nextgearcapital.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The SOAR status for this account has not changed in 21 days</description>
        <dueDateOffset>22</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.SOAR_Status_Change_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>SOAR status not changed for Account - {!Account.Name}</subject>
    </tasks>
</Workflow>
