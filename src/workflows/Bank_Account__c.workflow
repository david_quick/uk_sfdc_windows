<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Bank_Account_Title</fullName>
        <field>Name</field>
        <formula>Account__r.WFS_Ref__c + &apos;-BANK&apos;</formula>
        <name>Bank Account Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Bank Account Title setup</fullName>
        <actions>
            <name>Bank_Account_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
