<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Opportunity_owner_on_reject</fullName>
        <description>Notify Opportunity owner on reject</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Email</fullName>
        <description>Send Approval Email</description>
        <protected>false</protected>
        <recipients>
            <field>User_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Emails/Submit_Opp_for_Approval_email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Opportunity_Owner</fullName>
        <description>Update the Opportunity Owner field</description>
        <field>Opportunity_Owner__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>Opportunity Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Notify owner on Reject</fullName>
        <actions>
            <name>Notify_Opportunity_owner_on_reject</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Approval_Stage__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Notify Opportunity Owner when the approval stage is rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Owner</fullName>
        <actions>
            <name>Opportunity_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Opportunity Owner Email field</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Summit for Approval</fullName>
        <actions>
            <name>Send_Approval_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Approval_Stage__c.isActive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Approval_Stage__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Approval</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
